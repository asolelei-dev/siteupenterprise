-- phpMyAdmin SQL Dump
-- version 2.6.1-pl3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Feb 07, 2006 at 06:10 PM
-- Server version: 4.1.10
-- PHP Version: 5.0.4
-- 
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_adminusers`
-- 

DROP TABLE IF EXISTS `tbl_adminusers`;
CREATE TABLE IF NOT EXISTS `tbl_adminusers` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(50) NOT NULL default '',
  `pwd` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `userindex` (`username`)
) ;

-- 
-- Dumping data for table `tbl_adminusers`
-- 

INSERT INTO `tbl_adminusers` VALUES (1, 'admin', 'admin');

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_checkperiods`
-- 

DROP TABLE IF EXISTS `tbl_checkperiods`;
CREATE TABLE IF NOT EXISTS `tbl_checkperiods` (
  `pid` int(11) NOT NULL auto_increment,
  `period` int(2) NOT NULL default '0',
  `status` char(1) NOT NULL default 'Y',
  `authority` int(2) NOT NULL default '1',
  PRIMARY KEY  (`pid`,`period`)
) ;

-- 
-- Dumping data for table `tbl_checkperiods`
-- 

INSERT INTO `tbl_checkperiods` VALUES (1, 2, 'Y', 3);
INSERT INTO `tbl_checkperiods` VALUES (2, 5, 'Y', 2);
INSERT INTO `tbl_checkperiods` VALUES (3, 15, 'Y', 2);
INSERT INTO `tbl_checkperiods` VALUES (4, 30, 'Y', 1);
INSERT INTO `tbl_checkperiods` VALUES (5, 60, 'Y', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_commonsettings`
-- 

DROP TABLE IF EXISTS `tbl_commonsettings`;
CREATE TABLE IF NOT EXISTS `tbl_commonsettings` (
  `showcount` char(1) NOT NULL default 'N',
  `paypal_address` varchar(50) NOT NULL default '',
  `checkout_id` varchar(25) NOT NULL default '',
  `show_res` char(1) NOT NULL default '',
  `sms_down` varchar(160) NOT NULL default '',
  `sms_up` varchar(160) NOT NULL default '',
  `sms_userid` varchar(50) NOT NULL default '',
  `sms_password` varchar(50) NOT NULL default '',
  `contact_mailid` varchar(150) NOT NULL default '',
  `sms_url` varchar(200) NOT NULL default '',
  `sms_user` varchar(100) NOT NULL default '',
  `sms_pass` varchar(100) NOT NULL default '',
  `sms_message` varchar(100) NOT NULL default '',
  `sms_phone` varchar(100) NOT NULL default ''
) ;

-- 
-- Dumping data for table `tbl_commonsettings`
-- 

INSERT INTO `tbl_commonsettings` VALUES ('N', 'billing@alstrasoft.com', 'g0002', 'N', 'hello %%UserName%%, Your site %%SiteName%% has been down at %%Date%% %%Time%%. This is from Site Uptime Enterprise', 'hello %%UserName%%, Your site %%SiteName%% has been Up at %%Date%% %%Time%%. This is from Site Uptime Enterprise', 'ssssssssss', 'rrrrrrrrrrrr', 'support@alstrasoft.com', 'http://www.bulksms.co.uk:5567/eapi/submission/send_sms/2/2.0', 'username', 'password', 'message', 'msisdn');

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_contacts`
-- 

DROP TABLE IF EXISTS `tbl_contacts`;
CREATE TABLE IF NOT EXISTS `tbl_contacts` (
  `contactid` double NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `emailid` varchar(100) NOT NULL default '',
  `subject` varchar(100) NOT NULL default '',
  `comment` text NOT NULL,
  `cdate` date NOT NULL default '0000-00-00',
  `status` varchar(20) NOT NULL default 'New',
  `uid` int(15) NOT NULL default '0',
  PRIMARY KEY  (`contactid`)
) ;

-- 
-- Dumping data for table `tbl_contacts`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_country`
-- 

DROP TABLE IF EXISTS `tbl_country`;
CREATE TABLE IF NOT EXISTS `tbl_country` (
  `countryid` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `status` char(1) NOT NULL default 'Y',
  PRIMARY KEY  (`countryid`)
) ;

-- 
-- Dumping data for table `tbl_country`
-- 

INSERT INTO `tbl_country` VALUES (1, 'United States', 'Y');
INSERT INTO `tbl_country` VALUES (2, 'Afghanistan', 'Y');
INSERT INTO `tbl_country` VALUES (3, 'Albania', 'Y');
INSERT INTO `tbl_country` VALUES (4, 'Algeria', 'Y');
INSERT INTO `tbl_country` VALUES (5, 'American Samoa', 'Y');
INSERT INTO `tbl_country` VALUES (6, 'Andorra', 'Y');
INSERT INTO `tbl_country` VALUES (7, 'Angola', 'Y');
INSERT INTO `tbl_country` VALUES (8, 'Anguilla', 'Y');
INSERT INTO `tbl_country` VALUES (9, 'Antarctica', 'N');
INSERT INTO `tbl_country` VALUES (10, 'Antigua & Barbuda', 'Y');
INSERT INTO `tbl_country` VALUES (11, 'Argentina', 'Y');
INSERT INTO `tbl_country` VALUES (12, 'Armenia', 'Y');
INSERT INTO `tbl_country` VALUES (13, 'Aruba', 'Y');
INSERT INTO `tbl_country` VALUES (14, 'Australia', 'Y');
INSERT INTO `tbl_country` VALUES (15, 'Austria', 'Y');
INSERT INTO `tbl_country` VALUES (16, 'Azerbaijan', 'Y');
INSERT INTO `tbl_country` VALUES (17, 'Bahamas', 'Y');
INSERT INTO `tbl_country` VALUES (18, 'Bahrain', 'Y');
INSERT INTO `tbl_country` VALUES (19, 'Bangladesh', 'Y');
INSERT INTO `tbl_country` VALUES (20, 'Barbados', 'Y');
INSERT INTO `tbl_country` VALUES (21, 'Belarus', 'Y');
INSERT INTO `tbl_country` VALUES (22, 'Belgium', 'Y');
INSERT INTO `tbl_country` VALUES (23, 'Belize', 'Y');
INSERT INTO `tbl_country` VALUES (24, 'Benin', 'Y');
INSERT INTO `tbl_country` VALUES (25, 'Bermuda', 'Y');
INSERT INTO `tbl_country` VALUES (26, 'Bhutan', 'Y');
INSERT INTO `tbl_country` VALUES (27, 'Bolivia', 'Y');
INSERT INTO `tbl_country` VALUES (28, 'Bosnia & Herzegowina', 'Y');
INSERT INTO `tbl_country` VALUES (29, 'Botswana', 'Y');
INSERT INTO `tbl_country` VALUES (30, 'Bouvet Island', 'Y');
INSERT INTO `tbl_country` VALUES (31, 'Brazil', 'Y');
INSERT INTO `tbl_country` VALUES (32, 'British Indian Ocean Terr.', 'Y');
INSERT INTO `tbl_country` VALUES (33, 'Brunei Darussalam', 'Y');
INSERT INTO `tbl_country` VALUES (34, 'Bulgaria', 'Y');
INSERT INTO `tbl_country` VALUES (35, 'Burkina Faso', 'Y');
INSERT INTO `tbl_country` VALUES (36, 'Burma (Myanmar)', 'Y');
INSERT INTO `tbl_country` VALUES (37, 'Burundi', 'Y');
INSERT INTO `tbl_country` VALUES (38, 'Cambodia', 'Y');
INSERT INTO `tbl_country` VALUES (39, 'Cameroon', 'Y');
INSERT INTO `tbl_country` VALUES (40, 'Canada', 'Y');
INSERT INTO `tbl_country` VALUES (41, 'Cape Verde', 'Y');
INSERT INTO `tbl_country` VALUES (42, 'Cayman Islands', 'Y');
INSERT INTO `tbl_country` VALUES (43, 'Central African Rep', 'Y');
INSERT INTO `tbl_country` VALUES (44, 'Chad', 'Y');
INSERT INTO `tbl_country` VALUES (45, 'Chile', 'Y');
INSERT INTO `tbl_country` VALUES (46, 'China', 'Y');
INSERT INTO `tbl_country` VALUES (47, 'Christmas Island', 'Y');
INSERT INTO `tbl_country` VALUES (48, 'Cocos (Keeling) Isles', 'Y');
INSERT INTO `tbl_country` VALUES (49, 'Colombia', 'Y');
INSERT INTO `tbl_country` VALUES (50, 'Comoros', 'Y');
INSERT INTO `tbl_country` VALUES (51, 'Congo', 'Y');
INSERT INTO `tbl_country` VALUES (52, 'Congo, The Democratic Rep', 'Y');
INSERT INTO `tbl_country` VALUES (53, 'Cook Islands', 'Y');
INSERT INTO `tbl_country` VALUES (54, 'Costa Rica', 'Y');
INSERT INTO `tbl_country` VALUES (55, 'Croatia', 'Y');
INSERT INTO `tbl_country` VALUES (56, 'Cuba', 'Y');
INSERT INTO `tbl_country` VALUES (57, 'Cyprus', 'Y');
INSERT INTO `tbl_country` VALUES (58, 'Czech Republic', 'Y');
INSERT INTO `tbl_country` VALUES (59, 'Denmark', 'Y');
INSERT INTO `tbl_country` VALUES (60, 'Djibouti', 'Y');
INSERT INTO `tbl_country` VALUES (61, 'Dominica', 'Y');
INSERT INTO `tbl_country` VALUES (62, 'Dominican Republic', 'Y');
INSERT INTO `tbl_country` VALUES (63, 'East Timor', 'Y');
INSERT INTO `tbl_country` VALUES (64, 'Ecuador', 'Y');
INSERT INTO `tbl_country` VALUES (65, 'Egypt', 'Y');
INSERT INTO `tbl_country` VALUES (66, 'El Salvador', 'Y');
INSERT INTO `tbl_country` VALUES (67, 'Equatorial Guinea', 'Y');
INSERT INTO `tbl_country` VALUES (68, 'Eritrea', 'Y');
INSERT INTO `tbl_country` VALUES (69, 'Estonia', 'Y');
INSERT INTO `tbl_country` VALUES (70, 'Ethiopia', 'Y');
INSERT INTO `tbl_country` VALUES (71, 'Falkland Islands (Malvinas)', 'Y');
INSERT INTO `tbl_country` VALUES (72, 'Faroe Islands', 'Y');
INSERT INTO `tbl_country` VALUES (73, 'Fiji', 'Y');
INSERT INTO `tbl_country` VALUES (74, 'Finland', 'Y');
INSERT INTO `tbl_country` VALUES (75, 'France', 'Y');
INSERT INTO `tbl_country` VALUES (76, 'France, Metro', 'Y');
INSERT INTO `tbl_country` VALUES (77, 'French Guiana', 'Y');
INSERT INTO `tbl_country` VALUES (78, 'French Polynesia', 'Y');
INSERT INTO `tbl_country` VALUES (79, 'French Southern Terr.', 'Y');
INSERT INTO `tbl_country` VALUES (80, 'Gabon', 'Y');
INSERT INTO `tbl_country` VALUES (81, 'Gambia', 'Y');
INSERT INTO `tbl_country` VALUES (82, 'Georgia', 'Y');
INSERT INTO `tbl_country` VALUES (83, 'Germany', 'Y');
INSERT INTO `tbl_country` VALUES (84, 'Ghana', 'Y');
INSERT INTO `tbl_country` VALUES (85, 'Gibraltar', 'Y');
INSERT INTO `tbl_country` VALUES (86, 'Greece', 'Y');
INSERT INTO `tbl_country` VALUES (87, 'Greenland', 'Y');
INSERT INTO `tbl_country` VALUES (88, 'Grenada', 'Y');
INSERT INTO `tbl_country` VALUES (89, 'Guadeloupe', 'Y');
INSERT INTO `tbl_country` VALUES (90, 'Guam', 'Y');
INSERT INTO `tbl_country` VALUES (91, 'Guatemala', 'Y');
INSERT INTO `tbl_country` VALUES (92, 'Guinea', 'Y');
INSERT INTO `tbl_country` VALUES (93, 'Guinea-Bissau', 'Y');
INSERT INTO `tbl_country` VALUES (94, 'Guyana', 'Y');
INSERT INTO `tbl_country` VALUES (95, 'Haiti', 'Y');
INSERT INTO `tbl_country` VALUES (96, 'Heard And Mc Donald Isles', 'Y');
INSERT INTO `tbl_country` VALUES (97, 'Holy See (Vatican)', 'Y');
INSERT INTO `tbl_country` VALUES (98, 'Honduras', 'Y');
INSERT INTO `tbl_country` VALUES (99, 'Hong Kong', 'Y');
INSERT INTO `tbl_country` VALUES (100, 'Hungary', 'Y');
INSERT INTO `tbl_country` VALUES (101, 'Iceland', 'Y');
INSERT INTO `tbl_country` VALUES (102, 'India', 'Y');
INSERT INTO `tbl_country` VALUES (103, 'Indonesia', 'Y');
INSERT INTO `tbl_country` VALUES (104, 'Iran', 'Y');
INSERT INTO `tbl_country` VALUES (105, 'Iraq', 'Y');
INSERT INTO `tbl_country` VALUES (106, 'Ireland', 'Y');
INSERT INTO `tbl_country` VALUES (107, 'Israel', 'Y');
INSERT INTO `tbl_country` VALUES (108, 'Italy', 'Y');
INSERT INTO `tbl_country` VALUES (109, 'Ivory Coast (Cote D''Ivoire)', 'Y');
INSERT INTO `tbl_country` VALUES (110, 'Jamaica', 'Y');
INSERT INTO `tbl_country` VALUES (111, 'Japan', 'Y');
INSERT INTO `tbl_country` VALUES (112, 'Jordan', 'Y');
INSERT INTO `tbl_country` VALUES (113, 'Kazakhstan', 'Y');
INSERT INTO `tbl_country` VALUES (114, 'Kenya', 'Y');
INSERT INTO `tbl_country` VALUES (115, 'Kiribati', 'Y');
INSERT INTO `tbl_country` VALUES (116, 'Korea, Dem People''s Rep', 'Y');
INSERT INTO `tbl_country` VALUES (117, 'Korea, Republic Of', 'Y');
INSERT INTO `tbl_country` VALUES (118, 'Kuwait', 'Y');
INSERT INTO `tbl_country` VALUES (119, 'Kyrgyzstan', 'Y');
INSERT INTO `tbl_country` VALUES (120, 'Laos', 'Y');
INSERT INTO `tbl_country` VALUES (121, 'Latvia', 'Y');
INSERT INTO `tbl_country` VALUES (122, 'Lebanon', 'Y');
INSERT INTO `tbl_country` VALUES (123, 'Lesotho', 'Y');
INSERT INTO `tbl_country` VALUES (124, 'Liberia', 'Y');
INSERT INTO `tbl_country` VALUES (125, 'Libya', 'Y');
INSERT INTO `tbl_country` VALUES (126, 'Liechtenstein', 'Y');
INSERT INTO `tbl_country` VALUES (127, 'Lithuania', 'Y');
INSERT INTO `tbl_country` VALUES (128, 'Luxembourg', 'Y');
INSERT INTO `tbl_country` VALUES (129, 'Macau', 'Y');
INSERT INTO `tbl_country` VALUES (130, 'Macedonia (Republic of)', 'Y');
INSERT INTO `tbl_country` VALUES (131, 'Madagascar', 'Y');
INSERT INTO `tbl_country` VALUES (132, 'Malawi', 'Y');
INSERT INTO `tbl_country` VALUES (133, 'Malaysia', 'Y');
INSERT INTO `tbl_country` VALUES (134, 'Maldives', 'Y');
INSERT INTO `tbl_country` VALUES (135, 'Mali', 'Y');
INSERT INTO `tbl_country` VALUES (136, 'Malta', 'Y');
INSERT INTO `tbl_country` VALUES (137, 'Marshall Islands', 'Y');
INSERT INTO `tbl_country` VALUES (138, 'Martinique', 'Y');
INSERT INTO `tbl_country` VALUES (139, 'Mauritania', 'Y');
INSERT INTO `tbl_country` VALUES (140, 'Mauritius', 'Y');
INSERT INTO `tbl_country` VALUES (141, 'Mayotte', 'Y');
INSERT INTO `tbl_country` VALUES (142, 'Mexico', 'Y');
INSERT INTO `tbl_country` VALUES (143, 'Micronesia, Fed States', 'Y');
INSERT INTO `tbl_country` VALUES (144, 'Moldova, Rep', 'Y');
INSERT INTO `tbl_country` VALUES (145, 'Monaco', 'Y');
INSERT INTO `tbl_country` VALUES (146, 'Mongolia', 'Y');
INSERT INTO `tbl_country` VALUES (147, 'Montserrat', 'Y');
INSERT INTO `tbl_country` VALUES (148, 'Morocco', 'Y');
INSERT INTO `tbl_country` VALUES (149, 'Mozambique', 'Y');
INSERT INTO `tbl_country` VALUES (150, 'Namibia', 'Y');
INSERT INTO `tbl_country` VALUES (151, 'Nauru', 'Y');
INSERT INTO `tbl_country` VALUES (152, 'Nepal', 'Y');
INSERT INTO `tbl_country` VALUES (153, 'Netherlands', 'Y');
INSERT INTO `tbl_country` VALUES (154, 'Netherlands Antilles', 'Y');
INSERT INTO `tbl_country` VALUES (155, 'New Caledonia', 'Y');
INSERT INTO `tbl_country` VALUES (156, 'New Zealand', 'Y');
INSERT INTO `tbl_country` VALUES (157, 'Nicaragua', 'Y');
INSERT INTO `tbl_country` VALUES (158, 'Niger', 'Y');
INSERT INTO `tbl_country` VALUES (159, 'Nigeria', 'Y');
INSERT INTO `tbl_country` VALUES (160, 'Niue', 'Y');
INSERT INTO `tbl_country` VALUES (161, 'Norfolk Island', 'Y');
INSERT INTO `tbl_country` VALUES (162, 'Northern Mariana Isles', 'Y');
INSERT INTO `tbl_country` VALUES (163, 'Norway', 'Y');
INSERT INTO `tbl_country` VALUES (164, 'Oman', 'Y');
INSERT INTO `tbl_country` VALUES (165, 'Pakistan', 'Y');
INSERT INTO `tbl_country` VALUES (166, 'Palau', 'Y');
INSERT INTO `tbl_country` VALUES (167, 'Panama', 'Y');
INSERT INTO `tbl_country` VALUES (168, 'Papua New Guinea', 'Y');
INSERT INTO `tbl_country` VALUES (169, 'Paraguay', 'Y');
INSERT INTO `tbl_country` VALUES (170, 'Peru', 'Y');
INSERT INTO `tbl_country` VALUES (171, 'Philippines', 'Y');
INSERT INTO `tbl_country` VALUES (172, 'Pitcairn', 'Y');
INSERT INTO `tbl_country` VALUES (173, 'Poland', 'Y');
INSERT INTO `tbl_country` VALUES (174, 'Portugal', 'Y');
INSERT INTO `tbl_country` VALUES (175, 'Puerto Rico', 'Y');
INSERT INTO `tbl_country` VALUES (176, 'Qatar', 'Y');
INSERT INTO `tbl_country` VALUES (177, 'Reunion', 'Y');
INSERT INTO `tbl_country` VALUES (178, 'Romania', 'Y');
INSERT INTO `tbl_country` VALUES (179, 'Russian Federation', 'Y');
INSERT INTO `tbl_country` VALUES (180, 'Rwanda', 'Y');
INSERT INTO `tbl_country` VALUES (181, 'Saint Kitts & Nevis', 'Y');
INSERT INTO `tbl_country` VALUES (182, 'Saint Lucia', 'Y');
INSERT INTO `tbl_country` VALUES (183, 'Saint Vincent & Grenadines', 'Y');
INSERT INTO `tbl_country` VALUES (184, 'Samoa', 'Y');
INSERT INTO `tbl_country` VALUES (185, 'San Marino', 'Y');
INSERT INTO `tbl_country` VALUES (186, 'Sao Tome & Principe', 'Y');
INSERT INTO `tbl_country` VALUES (187, 'Saudi Arabia', 'Y');
INSERT INTO `tbl_country` VALUES (188, 'Senegal', 'Y');
INSERT INTO `tbl_country` VALUES (189, 'Seychelles', 'Y');
INSERT INTO `tbl_country` VALUES (190, 'Sierra Leona', 'Y');
INSERT INTO `tbl_country` VALUES (191, 'Singapore', 'Y');
INSERT INTO `tbl_country` VALUES (192, 'Slovakia (Slovak Rep)', 'Y');
INSERT INTO `tbl_country` VALUES (193, 'Slovenia', 'Y');
INSERT INTO `tbl_country` VALUES (194, 'Solomon Islands', 'Y');
INSERT INTO `tbl_country` VALUES (195, 'Somalia', 'Y');
INSERT INTO `tbl_country` VALUES (196, 'South Africa', 'Y');
INSERT INTO `tbl_country` VALUES (197, 'S Georgia & Sandwich Isles', 'Y');
INSERT INTO `tbl_country` VALUES (198, 'Spain', 'Y');
INSERT INTO `tbl_country` VALUES (199, 'Sri Lanka', 'Y');
INSERT INTO `tbl_country` VALUES (200, 'St. Helena', 'Y');
INSERT INTO `tbl_country` VALUES (201, 'St. Pierre & Miquelon', 'Y');
INSERT INTO `tbl_country` VALUES (202, 'Sudan', 'Y');
INSERT INTO `tbl_country` VALUES (203, 'Suriname', 'Y');
INSERT INTO `tbl_country` VALUES (204, 'Svalbard & Jan Mayen Islands', 'Y');
INSERT INTO `tbl_country` VALUES (205, 'Swaziland', 'Y');
INSERT INTO `tbl_country` VALUES (206, 'Sweden', 'Y');
INSERT INTO `tbl_country` VALUES (207, 'Switzerland', 'Y');
INSERT INTO `tbl_country` VALUES (208, 'Syria', 'Y');
INSERT INTO `tbl_country` VALUES (209, 'Taiwan', 'Y');
INSERT INTO `tbl_country` VALUES (210, 'Tajikistan', 'Y');
INSERT INTO `tbl_country` VALUES (211, 'Tanzania', 'Y');
INSERT INTO `tbl_country` VALUES (212, 'Thailand', 'Y');
INSERT INTO `tbl_country` VALUES (213, 'Togo', 'Y');
INSERT INTO `tbl_country` VALUES (214, 'Tokelau', 'Y');
INSERT INTO `tbl_country` VALUES (215, 'Tonga', 'Y');
INSERT INTO `tbl_country` VALUES (216, 'Trinidad & Tobago', 'Y');
INSERT INTO `tbl_country` VALUES (217, 'Tunisia', 'Y');
INSERT INTO `tbl_country` VALUES (218, 'Turkey', 'Y');
INSERT INTO `tbl_country` VALUES (219, 'Turkmenistan', 'Y');
INSERT INTO `tbl_country` VALUES (220, 'Turks & Caicos Islands', 'Y');
INSERT INTO `tbl_country` VALUES (221, 'Tuvalu', 'Y');
INSERT INTO `tbl_country` VALUES (222, 'Uganda', 'Y');
INSERT INTO `tbl_country` VALUES (223, 'Ukraine', 'Y');
INSERT INTO `tbl_country` VALUES (224, 'United Arab Emirates', 'Y');
INSERT INTO `tbl_country` VALUES (225, 'United Kingdom', 'Y');
INSERT INTO `tbl_country` VALUES (227, 'US Minor Outlying Isles', 'Y');
INSERT INTO `tbl_country` VALUES (228, 'Uruguay', 'Y');
INSERT INTO `tbl_country` VALUES (229, 'Uzbekistan', 'Y');
INSERT INTO `tbl_country` VALUES (230, 'Vanuatu', 'Y');
INSERT INTO `tbl_country` VALUES (231, 'Venezuela', 'Y');
INSERT INTO `tbl_country` VALUES (232, 'Viet Nam', 'Y');
INSERT INTO `tbl_country` VALUES (233, 'Virgin Isles (British)', 'Y');
INSERT INTO `tbl_country` VALUES (234, 'Virgin Isles (U.S.)', 'Y');
INSERT INTO `tbl_country` VALUES (235, 'Wallis & Futuna Islands', 'Y');
INSERT INTO `tbl_country` VALUES (236, 'Western Sahara', 'Y');
INSERT INTO `tbl_country` VALUES (237, 'Yemen', 'Y');
INSERT INTO `tbl_country` VALUES (238, 'Yugoslavia', 'Y');
INSERT INTO `tbl_country` VALUES (239, 'Zambia', 'Y');
INSERT INTO `tbl_country` VALUES (240, 'Zimbabwe', 'Y');

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_dailyservices`
-- 

DROP TABLE IF EXISTS `tbl_dailyservices`;
CREATE TABLE IF NOT EXISTS `tbl_dailyservices` (
  `service_id` int(15) NOT NULL default '0',
  `date_of_service` date NOT NULL default '0000-00-00',
  `location_id` int(11) NOT NULL default '1',
  `no_of_checks` int(5) NOT NULL default '0',
  `down_checks` int(5) NOT NULL default '0',
  `start_time` time NOT NULL default '00:00:00',
  `end_time` time NOT NULL default '00:00:00',
  `resp_time` double(12,4) NOT NULL default '0.0000',
  PRIMARY KEY  (`service_id`,`date_of_service`)
) ;

-- 
-- Dumping data for table `tbl_dailyservices`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_dayservices`
-- 

DROP TABLE IF EXISTS `tbl_dayservices`;
CREATE TABLE IF NOT EXISTS `tbl_dayservices` (
  `service_id` int(15) NOT NULL default '0',
  `time_of_check` datetime NOT NULL default '0000-00-00 00:00:00',
  `result` char(1) NOT NULL default 'Y',
  `resp_time` double(10,5) NOT NULL default '0.00000',
  `location` int(11) NOT NULL default '0'
) ;

-- 
-- Dumping data for table `tbl_dayservices`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_faqs`
-- 

DROP TABLE IF EXISTS `tbl_faqs`;
CREATE TABLE IF NOT EXISTS `tbl_faqs` (
  `f_id` int(11) NOT NULL auto_increment,
  `problem` text NOT NULL,
  `solution` text NOT NULL,
  `status` char(1) NOT NULL default 'Y',
  PRIMARY KEY  (`f_id`)
) ;

-- 
-- Dumping data for table `tbl_faqs`
-- 

INSERT INTO `tbl_faqs` VALUES (4, 'What services does Site Uptime Enterprise Provide?', 'For an overview of our services, please see our services and comparison pages. ', 'Y');
INSERT INTO `tbl_faqs` VALUES (5, 'What are the costs for your services?', 'Site Uptime Enterprise provides all of our basic services free of charge, we ask only that you register as a Site Uptime Enterprise member. We also premium services with additional features for a small monthly fee. ', 'Y');
INSERT INTO `tbl_faqs` VALUES (6, 'Why should I register with Site Uptime Enterprise?', 'Registration is quick and easy and provides immediate access to all of our services and tools. Best of all, registration is free. ', 'Y');
INSERT INTO `tbl_faqs` VALUES (7, 'Can I use your software on my web site?', 'We do not allow other sites to run our software but you may link to any of our pages from your site. ', 'Y');
INSERT INTO `tbl_faqs` VALUES (8, 'Do I need to download or install anything to use Site Uptime Enterprise?', 'No, all monitoring services work remotely from our servers. All you do is login to your password protected member area to use our service. You will be able to view account history details and have results emailed to you. ', 'Y');
INSERT INTO `tbl_faqs` VALUES (9, 'What protocols do you support?', 'We support: HTTP, FTP, POP3, SMTP. &lt;BR&gt;&lt;BR&gt;Additional protocols are available with our premium services.', 'Y');
INSERT INTO `tbl_faqs` VALUES (10, 'We already have an internal monitoring system, why would we need your system?', 'The Internet is a node/gateway based system. Your internal monitoring may tell you that a device is broken inside your network or even that the outbound gateway is having problems, but it may not inform you that inbound connections from different nodes on the Internet are able to access your website. Site Uptime Enterprise provides monitoring from various geographically diverse locations on different networks. ', 'Y');
INSERT INTO `tbl_faqs` VALUES (11, 'How can I contact Site Uptime Enterprise?', 'You may send an email to &lt;A href=&quot;mailto:info@alstrasoft.com&quot;&gt;info@SiteMo&lt;/A&gt;&lt;A href=&quot;mailto:info@alstrasoft.com&quot;&gt;&lt;/A&gt;&lt;A href=&quot;mailto:info@alstrasoft.com&quot;&gt;nitor.com&lt;/A&gt; or use our contact form.', 'Y');
INSERT INTO `tbl_faqs` VALUES (12, 'How many domains can I monitor?', 'Each &lt;STRONG&gt;account&lt;/STRONG&gt; may monitor &lt;STRONG&gt;1 domain&lt;/STRONG&gt;. If you need additional domains to be monitored, you can &lt;STRONG&gt;register&lt;/STRONG&gt; for our &lt;STRONG&gt;premium &lt;/STRONG&gt;service.', 'Y');
INSERT INTO `tbl_faqs` VALUES (13, 'How often do you monitor my site?', 'The monitoring interval for our free service is 30 or 60 minutes. You may select the monitoring interval that you want for each domain. Our Premium Service provides 5 and 15 minute monitoring intervals.', 'Y');
INSERT INTO `tbl_faqs` VALUES (14, 'How do you determine if a server is &quot;up&quot;?', 'Our software checks the server response code from your host and if the server response code is &quot;200&quot;, &quot;302&quot;, &quot;301&quot;, &quot;403&quot; or &quot;401&quot;, your hostname is considered &quot;up&quot;. ', 'Y');
INSERT INTO `tbl_faqs` VALUES (15, 'How far back do you keep monitoring statistics for my account?', 'Detailed statistics for each domain are kept for 2 months while statistic summaries are kept for the life of the account. All statistics for our premium accounts are keep for the life of the account.', 'Y');

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_locations`
-- 

DROP TABLE IF EXISTS `tbl_locations`;
CREATE TABLE IF NOT EXISTS `tbl_locations` (
  `loc_id` int(11) NOT NULL auto_increment,
  `loc_name` varchar(30) NOT NULL default '',
  `loc_country` varchar(30) NOT NULL default '',
  `ip_addr` varchar(19) NOT NULL default '',
  `status` char(1) NOT NULL default 'Y',
  PRIMARY KEY  (`loc_id`)
) ;

-- 
-- Dumping data for table `tbl_locations`
-- 

INSERT INTO `tbl_locations` VALUES (1, 'London', 'United Kingdom', '127.0.0.1', 'Y');

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_mailing`
-- 

DROP TABLE IF EXISTS `tbl_mailing`;
CREATE TABLE IF NOT EXISTS `tbl_mailing` (
  `id` int(10) NOT NULL auto_increment,
  `mail_name` varchar(50) NOT NULL default '',
  `mail_sub` varchar(100) NOT NULL default '',
  `mail_from` varchar(150) NOT NULL default '',
  `mail_txt` text NOT NULL,
  `mail_closing` varchar(150) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ;

-- 
-- Dumping data for table `tbl_mailing`
-- 

INSERT INTO `tbl_mailing` VALUES (1, 'Register Mail', 'Welcome To Site Uptime Enterprise', 'from Site Uptime Enterprise', '&lt;P&gt;Hello %%name%%, &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Thank you for registering with Site Monitor. &lt;/P&gt;', 'from Site Uptime Enterprise');
INSERT INTO `tbl_mailing` VALUES (2, 'New Service Mail', 'New services', 'Site Uptime Enterprise', '&lt;P&gt;Dear %%name%%, This is an automated message from &lt;A href=&quot;http://www.alstrasoft.com/&quot;&gt;Site Uptime Enterprise&lt;/A&gt;&lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;Host Name&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;%%host_name%% &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;Port&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;%%service_port%% &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;Result&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; %%result%% &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;Alert Type&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;%%alert_type%% &lt;/P&gt;\r\n&lt;P&gt;Time&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; %%time%% &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;Monitoring Location&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;%%location%% &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&lt;/P&gt;', 'Site Uptime Enterprise');
INSERT INTO `tbl_mailing` VALUES (3, 'Delete Service Mail', '%%host_name%% (%%service_port%%) has been lossed from Site Uptime Enterprise', 'Site Uptime Enterprise  < support@alstrasoft.com >', '&lt;P&gt;Dear %%name%% &lt;/P&gt;\r\n&lt;P&gt;This is an automated message from &lt;A href=&quot;http://www.alstrasoft.com/&quot;&gt;Site Uptime Enterprise&lt;/A&gt;&lt;/A&gt;.&lt;/P&gt;\r\n&lt;P&gt;Your service (%%host_name%%) has been lossed from Site Uptime Enterprise&lt;/P&gt;\r\n&lt;P&gt;If you want to add your site in Site Uptime Enterprise, Please click the below link&lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&lt;A href=&quot;http://www.alstrasoft.com&quot; target=_blank&gt;http://www.alstrasoft.com&lt;/A&gt;&lt;A href=&quot;http://www.alstrasoft.com/uptime/users/index.php&quot;&gt;&lt;/A&gt;&lt;A href=&quot;http://www.alstrasoft.com/uptime/users/index.php&quot; target=_blank&gt;/index.php&lt;/A&gt;&lt;/P&gt;\r\n&lt;P&gt;%%service_port%%&lt;/P&gt;', 'Site Uptime Enterprise');
INSERT INTO `tbl_mailing` VALUES (4, 'Forgot Password Mail', 'Site Uptime Enterprise', 'Site Uptime Enterprise', '&lt;P&gt;Hello %%name%%, &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;your EMail&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; %%emailid%%&lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Password&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;%%password%%.&lt;/P&gt;', 'Site Uptime Enterprise');
INSERT INTO `tbl_mailing` VALUES (5, 'Service Downtime  Mail', 'Site Down Notice from Site Uptime Enterprise', 'monitor@alstrasoft.com', '&lt;P&gt;Dear %%name%%,&lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; This is an Autogenerated Mail from &lt;A href=&quot;http://www.alstrasoft.com/&quot;&gt;Site Uptime Enterprise&lt;/A&gt;, Your Site is Not Available at this Time. &lt;BR&gt;&lt;BR&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Following Are the Details of Your Site : &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Host Name&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;%%host_name%% &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Service&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;%%service_port%% &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Result &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Failed&lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Alert Type&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Site is Not Available &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Time&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;%%time%% &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; This is An Autogenerated EMail, Dont Reply to This Mail &lt;/P&gt;', 'Site Uptime Enterprise');
INSERT INTO `tbl_mailing` VALUES (6, 'Service Uptime Mail', 'Site Up Notice from Site Uptime Enterprise', 'monitor@alstrasoft.com', '&lt;P&gt;Dear %%name%%,&lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; This is an Autogenerated Mail from &lt;A href=&quot;http://www.alstrasoft.com/&quot;&gt;Site Uptime Enterprise&lt;/A&gt;, Your Site is Available at this Time. &lt;BR&gt;&lt;BR&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Following Are the Details of Your Site : &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Host Name&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;%%host_name%% &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Service&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;%%service_port%% &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Result &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;%%result%%&lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Alert Type&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;%%alert_type%% &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Time&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; :&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;%%time%% &lt;/P&gt;\r\n&lt;P&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; This is An Autogenerated EMail, Dont Reply to This Mail &lt;/P&gt;', 'Site Uptime Enterprise');
INSERT INTO `tbl_mailing` VALUES (7, 'Credits Not Available', 'Your SMS Credits are not available', '', 'Insufficient sms credits', 'from Site Uptime Enterprise');

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_membership`
-- 

DROP TABLE IF EXISTS `tbl_membership`;
CREATE TABLE IF NOT EXISTS `tbl_membership` (
  `m_id` int(11) NOT NULL auto_increment,
  `mem_name` varchar(35) NOT NULL default '',
  `total_sites` int(10) NOT NULL default '0',
  `amt_monthly` float(10,2) NOT NULL default '0.00',
  `amt_yearly` float(12,2) NOT NULL default '0.00',
  `monitor_interval_level` char(1) NOT NULL default 'Y',
  `multi_monitor` char(1) NOT NULL default 'Y',
  `email_alerts` char(1) NOT NULL default 'Y',
  `monthly_reports` char(1) NOT NULL default 'Y',
  `online_stats` char(1) NOT NULL default 'Y',
  `control_panel` char(1) NOT NULL default 'Y',
  `http_check` char(1) NOT NULL default 'Y',
  `smtp_check` char(1) NOT NULL default 'Y',
  `pop3_check` char(1) NOT NULL default 'Y',
  `ftp_check` char(1) NOT NULL default 'Y',
  `public_stats` char(1) NOT NULL default 'Y',
  `error_details` char(1) NOT NULL default 'Y',
  `ping_monitoring` char(1) NOT NULL default 'Y',
  `custom_time_zone` char(1) NOT NULL default 'Y',
  `multi_alerts` char(1) NOT NULL default 'Y',
  `custom_ports` char(1) NOT NULL default 'Y',
  `ssl_server` char(1) NOT NULL default 'Y',
  `addnl_service_monitor` char(1) NOT NULL default 'Y',
  `status` char(1) NOT NULL default 'Y',
  `sms_alert` char(1) NOT NULL default 'N',
  `mem_rating` int(2) NOT NULL default '0',
  PRIMARY KEY  (`m_id`,`mem_name`)
) ;

-- 
-- Dumping data for table `tbl_membership`
-- 

INSERT INTO `tbl_membership` VALUES (1, 'Free Account', 1, 0.00, 0.00, '1', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 3);
INSERT INTO `tbl_membership` VALUES (2, 'Premium Account', 5, 5.00, 50.00, '2', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'N', 2);
INSERT INTO `tbl_membership` VALUES (3, 'Premium Plus Account', 10, 10.00, 100.00, '2', 'N', 'Y', 'Y', 'Y', 'N', 'Y', 'N', 'Y', 'N', 'Y', 'N', 'Y', 'Y', 'Y', 'Y', 'N', 'Y', 'Y', 'Y', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_monthlyservices`
-- 

DROP TABLE IF EXISTS `tbl_monthlyservices`;
CREATE TABLE IF NOT EXISTS `tbl_monthlyservices` (
  `service_id` int(15) NOT NULL default '0',
  `date_of_service` date NOT NULL default '0000-00-00',
  `no_of_checks` int(10) NOT NULL default '0',
  `down_checks` int(10) NOT NULL default '0',
  `resp_time` double(12,5) NOT NULL default '0.00000',
  PRIMARY KEY  (`service_id`,`date_of_service`)
) ;

-- 
-- Dumping data for table `tbl_monthlyservices`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_newusers`
-- 

DROP TABLE IF EXISTS `tbl_newusers`;
CREATE TABLE IF NOT EXISTS `tbl_newusers` (
  `userid` int(15) NOT NULL auto_increment,
  `emailid` varchar(35) NOT NULL default '',
  `password` varchar(15) NOT NULL default '',
  `name` varchar(30) NOT NULL default '',
  `joindate` date NOT NULL default '0000-00-00',
  `memtype` int(5) NOT NULL default '0',
  `company` varchar(25) NOT NULL default '',
  `address` varchar(30) NOT NULL default '',
  `state` int(11) NOT NULL default '0',
  `city` varchar(20) NOT NULL default '',
  `zip` varchar(10) NOT NULL default '',
  `country` int(11) NOT NULL default '0',
  `aemail` varchar(50) NOT NULL default '',
  `aemail1` varchar(50) default NULL,
  `aemail2` varchar(50) default NULL,
  `aemail3` varchar(50) default NULL,
  `aemail4` varchar(50) default NULL,
  `report` char(1) NOT NULL default 'N',
  `timezone` varchar(5) NOT NULL default '3',
  `sessionid` varchar(100) NOT NULL default '',
  `smscredits` int(10) NOT NULL default '0',
  `mobile` varchar(20) NOT NULL default '',
  `mobile1` varchar(20) default NULL,
  `mobile2` varchar(20) default NULL,
  `mobile3` varchar(20) default NULL,
  `mobile4` varchar(20) default NULL,
  `add_sites` int(10) NOT NULL default '0',
  PRIMARY KEY  (`userid`),
  KEY `userid` (`userid`)
) ;

-- 
-- Dumping data for table `tbl_newusers`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_quickcheck`
-- 

DROP TABLE IF EXISTS `tbl_quickcheck`;
CREATE TABLE IF NOT EXISTS `tbl_quickcheck` (
  `qc_id` int(11) NOT NULL auto_increment,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `qc_url` varchar(100) NOT NULL default '',
  `qc_port` int(10) NOT NULL default '0',
  `custom_port` int(10) NOT NULL default '0',
  `location1` char(1) NOT NULL default 'Y',
  `result1` varchar(10) default NULL,
  `resp_time1` varchar(10) default NULL,
  PRIMARY KEY  (`qc_id`)
) ;

-- 
-- Dumping data for table `tbl_quickcheck`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_resources`
-- 

DROP TABLE IF EXISTS `tbl_resources`;
CREATE TABLE IF NOT EXISTS `tbl_resources` (
  `res_id` int(11) NOT NULL auto_increment,
  `title` varchar(100) NOT NULL default '',
  `url` varchar(100) NOT NULL default '',
  `description` text NOT NULL,
  `status` char(1) NOT NULL default 'Y',
  PRIMARY KEY  (`res_id`)
) ;

-- 
-- Dumping data for table `tbl_resources`
-- 

INSERT INTO `tbl_resources` VALUES (1, 'SiteReportCard', 'http://www.stiereportcard.com', 'Free website optimization, promotion and analysis tools to help you improve your website. &lt;FONT color=#ff00ff&gt;The tools help you find broken&lt;/FONT&gt; links, validate HTML, improve &lt;STRONG&gt;meta &lt;/STRONG&gt;tags, &lt;EM&gt;check&lt;/EM&gt;&lt;U&gt; spelling&lt;/U&gt;, etc. ', 'Y');
INSERT INTO `tbl_resources` VALUES (2, 'Web Host Reviews', 'http://www.myhostreviews.com', 'Read and write reviews and ratings for web hosting companies. Search for top rated web hosting companies. ', 'Y');
INSERT INTO `tbl_resources` VALUES (3, 'Free Webmaster Tools', 'http://www.free-webmaster-tools.com', 'A collection of quality tools, over &lt;STRONG&gt;&lt;FONT color=#ff0000&gt;1400 resources&lt;/FONT&gt;&lt;/STRONG&gt; for your web development needs, and insightful articles/tips.', 'Y');
INSERT INTO `tbl_resources` VALUES (4, 'Web Server Index', 'http://www.webserverindex.com', 'Dedicated server reviews and information.', 'Y');
INSERT INTO `tbl_resources` VALUES (5, 'Addpro.com', 'http://www.addpro.com', 'Search Engine Submission resources.', 'Y');
INSERT INTO `tbl_resources` VALUES (6, 'Instant Position Website', 'http://www.instantposition.com', 'Free search engine optimization tools, how-to guides, articles and free search engine submissions.', 'Y');
INSERT INTO `tbl_resources` VALUES (7, 'SiteProNews', 'http://www.sitepronews.com', 'Free newsletter and webmaster resource site.', 'Y');
INSERT INTO `tbl_resources` VALUES (8, 'Web-Authoring.com', 'http://www.web-authoring.com', 'Webmaster directory with over 2500 resources and links.', 'Y');
INSERT INTO `tbl_resources` VALUES (9, 'CodingForums.com', 'http://www.codingforums.com', 'Web coding and development forums. Get help on JavaScript, PHP, CSS, XML, mySQL, ASP, and more! ', 'Y');
INSERT INTO `tbl_resources` VALUES (10, 'Webmaster Drive', 'http://webmasterdrive.com', '', 'Y');
INSERT INTO `tbl_resources` VALUES (11, 'Site Point', 'http://www.sitepoint.com', '&lt;STRONG&gt;&lt;FONT size=7&gt;this is the good site i&amp;nbsp;ever saw&lt;/FONT&gt;&lt;/STRONG&gt;', 'Y');
INSERT INTO `tbl_resources` VALUES (12, 'Webmaster Edge', 'http://www.webmasteredge.com', '', 'Y');
INSERT INTO `tbl_resources` VALUES (13, 'Webmaster Tools Central', 'http://www.webmastertoolscentral.com', '', 'Y');
INSERT INTO `tbl_resources` VALUES (14, 'Web Developer''s Virtual Library', 'http://www.wdvl.com', '', 'N');

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_service_ports`
-- 

DROP TABLE IF EXISTS `tbl_service_ports`;
CREATE TABLE IF NOT EXISTS `tbl_service_ports` (
  `service_id` int(11) NOT NULL auto_increment,
  `service_name` varchar(25) NOT NULL default '',
  `port_no` int(10) NOT NULL default '0',
  `status` char(1) NOT NULL default 'Y',
  `authority` int(2) NOT NULL default '0',
  PRIMARY KEY  (`service_id`,`port_no`),
  UNIQUE KEY `service_name` (`service_name`,`port_no`)
) ;

-- 
-- Dumping data for table `tbl_service_ports`
-- 

INSERT INTO `tbl_service_ports` VALUES (1, 'http (80)', 80, 'Y', 1);
INSERT INTO `tbl_service_ports` VALUES (2, 'smtp (25)', 25, 'Y', 1);
INSERT INTO `tbl_service_ports` VALUES (3, 'ftp (21)', 21, 'Y', 1);
INSERT INTO `tbl_service_ports` VALUES (4, 'pop3 (110)', 110, 'Y', 1);
INSERT INTO `tbl_service_ports` VALUES (5, 'ping (2534)', 2534, 'Y', 2);
INSERT INTO `tbl_service_ports` VALUES (6, 'Custom Port', -430, 'Y', 3);
INSERT INTO `tbl_service_ports` VALUES (7, 'https (443)', 443, 'Y', 3);

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_services`
-- 

DROP TABLE IF EXISTS `tbl_services`;
CREATE TABLE IF NOT EXISTS `tbl_services` (
  `service_id` int(10) NOT NULL auto_increment,
  `service_name` varchar(50) NOT NULL default '',
  `userid` int(11) NOT NULL default '0',
  `date_add` datetime NOT NULL default '0000-00-00 00:00:00',
  `host_name` varchar(50) NOT NULL default '',
  `service_port` int(5) NOT NULL default '0',
  `custom_port` int(5) NOT NULL default '0',
  `period` int(2) NOT NULL default '0',
  `location` int(2) NOT NULL default '0',
  `status_email` char(1) NOT NULL default 'Y',
  `email` varchar(50) NOT NULL default '',
  `status_sms` char(1) NOT NULL default 'N',
  `last_check` datetime NOT NULL default '0000-00-00 00:00:00',
  `active` char(1) NOT NULL default 'Y',
  `check_status` int(2) NOT NULL default '0',
  `monitor_interval_level` char(1) NOT NULL default '3',
  `check_sms_status` int(2) NOT NULL default '0',
  `mobile` varchar(20) default NULL,
  `multi_mails` text,
  `multi_mobiles` text,
  `smsalert_down` int(2) NOT NULL default '0',
  PRIMARY KEY  (`service_id`)
) ;

-- 
-- Dumping data for table `tbl_services`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_states`
-- 

DROP TABLE IF EXISTS `tbl_states`;
CREATE TABLE IF NOT EXISTS `tbl_states` (
  `cid` int(11) NOT NULL default '0',
  `stateid` int(11) NOT NULL auto_increment,
  `statename` varchar(50) NOT NULL default '',
  `status` char(1) NOT NULL default 'Y',
  PRIMARY KEY  (`stateid`)
) ;

-- 
-- Dumping data for table `tbl_states`
-- 

INSERT INTO `tbl_states` VALUES (45, 1, 'Anhui', 'Y');
INSERT INTO `tbl_states` VALUES (45, 2, 'Beijing', 'Y');
INSERT INTO `tbl_states` VALUES (45, 3, 'Chongqing', 'Y');
INSERT INTO `tbl_states` VALUES (45, 4, ' Fujian', 'Y');
INSERT INTO `tbl_states` VALUES (45, 5, 'Gansu', 'Y');
INSERT INTO `tbl_states` VALUES (45, 6, 'Guangdong', 'Y');
INSERT INTO `tbl_states` VALUES (45, 7, 'Guangxi', 'Y');
INSERT INTO `tbl_states` VALUES (45, 8, 'Guizhou', 'Y');
INSERT INTO `tbl_states` VALUES (45, 9, 'Hainan', 'Y');
INSERT INTO `tbl_states` VALUES (45, 10, 'Hebei', 'Y');
INSERT INTO `tbl_states` VALUES (45, 11, 'Heilongjiang', 'Y');
INSERT INTO `tbl_states` VALUES (45, 12, 'Henan', 'Y');
INSERT INTO `tbl_states` VALUES (45, 13, 'Hubei', 'Y');
INSERT INTO `tbl_states` VALUES (45, 14, 'Hunan', 'Y');
INSERT INTO `tbl_states` VALUES (45, 15, 'Jiangsu', 'Y');
INSERT INTO `tbl_states` VALUES (45, 16, 'Jiangxi', 'Y');
INSERT INTO `tbl_states` VALUES (45, 17, 'Jilin', 'Y');
INSERT INTO `tbl_states` VALUES (45, 18, 'Liaoning', 'Y');
INSERT INTO `tbl_states` VALUES (45, 19, 'Nei Mongol', 'Y');
INSERT INTO `tbl_states` VALUES (45, 20, 'Ningxia', 'Y');
INSERT INTO `tbl_states` VALUES (45, 21, 'Qinghai', 'Y');
INSERT INTO `tbl_states` VALUES (45, 22, 'Shaanxi', 'Y');
INSERT INTO `tbl_states` VALUES (45, 23, 'Shandong', 'Y');
INSERT INTO `tbl_states` VALUES (45, 24, 'Shanghai', 'Y');
INSERT INTO `tbl_states` VALUES (45, 25, 'Shanxi', 'Y');
INSERT INTO `tbl_states` VALUES (45, 26, 'Sichuan', 'Y');
INSERT INTO `tbl_states` VALUES (45, 27, 'Tianjin', 'Y');
INSERT INTO `tbl_states` VALUES (45, 28, 'Xinjiang', 'Y');
INSERT INTO `tbl_states` VALUES (45, 29, 'Xizang', 'Y');
INSERT INTO `tbl_states` VALUES (45, 30, 'Yunnan', 'Y');
INSERT INTO `tbl_states` VALUES (45, 31, 'Zhejiang', 'Y');
INSERT INTO `tbl_states` VALUES (103, 32, 'Aceh', 'Y');
INSERT INTO `tbl_states` VALUES (103, 33, 'Bali', 'Y');
INSERT INTO `tbl_states` VALUES (103, 34, 'Banten', 'Y');
INSERT INTO `tbl_states` VALUES (103, 35, 'Bengkulu', 'Y');
INSERT INTO `tbl_states` VALUES (103, 36, 'Daerah Istimewa Yogyakarta', 'Y');
INSERT INTO `tbl_states` VALUES (103, 37, 'Daerah Tingkat I Kalimantan Barat', 'Y');
INSERT INTO `tbl_states` VALUES (103, 38, 'Irian Jaya', 'Y');
INSERT INTO `tbl_states` VALUES (103, 39, 'Jakarta Raya', 'Y');
INSERT INTO `tbl_states` VALUES (103, 40, 'Kalimantan Tengah', 'Y');
INSERT INTO `tbl_states` VALUES (103, 41, 'Kalimantan Timur', 'Y');
INSERT INTO `tbl_states` VALUES (103, 42, 'Nusa Tenggara Barat', 'Y');
INSERT INTO `tbl_states` VALUES (103, 43, 'Nusa Tenggara Timur', 'Y');
INSERT INTO `tbl_states` VALUES (103, 44, 'Propinsi Jambi', 'Y');
INSERT INTO `tbl_states` VALUES (103, 45, 'Propinsi Jawa Barat', 'Y');
INSERT INTO `tbl_states` VALUES (103, 46, 'Propinsi Jawa Tengah', 'Y');
INSERT INTO `tbl_states` VALUES (103, 47, 'Propinsi Jawa Timur', 'Y');
INSERT INTO `tbl_states` VALUES (103, 48, 'Propinsi Kalimantan Selatan', 'Y');
INSERT INTO `tbl_states` VALUES (103, 49, 'Propinsi Lampung', 'Y');
INSERT INTO `tbl_states` VALUES (103, 50, 'Propinsi Maluku', 'Y');
INSERT INTO `tbl_states` VALUES (103, 51, 'Propinsi Sulawesi Selatan', 'Y');
INSERT INTO `tbl_states` VALUES (103, 52, 'Propinsi Sulawesi Utara', 'Y');
INSERT INTO `tbl_states` VALUES (103, 53, 'Propinsi Sumatera Selatan', 'Y');
INSERT INTO `tbl_states` VALUES (103, 54, 'Propinsi Sumatera Utara', 'Y');
INSERT INTO `tbl_states` VALUES (103, 55, 'Riau', 'Y');
INSERT INTO `tbl_states` VALUES (103, 56, 'Sulawesi Tengah', 'Y');
INSERT INTO `tbl_states` VALUES (103, 57, 'Sulawesi Tenggara', 'Y');
INSERT INTO `tbl_states` VALUES (103, 58, 'Sumatera Barat', 'Y');
INSERT INTO `tbl_states` VALUES (116, 59, 'Busan', 'Y');
INSERT INTO `tbl_states` VALUES (116, 60, 'Chungcheongbuk-do', 'Y');
INSERT INTO `tbl_states` VALUES (116, 61, 'Chungcheongnam-do', 'Y');
INSERT INTO `tbl_states` VALUES (116, 62, 'Daegu', 'Y');
INSERT INTO `tbl_states` VALUES (116, 63, 'Daejeon', 'Y');
INSERT INTO `tbl_states` VALUES (116, 64, 'Gangwon-do', 'Y');
INSERT INTO `tbl_states` VALUES (116, 65, 'Gwangju', 'Y');
INSERT INTO `tbl_states` VALUES (116, 66, 'Gyeonggi-do', 'Y');
INSERT INTO `tbl_states` VALUES (116, 67, 'Gyeongsangbuk-do', 'Y');
INSERT INTO `tbl_states` VALUES (116, 68, 'Gyeongsangnam-do', 'Y');
INSERT INTO `tbl_states` VALUES (116, 69, 'Incheon', 'Y');
INSERT INTO `tbl_states` VALUES (116, 70, 'Jeju-do', 'Y');
INSERT INTO `tbl_states` VALUES (116, 71, 'Jeollabuk-do', 'Y');
INSERT INTO `tbl_states` VALUES (116, 72, 'Jeollanam-do', 'Y');
INSERT INTO `tbl_states` VALUES (116, 73, 'Seoul', 'Y');
INSERT INTO `tbl_states` VALUES (116, 74, 'Ulsan', 'Y');
INSERT INTO `tbl_states` VALUES (1, 75, 'Alabama', 'Y');
INSERT INTO `tbl_states` VALUES (1, 76, 'Alaska', 'Y');
INSERT INTO `tbl_states` VALUES (1, 77, 'Arizona', 'Y');
INSERT INTO `tbl_states` VALUES (1, 78, 'Arkansas', 'Y');
INSERT INTO `tbl_states` VALUES (1, 79, 'California', 'Y');
INSERT INTO `tbl_states` VALUES (1, 80, 'Colorado', 'Y');
INSERT INTO `tbl_states` VALUES (1, 81, 'Connecticut', 'Y');
INSERT INTO `tbl_states` VALUES (1, 82, 'Delaware', 'Y');
INSERT INTO `tbl_states` VALUES (1, 83, 'District of Columbia', 'Y');
INSERT INTO `tbl_states` VALUES (1, 84, 'Georgia', 'Y');
INSERT INTO `tbl_states` VALUES (1, 85, 'Hawaii', 'Y');
INSERT INTO `tbl_states` VALUES (1, 86, 'Idaho', 'Y');
INSERT INTO `tbl_states` VALUES (1, 87, 'Illinois', 'Y');
INSERT INTO `tbl_states` VALUES (1, 88, 'Indiana', 'Y');
INSERT INTO `tbl_states` VALUES (1, 89, 'Iowa', 'Y');
INSERT INTO `tbl_states` VALUES (1, 90, 'Kansas', 'Y');
INSERT INTO `tbl_states` VALUES (1, 91, 'Kentucky', 'Y');
INSERT INTO `tbl_states` VALUES (1, 92, 'Louisiana', 'Y');
INSERT INTO `tbl_states` VALUES (1, 93, 'Maine', 'Y');
INSERT INTO `tbl_states` VALUES (1, 94, 'Maryland', 'Y');
INSERT INTO `tbl_states` VALUES (1, 95, 'Massachusetts', 'Y');
INSERT INTO `tbl_states` VALUES (1, 96, 'Michigan', 'Y');
INSERT INTO `tbl_states` VALUES (1, 97, 'Minnesota', 'Y');
INSERT INTO `tbl_states` VALUES (1, 98, 'Mississippi', 'Y');
INSERT INTO `tbl_states` VALUES (1, 99, 'Missouri', 'Y');
INSERT INTO `tbl_states` VALUES (1, 100, 'Montana', 'Y');
INSERT INTO `tbl_states` VALUES (1, 101, 'Nebraska', 'Y');
INSERT INTO `tbl_states` VALUES (1, 102, 'Nevada', 'Y');
INSERT INTO `tbl_states` VALUES (1, 103, 'New Hampshire', 'Y');
INSERT INTO `tbl_states` VALUES (1, 104, 'New Jersey', 'Y');
INSERT INTO `tbl_states` VALUES (1, 105, 'New Mexico', 'Y');
INSERT INTO `tbl_states` VALUES (1, 106, 'New York', 'Y');
INSERT INTO `tbl_states` VALUES (1, 107, 'North Carolina', 'Y');
INSERT INTO `tbl_states` VALUES (1, 108, 'North Dakota', 'Y');
INSERT INTO `tbl_states` VALUES (1, 109, 'Ohio', 'Y');
INSERT INTO `tbl_states` VALUES (1, 110, 'Oklahoma', 'Y');
INSERT INTO `tbl_states` VALUES (1, 111, 'Oregon', 'Y');
INSERT INTO `tbl_states` VALUES (1, 112, 'Pennsylvania', 'Y');
INSERT INTO `tbl_states` VALUES (1, 113, 'Rhode Island', 'Y');
INSERT INTO `tbl_states` VALUES (1, 114, 'South Carolina', 'Y');
INSERT INTO `tbl_states` VALUES (1, 115, 'South Dakota', 'Y');
INSERT INTO `tbl_states` VALUES (1, 116, 'Tennessee', 'Y');
INSERT INTO `tbl_states` VALUES (1, 117, 'Texas', 'Y');
INSERT INTO `tbl_states` VALUES (1, 118, 'Utah', 'Y');
INSERT INTO `tbl_states` VALUES (1, 119, 'Vermont', 'Y');
INSERT INTO `tbl_states` VALUES (1, 120, 'Virginia', 'Y');
INSERT INTO `tbl_states` VALUES (1, 121, 'Washington', 'Y');
INSERT INTO `tbl_states` VALUES (1, 122, 'West Virginia', 'Y');
INSERT INTO `tbl_states` VALUES (1, 123, 'Wisconsin', 'Y');
INSERT INTO `tbl_states` VALUES (1, 124, 'Wyoming', 'Y');
INSERT INTO `tbl_states` VALUES (1, 125, 'Florida', 'Y');
INSERT INTO `tbl_states` VALUES (102, 126, 'Andaman & Nicobar (U.T.)', 'Y');
INSERT INTO `tbl_states` VALUES (102, 127, 'Lakshadweep (U.T.)', 'Y');
INSERT INTO `tbl_states` VALUES (102, 128, 'Andhra Pradesh', 'Y');
INSERT INTO `tbl_states` VALUES (102, 129, 'Madhya Pradesh', 'Y');
INSERT INTO `tbl_states` VALUES (102, 130, 'Arunachal Pradesh', 'Y');
INSERT INTO `tbl_states` VALUES (102, 131, 'Maharashtra', 'Y');
INSERT INTO `tbl_states` VALUES (102, 132, 'Assam', 'Y');
INSERT INTO `tbl_states` VALUES (102, 133, 'Manipur', 'Y');
INSERT INTO `tbl_states` VALUES (102, 134, 'Bihar', 'Y');
INSERT INTO `tbl_states` VALUES (102, 135, 'Meghalaya', 'Y');
INSERT INTO `tbl_states` VALUES (102, 136, 'Chandigarh (U.T.)', 'Y');
INSERT INTO `tbl_states` VALUES (102, 137, 'Mizoram', 'Y');
INSERT INTO `tbl_states` VALUES (102, 138, 'Chhattisgarh', 'Y');
INSERT INTO `tbl_states` VALUES (102, 139, 'Nagaland', 'Y');
INSERT INTO `tbl_states` VALUES (102, 140, 'Dadra & Nagar Haveli (U.T.)', 'Y');
INSERT INTO `tbl_states` VALUES (102, 141, 'Orissa', 'Y');
INSERT INTO `tbl_states` VALUES (102, 142, 'Daman & Diu (U.T.)', 'Y');
INSERT INTO `tbl_states` VALUES (102, 143, 'Pondicherry (U.T.)', 'Y');
INSERT INTO `tbl_states` VALUES (102, 144, 'Delhi  (U.T.-NCT)', 'Y');
INSERT INTO `tbl_states` VALUES (102, 145, 'Punjab', 'Y');
INSERT INTO `tbl_states` VALUES (102, 146, 'Goa', 'Y');
INSERT INTO `tbl_states` VALUES (102, 147, 'Rajasthan', 'Y');
INSERT INTO `tbl_states` VALUES (102, 148, 'Gujarat', 'Y');
INSERT INTO `tbl_states` VALUES (102, 149, 'Sikkim', 'Y');
INSERT INTO `tbl_states` VALUES (102, 150, 'Haryana', 'Y');
INSERT INTO `tbl_states` VALUES (102, 151, 'Tamil Nadu', 'Y');
INSERT INTO `tbl_states` VALUES (102, 152, 'Himachal Pradesh', 'Y');
INSERT INTO `tbl_states` VALUES (102, 153, 'Tripura', 'Y');
INSERT INTO `tbl_states` VALUES (102, 154, 'Jammu & Kashmir', 'Y');
INSERT INTO `tbl_states` VALUES (102, 155, 'Uttar Pradesh', 'Y');
INSERT INTO `tbl_states` VALUES (102, 156, 'Jharkhand', 'Y');
INSERT INTO `tbl_states` VALUES (102, 157, 'Uttaranchal', 'Y');
INSERT INTO `tbl_states` VALUES (102, 158, 'Karnataka', 'Y');
INSERT INTO `tbl_states` VALUES (102, 159, 'West Bengal', 'Y');
INSERT INTO `tbl_states` VALUES (102, 160, 'Kerala', 'Y');
INSERT INTO `tbl_states` VALUES (111, 170, 'Jo Jo Ku Ku', 'Y');

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_support`
-- 

DROP TABLE IF EXISTS `tbl_support`;
CREATE TABLE IF NOT EXISTS `tbl_support` (
  `suppid` double NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `emailid` varchar(100) NOT NULL default '',
  `subject` varchar(100) NOT NULL default '',
  `comment` text NOT NULL,
  `cdate` date NOT NULL default '0000-00-00',
  `status` varchar(20) NOT NULL default 'new',
  `uid` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`suppid`)
) ;

-- 
-- Dumping data for table `tbl_support`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_testimonials`
-- 

DROP TABLE IF EXISTS `tbl_testimonials`;
CREATE TABLE IF NOT EXISTS `tbl_testimonials` (
  `test_id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `description` text NOT NULL,
  `status` char(1) NOT NULL default 'N',
  PRIMARY KEY  (`test_id`)
) ;

-- 
-- Dumping data for table `tbl_testimonials`
-- 

INSERT INTO `tbl_testimonials` VALUES (1, 'Elliot B., Incognito Networks', 'Since we came across Site Uptime Enterprise, we have not looked back. The website monitoring Site Uptime Enterprise&amp;nbsp;provides is an outstanding service at an equally outstanding price. We have every aspect of our servers monitored with our Premium level account with monitoring intervals of 5 minutes. If any issue arises with our servers, Site Uptime Enterprise notifies us instantly, allowing us to address the issue quickly and efficiently. We have found Site Uptime Enterprise an invaluable service, and the best website monitoring service we have seen yet. Keep up the great work Site Uptime Enterprise!', 'Y');
INSERT INTO `tbl_testimonials` VALUES (2, 'James', '&lt;STRONG&gt;myself&lt;/STRONG&gt; have just signed up to this service as a friend of mine had passed me onto this service and since being with this service I have found it to be a &lt;EM&gt;great asset as it&lt;/EM&gt; will help determine if my website is down as I host others website also. So thank you for bringing us a great site monitoring program.', 'Y');
INSERT INTO `tbl_testimonials` VALUES (4, 'Steve C.', 'I''ve only just registered but this looks to be a fantastic service. My host provider is not publishing my sites uptime so now I can find out for myself and challenge them on it if it''s not up to par.', 'Y');
INSERT INTO `tbl_testimonials` VALUES (5, 'Stephen M.', 'Your service is great, I love being able to ensure that my site is available to my customers! I have recommended your services to many of my friends, keep up the great work!', 'Y');
INSERT INTO `tbl_testimonials` VALUES (6, 'David C.', 'I just want to say thanks for your terrific service. My ecommerce site needs to be available 100% of the time and when it isn''t, I need to know.', 'Y');
INSERT INTO `tbl_testimonials` VALUES (7, 'Tara K.', 'Site Uptime Enterprise is such a great site, I can&#039;t stop raving about your service to all my friends who have website&#039;s. Thanks for all the help!', 'Y');
INSERT INTO `tbl_testimonials` VALUES (8, 'Jon T.', 'I can''t believe that you provide this service free of charge. I was using NetMechanic services but yours are better and I don''t have to pay a dime!.', 'Y');
INSERT INTO `tbl_testimonials` VALUES (9, 'Katy E.', 'After signing up for your service I didn''t expect much since it was free but I can''t believe how useful your uptime stats are. Thanks so much.', 'Y');
INSERT INTO `tbl_testimonials` VALUES (10, 'Janice W.', 'Thanks so much for your free service, it makes my job as webmaster much easier.', 'Y');
INSERT INTO `tbl_testimonials` VALUES (11, 'D. Deagle', 'I just wanted to say thanks for such a great service. I''ve been telling everyone I know. Keep up the good work!', 'Y');

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_timezones`
-- 

DROP TABLE IF EXISTS `tbl_timezones`;
CREATE TABLE IF NOT EXISTS `tbl_timezones` (
  `tzval` float(4,2) NOT NULL default '0.00',
  `tzstring` varchar(20) NOT NULL default ''
) ;

-- 
-- Dumping data for table `tbl_timezones`
-- 

INSERT INTO `tbl_timezones` VALUES (-12.00, 'GMT-12:00');
INSERT INTO `tbl_timezones` VALUES (-11.00, 'GMT-11:00');
INSERT INTO `tbl_timezones` VALUES (-10.00, 'GMT-10:00');
INSERT INTO `tbl_timezones` VALUES (-9.00, 'GMT-09:00');
INSERT INTO `tbl_timezones` VALUES (-8.00, 'GMT-08:00');
INSERT INTO `tbl_timezones` VALUES (-7.00, 'GMT-07:00');
INSERT INTO `tbl_timezones` VALUES (-6.00, 'GMT-06:00');
INSERT INTO `tbl_timezones` VALUES (-5.00, 'GMT-05:00');
INSERT INTO `tbl_timezones` VALUES (-4.00, 'GMT-04:00');
INSERT INTO `tbl_timezones` VALUES (-3.50, 'GMT-03:30');
INSERT INTO `tbl_timezones` VALUES (-3.00, 'GMT-03:00');
INSERT INTO `tbl_timezones` VALUES (-2.00, 'GMT-02:00');
INSERT INTO `tbl_timezones` VALUES (-1.00, 'GMT-01:00');
INSERT INTO `tbl_timezones` VALUES (0.00, 'GMT 00.00');
INSERT INTO `tbl_timezones` VALUES (1.00, 'GMT+01:00');
INSERT INTO `tbl_timezones` VALUES (2.00, 'GMT+02:00');
INSERT INTO `tbl_timezones` VALUES (3.00, 'GMT+03:00');
INSERT INTO `tbl_timezones` VALUES (3.50, 'GMT+03:30');
INSERT INTO `tbl_timezones` VALUES (4.00, 'GMT+04:00');
INSERT INTO `tbl_timezones` VALUES (4.50, 'GMT+04:30');
INSERT INTO `tbl_timezones` VALUES (5.00, 'GMT+05:00');
INSERT INTO `tbl_timezones` VALUES (5.50, 'GMT+05:30');
INSERT INTO `tbl_timezones` VALUES (5.75, 'GMT+05:45');
INSERT INTO `tbl_timezones` VALUES (6.00, 'GMT+06:00');
INSERT INTO `tbl_timezones` VALUES (6.50, 'GMT+06:30');
INSERT INTO `tbl_timezones` VALUES (7.00, 'GMT+07:00');
INSERT INTO `tbl_timezones` VALUES (8.00, 'GMT+08:00');
INSERT INTO `tbl_timezones` VALUES (9.00, 'GMT+09:00');
INSERT INTO `tbl_timezones` VALUES (9.50, 'GMT+09:30');
INSERT INTO `tbl_timezones` VALUES (10.00, 'GMT+10:00');
INSERT INTO `tbl_timezones` VALUES (11.00, 'GMT+11:00');
INSERT INTO `tbl_timezones` VALUES (12.00, 'GMT+12:00');
INSERT INTO `tbl_timezones` VALUES (13.00, 'GMT+13:00');

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_users`
-- 

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `userid` int(15) NOT NULL auto_increment,
  `emailid` varchar(35) NOT NULL default '',
  `password` varchar(15) NOT NULL default '',
  `name` varchar(30) NOT NULL default '',
  `joindate` date NOT NULL default '0000-00-00',
  `memtype` int(5) NOT NULL default '1',
  `company` varchar(25) NOT NULL default '',
  `address` varchar(30) NOT NULL default '',
  `state` int(11) NOT NULL default '0',
  `city` varchar(20) NOT NULL default '',
  `zip` varchar(10) NOT NULL default '',
  `country` int(11) NOT NULL default '0',
  `aemail` varchar(50) NOT NULL default '',
  `aemail1` varchar(50) default NULL,
  `aemail2` varchar(50) default NULL,
  `aemail3` varchar(50) default NULL,
  `aemail4` varchar(50) default NULL,
  `report` char(1) NOT NULL default 'N',
  `timezone` float(4,2) NOT NULL default '0.00',
  `smscredits` int(10) NOT NULL default '0',
  `mobile` varchar(20) NOT NULL default '',
  `mobile1` varchar(20) default NULL,
  `mobile2` varchar(20) default NULL,
  `mobile3` varchar(20) default NULL,
  `mobile4` varchar(20) default NULL,
  `add_sites` int(10) NOT NULL default '0',
  `smscredits_status` int(2) NOT NULL default '0',
  `mem_auth` int(2) NOT NULL default '1',
  PRIMARY KEY  (`userid`),
  KEY `userid` (`userid`)
) ;

-- 
-- Dumping data for table `tbl_users`
-- 
