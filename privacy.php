<?
include("admin/include/conn.inc");
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site Uptime Enterprise</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="monitor_style.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="730" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="504" valign="top"><TABLE class=formtext border=0 cellPadding=0 cellSpacing=0 width="480">
                          <TBODY>
                            <TR> 
                              <TD align="left" valign="top" class=body> <table width="450" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                    <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                    <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                      </strong></font><span class="c3">Privacy 
                                      Policy </span></B></td>
                                  </tr>
                                  <tr > 
                                    <td background="images/dotline.gif" colspan="3"></td>
                                  </tr>
                                </table>
                                <P> <span class="lineheight">Site Uptime Enterprise recognizes 
                                  the importance of protecting the privacy of 
                                  all information provided by users of our services. 
                                  We have adopted this privacy policy to assure 
                                  our users that their personal information will 
                                  be kept confidential. </span>
                                <p align="justify" class="lineheight" style="margin-left:10px;margin-right:10px;">The 
                                  registration form requires users to 
                                  provide their e-mail address and other contact 
                                  information. This information is kept confidential 
                                  and will only be used for account verification 
                                  and notifications to the email address listed 
                                  on your account. We do not disclose, sell or 
                                  give this information to any other companies 
                                  or organizations without your consent. </p>
                                <p align="justify" class="lineheight" style="margin-left:10px;margin-right:10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                  We occasionally use &quot;cookies&quot;, 
                                  which allows us to recognize who you are at 
                                  subsequent visits to our site and to your member 
                                  control panel. You can change your browser settings 
                                  to accept or reject cookies. </p>
                                <p style="margin-left:10px;margin-right:10px;"><span class="lineheight">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                  </span><span class="c1">If you have any questions 
                                  regarding this policy, please contact <br>
                                  </span><strong><span class="link"><a href="mailto: info@alstrasoft.com" class="link">info@alstrasoft.com</a>.</span></strong><span class="lineheight"> 
                                  </span><br>
                                </p></TD>
                            </TR>
                          </TBODY>
                        </TABLE></td>
						<td align="right" valign="top" height="100%">
                      <? include("rightbar.php"); ?></td>
                      
                    </tr>
                    <tr > 
                      <td background="images/dotline.gif" height="1" colspan="3" valign="top"></td>
                    </tr>
                    <tr align="center" > 
                      <td  height="12" colspan="3" valign="top" class="tre12"></td>
                    </tr>
                    <tr align="center" > 
                      <td colspan="3"><? include("footer.php"); ?></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
