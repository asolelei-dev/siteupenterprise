	function UtilBeginScript()
    {
	return String.fromCharCode(60, 115, 99, 114, 105, 112, 116, 62);
    }

    function UtilEndScript()
    {
	return String.fromCharCode(60, 47, 115, 99, 114, 105, 112, 116, 62);
    }

function IDGenerator(nextID)
	{
		this.nextID = nextID;
		this.GenerateID = IDGeneratorGenerateID;
	}

	function IDGeneratorGenerateID()
	{
		return this.nextID++;
	}

	var BUTTON_IMAGE_PREFIX = "buttonImage";
	var BUTTON_DIV_PREFIX = "buttonDiv";
	var BUTTON_PAD1_PREFIX = "buttonPad1";
	var BUTTON_PAD2_PREFIX = "buttonPad2";
	var buttonMap = new Object();

	function Button
	(
		idGenerator,
		caption,
		action,
		text,
		image
	)
	{
		this.idGenerator = idGenerator;
		this.caption = caption;
		this.action = action;
		this.text = text;
		this.image = image;
		this.enabled = true;
		this.Instantiate = ButtonInstantiate;
		this.Enable = ButtonEnable;
	}

	function ButtonInstantiate()
	{
		this.id = this.idGenerator.GenerateID();
		buttonMap[this.id] = this;
		var html = "";
		html += '<div id="';
		html += BUTTON_DIV_PREFIX;
		html += this.id;
		html += '" class="ButtonNormal"';
		html += ' onselectstart="ButtonOnSelectStart()"';
		html += ' ondragstart="ButtonOnDragStart()"';
		html += ' onmousedown="ButtonOnMouseDown(this)"';
		html += ' onmouseup="ButtonOnMouseUp(this)"';
		html += ' onmouseout="ButtonOnMouseOut(this)"';
		html += ' onmouseover="ButtonOnMouseOver(this)"';
		html += ' onclick="ButtonOnClick(this)"';
		html += ' ondblclick="ButtonOnDblClick(this)"';
		html += ' onfocus="alert()"';
		html += '>';
		html += '<a href="#" style="cursor:hand" onmouseover="window.status=\'' + this.caption + '\';return true;" onmouseout="window.status=window.defaultStatus;return true;">';
		html += '<table cellpadding=0 cellspacing=0 border=0><tr><td><img id="';
		html += BUTTON_PAD1_PREFIX;
		html += this.id;
		html += '" width=2 height=2></td><td></td><td></td></tr><tr><td></td><td><table cellpadding=0 cellspacing=0 border=0><tr>';
		html += '<td>';
		html += '<img id="';
		html += BUTTON_IMAGE_PREFIX;
		html += this.id;
		html += '" src="';
		html += this.image;
		html += '" title="';
		html += this.caption;
		html += '">';
		html += '</td>';
		if (this.text != "") {
		    html += '<td>&nbsp;</td>';
		    html += '<td class=ButtonText>';
		    html += this.text;
		    html += '</td>';
		}
		html += '</tr></table></td><td></td></tr><tr><td></td><td></td><td><img id="';
		html += BUTTON_PAD2_PREFIX;
		html += this.id;
		html += '" width=2 height=2></td></tr></table>';
		html += '</a>';
		html += '</div>';
		document.write(html);
	}

	function ButtonEnable(enabled)
	{
		this.enabled = enabled;
		if (this.enabled)
		{
			document.all[BUTTON_DIV_PREFIX + this.id].className = "ButtonNormal";
		}
		else
		{
			document.all[BUTTON_DIV_PREFIX + this.id].className = "ButtonDisabled";
		}
	}

	function ButtonOnSelectStart()
	{
		window.event.returnValue = false;
	}

	function ButtonOnDragStart()
	{
		window.event.returnValue = false;
	}

	function ButtonOnMouseDown(element)
	{
		if (event.button == 1)
		{
			var id = element.id.substring(BUTTON_DIV_PREFIX.length, element.id.length);
			var button = buttonMap[id];
			if (button.enabled)
			{
				ButtonPushButton(id);
			}
		}
	}

	function ButtonOnMouseUp(element)
	{
		if (event.button == 1)
		{
			var id = element.id.substring(BUTTON_DIV_PREFIX.length, element.id.length);
			var button = buttonMap[id];
			if (button.enabled)
			{
				ButtonReleaseButton(id);
			}
		}
	}

	function ButtonOnMouseOut(element)
	{
		var id = element.id.substring(BUTTON_DIV_PREFIX.length, element.id.length);
		var button = buttonMap[id];
		if (button.enabled)
		{
			ButtonReleaseButton(id);
		}
	}

	function ButtonOnMouseOver(element)
	{
		var id = element.id.substring(BUTTON_DIV_PREFIX.length, element.id.length);
		var button = buttonMap[id];
		if (button.enabled)
		{
			ButtonReleaseButton(id);
			document.all[BUTTON_DIV_PREFIX + id].className = "ButtonMouseOver";
		}
	}

	function ButtonOnClick(element)
	{
		var id = element.id.substring(BUTTON_DIV_PREFIX.length, element.id.length);
		var button = buttonMap[id];
		if (button.enabled)
		{
			eval(button.action);
		}
	}

	function ButtonOnDblClick(element)
	{
		ButtonOnClick(element);
	}

	function ButtonPushButton(id)
	{
		document.all[BUTTON_PAD1_PREFIX + id].width = 3;
		document.all[BUTTON_PAD1_PREFIX + id].height = 3;
		document.all[BUTTON_PAD2_PREFIX + id].width = 1;
		document.all[BUTTON_PAD2_PREFIX + id].height = 1;
		document.all[BUTTON_DIV_PREFIX + id].className = "ButtonPressed";
	}

	function ButtonReleaseButton(id)
	{
		document.all[BUTTON_PAD1_PREFIX + id].width = 2;
		document.all[BUTTON_PAD1_PREFIX + id].height = 2;
		document.all[BUTTON_PAD2_PREFIX + id].width = 2;
		document.all[BUTTON_PAD2_PREFIX + id].height = 2;
		document.all[BUTTON_DIV_PREFIX + id].className = "ButtonNormal";
	}

	var IMAGE_CHOOSER_FRAME_PREFIX = "imageChooserFrame";
    var IMAGE_CHOOSER_CONTENT_PREFIX = "imageChooserContent";
    var IMAGE_CHOOSER_IMG_PREFIX = "imageChooserImg";
    var IMAGE_CHOOSER_ICON_PREFIX = "imageChooserIcon";
    var imageChooserMap = new Object();

    function ImageChooser
    (
	    idGenerator,
	    numRows,
	    numCols,
	    imageWidth,
	    imageHeight,
	    images,
	    callback
    )
    {
	    this.idGenerator = idGenerator;
	    this.numRows = numRows;
	    this.numCols = numCols;
	    this.imageWidth = imageWidth;
	    this.imageHeight = imageHeight;
	    this.images = images;
	    this.callback = callback;
	    this.Instantiate = ImageChooserInstantiate;
	    this.Show = ImageChooserShow;
	    this.Hide = ImageChooserHide;
	    this.IsShowing = ImageChooserIsShowing;
	    this.SetUserData = ImageChooserSetUserData;
    }

    function ImageChooserInstantiate()
    {
	    this.id = this.idGenerator.GenerateID();
	    imageChooserMap[this.id] = this;
	    var width = (this.imageWidth + 4) * this.numCols + 2;
	    var height = (this.imageHeight + 4) * this.numRows + 2;
	    document.write(
		'<iframe id="' + IMAGE_CHOOSER_FRAME_PREFIX + this.id + '" class="Selector" marginwidth=0 marginheight=0 frameborder=0 scrolling=no width=' + width + ' height=' + height + ' style="display:none"></iframe>'
	    );
	    ImageChooserInitContent(this.id);
    }

    function ImageChooserShow(x, y)
    {
	    var f = eval(IMAGE_CHOOSER_FRAME_PREFIX + this.id);
	    if (
		f.document.body.innerHTML == ""
	    ) {
		f.document.body.innerHTML = this.content;
	    }
	    f.document.body.style.border = "#737373 solid 1px";
	    d.all[IMAGE_CHOOSER_FRAME_PREFIX + this.id].style.left = x;
	    d.all[IMAGE_CHOOSER_FRAME_PREFIX + this.id].style.top = y;
	    d.all[IMAGE_CHOOSER_FRAME_PREFIX + this.id].style.display = "block";
    }

    function ImageChooserHide()
    {
	    d.all[IMAGE_CHOOSER_FRAME_PREFIX + this.id].style.display = "none";
    }

    function ImageChooserIsShowing()
    {
	    return d.all[IMAGE_CHOOSER_FRAME_PREFIX + this.id].style.display == "block";
    }

    function ImageChooserSetUserData(userData)
    {
	this.userData = userData;
    }

    function ImageChooserOnMouseOver(id)
    {
	    var f = eval(IMAGE_CHOOSER_FRAME_PREFIX + id);
	    if (f.event.srcElement.tagName == "IMG") {
		    var underscore = f.event.srcElement.id.indexOf("_");
		    if (underscore != -1) {
			    var id = f.event.srcElement.id.substring(IMAGE_CHOOSER_IMG_PREFIX.length, underscore);
			    var index = f.event.srcElement.id.substring(underscore + 1);
			    f[IMAGE_CHOOSER_ICON_PREFIX + id + "_" + index].style.borderColor = "black";
		    }
	    }
    }

    function ImageChooserOnMouseOut(id)
    {
	    var f = eval(IMAGE_CHOOSER_FRAME_PREFIX + id);
	    if (f.event.srcElement.tagName == "IMG") {
		    var underscore = f.event.srcElement.id.indexOf("_");
		    if (underscore != -1) {
			    var id = f.event.srcElement.id.substring(IMAGE_CHOOSER_IMG_PREFIX.length, underscore);
			    var index = f.event.srcElement.id.substring(underscore + 1);
			    f[IMAGE_CHOOSER_ICON_PREFIX + id + "_" + index].style.borderColor = "white";
		    }
	    }
    }

    function ImageChooserOnClick(id)
    {
	    var f = eval(IMAGE_CHOOSER_FRAME_PREFIX + id);
	    if (f.event.srcElement.tagName == "IMG") {
		    var underscore = f.event.srcElement.id.indexOf("_");
		    if (underscore != -1) {
			    var id = f.event.srcElement.id.substring(IMAGE_CHOOSER_IMG_PREFIX.length, underscore);
			    var imageChooser = imageChooserMap[id];
			    imageChooser.Hide();
			    var index = f.event.srcElement.id.substring(underscore + 1);
			    if (imageChooser.callback) {
				    imageChooser.callback(imageChooser.images[index], imageChooser.userData);
			    }
		    }
	    }
    }

    function ImageChooserInitContent(id)
    {
	imageChooser = imageChooserMap[id];
	imageChooser.content = "";
	imageChooser.content += '<table id="' + IMAGE_CHOOSER_CONTENT_PREFIX + imageChooser.id + '" cellpadding=1 cellspacing=0 border=0>';
	for (var i = 0; i < imageChooser.numRows; i++) {
		imageChooser.content += '<tr>';
		for (var j = 0; j < imageChooser.numCols; j++) {
			imageChooser.content += '<td>';
			var k = i * imageChooser.numCols + j;
			imageChooser.content += '<div id="' + IMAGE_CHOOSER_ICON_PREFIX + imageChooser.id + '_' + k + '" style="border:white solid 1px;cursor:hand">';
			imageChooser.content += '<img src="' + imageChooser.images[k] + '" id="' + IMAGE_CHOOSER_IMG_PREFIX + imageChooser.id + '_' + k + '" width=' + imageChooser.imageWidth + ' height=' + imageChooser.imageHeight + ' onmouseover="parent.ImageChooserOnMouseOver(' + imageChooser.id + ')" onmouseout="parent.ImageChooserOnMouseOut(' + imageChooser.id + ')" onclick="parent.ImageChooserOnClick(' + imageChooser.id + ')">';
			imageChooser.content += '</div>';
			imageChooser.content += '</td>';
		}
		imageChooser.content += '</tr>';
	}
	imageChooser.content += '</table>';
    }


	var EDITOR_COMPOSITION_PREFIX = "editorComposition";
	var EDITOR_TOOLBAR_PREFIX = "editorToolbar";
	var EDITOR_SMILEY_BUTTON_PREFIX = "editorSmileyButton";
	var EDITOR_IMAGE_CHOOSER_PREFIX = "editorImageChooser";
	var EDITOR_DEFAULT_FONT_FAMILY = "verdana";
	var EDITOR_DEFAULT_FONT_SIZE = "8pt";
	var EDITOR_DEFAULT_FONT_COLOR = "#000000";
	var EDITOR_FONT_PREFIX = "font";
	var EDITOR_SIZE_PREFIX = "size";
	var EDITOR_FORE_PREFIX = "fore";
	var EDITOR_BACK_PREFIX = "back";
	var EDITOR_ALIGN_PREFIX = "align";
	var EDITOR_LIST_PREFIX = "list";
	var editorMap = new Object();
	var editorIDGenerator = null;
	var editorActive = 0;

	function Editor(idGenerator)
	{
		this.idGenerator = idGenerator;
		this.textMode = false;
		this.backgroundColor = "";
		this.backgroundImage = "";
		this.foregroundColor = "";
		this.fontFamily = "";
		this.fontSize = "";
		this.stationery = false;
		this.stationeryWindow = null;
		this.instantiated = false;
		this.Instantiate = EditorInstantiate;
		this.GetText = EditorGetText;
		this.SetText = EditorSetText;
		this.GetHTML = EditorGetHTML;
		this.SetHTML = EditorSetHTML;
		this.Focus = EditorFocus;
		this.SetDomain = SetDomain;
		this.SetBackgroundColor = EditorSetBackgroundColor;
		this.GetBackgroundColor = EditorGetBackgroundColor;
		this.RemoveBackgroundColor = EditorRemoveBackgroundColor;
		this.SetBackgroundImage = EditorSetBackgroundImage;
		this.GetBackgroundImage = EditorGetBackgroundImage;
		this.RemoveBackgroundImage = EditorRemoveBackgroundImage;
		this.SetForegroundColor = EditorSetForegroundColor;
		this.GetForegroundColor = EditorGetForegroundColor;
		this.RemoveForegroundColor = EditorRemoveForegroundColor;
		this.SetFontFamily = EditorSetFontFamily;
		this.GetFontFamily = EditorGetFontFamily;
		this.RemoveFontFamily = EditorRemoveFontFamily;
		this.SetFontSize = EditorSetFontSize;
		this.GetFontSize = EditorGetFontSize;
		this.RemoveFontSize = EditorRemoveFontSize;
		this.SetStationery = EditorSetStationery;
		this.GetStationery = EditorGetStationery;
		this.ViewHTMLSource = EditorViewHTMLSource;
		this.Unload = EditorUnload;
	}

	function EditorInstantiate()
	{
		if (this.instantiated) {
			return;
		}
		this.id = this.idGenerator.GenerateID();
		editorMap[this.id] = this;
		editorIDGenerator = this.idGenerator;
		var html = "";
		html += "<table bgcolor=#E0E0E0 cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">";
		html += "<tr>";
		html += "<td id=\"" + EDITOR_TOOLBAR_PREFIX + this.id + "\" class=\"Toolbar\">";
		html += "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">";
		html += "<tr>";
<!-- cut, paste, copy -->
		html += "<td>";
		html += UtilBeginScript();
		html += "var cutButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"Cut\",";
		html += "\"EditorOnCut(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_cut_1.gif\"";
		html += ");";
		html += "cutButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
		html += "<td>";
		html += UtilBeginScript();
		html += "var copyButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"Copy\",";
		html += "\"EditorOnCopy(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_copy_1.gif\"";
		html += ");";
		html += "copyButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
		html += "<td>";
		html += UtilBeginScript();
		html += "var pasteButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"Paste\",";
		html += "\"EditorOnPaste(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_paste_1.gif\"";
		html += ");";
		html += "pasteButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
		html += "<td>";
		html += "<div class=\"Divider\"></div>";
		html += "</td>";
<!-- font -->
		html += "<td>";
		html += UtilBeginScript();
		html += "var fontButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"Font Face\",";
		html += "\"EditorOnFontDropDown(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_fontface_1.gif\"";
		html += ");";
		html += "fontButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
<!-- size -->
		html += "<td>";
		html += UtilBeginScript();
		html += "var sizeButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"Font Size\",";
		html += "\"EditorOnSizeDropDown(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_fontsize_1.gif\"";
		html += ");";
		html += "sizeButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
<!-- bold, italic, underline-->
		html += "<td>";
		html += "<div class=\"Divider\"></div>";
		html += "</td>";
		html += "<td>";
		html += UtilBeginScript();
		html += "var boldButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"Bold\",";
		html += "\"EditorOnBold(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_bold_1.gif\"";
		html += ");";
		html += "boldButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
		html += "<td>";
		html += UtilBeginScript();
		html += "var italicButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"Italic\",";
		html += "\"EditorOnItalic(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_italic_1.gif\"";
		html += ");";
		html += "italicButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
		html += "<td>";
		html += UtilBeginScript();
		html += "var underlineButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"Underline\",";
		html += "\"EditorOnUnderline(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_uline_1.gif\"";
		html += ");";
		html += "underlineButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
		html += "<td>";
		html += "<div class=\"Divider\"></div>";
		html += "</td>";
<!-- foreground text color -->
		html += "<td>";
		html += UtilBeginScript();
		html += "var foregroundTextColorButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"Text Color\",";
		html += "\"EditorOnForegroundTextColor(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_coltext_1.gif\"";
		html += ");";
		html += "foregroundTextColorButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
<!-- background text color -->
		html += "<td>";
		html += UtilBeginScript();
		html += "var backgroundTextColorButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"Highlight Color\",";
		html += "\"EditorOnBackgroundTextColor(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_colhilite_1.gif\"";
		html += ");";
		html += "backgroundTextColorButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
<!-- insert weblink -->
		html += "<td>";
		html += UtilBeginScript();
		html += "var createHyperlinkButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"Create Hyperlink\",";
		html += "\"EditorOnCreateHyperlink(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_link_1.gif\"";
		html += ");";
		html += "createHyperlinkButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
		html += "<td>";
		html += "<div class=\"Divider\"></div>";
		html += "</td>";
<!-- alignment -->
		html += "<td>";
		html += UtilBeginScript();
		html += "var alignmentButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"Align Text\",";
		html += "\"EditorOnAlignment(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_justpd_1.gif\"";
		html += ");";
		html += "alignmentButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
<!-- bulleted list -->
		html += "<td>";
		html += UtilBeginScript();
		html += "var bulletedListButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"List\",";
		html += "\"EditorOnList(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_listpd_1.gif\"";
		html += ");";
		html += "bulletedListButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
		html += "<td>";
		html += "<div class=\"Divider\"></div>";
		html += "</td>";
		html += "<td>";
		html += UtilBeginScript();
		html += "var decreaseIndentButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"Decrease Indent\",";
		html += "\"EditorOnDecreaseIndent(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_ileft_1.gif\"";
		html += ");";
		html += "decreaseIndentButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
		html += "<td>";
		html += UtilBeginScript();
		html += "var increaseIndentButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"Increase Indent\",";
		html += "\"EditorOnIncreaseIndent(" + this.id + ")\",";
		html += "\"\",";
		html += "\"images/tb_iright_1.gif\"";
		html += ");";
		html += "increaseIndentButton.Instantiate();";
		html += UtilEndScript();
		html += "</td>";
<!-- stationery -->
		if (this.stationery) {
		    html += "<td>";
		    html += "<div class=\"Divider\"></div>";
		    html += "</td>";
		    html += "<td>";
		    html += UtilBeginScript();
		    html += "var stationeryButton = new Button(";
		    html += "editorIDGenerator,";
		    html += "\"Apply Stationery\",";
		    html += "\"EditorOnStationery(" + this.id + ")\",";
		    html += "\"Stationery\",";
		    html += "\"images/tb_station_2.gif\"";
		    html += ");";
		    html += "stationeryButton.Instantiate();";
		    html += UtilEndScript();
		    html += "</td>";
		}
		html += "</tr>";
		html += "</table>";
		html += "</td>";
		html += "</tr>";
		html += "<tr>";
		html += "<td>";
		html += "<iframe id=\"" + EDITOR_COMPOSITION_PREFIX + this.id + "\" width=\"100%\" height=\"175\">";
		html += "</iframe>";
		html += "</td>";
		html += "</tr>";
		html += "</table>";
		html += UtilBeginScript();
		html += "var " + EDITOR_IMAGE_CHOOSER_PREFIX + this.id + " = new ImageChooser(";
		html += "editorIDGenerator,";
		html += "8, 5,";
		html += "18, 18,";
		html += "[";
		html += "],";
		html += "EditorOnEndInsertSmiley";
		html += ");";
		html += EDITOR_IMAGE_CHOOSER_PREFIX + this.id + ".SetUserData(" + this.id + ");";
		html += EDITOR_IMAGE_CHOOSER_PREFIX + this.id + ".Instantiate();";
		html += UtilEndScript();
		html +="<iframe id='" + EDITOR_FONT_PREFIX + this.id + "' class=Selector width=125 height=235 marginwidth=0 marginheight=0 frameborder=0 scrolling=no style='top:30px;left:86px;display:none'></iframe>";
		html += "<iframe id='" + EDITOR_SIZE_PREFIX + this.id + "' class=Selector width=145 height=256 marginwidth=0 marginheight=0 frameborder=0 scrolling=no style='top:30px;left:114px;display:none'></iframe>";
		html += "<iframe id='" + EDITOR_ALIGN_PREFIX + this.id + "' class=Selector width=102 height=110 marginwidth=0 marginheight=0 frameborder=0 scrolling=no style='top:30px;left:344px;display:none'></iframe>";
		html += "<iframe id='" + EDITOR_LIST_PREFIX + this.id + "' class=Selector width=121 height=60 marginwidth=0 marginheight=0 frameborder=0 scrolling=no style='top:30px;left:372px;display:none'></iframe>";
		html += "<iframe id='" + EDITOR_FORE_PREFIX + this.id + "' class=Selector src='' width=168 height=186 frameborder=0 scrolling=no style='top:30px;left:230px;display:none;'></iframe>";
		html += "<iframe id='" + EDITOR_BACK_PREFIX + this.id + "' class=Selector src='' width=168 height=186 frameborder=0 scrolling=no style='top:30px;left:258px;display:none;'></iframe>";
		document.write(html);
		html = '';
		html += '<body style="color=' + EDITOR_DEFAULT_FONT_COLOR + ';font-family:' + EDITOR_DEFAULT_FONT_FAMILY + ';font-size:' + EDITOR_DEFAULT_FONT_SIZE + '">';
//		html += '<div></div>';
		html += '</body>';
		eval(EDITOR_COMPOSITION_PREFIX + this.id).document.designMode = "on";
		eval(EDITOR_COMPOSITION_PREFIX + this.id).document.open();
		eval(EDITOR_COMPOSITION_PREFIX + this.id).document.write(html);
		eval(EDITOR_COMPOSITION_PREFIX + this.id).document.close();
		eval(EDITOR_COMPOSITION_PREFIX + this.id).document.onclick = new Function("EditorOnClick(" + this.id + ")");
		EditorInitDropDowns(this.id);

		editorIDGenerator = null;
		this.instantiated = true;

	}

	function EditorGetText()
	{
		return eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.innerText;
	}

	function EditorSetText(text)
	{
		text = text.replace(/\n/g, "<br>");
	//	text = "<div>" + text + "</div>";
		eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.innerHTML = text;
	}

	function EditorGetHTML()
	{
		if (this.textMode) {
			return eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.innerText;
		}
		var html =  eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.innerHTML;
		return html.toLowerCase() == "<div></div>" ? "" : html;
	}

	function EditorSetHTML(html)
	{
		if (this.textMode) {
			eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.innerText = html;
		}
		else {
	//		html = "<div>" + html + "</div>";
			eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.innerHTML = html;
		}
	}

	function EditorFocus()
	{
	    eval(EDITOR_COMPOSITION_PREFIX + this.id).focus();
	}

	function SetDomain(d)
	{
	    eval(EDITOR_COMPOSITION_PREFIX + this.id).document.domain = d;
	}

	function EditorOnCut(id)
	{
		EditorFormat(id, "cut");
	}

	function EditorOnCopy(id)
	{
		EditorFormat(id, "copy");
	}

	function EditorOnPaste(id)
	{
		EditorFormat(id, "paste");
	}

	function EditorOnBold(id)
	{
		EditorFormat(id, "bold");
	}

	function EditorOnItalic(id)
	{
		EditorFormat(id, "italic");
	}

	function EditorOnUnderline(id)
	{
		EditorFormat(id, "underline");
	}

	function EditorOnAlignLeft(id)
	{
		EditorFormat(id, "justifyleft");
	}

	function EditorOnCenter(id)
	{
		EditorFormat(id, "justifycenter");
	}

	function EditorOnAlignRight(id)
	{
		EditorFormat(id, "justifyright");
	}

	function EditorOnAlignJustify(id)
	{
		EditorFormat(id, "justifyjustify");
	}

	function EditorOnNumberedList(id)
	{
		EditorFormat(id, "insertOrderedList");
	}

	function EditorOnBulletedList(id)
	{
		EditorFormat(id, "insertUnorderedList");
	}

	function EditorOnDecreaseIndent(id)
	{
		EditorFormat(id, "outdent");
	}

	function EditorOnIncreaseIndent(id)
	{
		EditorFormat(id, "indent");
	}

	function EditorOnStationery(id)
	{
	    if (!EditorValidateMode(id)) {
		    return;
	    }
	    EditorHideAllDropDowns(id);
	    var editor = editorMap[id];
	    var width = 388;
	    var height = 324;
	    var left = (screen.availWidth - width) / 2;
	    var top = (screen.availHeight - height) / 2;
	    editor.stationeryWindow = window.open(
		"/ym/Stationery?YY=48490",
		"stationery",
		"left=" + left
		+
		",top=" + top
		+
		",width=" + width
		+
		",height=" + height
	    );
	    editor.stationeryWindow.focus();
	}

	function EditorOnCreateHyperlink(id)
	{
		if (!EditorValidateMode(id)) {
			return;
		}
		EditorHideAllDropDowns(id);
		var anchor = EditorGetElement("A", eval(EDITOR_COMPOSITION_PREFIX + id).document.selection.createRange().parentElement());
		var link = prompt("enter link location (eg. http://www.SiteMonitor.com):", anchor ? anchor.href : "http://");

		if (link && link != "http://") {

			if (eval(EDITOR_COMPOSITION_PREFIX + id).document.selection.type == "None") {
				var range = eval(EDITOR_COMPOSITION_PREFIX + id).document.selection.createRange();
				link = "<A HREF=" + link + "></A>";
				range.pasteHTML(link);
				range.select();
			}
			else {
				EditorFormat(id, "CreateLink", link);
			}
		}

	}

	function EditorOnStartInsertSmiley(id)
	{
		if (!EditorValidateMode(id)) {
			return;
		}
		if (eval(EDITOR_IMAGE_CHOOSER_PREFIX + id).IsShowing()) {
			eval(EDITOR_IMAGE_CHOOSER_PREFIX + id).Hide();
		}
		else {
			EditorHideAllDropDowns(id);
			var editor = editorMap[id];
			editor.selectionRange = eval(EDITOR_COMPOSITION_PREFIX + id).document.selection.createRange();
			eval(EDITOR_IMAGE_CHOOSER_PREFIX + id).Show(eval(EDITOR_SMILEY_BUTTON_PREFIX + id).offsetLeft+2, eval(EDITOR_TOOLBAR_PREFIX + id).offsetTop+10);

		}
	}

	function EditorOnEndInsertSmiley(image, id)
	{
	    if (!EditorValidateMode(id)) {
		return;
	    }
	    var imgTag = '<img src="' + image + '">';
	    var editor = editorMap[id];
	    var bodyRange = eval(EDITOR_COMPOSITION_PREFIX + id).document.body.createTextRange();
	    if (bodyRange.inRange(editor.selectionRange)) {
		editor.selectionRange.pasteHTML(imgTag);
		eval(EDITOR_COMPOSITION_PREFIX + id).focus();
	    }
	    else {
		eval(EDITOR_COMPOSITION_PREFIX + id).document.body.innerHTML += imgTag;
		editor.selectionRange.collapse(false);
		editor.selectionRange.select();
	    }
	}

	function EditorOnFont(id, select)
	{
		EditorFormat(id, "fontname", select);
	}

	function EditorOnSize(id, select)
	{
		EditorFormat(id, "fontsize", select);
	}

	function EditorOnFontDropDown(id)
	{
	    if (!EditorValidateMode(id)) {
		    return;
	    }
	    EditorToggleDropDown(id, EDITOR_FONT_PREFIX);
	}

	function EditorOnSizeDropDown(id)
	{
	    if (!EditorValidateMode(id)) {
		    return;
	    }
	    EditorToggleDropDown(id, EDITOR_SIZE_PREFIX);
	}

	function EditorOnForegroundTextColor(id)
	{
		if (!EditorValidateMode(id)) {
			return;
		}
		EditorToggleDropDown(id, EDITOR_FORE_PREFIX);
	}

	function EditorOnBackgroundTextColor(id)
	{
		if (!EditorValidateMode(id)) {
			return;
		}
		EditorToggleDropDown(id, EDITOR_BACK_PREFIX);
	}

	function EditorOnAlignment(id)
	{
	    if (!EditorValidateMode(id)) {
		    return;
	    }
	    EditorToggleDropDown(id, EDITOR_ALIGN_PREFIX);
	}

	function EditorOnList(id)
	{
	    if (!EditorValidateMode(id)) {
		    return;
	    }
	    EditorToggleDropDown(id, EDITOR_LIST_PREFIX);
	}

	function EditorOnViewHTMLSource(id, textMode)
	{
		var editor = editorMap[id];
		editor.ViewHTMLSource(textMode);
	}

	function EditorOnClick(id)
	{
		EditorHideAllDropDowns(id);
	}

	function EditorValidateMode(id)
	{
		var editor = editorMap[id];
		if (!editor.textMode) {
			return true;
		}
		alert("Please uncheck the \"View HTML Source\" checkbox to use the toolbars.");
		eval(EDITOR_COMPOSITION_PREFIX + id).focus();
		return false;
	}

	function EditorFormat(id, what, opt)
	{
		if (!EditorValidateMode(id)) {
			return;
		}
		if (opt == "removeFormat") {
			what = opt;
			opt = null;
		}
		EditorHideAllDropDowns(id);
		eval(EDITOR_COMPOSITION_PREFIX + id).focus();
		if (opt == null) {
			eval(EDITOR_COMPOSITION_PREFIX + id).document.execCommand(what);
		}
		else
		{
			eval(EDITOR_COMPOSITION_PREFIX + id).document.execCommand(what,"",opt);
		}
	}

	function EditorCleanHTML(id)
	{
		var fonts = eval(EDITOR_COMPOSITION_PREFIX + id).document.body.all.tags("FONT");
		for (var i = fonts.length - 1; i >= 0; i--) {
			var font = fonts[i];
			if (font.style.backgroundColor == "#ffffff") {
				font.outerHTML = font.innerHTML;
			}
		}
	}

	function EditorGetElement(tagName, start)
	{
		while (start && start.tagName != tagName) {
			start = start.parentElement;
		}
		return start;
	}

	function EditorSetBackgroundColor(color)
	{
	    eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.style.backgroundColor = color;
	    this.backgroundColor = color;
	}

	function EditorGetBackgroundColor()
	{
	    return this.backgroundColor;
	}

	function EditorRemoveBackgroundColor(color)
	{
	    eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.style.backgroundColor = "";
	    this.backgroundColor = "";
	}

	function EditorSetBackgroundImage(url)
	{
	    eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.style.backgroundImage = 'url(' + url + ')';
	    this.backgroundImage = url;
	}

	function EditorGetBackgroundImage()
	{
	    return this.backgroundImage;
	}

	function EditorRemoveBackgroundImage(url)
	{
	    eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.style.backgroundImage = "none";
	    this.backgroundImage = "";
	}

	function EditorSetForegroundColor(color)
	{
	    eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.style.color = color;
	    this.foregroundColor = color;
	}

	function EditorGetForegroundColor()
	{
	    return this.foregroundColor;
	}

	function EditorRemoveForegroundColor(color)
	{
	    eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.style.color = "";
	    this.foregroundColor = "";
	}

	function EditorSetFontFamily(fontFamily)
	{
	    if (fontFamily == "") {
		fontFamily = EDITOR_DEFAULT_FONT_FAMILY;
	    }
	    eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.style.fontFamily = fontFamily;
	    this.fontFamily = fontFamily;
	}

	function EditorGetFontFamily()
	{
	    return this.fontFamily;
	}

	function EditorRemoveFontFamily(font)
	{
	    eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.style.fontFamily = EDITOR_DEFAULT_FONT_FAMILY;
	    this.fontFamily = EDITOR_DEFAULT_FONT_FAMILY;
	}

	function EditorSetFontSize(fontSize)
	{
	    if (fontSize == "") {
		fontSize = EDITOR_DEFAULT_FONT_SIZE;
	    }
	    eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.style.fontSize = fontSize;
	    this.fontSize = fontSize;
	}

	function EditorGetFontSize()
	{
	    return this.fontSize;
	}

	function EditorRemoveFontSize(font)
	{
	    eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.style.fontSize = EDITOR_DEFAULT_FONT_SIZE;
	    this.fontSize = EDITOR_DEFAULT_FONT_SIZE;
	}

	function EditorSetStationery(on)
	{
	    this.stationery = on;
	}

	function EditorGetStationery()
	{
	    return this.stationery;
	}

	function EditorViewHTMLSource(textMode)
	{
	    this.textMode = textMode;
	    if (this.textMode) {
		    eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.innerText = eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.innerHTML;
	    }
	    else {
		    eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.innerHTML = eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.innerText;
	    }
	    eval(EDITOR_COMPOSITION_PREFIX + this.id).focus();
	}

	function EditorUnload()
	{
	    if (this.stationeryWindow != null) {
		this.stationeryWindow.close();
	    }
	}

	function EditorShowDropDown(id, prefix)
	{
	    editorActive = id;
	    EditorHideAllDropDowns(id);
	    EditorPrepareDropDownContents(id, prefix);
	    eval(prefix + id).document.body.style.border = "#737373 solid 1px";
	    d.all[prefix + id].style.display = "inline";
	}

	function EditorHideDropDown(id, prefix)
	{
	    d.all[prefix + id].style.display = "none";
	}

	function EditorToggleDropDown(id, prefix)
	{
	    if (d.all[prefix + id].style.display == "none") {
		EditorShowDropDown(id, prefix);
	    }
	    else {
		EditorHideDropDown(id, prefix);
	    }
	}

	function EditorHideAllDropDowns(id)
	{
	    var editor = editorMap[id];
	    for (var i in editor.dropDownMap) {
		EditorHideDropDown(id, i);
	    }
	    eval(EDITOR_IMAGE_CHOOSER_PREFIX + id).Hide();
	}

	function EditorPrepareDropDownContents(id, prefix)
	{
	    var editor = editorMap[id];
	    var dropDown = editor.dropDownMap[prefix];
	    if (dropDown.external) {
		if (d.all[prefix + id].src == "") {
		    d.all[prefix + id].src = dropDown.content;
		}
	    }
	    else {
		if (eval(prefix + id).document.body.innerHTML == "") {
		    eval(prefix + id).document.body.innerHTML = dropDown.content;
		}
	    }
	}

	function EditorInitDropDowns(id)
	{
	    var editor = editorMap[id];
	    editor.dropDownMap = new Object();
	    editor.dropDownMap[EDITOR_FONT_PREFIX] = {
		external: false,
		content: (
		    "<div onclick=\"parent.EditorHideDropDown(" + id + ", '" + EDITOR_FONT_PREFIX + "')\">"
		    +
		    "<table width=100% cellpadding=5 cellspacing=0 border=0>"
			+
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='arial' size=-1><a class=SelectItem href=\"javascript:parent.EditorOnFont(" + id + ",'arial');void(0);\" style=\"text-decoration:none;color:black;\">arial</a></font></td></tr>"
			+
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='Arial Narrow' size=-1><a class=SelectItem href=\"javascript:parent.EditorOnFont(" + id + ",'Arial Narrow');void(0);\" style=\"text-decoration:none;color:black;\">Arial Narrow</a></font></td></tr>"
		    +
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='Arial black' size=-1><a class=SelectItem href=\"javascript:parent.EditorOnFont(" + id + ",'Arial black');void(0);\" style=\"text-decoration:none;color:black;\">Arial Black</a></font></td></tr>"
		    +
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='verdana' size=-1><a class=SelectItem href=\"javascript:parent.EditorOnFont(" + id + ",'verdana');void(0);\" style=\"text-decoration:none;color:black;\">verdana</a></font></td></tr>"
		    +
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='sans-serif' size=-1><a class=SelectItem href=\"javascript:parent.EditorOnFont(" + id + ",'sans-serif');void(0);\" style=\"text-decoration:none;color:black;\">sans-serif</a></font></td></tr>"
		    +
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='comic sans ms' size=-1><a class=SelectItem href=\"javascript:parent.EditorOnFont(" + id + ",'comic sans ms');void(0);\" style=\"text-decoration:none;color:black;\">Comic Sans MS</a></font></td></tr>"
		    +
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='courier' size=-1><a class=SelectItem href=\"javascript:parent.EditorOnFont(" + id + ",'courier');void(0);\" style=\"text-decoration:none;color:black;\">Courier</a></font></td></tr>"
		    +
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='system' size=-1><a class=SelectItem href=\"javascript:parent.EditorOnFont(" + id + ",'system');void(0);\" style=\"text-decoration:none;color:black;\">System</a></font></td></tr>"
		    +
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='times new roman' size=-1><a class=SelectItem href=\"javascript:parent.EditorOnFont(" + id + ",'times new roman');void(0);\" style=\"text-decoration:none;color:black;\">Times New Roman</a></font></td></tr>"
		    
		    +
		    "</table>"
		    +
		    "</div>"
		)
	    };
	    editor.dropDownMap[EDITOR_SIZE_PREFIX] = {
		external: false,
		content: (
		    "<div onclick=\"parent.EditorHideDropDown(" + id + ", '" + EDITOR_SIZE_PREFIX + "')\">"
		    +
		    "<table width=100% cellpadding=5 cellspacing=0 border=0>"
		    +
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='verdana' size=1><a class=SelectItem href=\"javascript:parent.EditorOnSize(" + id + ",'1');void(0);\" style=\"text-decoration:none;color:black;\">Size 1</a></font></td></tr>"
		    +
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='verdana' size=2><a class=SelectItem href=\"javascript:parent.EditorOnSize(" + id + ",'2');void(0);\" style=\"text-decoration:none;color:black;\">Size 2</a></font></td></tr>"
		    +
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='verdana' size=3><a class=SelectItem href=\"javascript:parent.EditorOnSize(" + id + ",'3');void(0);\" style=\"text-decoration:none;color:black;\">Size 3</a></font></td></tr>"
		    +
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='verdana' size=4><a class=SelectItem href=\"javascript:parent.EditorOnSize(" + id + ",'4');void(0);\" style=\"text-decoration:none;color:black;\">Size 4</a></font></td></tr>"
		    +
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='verdana' size=5><a class=SelectItem href=\"javascript:parent.EditorOnSize(" + id + ",'5');void(0);\" style=\"text-decoration:none;color:black;\">Size 5</a></font></td></tr>"
		    +
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='verdana' size=6><a class=SelectItem href=\"javascript:parent.EditorOnSize(" + id + ",'6');void(0);\" style=\"text-decoration:none;color:black;\">Size 6</a></font></td></tr>"
		    +
		    "<tr><td onmouseover=\"this.style.backgroundColor='#dddddd';window.status='';return true;\" onMouseOut=\"this.style.backgroundColor='white';\"><font face='verdana' size=7><a class=SelectItem href=\"javascript:parent.EditorOnSize(" + id + ",'7');void(0);\" style=\"text-decoration:none;color:black;\">Size 7</a></font></td></tr>"
		    +
		    "</table>"
		    +
		    "</div>"
		)
	    };
	    editor.dropDownMap[EDITOR_ALIGN_PREFIX] = {
		external: false,
		content: (
		    "<div onclick=\"parent.EditorHideDropDown(" + id + ", '" + EDITOR_ALIGN_PREFIX + "')\">"
		    +
		    "<table width=100% border=0 cellspacing=0 cellpadding=2>"
		    +
		    "<tr><td><a href=javascript:parent.EditorOnAlignLeft(0);void(0); style=\"text-decoration:none;color:black;\"><img src=images/tb_justleft_1.gif style=\"border:1px solid white;\" onmouseover='this.style.border=\"1px solid black\";window.status=\"\";return true;' onmouseout='this.style.border=\"1px solid white\";\'></a></td><td style='font-family:Verdana;font-size:11px;'>Flush Left</td></tr>"
		    +
		    "<tr><td><a href=javascript:parent.EditorOnCenter(0);void(0); style=\"text-decoration:none;color:black;\"><img src=images/tb_justctr_1.gif style=\"border:1px solid white;\" onmouseover='this.style.border=\"1px solid  black\";window.status=\"\";return true;' onmouseout='this.style.border=\"1px solid white\";\'></td><td style='font-family:Verdana;font-size:11px;'>Centered</td></tr>"
		    +
		    "<tr><td><a href=javascript:parent.EditorOnAlignRight(0);void(0); style=\"text-decoration:none;color:black;\"><img src=images/tb_justright_1.gif style=\"border:1px solid white;\" onmouseover='this.style.border=\"1px solid black\";window.status=\"\";return true;' onmouseout='this.style.border=\"1px solid white\";\'></td><td style='font-family:Verdana;font-size:11px;'>Flush Right</td></tr>"
		    +
			"<tr><td><a href=javascript:parent.EditorOnAlignJustify(0);void(0); style=\"text-decoration:none;color:black;\"><img src=images/tb_justright_1.gif style=\"border:1px solid white;\" onmouseover='this.style.border=\"1px solid black\";window.status=\"\";return true;' onmouseout='this.style.border=\"1px solid white\";\'></td><td style='font-family:Verdana;font-size:11px;'>Justify</td></tr>"
			+
		    "</table>"
		    +
		    "</div>"
		)
	    };
	    editor.dropDownMap[EDITOR_LIST_PREFIX] = {
		external: false,
		content: (
		    "<div onclick=\"parent.EditorHideDropDown(" + id + ", '" + EDITOR_LIST_PREFIX + "')\">"
		    +
		    "<table width=100% border=0 cellspacing=0 cellpadding=2>"
		    +
		    "<tr><td><a href=javascript:parent.EditorOnNumberedList(0);void(0); style=\"text-decoration:none;color:black;\"><img src=images/tb_listnum_1.gif style=\"border:1px solid white;\" onmouseover='this.style.border=\"1px solid black\";window.status=\"\";return true;' onmouseout='this.style.border=\"1px solid white\";\'></a></td><td style='font-family:Verdana;font-size:11px;'>Numbered List</td></tr>"
		    +
		    "<tr><td><a href=javascript:parent.EditorOnBulletedList(0);void(0); style=\"text-decoration:none;color:black;\"><img src=images/tb_listblt_1.gif style=\"border:1px solid white;\" onmouseover='this.style.border=\"1px solid  black\";window.status=\"\";return true;' onmouseout='this.style.border=\"1px solid white\";\'></td><td style='font-family:Verdana;font-size:11px;'>Bulleted List</td></tr>"
		    +
		    "</table>"
		    +
		    "</div>"
		)
	    };
	    editor.dropDownMap[EDITOR_FORE_PREFIX] = {
		external: true,
		content: "forecolor.html"
	    };
	    editor.dropDownMap[EDITOR_BACK_PREFIX] = {
		external: true,
		content: "backcolor.html"
	    };
	}


	  var remote=null;
    var sigAttMap = [false];

    function OnFromAddrChange()
    {
	var i = document.frmadmmod.FromAddr.selectedIndex;
	if (i >= 0 && i < sigAttMap.length) {
	    document.all.SA.checked = sigAttMap[i];
	}
    }

    function OnMoneyChange()
    {
	var money = document.frmadmmod.Money.value;
	for (var i = 0; i < money.length; i++) {
	    if (money.charAt(i) != " ") {
		document.frmadmmod.SendMoney.checked = true;
		return;
	    }
	}
	document.frmadmmod.SendMoney.checked = false;
    }

    function rs(n,u,w,h,x) {
	args="width="+w+",height="+h+",resizable=yes,scrollbars=yes,status=0";
	remote=window.open(u,n,args);
	if (remote != null) {
	    if (remote.opener == null)
		remote.opener = self;
	}
	if (x == 1) { return remote; }
    }

    function setFormat()
    {
       if(document.frmadmmod.Format.checked) {
          document.frmadmmod.Format.value="html";
       } else {
          document.frmadmmod.Format.value="plain";
       }
    }


    function AttachFiles()
    {
	SetVals();
	document.frmadmmod.ATT.value = "1";
	document.frmadmmod.submit();
    }

    function AttachVideo()
    {
	SetVals();
	document.frmadmmod.VID.value = "1";
	document.frmadmmod.submit();
    }

    function RemoveAttachment(index)
    {
	SetVals();
	document.frmadmmod.action += "&UNATTACH=File" + index;
	document.frmadmmod.submit();
    }

    function RemoveVideo()
    {
	SetVals();
	document.frmadmmod.Video.value = "";
	document.frmadmmod.submit();
    }

    function Switch()
    {
	if (editor.GetText() != "" && editor.GetText() != editor.GetHTML()) {
	    var conf = confirm("This will convert your message into plain text.  All formatting will be lost.  Continue?");
	    if (!conf) {
		return;
	    }
	}
	document.frmadmmod.txtarea.value = editor.GetText();
	document.frmadmmod.action = document.frmadmmod.action + "&SWITCH=1";
	document.frmadmmod.submit();
    }

    function SetHtml() {
	editor.SetHTML(document.frmadmmod.PlainMsg.value);
    }

    function RestoreBackground()
    {
	editor.SetBackgroundImage(
	    ""
	);
    }

    function SetVals() {
	document.frmadmmod.txtarea.value = editor.GetHTML();
    }

    function SaveMsg()
    {
	document.frmadmmod.PlainMsg.value = editor.GetHTML();
    }

    function SetBackground(ref, desc, def, fg, ff, fs, solid, custom)
    {
	document.frmadmmod.BGRef.value = ref;
	document.frmadmmod.BGDesc.value = desc;
	document.frmadmmod.BGDef.value = def;
	document.frmadmmod.BGFg.value = fg;
	document.frmadmmod.BGFF.value = ff;
	document.frmadmmod.BGFS.value = fs;
	document.frmadmmod.BGSolid.value = solid ? "1" : "0";
	document.frmadmmod.BGCustom.value = custom ? "1" : "0";
	if (solid) {
	    editor.SetBackgroundColor(ref);
	}
	else {
	    editor.SetBackgroundImage(
		custom ?
		    ("/ym/Stationery?Data=" + ref + "&UrlExtras")
		    :
		    ref
	    );
	}
	editor.SetForegroundColor(fg);
	editor.SetFontFamily(ff);
	editor.SetFontSize(fs);
    }

    function RemoveBackground()
    {
	document.frmadmmod.BGRef.value = "";
	document.frmadmmod.BGSolid.value = "";
	document.frmadmmod.BGCustom.value = "";
	editor.RemoveBackgroundColor();
	editor.RemoveBackgroundImage();
	editor.RemoveForegroundColor();
	editor.RemoveFontFamily();
	editor.RemoveFontSize();
    }

    function GetBackground()
    {
	return {
	    ref: document.frmadmmod.BGRef.value,
	    desc: document.frmadmmod.BGDesc.value,
	    def: document.frmadmmod.BGDef.value,
	    fg: document.frmadmmod.BGFg.value,
	    ff: document.frmadmmod.BGFF.value,
	    fs: document.frmadmmod.BGFS.value,
	    solid: document.frmadmmod.BGSolid.value == "1",
	    custom: document.frmadmmod.BGCustom.value == "1"
	};
    }
