<?
	ob_start();
	session_start();
	error_reporting(E_ALL);

	if (!session_is_registered("euser"))
	{
		header("Location: login.php");
		exit;
	} 
	include("include/conn.inc");
	
	if(isset($_POST['act']))
	{
		if($_POST['act'] == "Delete")
		{
			$sql_DelMemberShip = "delete from tbl_membership where m_id = $_POST[mid]";
			$db->query($sql_DelMemberShip);
			
		}	
		elseif($_POST['act'] == "UpdateAll")	//if($_POST['act'] == "Delete")
		{
			$arrMSIDs = explode( ",", $_POST['msids']);
			
			for($i=0; $i< count($arrMSIDs); $i++)
			{
				$str = "cbRating" . $arrMSIDs[$i];
				echo "<br>", $sql_UpdateMS = "update tbl_membership set mem_rating = $_POST[$str] where m_id = $arrMSIDs[$i]";
				$db->query($sql_UpdateMS);
			}
			
			
			
		}	//elseif($_POST['act'] == "UpdateAll")
		
		header("Location: membershiplist.php");
		
	}	//if(isset($_POST['act']))

	//Initializing to EMPTY String
	$opt = array('Y' => 'Yes', 'N' => 'No');
	
	$strMSID = "";
	$strMSName = "";
	$strMSRating = "";
	$strMSMFee = "";
	$strMSYFee = "";
	$strNoOfMonitorsAllowed = "";
	$strMonitorsLevel = "";
	$strMultiMonitor = "";
	$strEMailAlerts = "";
	$strMonthlyReports = "";
	$strOnlineStats = "";
	$strControlPanel = "";
	$strHTTPCheck = "";
	$strSMTPCheck = "";
	$strPOP3Check = "";
	$strFTPCheck  ="";
	$strPublicStats = "";
	$strErrorDetails = "";
	$strPingMonitoring = "";
	$strCustomTimeZone = "";
	$strMultiAlerts = "";
	$strCustomPorts = "";
	$strSSLServer = "";
	$strAddnlServiceMonitors = "";
	$strSMSAlerts = "";
	$strStatus = "";
	$strAction = "";
	
	
	$sql_MSList = "select * from tbl_membership order by mem_rating";
	$ds_MSList  = $db->query($sql_MSList);
	$nMSCount  = mysqli_num_rows($ds_MSList);
	
	$i=1;

	while($rs_MSList = mysqli_fetch_array($ds_MSList))
	{
		$i++;
		if ($rs_MSList['status'] == "Y")
			$strTDBGColor = " bgcolor = '#E6FFEE'";
		else
			$strTDBGColor = " bgcolor = '#FFE1E1'";
		
		$strMSID .= $rs_MSList['m_id'] . ",";
		$strMSName .= "<td $strTDBGColor>$rs_MSList[mem_name]</td>";

		$strMSRating .= "<td $strTDBGColor>
							<select class='combobox' name='cbRating$rs_MSList[m_id]'>";
							
		for($j = 1; $j <= $nMSCount; $j++)
		{
			if($j == $rs_MSList['mem_rating'])
				$strMSRating .= "<option selected value='$j'>$j</option>";
			else
				$strMSRating .= "<option value='$j'>$j</option>";
		}
								
		$strMSRating .= "</select></td>";
		$strMSMFee .= "<td $strTDBGColor>\$$rs_MSList[amt_monthly]</td>";
		$strMSYFee .= "<td $strTDBGColor>\$$rs_MSList[amt_yearly]</td>";
		$strNoOfMonitorsAllowed .= "<td $strTDBGColor>$rs_MSList[total_sites]</td>";

		$sql_MIL = "select * from tbl_checkperiods where authority <= $rs_MSList[monitor_interval_level]";
		$ds_MIL  = $db->query($sql_MIL);
		
		$strMin = "";
		
		while($rs_MIL = mysqli_fetch_array($ds_MIL))
		{
			$strMin .= $rs_MIL['period'] . ", ";
		}

		$strMonitorsLevel .= "<td $strTDBGColor>" . substr($strMin, 0, strlen($strMin)-2) . " Mins. </td>";
		$strMultiMonitor .= "<td $strTDBGColor>" . $opt[$rs_MSList['multi_monitor']] . "</td>";
		$strEMailAlerts .= "<td $strTDBGColor>" . $opt[$rs_MSList['email_alerts']] . "</td>";
		$strMonthlyReports .= "<td $strTDBGColor>" . $opt[$rs_MSList['monthly_reports']] . "</td>";
		$strOnlineStats .= "<td $strTDBGColor>" . $opt[$rs_MSList['online_stats']] . "</td>";
		$strControlPanel .= "<td $strTDBGColor>" . $opt[$rs_MSList['control_panel']] . "</td>";
		$strHTTPCheck .= "<td $strTDBGColor>" . $opt[$rs_MSList['http_check']] . "</td>";
		$strSMTPCheck .= "<td $strTDBGColor>" . $opt[$rs_MSList['smtp_check']] . "</td>";
		$strPOP3Check .= "<td $strTDBGColor>" . $opt[$rs_MSList['pop3_check']] . "</td>";
		$strFTPCheck  .= "<td $strTDBGColor>" . $opt[$rs_MSList['ftp_check']] . "</td>";
		$strPublicStats .= "<td $strTDBGColor>" . $opt[$rs_MSList['public_stats']] . "</td>";
		$strErrorDetails .= "<td $strTDBGColor>" . $opt[$rs_MSList['error_details']] . "</td>";
		$strPingMonitoring .= "<td $strTDBGColor>" . $opt[$rs_MSList['ping_monitoring']] . "</td>";
		$strCustomTimeZone .= "<td $strTDBGColor>" . $opt[$rs_MSList['custom_time_zone']] . "</td>";
		$strMultiAlerts .= "<td $strTDBGColor>" . $opt[$rs_MSList['multi_alerts']] . "</td>";
		$strCustomPorts .= "<td $strTDBGColor>" . $opt[$rs_MSList['custom_ports']] . "</td>";
		$strSSLServer .= "<td $strTDBGColor>" . $opt[$rs_MSList['ssl_server']] . "</td>";
		$strAddnlServiceMonitors .= "<td $strTDBGColor>" . $opt[$rs_MSList['addnl_service_monitor']] . "</td>";
		$strSMSAlerts .= "<td $strTDBGColor>" . $opt[$rs_MSList['sms_alert']] . "</td>";
		
		if($rs_MSList['status'] == "Y")
			$strStatus .= "<td $strTDBGColor>Active</td>";
		else
			$strStatus .= "<td $strTDBGColor><strong><font color='ff0000'>InActive</font></strong></td>";
					
		$strAction .= "<td bgcolor='#efefef' align='center' style='border:1px solid #ff0ff0'><a href='editmembership.php?mid=$rs_MSList[m_id]'>Edit</a> / <a href='javascript:deleteMemberShip($rs_MSList[m_id])'>Delete</a></td>";
		
	
	}	//while($rs_MSList = mysqli_fetch_array($ds_MSList))
	$strMSID = substr($strMSID, 0, strlen($strMSID) - 1);
?>
<html>
<head>
<title>Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css1.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--

	function compareNum(a, b)
	{
		return a-b;
	}

	function onbtnUpdate_Click()
	{
		var frm = document.frmMSDet;
		var arrmsids = frm.msids.value.split(",");
		var arrMSRating = new Array(arrmsids.length);

		for(i=0; i < arrmsids.length; i++)
			arrMSRating[i] = document.getElementById("cbRating" + arrmsids[i]).value;

		arrMSRating.sort(compareNum);

		for(i=0; i< arrMSRating.length-1; i++)
		{
			if(arrMSRating[i] == arrMSRating[i+1])
			{
				alert("Select Different Ratings For Each of the Membership");
				document.getElementById("cbRating" + arrmsids[0]).focus();
				return false;
			}
		}
		
		frm.act.value = "UpdateAll";
		frm.submit();

	}

	function onbtnAddNew_Click()
	{
		var frm = document.frmMSDet;
		frm.action = "newmembership.php";
		frm.submit();
	}

	function deleteMemberShip(no)
	{
		if(confirm("Do You Want To Really Delete This MemberShip Package"))
		{
			var frm = document.frmMSDet;
			
			frm.act.value = "Delete";
			frm.mid.value = no;
			frm.action = "";
			frm.submit();
		}
	}


function submitform()
{
 document.form1.action.value = "update";
 form1.submit();
}
-->
</script>
</head>

<body>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="body">
  <tr> 
    <td width="42%" valign="top"><img src="../images/logo_247.gif"></td>
    <td width="39%" align="right" valign="bottom"><font color="#666666" size="5" face="Times New Roman, Times, serif"><strong>Admin Panel</strong></font></td>
    <td width="19%" valign="top">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="80%" valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="body">
              <td> <table border="0" cellspacing="0" cellpadding="1" class="body" width="100%">
                    <tr>
                      <td align="left" valign="middle" background="images/tile_back_small1.gif"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b> 
                        &nbsp;&nbsp;Manage MemberShips </b></font></td>
                      <td align="right" valign="middle" background="images/tile_back_small1.gif"><a href="index.php"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b>Main 
                        Menu</b></font></a>&nbsp;&nbsp;&nbsp</td>
                      <td height="25" align="center" valign="middle" background="images/tile_back_small1.gif"></td>
                    </tr>
                  </table></td>
              <tr> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td height="1" colspan="2" align="center" valign="middle">
			<form name="frmMSDet" method="post">
			        <table width="99%" border="0" cellpadding="2" cellspacing="0" class="body">
                      <tr> 
                        <TD height="19">Membership Name 
                          <input name="act" type="hidden" id="act2"> <input name="mid" type="hidden" id="mid">
                          <input name="msids" type="hidden" id="msids" value="<? echo $strMSID; ?>"></TD>
                        <? echo $strMSName; ?> </tr>
                      <tr>
                        <TD height="19">Membership Rating</TD>
						<? echo $strMSRating; ?>
                      </tr>
                      <tr> 
                        <TD height="19">Monthly Subscription Fee</TD>
                        <? echo $strMSMFee; ?> </tr>
                      <tr> 
                        <TD height="19">Yearly Subscription Fee</TD>
                        <? echo $strMSYFee; ?> </tr>
                      <tr> 
                        <TD height="19">Max No. of Monitors Allowed</TD>
                        <? echo $strNoOfMonitorsAllowed; ?> </tr>
                      <tr> 
                        <TD height="19">Allowed Monitor Intervals</TD>
                        <? echo $strMonitorsLevel; ?> </tr>
                      <tr> 
                        <TD height="19">Allowed Monitoring Locations </TD>
                        <? echo $strMultiMonitor; ?> </tr>
                      <tr> 
                        <TD height="19">Can Send Monthly Reports</TD>
                        <? echo $strEMailAlerts; ?> </tr>
                      <tr> 
                        <TD height="19">Can Send E-Mail Alerts</TD>
                        <? echo $strMonthlyReports; ?> </tr>
                      <tr> 
                        <TD height="19">Show Online Statistics</TD>
                        <? echo $strOnlineStats; ?> </tr>
                      <tr> 
                        <TD height="19">Access To Control Panel</TD>
                        <? echo $strControlPanel; ?> </tr>
                      <tr> 
                        <TD height="19">Can Check HTTP(80) Port</TD>
                        <? echo $strHTTPCheck; ?> </tr>
                      <tr> 
                        <TD height="19">Can Check SMTP(25) Port</TD>
                        <? echo $strSMTPCheck; ?> </tr>
                      <tr> 
                        <TD height="19">Can Check POP3(110) Port</TD>
                        <? echo $strPOP3Check; ?> </tr>
                      <tr> 
                        <TD height="19">Can Check FTP(21) Port</TD>
                        <? echo $strFTPCheck; ?> </tr>
                      <tr> 
                        <TD height="19">Can Check Custom Port</TD>
                        <? echo $strCustomPorts; ?> </tr>
                      <tr> 
                        <TD height="19">Can Check SSL Servers</TD>
                        <? echo $strSSLServer; ?> </tr>
                      <tr> 
                        <TD height="19">Can Provide Additional Statistics</TD>
                        <? echo $strPublicStats; ?> </tr>
                      <tr> 
                        <TD height="19">Can Store Error Details</TD>
                        <? echo $strErrorDetails; ?> </tr>
                      <tr> 
                        <TD height="19">Can Monitor Via Ping Utility</TD>
                        <? echo $strPingMonitoring; ?> </tr>
                      <tr> 
                        <TD height="19">Can Show Customer Time </TD>
                        <? echo $strCustomTimeZone; ?> </tr>
                      <tr> 
                        <TD height="19">Can Send Alerts To Multiple EMails</TD>
                        <? echo $strMultiAlerts; ?> </tr>
                      <tr> 
                        <TD height="19">Can Send SMS Alerts</TD>
                        <? echo $strSMSAlerts; ?> </tr>
                      <tr> 
                        <TD height="19">Allow Additional Service Monitors</TD>
                        <? echo $strAddnlServiceMonitors; ?> 
                        <? //echo $strSSLServer; //Omitted ?>
                      </tr>
                      <tr> 
                        <TD height="19" align="left">Status</TD>
                        <? echo $strStatus; ?> </tr>
                      <tr> 
                        <TD height="19" align="left">Actions</TD>
                        <? echo $strAction; ?> </tr>
                      <tr> 
                        <TD height="19" align="left">&nbsp;</TD>
                      </tr>
                      <tr> 
                        <TD colspan="<? echo $i; ?>" height="19" align="center"> 
                          <input name="btnAddNew" type="button" class="button" id="btnAddNew" value="Add New MemberShip Package" onClick="onbtnAddNew_Click();"> 
						  &nbsp;&nbsp;&nbsp;&nbsp;<input name="tbnUpdate" type="button" class="button" id="btnUpdate" onClick="onbtnUpdate_Click();" value="Update MemberShip Package">
                        </TD>
                      </tr>
                    </table>
		</form>
                </td>
              </tr>
            </table></td>
          <td width="17%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td> <? include_once("sidebar.php") ?> </td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
</body>
</html>
