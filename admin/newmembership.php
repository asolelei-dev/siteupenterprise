<?
	ob_start();
	session_start();
	error_reporting(E_ALL);

	if (!session_is_registered("euser"))
	{
		header("Location: login.php");
		exit;
	} 
	include("include/conn.inc");
	
	if(isset($_POST['act']))
	{
		
		if($_POST['act'] == "AddNew")
		{
			$sql_InsertMembership = "insert into tbl_membership (mem_name, total_sites, amt_monthly, amt_yearly, monitor_interval_level, multi_monitor, 
										email_alerts, monthly_reports, online_stats, control_panel, http_check, smtp_check, pop3_check, ftp_check, 
										public_stats, error_details, ping_monitoring, custom_time_zone, multi_alerts, custom_ports, addnl_service_monitor,
										status, ssl_server, sms_alert) values('$_POST[txtMName]', '$_POST[txtNoOfMonAllowed]', '$_POST[txtMSMFee]',
										'$_POST[txtMSYFee]', '$_POST[cbMILevel]', '$_POST[rbMultiMonitor]', '$_POST[rbEMailAlerts]',
										'$_POST[rbMonthlyReports]', '$_POST[rbOnlineStats]', '$_POST[rbControlPanel]', '$_POST[rbHTTPCheck]',
										'$_POST[rbSMTPCheck]', '$_POST[rbPOP3Check]', '$_POST[rbFTPCheck]', '$_POST[rbPublicStats]',
										'$_POST[rbErrorDetails]', '$_POST[rbPingMonitoring]', '$_POST[rbCustomTimeZone]', '$_POST[rbMultiAlerts]',
										'$_POST[rbCustomPorts]', '$_POST[rbSSL]', '$_POST[rbAddnlServiceMonitors]', '$_POST[rbStatus]',
										'$_POST[rbSMSAlert]')";
			$db->query($sql_InsertMembership);
		
			header("Location: membershiplist.php");
		}	//if($_POST['act'] == "AddNew")
	}	//if(isset($_POST['act']))
?>
<html>
<head>
<title>Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css1.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function submitform()
{
 document.form1.action.value = "update";
 form1.submit();
}
  </script>
</head>

<body>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="body">
  <tr> 
    <td width="42%" valign="top"><img src="../images/logo_247.gif"></td>
    <td width="39%" align="right" valign="bottom"><font color="#666666" size="5" face="Times New Roman, Times, serif"><strong>Admin Panel</strong></font></td>
    <td width="19%" valign="top">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="80%" valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="body">
              <td> <table border="0" cellspacing="0" cellpadding="1" class="body" width="100%">
                    <tr>
                      <td align="left" valign="middle" background="images/tile_back_small1.gif"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b>&nbsp;&nbsp;Manage MemberShips &raquo; Addnew </b></font></td>
                      <td align="right" valign="middle" background="images/tile_back_small1.gif"><a href="index.php"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b>Main 
                        Menu</b></font></a>&nbsp;&nbsp;&nbsp</td>
                      <td height="25" align="center" valign="middle" background="images/tile_back_small1.gif"></td>
                    </tr>
                  </table></td>
              <tr> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td height="1" colspan="2" align="center" valign="middle">
		<form name="frmMSDet" method="post">
			        <table width="80%" border="0" cellpadding="2" cellspacing="0" class="body">
                      <tr> 
                        <TD height="19">Membership Name</TD>
                        <TD valign="middle">&nbsp; <input name="txtMName" type="text" class="inputbox" id="txtMName" maxlength="35"> 
                          <input name="act" type="hidden" id="act" value="AddNew"></TD>
                      </tr>
                      <tr> 
                        <TD height="19">Monthly Subscription Fee</TD>
                        <TD valign="middle">&nbsp; <input name="txtMSMFee" type="text" class="inputbox" id="txtMSMFee" maxlength="10">
                          $</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Yearly Subscription Fee</TD>
                        <TD valign="middle">&nbsp; <input name="txtMSYFee" type="text" class="inputbox" id="txtMSYFee" maxlength="8">
                          $</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Max No. of Monitors Allowed</TD>
                        <TD valign="middle">&nbsp; <input name="txtNoOfMonAllowed" type="text" class="inputbox" id="txtNoOfMonAllowed" maxlength="10"></TD>
                      </tr>
                      <tr> 
                        <TD height="19">Allowed Monitor Intervels Upto</TD>
                        <TD valign="middle"> &nbsp; <select name="cbMILevel" class="combobox" id="cbMILevel">
                            <?
									$sql_MILevelList = "select * from tbl_checkperiods where status = 'Y'";
									$ds_MILevelList  = $db->query($sql_MILevelList);

									while($rs_MILList  = mysqli_fetch_array($ds_MILevelList))
									{
								?>
                            <option value="<? echo $rs_MILList['authority']; ?>"><? echo $rs_MILList['period']; ?>&nbsp;Mins.</option>
                            <?
									}	//while($rs_MILList  = mysqli_fetch_array($ds_MILevelList))
								?>
                          </select> </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Allowed Monitering Locations </TD>
                        <TD valign="middle"><input name="rbMultiMonitor" type="radio" value="Y" checked>
                          All&nbsp;&nbsp; <input type="radio" name="rbMultiMonitor" value="N">
                          Only Main Site</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Send Monthly Reports</TD>
                        <TD valign="middle"><input name="rbMonthlyReports" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbMonthlyReports" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Send E-Mail Alerts</TD>
                        <TD valign="middle"><input name="rbEMailAlerts" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbEMailAlerts" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Show Online Statistics</TD>
                        <TD valign="middle"><input name="rbOnlineStats" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbOnlineStats" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Access To Control Panel</TD>
                        <TD valign="middle"><input name="rbControlPanel" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbControlPanel" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Check HTTP(80) Port</TD>
                        <TD valign="middle"><input name="rbHTTPCheck" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbHTTPCheck" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Check SMTP(25) Port</TD>
                        <TD valign="middle"><input name="rbSMTPCheck" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbSMTPCheck" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Check POP3(110) Port</TD>
                        <TD valign="middle"><input name="rbPOP3Check" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbPOP3Check" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Check FTP(21) Port</TD>
                        <TD valign="middle"><input name="rbFTPCheck" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbFTPCheck" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Check Custom Port</TD>
                        <TD valign="middle"><input name="rbCustomPorts" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbCustomPorts" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Check SSL Servers</TD>
                        <TD valign="middle"><input name="rbSSL" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbSSL" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Provide Additional Statistics</TD>
                        <TD valign="middle"><input name="rbPublicStats" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbPublicStats" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Store Error Details</TD>
                        <TD valign="middle"><input name="rbErrorDetails" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbErrorDetails" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Monitor Via Ping Utility</TD>
                        <TD valign="middle"><input name="rbPingMonitoring" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbPingMonitoring" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Show Customer Time </TD>
                        <TD valign="middle"><input name="rbCustomTimeZone" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbCustomTimeZone" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Send Alerts To Multiple EMails</TD>
                        <TD valign="middle"><input name="rbMultiAlerts" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbMultiAlerts" value="N">
                          No</TD>
                      </tr>
                      <tr>
                        <TD height="19">Can Send SMS Alerts</TD>
                        <TD valign="middle"><input name="rbSMSAlert" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbSMSAlert" value="N">
                          No</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Allow Additional Service Monitors</TD>
                        <TD valign="middle"> <input name="rbAddnlServiceMonitors" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbAddnlServiceMonitors" value="N">
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19" align="left">Status</TD>
                        <TD valign="middle"> <input name="rbStatus" type="radio" value="Y" checked>
                          Yes 
                          <input type="radio" name="rbStatus" value="N">
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19" align="right">&nbsp;</TD>
                        <TD valign="middle">&nbsp;</TD>
                      </tr>
                      <tr> 
                        <TD height="19" align="right"><input name="btnAddNew" type="submit" class="button" id="btnAddNew" value="AddNew"> 
                          &nbsp;&nbsp;</TD>
                        <TD valign="middle">&nbsp;&nbsp; <input name="btnCancel" type="button" class="button" id="btnCancel" value="Cancel" onclick="document.location.replace('membershiplist.php');"></TD>
                      </tr>
                    </table>
		</form>
                </td>
              </tr>
            </table></td>
          <td width="17%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td> <? include_once("sidebar.php") ?> </td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
</body>
</html>
