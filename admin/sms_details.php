<?
	session_start();
	error_reporting(0);

	if (!session_is_registered("euser"))
	{
		header("Location:login.php");
		exit;
	} 
	include("include/conn.inc");
	
	
	if(isset($_POST['act']))
	{
		if($_POST['act'] == "Update")
		{
			$sql_UpdateCSet = "update tbl_commonsettings set sms_url = '$_POST[txtsmsurl]', sms_user = '$_POST[txtsmsuser]', sms_pass = '$_POST[txtsmspass]', 
				sms_message = '$_POST[txtsmsmessage]', sms_phone = '$_POST[txtsmsphone]', sms_down = '$_POST[txtsmsdown]' , sms_up = '$_POST[txtsmsup]' , sms_userid = '$_POST[txtuserid]' , sms_password = '$_POST[txtpassword]'  ";
			$db->query($sql_UpdateCSet);
			
			header("Location: index.php");
		
		}	//if($_POST['act'] == "Update")
	
	}	//if(isset($_POST['act']))
	
	$res_settings = mysqli_fetch_array($db->query("select * from tbl_commonsettings"));
	
	
	
?>
<html>
<head>
<title>Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css1.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

	function cancel()
	{
		document.frmCSet.action="index.php";
		document.frmCSet.submit();
	}
	
	function checktext(txtsms)
	{
		if(txtsms.value.length < 140)
			return true;
		else
			return false;
		
	}
	


</script>
</head>

<body>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="body">
  <tr> 
    <td width="42%" valign="top"><img src="../images/logo_247.gif"></td>
    <td width="39%" align="right" valign="bottom"><font color="#666666" size="5" face="Times New Roman, Times, serif"><strong>Admin Panel</strong></font></td>
    <td width="19%" valign="top">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="80%" valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="body">
              <td> <table border="0" cellspacing="0" cellpadding="1" class="body" width="100%">
                    <tr>
                      <td align="left" valign="middle" background="images/tile_back_small1.gif"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b> 
                        &nbsp;Manage SMS Settings </b></font></td>
                      <td align="right" valign="middle" background="images/tile_back_small1.gif"><a href="index.php"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b>Main 
                        Menu</b></font></a>&nbsp;&nbsp;&nbsp</td>
                      <td height="25" align="center" valign="middle" background="images/tile_back_small1.gif"></td>
                    </tr>
                  </table></td>
              <tr> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td height="1" colspan="2">
					<form name="frmCSet" method="post">
					  
                    <table width="100%" border="0" cellpadding="1" cellspacing="1" class="body">
                      <tr> 
                        <td width="25%" height="25" align="right">&nbsp; <input type="hidden" name="act" value="Update"></td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2"  height="25" align="center" class="box1"><strong>SMS 
                          URL and Fields Details</strong></td>
                      </tr>
                      <tr> 
                        <td align="right">SMS URL&nbsp;:&nbsp;</td>
                        <td> <input type="text" name="txtsmsurl" value="<? echo $res_settings['sms_url']; ?>" maxlength="200" size="75"></td>
                      </tr>
                      <tr> 
                        <td align="right">UserName Field&nbsp;:&nbsp;</td>
                        <td> <input type="text" name="txtsmsuser" value="<? echo $res_settings['sms_user']; ?>" maxlength="100" size="30"></td>
                      </tr>
                      <tr> 
                        <td align="right">Password Field&nbsp;:&nbsp;</td>
                        <td> <input type="text" name="txtsmspass" value="<? echo $res_settings['sms_pass']; ?>" maxlength="100" size="30"></td>
                      </tr>
                      <tr> 
                        <td align="right">Message Field&nbsp;:&nbsp;</td>
                        <td> <input type="text" name="txtsmsmessage" value="<? echo $res_settings['sms_message']; ?>" maxlength="100" size="30"></td>
                      </tr>
                      <tr> 
                        <td align="right">Mobile Field&nbsp;:&nbsp;</td>
                        <td> <input type="text" name="txtsmsphone" value="<? echo $res_settings['sms_phone']; ?>" maxlength="100" size="30"></td>
                      </tr>
                      <tr> 
                        <td colspan="2" height="10"></td>
                      </tr>
                      <tr> 
                        <td colspan="2"  height="25" align="center" class="box1"><strong>Other 
                          SMS Details</strong></td>
                      </tr>
                      <tr> 
                        <td align="right">Login ID&nbsp;:&nbsp;</td>
                        <td> <input type="text" name="txtuserid" value="<? echo $res_settings['sms_userid']; ?>" maxlength="50"></td>
                      </tr>
                      <tr> 
                        <td align="right">Password&nbsp;:&nbsp;</td>
                        <td> <input type="text" name="txtpassword" value="<? echo $res_settings['sms_password']; ?>" maxlength="50"></td>
                      </tr>
                      <tr align="center"> 
                        <td align="right">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td align="center" height="20" colspan="2" class="body"><strong><font color="#0000FF">Use 
                          following data fields without spacing for more information 
                          </font></strong></td>
                      </tr>
                      <tr align="center"> 
                        <td colspan="2"> <table width="80%" class="body">
                            <tr> 
                              <td align="right"><strong>Site Name </strong>&nbsp;&nbsp; 
                                -- &nbsp;</td>
                              <td align="left">&nbsp;%%SiteName%%</td>
                              <td align="right"><strong>User Name </strong>&nbsp;&nbsp; 
                                -- &nbsp;</td>
                              <td align="left">&nbsp;%%UserName%%</td>
                            </tr>
                            <tr> 
                              <td align="right"><strong>Date</strong>&nbsp;&nbsp; 
                                -- &nbsp;</td>
                              <td align="left">&nbsp;%%Date%%</td>
                              <td align="right"><strong>Time&nbsp;</strong>&nbsp; 
                                -- &nbsp;</td>
                              <td align="left">&nbsp;%%Time%%</td>
                            </tr>
                          </table></td>
                      </tr>
                      <tr> 
                        <td colspan="2"  height="25" align="center" class="box1"><strong>SMS 
                          Settings for Down Time</strong></td>
                      </tr>
                      <tr> 
                        <td colspan="2" height="10"></td>
                      </tr>
                      <tr> 
                        <td align="right">SMS Sending Message&nbsp;:&nbsp;</td>
                        <td> <textarea name="txtsmsdown" cols="45" rows="5" onKeyPress="javascript: return checktext(this);" onKeyUp="this.form.downcount.value = this.value.length;"><? echo $res_settings['sms_down']; ?></textarea></td>
                      </tr>
                      <tr align="center"> 
                        <td align="right">&nbsp;</td>
                        <td align="left" class="body"><font color="#FF0000"> 
                          <input type="text" name="downcount" size="4" readonly="" value="<? echo strlen($res_settings['sms_down']); ?>">
                          &nbsp;<strong>Maximam 160 (including data fields) characters 
                          should be add to SMS.</strong></font></td>
                      </tr>
                      <tr align="center"> 
                        <td align="right">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2" height="25" align="center" class="box1"><strong>SMS 
                          Settings for Up Time</strong></td>
                      </tr>
                      <tr> 
                        <td colspan="2" height="10"></td>
                      </tr>
                      <tr> 
                        <td align="right">SMS Sending Message&nbsp;:&nbsp;</td>
                        <td> <textarea name="txtsmsup" cols="45" rows="5" onKeyPress="javascript: return checktext(this);" onKeyUp="this.form.upcount.value = this.value.length;"><? echo $res_settings['sms_up']; ?></textarea></td>
                      </tr>
                      <tr align="center"> 
                        <td align="right">&nbsp;</td>
                        <td align="left" class="body"><font color="#FF0000"> 
                          <input type="text" name="upcount" size="4" readonly="" value="<? echo strlen($res_settings['sms_up']); ?>">
                          &nbsp; <strong>Maximam 160 (including data fields) characters 
                          should be add to SMS.</strong></font></td>
                      </tr>
                      <tr align="center"> 
                        <td align="right">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                      </tr>
                      <tr align="center"> 
                        <td colspan="2" align="center"><input name="btnUpdate" type="submit" class="button" id="btnUpdate" value="Save Settings"> 
                          &nbsp;&nbsp; &nbsp;&nbsp; <input name="BtnCancel" type="button" class="button" id="BtnCancel" value="Cancel" onClick="cancel();"></td>
                      </tr>
                      <tr> 
                        <td align="right">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
				  </form>
                </td>
              </tr>
            </table></td>
          <td width="17%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td> <? include_once("sidebar.php") ?> </td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
</body>
</html>
