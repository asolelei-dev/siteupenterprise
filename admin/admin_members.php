<?
	session_start();
//	error_reporting(0);
	include("pager.php");
	if (!session_is_registered("euser"))
	{
		header("Location:login.php");
		exit;
	} 
	include("include/conn.inc");
	
	if(isset($_POST['act']))
	{
		
		if($_POST['act'] == "delete")
		{
			$db->query("delete from tbl_users where userid=$_POST[txtuserid]");
			$db->query("delete from tbl_services where userid=$_POST[txtuserid]");
						
		} // if($_POST['act'] == "delete")
	
	//header("Location: admin_members.php");
	
	
	}	//if(isset($_POST['act']))
	
	
	
?>
<html>
<head>
<title>Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css1.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

  function del(userid)
  {
   if(confirm("Are You sure to delete the selected Title?")){
    document.form1.act.value = "delete";
	document.form1.txtuserid.value = userid;
	document.form1.action = "admin_members.php";
    form1.submit();
    }	
  }
  function detailsfun(userid)
  {
  	 document.form1.txtuserid.value = userid;
	 document.form1.action = "members_details.php";
    form1.submit();
  }
  </script>
</head>

<body>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="body">
  <tr> 
    <td width="42%" valign="top"><img src="../images/logo_247.gif"></td>
    <td width="39%" align="right" valign="bottom"><font color="#666666" size="5" face="Times New Roman, Times, serif"><strong>Admin Panel</strong></font></td>
    <td width="19%" valign="top">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="80%" valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="body">
              <td> <table border="0" cellspacing="0" cellpadding="1" class="body" width="100%">
                    <tr>
                      <td align="left" valign="middle" background="images/tile_back_small1.gif"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b> 
                        &nbsp;Manage User Settings</b></font></td>
                      <td align="right" valign="middle" background="images/tile_back_small1.gif"><a href="index.php"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b>Main 
                        Menu</b></font></a>&nbsp;&nbsp;&nbsp</td>
                      <td height="25" align="center" valign="middle" background="images/tile_back_small1.gif"></td>
                    </tr>
                  </table></td>
              <tr> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td height="1" colspan="2">
	<form name="form1" action="" method="post">
	 <input type="hidden" name="txtuserid" value="">
	 <input type="hidden" name="act" value="">
	 <table width="100%" border="0" cellpadding="2" cellspacing="0" class="body">
          
          <tr> 
            <td width="58%">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <? 
	   /* Instantiate class */ 													
				$p = new Pager;
   	   /* Show how many results per page? */ 
				$limit = 15; 
 	  /* Find the start depending on $_GET['page'] (declared if it's null) */ 
		 	$start = $p->findStart($limit); 
	  /* Find the number of rows returned from a query */ 
             $count = mysqli_num_rows($db->query("select * from tbl_users"));
 		     $pages = $p->findpages($count, $limit);
		
        $query1="select * from tbl_users order by userid limit ".$start.",".$limit;
        $result=$db->query($query1); 
		?>
          <tr> 
            <td width="100%" colspan="2" align="left" class="body"> <table border=1 width='80%' class='body' cellpadding=2 cellspacing=0 align='center'>
                <th class='t_header'>Sno.</th>
				<th class='t_header'>Name</th>
                <th class='t_header'>user ID</th>
                <th class='t_header'>Joining Date</th>
               <!-- <th class='t_header'>PayPal Status</th> -->
                
                <th class='t_header'>Action</th>
                <? $i=$start+1;
	   if(mysqli_num_rows($result) > 0) {	
	  while($r=mysqli_fetch_array($result))
      {
   	      ?>
                <tr> 
                  <td align='center'><? echo $i ; ?></td>
				  <td align='left'>&nbsp;&nbsp;<? echo $r['name']?></a>&nbsp; 
                   </td>
                  <td align='left'>&nbsp;&nbsp;<? echo $r['emailid']?>&nbsp; 
                   </td>
                  <td align='center'><? echo date("m/d/Y", strtotime($r['joindate']))?></td>
                  <td align='center'><a href=javascript:del('<? echo $r['userid'] ?>')>Delete</a>
				  /<a href='javascript:detailsfun(<? echo $r['userid'] ?>)' > Details </a></td>
                </tr>
                <? $i++;
	   } }else {  ?>
                <tr> 
                  <td align='center' colspan=6>No Users Found</td>
                </tr>
                <? } ?>
              </table></td>
          </tr>
          <tr> 
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr> 
            <td align="center" colspan="4"><font face="verdana" size="2" color="#B92845"><strong> 
              <? 	if ($pages>1) {
			/* Now get the page list and echo it */ 
			$pagelist = $p->pageList($_GET['page'], $pages); 
			//echo $pagelist; 
			/* a simple "Previous | Next" listing  */ 
			$next_prev = $p->nextPrev($_GET['page'], $pages); 
	        echo("<tr><td WIDTH='80%' colspan=4 align=center ><TABLE WIDTH='98%' BORDER=0 CELLSPACING=0><TR><TD align=left><font face=verdana size=1 color=#7c7c81>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pages</font>&nbsp;".$pagelist . "</td><td align=right>".$next_prev); 
			echo("</TD></TR></TABLE></td></tr><tr><td colspan=4></td></tr>");
				}				
		   ?>
              </strong></font></td>
          </tr>
          <!-- <tr>
	   <td valign="bottom" align="center" ><input type="submit" name="Addnew" value="Addnew" class="button"></td></tr> -->
        </table>
	 </form>
                </td>
              </tr>
            </table></td>
          <td width="17%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td> <? include_once("sidebar.php") ?> </td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
</body>
</html>
