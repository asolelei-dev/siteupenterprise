<?
	ob_start();
	session_start();
	error_reporting(E_ALL);

	if (!session_is_registered("euser"))
	{
		header("Location: login.php");
		exit;
	} 
	include("include/conn.inc");
	
	if(isset($_GET['mid']))
	{
		$sql_MSDet = "select * from tbl_membership where m_id = $_GET[mid]";
		$ds_MSDet  = $db->query($sql_MSDet);
		$rs_MSD  = mysqli_fetch_array($ds_MSDet);
		
		$MIL = $rs_MSD['monitor_interval_level'];
	}
	
	if(isset($_POST['act']))
	{
		if($_POST['act'] == "Update")
		{
			//echo "<br>HFMIL", $_POST['hfMIL'];
			//echo "<br>MILevel : ", $_POST['cbMILevel'];
			
			if($_POST['hfMIL'] != $_POST['cbMILevel'])
			{
				$sql_SList = "select service_id, service_name, host_name, s.userid from tbl_services s, tbl_membership m, tbl_users u 
										where s.userid = u.userid and u.memtype = m.m_id and s.monitor_interval_level = $_POST[hfMIL] and m.m_id = $_POST[mid]";
				$ds_SList  = $db->query($sql_SList);
				
				if(mysqli_num_rows($ds_SList) > 0)
				{
					while($rs_SList = mysqli_fetch_array($ds_SList))
					{
						$sql_UServ = "update tbl_services set monitor_interval_level = $_POST[cbMILevel] where service_id = $rs_SList[service_id]";
						$db->query($sql_UServ);
					}
				}
			}
			
			
			$sql_UpdateMembership = "update tbl_membership set mem_name = '$_POST[txtMName]', total_sites = '$_POST[txtNoOfMonAllowed]',
										amt_monthly = '$_POST[txtMSMFee]', amt_yearly = '$_POST[txtMSYFee]', monitor_interval_level = '$_POST[cbMILevel]',
										multi_monitor = '$_POST[rbMultiMonitor]', email_alerts = '$_POST[rbEMailAlerts]',
										monthly_reports = '$_POST[rbMonthlyReports]', online_stats = '$_POST[rbOnlineStats]', 
										control_panel = '$_POST[rbControlPanel]', http_check = '$_POST[rbHTTPCheck]',
										smtp_check = '$_POST[rbSMTPCheck]', pop3_check = '$_POST[rbPOP3Check]', 
										ftp_check = '$_POST[rbFTPCheck]', public_stats = '$_POST[rbPublicStats]',
										error_details = '$_POST[rbErrorDetails]', ping_monitoring = '$_POST[rbPingMonitoring]',
										custom_time_zone = '$_POST[rbCustomTimeZone]', multi_alerts = '$_POST[rbMultiAlerts]',
										custom_ports = '$_POST[rbCustomPorts]', addnl_service_monitor = '$_POST[rbAddnlServiceMonitors]',
										status = '$_POST[rbStatus]', ssl_server = '$_POST[rbSSL]', sms_alert='$_POST[rbSMSAlerts]' where m_id = $_POST[mid]";
			$db->query($sql_UpdateMembership);
			
			

		}	//if($_POST['act'] == "Update")
		
		header("Location: membershiplist.php");
		
	}	//if(isset($_POST['act']))
?>
<html>
<head>
<title>Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css1.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function submitform()
{
 document.form1.action.value = "update";
 form1.submit();
}
  </script>
</head>

<body>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="body">
  <tr> 
    <td width="42%" valign="top"><img src="../images/logo_247.gif"></td>
    <td width="39%" align="right" valign="bottom"><font color="#666666" size="5" face="Times New Roman, Times, serif"><strong>Admin Panel</strong></font></td>
    <td width="19%" valign="top">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="80%" valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="body">
              <td> <table border="0" cellspacing="0" cellpadding="1" class="body" width="100%">
                    <tr>
                      <td align="left" valign="middle" background="images/tile_back_small1.gif"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b>&nbsp;&nbsp;Manage 
                        MemberShips &raquo; Edit </b></font></td>
                      <td align="right" valign="middle" background="images/tile_back_small1.gif"><a href="index.php"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b>Main 
                        Menu</b></font></a>&nbsp;&nbsp;&nbsp</td>
                      <td height="25" align="center" valign="middle" background="images/tile_back_small1.gif"></td>
                    </tr>
                  </table></td>
              <tr> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td height="1" colspan="2" align="center" valign="middle">
		<form name="frmMSDet" method="post">
			        <table width="80%" border="0" cellpadding="2" cellspacing="0" class="body">
                      <tr> 
                        <TD height="19">Membership Name</TD>
                        <TD valign="middle"> &nbsp; <input name="txtMName" type="text" class="inputbox" id="txtMName" maxlength="35" value="<? echo $rs_MSD['mem_name'] ?>"> 
                          <input name="act" type="hidden" id="act" value="Update"> 
                          <input name="mid" type="hidden" id="mid" value="<? echo $_REQUEST['mid']; ?>">
                          <input name="hfMIL" type="hidden" id="hfMIL" value="<? echo $rs_MSD['monitor_interval_level'];?>"> </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Monthly Subscription Fee</TD>
                        <TD valign="middle"> &nbsp; <input name="txtMSMFee" type="text" class="inputbox" id="txtMSMFee" maxlength="10" value="<? echo $rs_MSD['amt_monthly']; ?>">
                          $ </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Yearly Subscription Fee</TD>
                        <TD valign="middle"> &nbsp; <input name="txtMSYFee" type="text" class="inputbox" id="txtMSYFee" maxlength="8" value="<? echo $rs_MSD['amt_yearly']; ?>">
                          $ </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Max No. of Monitors Allowed</TD>
                        <TD valign="middle"> &nbsp; <input name="txtNoOfMonAllowed" type="text" class="inputbox" id="txtNoOfMonAllowed" maxlength="10" value="<? echo $rs_MSD['total_sites']; ?>"> 
                        </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Allowed Monitor Intervels Upto</TD>
                        <TD valign="middle"> &nbsp; <select name="cbMILevel" class="combobox" id="cbMILevel">
                            <?
									$sql_MILevelList = "select * from tbl_checkperiods where status = 'Y' order by authority, period desc";
									$ds_MILevelList  = $db->query($sql_MILevelList);

									while($rs_MILList  = mysqli_fetch_array($ds_MILevelList))
									{
										if($rs_MILList['authority'] == $rs_MSD['monitor_interval_level'])
										{
								?>
                            <option selected value="<? echo $rs_MILList['authority']; ?>"><? echo $rs_MILList['period']; ?>&nbsp;Mins.</option>
                            <?
										}
										else
										{
								?>
                            <option value="<? echo $rs_MILList['authority']; ?>"><? echo $rs_MILList['period']; ?>&nbsp;Mins.</option>
                            <?
										}
									}	//while($rs_MILList  = mysqli_fetch_array($ds_MILevelList))
								?>
                          </select> </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Allowed Monitering Locations </TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['multi_monitor'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbMultiMonitor" type="radio" value="Y" <? echo $strYes; ?>>
                          All&nbsp;&nbsp; <input type="radio" name="rbMultiMonitor" value="N" <? echo $strNo; ?>>
                          Only Main Site</TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Send Monthly Reports</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['monthly_reports'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbMonthlyReports" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbMonthlyReports" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Send E-Mail Alerts</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['email_alerts'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbEMailAlerts" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbEMailAlerts" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Show Online Statistics</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['online_stats'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbOnlineStats" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbOnlineStats" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Access To Control Panel</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['control_panel'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbControlPanel" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbControlPanel" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Check HTTP(80) Port</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['http_check'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbHTTPCheck" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbHTTPCheck" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Check SMTP(25) Port</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['smtp_check'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbSMTPCheck" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbSMTPCheck" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Check POP3(110) Port</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['pop3_check'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbPOP3Check" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbPOP3Check" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Check FTP(21) Port</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['ftp_check'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbFTPCheck" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbFTPCheck" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Check Custom Port</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['custom_ports'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbCustomPorts" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbCustomPorts" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Check SSL Servers</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['ssl_server'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbSSL" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbSSL" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Provide Additional Statistics</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['public_stats'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbPublicStats" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbPublicStats" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Store Error Details</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['error_details'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbErrorDetails" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbErrorDetails" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Monitor Via Ping Utility</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['ping_monitoring'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbPingMonitoring" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbPingMonitoring" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Show Customer Time </TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['custom_time_zone'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbCustomTimeZone" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbCustomTimeZone" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Can Send Alerts To Multiple EMails</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['multi_alerts'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbMultiAlerts" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbMultiAlerts" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr>
                        <TD height="19">Can Send SMS Alerts</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['sms_alert'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbSMSAlerts" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbSMSAlerts" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19">Allow Additional Service Monitors</TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['addnl_service_monitor'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbAddnlServiceMonitors" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbAddnlServiceMonitors" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19" align="left">Status </TD>
                        <TD valign="middle"> 
                          <?
								if($rs_MSD['status'] == "Y")
								{
									$strYes = "Checked";
									$strNo  = "";
								}
								else
								{
									$strYes = "";
									$strNo  = "checked";
								}
							?>
                          <input name="rbStatus" type="radio" value="Y" <? echo $strYes; ?>>
                          Yes 
                          <input type="radio" name="rbStatus" value="N" <? echo $strNo; ?>>
                          No </TD>
                      </tr>
                      <tr> 
                        <TD height="19" align="right">&nbsp;</TD>
                        <TD valign="middle">&nbsp;</TD>
                      </tr>
                      <tr> 
                        <TD height="19" align="right"><input name="btnUpdate" type="submit" class="button" id="btnUpdate" value="Update"> 
                          &nbsp;&nbsp;</TD>
                        <TD valign="middle">&nbsp;&nbsp; <input name="btnCancel" type="button" class="button" id="btnCancel" value="Cancel" onclick="document.location.replace('membershiplist.php');"></TD>
                      </tr>
                    </table>
		</form>
                </td>
              </tr>
            </table></td>
          <td width="17%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td> <? include_once("sidebar.php") ?> </td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
</body>
</html>
