<?
	session_start();
	error_reporting(0);
	include("pager.php");
	if (!session_is_registered("euser"))
	{
		header("Location:login.php");
		exit;
	} 
	include("include/conn.inc");
	//print_r($_POST);
	if(!isset($_REQUEST['txtuserid']))
	{
		header("Location: admin_members.php");
		exit();
	}
	
	if($_POST['action'] == "update")
	{
		$res_auth = mysqli_fetch_array($db->query("select authority from tbl_checkperiods where authority = $_POST[selmemtype]"));
		
		if($_POST['selmemtype'] == 3 || $_POST['selmemtype'] == 2)
			$auth = 2;
		else 
			$auth = 1;
		
		if(!($_POST['selmemtype'] == 3 && $res_auth['authority'] == 3))
			$db->query("update tbl_services set  monitor_interval_level = $auth where userid = $_POST[txtuserid]");
			
		
		if(isset($_POST['txtsmscredits']))
			$db->query("update tbl_users set memtype = $_POST[selmemtype], mem_auth = $auth, smscredits = $_POST[txtsmscredits], add_sites = $_POST[txtaddsites] where userid = $_POST[txtuserid]");
		else
			$db->query("update tbl_users set memtype = $_POST[selmemtype], mem_auth = $auth, add_sites = $_POST[txtaddsites] where userid = $_POST[txtuserid]");
	}
	
?>
<html>
<head>
<title>Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css1.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
	function submitform()
	{
		document.form1.action.value = "update";
		form1.submit();
	}
	
	function onbtnCancel_Click()
	{
		window.location.replace("admin_members.php");
		//document.form1.action = "index.php";
		//form1.submit();
	}
</script>
</head>

<body>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="body">
  <tr> 
    <td width="42%" valign="top"><img src="../images/logo_247.gif"></td>
    <td width="39%" align="right" valign="bottom"><font color="#666666" size="5" face="Times New Roman, Times, serif"><strong>Admin Panel</strong></font></td>
    <td width="19%" valign="top">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="80%" valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="body">
              <td> <table border="0" cellspacing="0" cellpadding="1" class="body" width="100%">
                    <tr>
                      <td align="left" valign="middle" background="images/tile_back_small1.gif"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b> 
                        &nbsp;Manage User Settings</b></font></td>
                      <td align="right" valign="middle" background="images/tile_back_small1.gif"><a href="index.php"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b>Main 
                        Menu</b></font></a>&nbsp;&nbsp;&nbsp</td>
                      <td height="25" align="center" valign="middle" background="images/tile_back_small1.gif"></td>
                    </tr>
                  </table></td>
              <tr> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td height="1" colspan="2">
		<form name="form1" action="members_details.php" method="post">
			
		<table width="100%" border="0" cellpadding="2" cellspacing="0" class="body">
		<tr><td>
      <? 
     $q="select * from tbl_users where userid=$_REQUEST[txtuserid]";
	 $res=$db->query($q);
	 $r=mysqli_fetch_array($res);
	 
	?>
	
  	                      <table width="100%" align="center" cellpadding="2" cellspacing="0" border="0" class="body">
                            <tr> 
                              <td  align="right" width="40%">Name : </td>
                              <td align="left"><? echo $r['name'] ?> </td>
                            </tr>
                            <tr> 
                              <td  align="right" width="40%"> Email Id : </td>
                              <td align="left"><? echo $r['emailid'] ?></td>
                            </tr>
                            <tr> 
                              <td  align="right" width="40%">Company Name : </td>
                              <td align="left"><? echo $r['company'] ?></td>
                            </tr>
                            <tr> 
                              <td  align="right" width="40%"> Address : </td>
                              <td align="left"><? echo $r['address']; ?></td>
                            </tr>
                            <? if($r['phone'] != '')
							{ ?>
                            <tr> 
                              <td  align="right" width="40%"> Phone no : </td>
                              <td align="left"><? echo $r['phone']; ?></td>
                            </tr>
                            <? } ?>
                            <tr> 
                              <td  align="right" width="40%">City : </td>
                              <td align="left"><? echo $r['city'] ?></td>
                            </tr>
                            <? $qy="select statename from tbl_states where stateid=".$r['state'];
		  	$q1=$db->query($qy); 
			$rr=mysqli_fetch_array($q1);
		   ?>
                            <tr> 
                              <td  align="right">State : </td>
                              <td align="left"><? echo $rr[0] ?></td>
                            </tr>
                            <tr> 
                              <td  align="right"> Zip : </td>
                              <td align="left"><? echo $r['zip'] ?></td>
                            </tr>
                            <? $qy="select name from tbl_country where countryid=$r[country]";
								$q1=$db->query($qy); 
								$rr=mysqli_fetch_array($q1);
							   ?>
                            <tr> 
                              <td  align="right" width="40%">Country : </td>
                              <td align="left"><? echo $rr[0] ?></td>
                            </tr>
                            <tr> 
                              <td  align="right">Joining Date : </td>
                              <td align="left"><? echo date("m/d/Y",strtotime($r['joindate'])); ?></td>
                            </tr>
                            <tr> 
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr> 
                              <td class="t_header">Services</td>
                              <td>(Click on Service Name To View Details)</td>
                            </tr>
                            <tr> 
                              <td colspan="2" align="center">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td colspan="2" align="center"> 
                                <?
								$sql_SList = "select * from tbl_services where userid = $_REQUEST[txtuserid]";
								$ds_SList  = $db->query($sql_SList);
								
								while($rs_SList = mysqli_fetch_array($ds_SList))
								{
									if($rs_SList['service_name'] != "")
										echo "<a href='services.php?sid=$rs_SList[service_id]&uid=$rs_SList[userid]'>$rs_SList[service_name]</a> &nbsp;&nbsp;&nbsp;";
									else
										echo "<a href='services.php?sid=$rs_SList[service_id]&uid=$rs_SList[userid]'>$rs_SList[host_name]</a> &nbsp;&nbsp;&nbsp;";
								}	//while($rs_SList = mysqli_fetch_array($ds_SList))
							  ?>
                              </td>
                            </tr>
                            <tr> 
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr> 
                              <td class="t_header"> Update Settings </td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr> 
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr> 
                              <td align="right"> MemberShip Type : </td>
                              <td><select name="selmemtype" class="combobox">
                                  <? $qry_mtype = $db->query("select * from tbl_membership"); 
							  	while($res_mtype = mysqli_fetch_array($qry_mtype))
								{
									if($res_mtype['status'] == 'Y')
									{
							  	?>
                                  <option value="<? echo $res_mtype['m_id']; ?>" <? if ($res_mtype['m_id'] == $r['memtype']) echo 'selected'; ?> > 
                                  <? echo $res_mtype['mem_name']; ?> </option>
                                  <? 
								  }
								  elseif($res_mtype['m_id'] == $r['memtype'])
								  {
								  ?>
                                  <option value="<? echo $res_mtype['m_id']; ?>" <? if ($res_mtype['m_id'] == $r['memtype']) echo 'selected'; ?> > 
                                  <? echo $res_mtype['mem_name']; ?>&nbsp;&nbsp;&nbsp;<font color="#FF0000">*</font> 
                                  </option>
                                  <?
								  } // if
							  } //while ?>
                                </select> </td>
                            </tr>
                            <tr> 
                              <td align="right"> Additional Sites : </td>
                              <td> <input type="text" name="txtaddsites" value="<? echo $r['add_sites']; ?>" size="4"></td>
                            </tr>
							<? 
							$res_mem = mysqli_fetch_array($db->query("select sms_alert from tbl_membership where m_id = $r[memtype]"));
							if($res_mem['sms_alert'] == 'Y')
							{ ?>
                            <tr> 
                              <td align="right"> Enter SMS Credits : </td>
                              <td> <input name="txtsmscredits" type="text" id="txtsmscredits" value="<? echo $r['smscredits']; ?>" size="4"></td>
                            </tr>
							<? } ?>
                            <tr> 
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr> 
                              <td align="right"><input type="button" onClick="javascript: submitform()" class="button" value="Update">
                                &nbsp;&nbsp;</td>
                              <td>&nbsp;&nbsp;
                                <input type="button"  class="button" value="Cancel" onClick="onbtnCancel_Click()"></td>
                            </tr>
                            <tr> 
                              <td colspan="2" align="center">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td colspan="2" align="center"> <input type="hidden" name="action" value="cancel"> 
                                <input type="hidden" name="txtuserid" value="<? echo $_REQUEST['txtuserid']; ?>"> 
                              </td>
                            </tr>
                          </table>
			</table>
					
		</form>
                </td>
              </tr>
            </table></td>
          <td width="17%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td> <? include_once("sidebar.php") ?> </td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
</body>
</html>
