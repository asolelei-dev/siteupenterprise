<html>
   <head>
     
    <LINK href="stylesheets/pim_compose.css" type=text/css rel=stylesheet>

  <title></title>
  </head>

<body>

<table width=390 border=0 cellpadding=3 class="text">
<tr>
<td align="center" class="Heading" width="50%">
<strong> Feild </strong>
</td>
<td align="center" class="Heading" width="50%">
<strong>Description</strong>
</td>
</tr>
<tr>
 <td align="center" class="Text" height="25">
 %%name%%
 </td>
 <td align="center" class="Text" >
 User Name
 </td>
</tr>
<tr>
 <td align="center" class="Text" height="25">
 %%emailid%%
 </td>
 <td align="center" class="Text">
 User Mailid
 </td>
</tr>
<tr>
 <td align="center" class="Text" height="25">
 %%password%%
 </td>
 <td align="center" class="Text">
 User password
 </td>
 </tr>
<tr>
 <td align="center" class="Text" height="25">
 %%joindate%%
 </td>
 <td align="center" class="Text">
 User Joined Date
 </td>
 </tr>
<tr>
 <td align="center" class="Text" height="25">
 %%memtype%%
 </td>
 <td align="center" class="Text">
 Member Level
 </td>
 </tr>
<tr>
 <td align="center" class="Text" height="25">
 %%company%%
 </td>
 <td align="center" class="Text">
 User Company Name
 </td>
 </tr>
<tr>
 <td align="center" class="Text" height="25">
 %%address%%
 </td>
 <td align="center" class="Text">
 User Address
 </td>
 </tr>
<tr>
 <td align="center" class="Text" height="25">
 %%state%%
 </td>
 <td align="center" class="Text">
 User State Name
 </td>
</tr>
<tr>
 <td align="center" class="Text" height="25">
 %%city%%
 </td>
 <td align="center" class="Text">
 User City Name
 </td>
</tr>
<tr>
 <td align="center" class="Text" height="25">
 %%country%%
 </td>
 <td align="center" class="Text">
 User Country Name
 </td>
</tr>
<tr>
 <td align="center" class="Text" height="25">
 %%zip%%
 </td>
 <td align="center" class="Text">
 User Zip Code
 </td>
</tr>
<tr>
 <td align="center" class="Text" height="25">
 %%aemail%%
 </td>
 <td align="center" class="Text">
 User Alert Email Id
 </td>
</tr>
<? if($_GET['act'] != 1 && $_GET['act'] != 4 )
{ ?>
<tr>
 <td align="center" class="Text" height="25">
 %%host_name%%
 </td>
 <td align="center" class="Text">
 User Service Host Name (Domain Name)
 </td>
</tr>
<tr>
 <td align="center" class="Text" height="25">
 %%date_add%%
 </td>
 <td align="center" class="Text">
 User Service Added Date
 </td>
</tr>
<tr>
 <td align="center" class="Text" height="25">
 %%sevice_port%%
 </td>
 <td align="center" class="Text">
 User Service Port <br> (Custom Port Also)
 </td>
</tr>
<tr>
 <td align="center" class="Text" height="25"> 
 %%period%%
 </td>
 <td align="center" class="Text">
 Service Time Interval
 </td>
</tr>
<tr>
 <td align="center" class="Text" height="25">
 %%location%%
 </td>
 <td align="center" class="Text">
 Service Monitoring Location
 </td>
</tr>
<? if ($_GET['act'] != 3)
{ ?>
<tr>
 <td align="center" class="Text" height="25">
 %%time%%
 </td>
    <td align="center" class="Text"> Service Monitoring Date, Uptime or Downtime 
    </td>
</tr>
<tr>
 <td align="center" class="Text" height="25">
 %%result%%
 </td>
    <td align="center" class="Text"> Service Monitoring Result <br> (Ok or Failed) 
    </td>
</tr>
<tr>
 <td align="center" class="Text" height="25">
 %%alert_type%%
 </td>
    <td align="center" class="Text"> Service Monitoring Alert Type <br>( is Available or not) 
    </td>
</tr>

<? } 
 } ?>
</table>
</body>
</html>