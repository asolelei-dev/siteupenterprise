<?
	session_start();
	error_reporting(0);

	if (!session_is_registered("euser"))
	{
		header("Location:login.php");
		exit;
	} 
	include("include/conn.inc");
	
	$sql_CommonSet = "select * from tbl_commonsettings";
	$ds_CommonSet  = $db->query($sql_CommonSet);
	$rs_CommonSet  = mysqli_fetch_array($ds_CommonSet);
	
	if(isset($_POST['act']))
	{
		if($_POST['act'] == "Update")
		{
			$sql_UpdateCSet = "update tbl_commonsettings set showcount = '$_POST[rbShowCount]' ,
			paypal_address = '$_POST[txtpaypal]' , checkout_id = '$_POST[txtcheckout]',  contact_mailid = '$_POST[txtcontact]' ";
			$db->query($sql_UpdateCSet);
			
			header("Location: index.php");
		
		}	//if($_POST['act'] == "Update")
	
	}	//if(isset($_POST['act']))
	
	$sql_SCount = "select * from tbl_services";
	$ds_SCount  = $db->query($sql_SCount);
	$nSCount    = mysqli_num_rows($ds_SCount);
	$res_settings = mysqli_fetch_array($db->query("select * from tbl_commonsettings"));
	
	
?>
<html>
<head>
<title>Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css1.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

	function cancel()
	{
		document.frmCSet.action="index.php";
		document.frmCSet.submit();
	}

</script>
</head>

<body>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="body">
  <tr> 
    <td width="42%" valign="top"><img src="../images/logo_247.gif"></td>
    <td width="39%" align="right" valign="bottom"><font color="#666666" size="5" face="Times New Roman, Times, serif"><strong>Admin Panel</strong></font></td>
    <td width="19%" valign="top">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="80%" valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="body">
              <td> <table border="0" cellspacing="0" cellpadding="1" class="body" width="100%">
                    <tr>
                      <td align="left" valign="middle" background="images/tile_back_small1.gif"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b> 
                        &nbsp;Manage Common Settings </b></font></td>
                      <td align="right" valign="middle" background="images/tile_back_small1.gif"><a href="index.php"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b>Main 
                        Menu</b></font></a>&nbsp;&nbsp;&nbsp</td>
                      <td height="25" align="center" valign="middle" background="images/tile_back_small1.gif"></td>
                    </tr>
                  </table></td>
              <tr> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td height="1" colspan="2">
					<form name="frmCSet" method="post">
					  
                    <table width="100%" border="0" cellpadding="1" cellspacing="1" class="body">
                      <tr> 
                        <td width="50%" align="right">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr> 
                        <td align="right">Show No. Of Services Monitoring In Header</td>
                        <td><input type="radio" name="rbShowCount" value="Y" <? if($rs_CommonSet['showcount'] == "Y") echo "checked"; ?>>
                          Yes 
                          <input type="radio" name="rbShowCount" value="N" <? if($rs_CommonSet['showcount'] == "N") echo "checked"; ?>>
                          No 
                          <input name="act" type="hidden" id="act" value="Update"></td>
                      </tr>
                      <tr> 
                        <td align="right">No. Of Services Monitoring Currently&nbsp;</td>
                        <td>:&nbsp;<strong><font color="#FF0000"><? echo $nSCount; ?></font></strong></td>
                      </tr>
                      <tr align="center"> 
                        <td align="right">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                      </tr>
                      <!-- <tr> 
                        <td align="right">Enable Related Resources in Pages </td>
                        <td><input type="radio" name="rbShowRes" value="Y" <? // if($rs_CommonSet['show_res'] == "Y") echo "checked"; ?>>
                          Yes 
                          <input type="radio" name="rbShowRes" value="N" <? // if($rs_CommonSet['show_res'] == "N") echo "checked"; ?>>
                          No 
                          <input name="act" type="hidden" id="act" value="Update"></td>
                      </tr>
					  -->
                      <tr align="center"> 
                        <td align="right">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                      </tr>
                      <tr align="center"> 
                        <td align="right">Paypal Address &nbsp;</td>
                        <td align="left">:&nbsp; <input type="text" name="txtpaypal" value="<? echo $res_settings['paypal_address']; ?>"></td>
                      </tr>
                      <tr align="center"> 
                        <td align="right">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                      </tr>
<input type="hidden" name="txtcheckout" value="<? echo $res_settings['checkout_id']; ?>"></td>
     
                      <tr align="center"> 
                        <td align="right">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                      </tr>
                      <tr align="center"> 
                        <td align="right">Contact Mail Id &nbsp; </td>
                        <td align="left">:&nbsp; <input type="text" name="txtcontact" value="<? echo $res_settings['contact_mailid']; ?>"></td>
                      </tr>
                      <tr align="center"> 
                        <td align="right">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                      </tr>
                      <tr align="center"> 
                        <td align="right"><input name="btnUpdate" type="submit" class="button" id="btnUpdate" value="Save Settings"> 
                          &nbsp;&nbsp; </td>
                        <td align="left">&nbsp;&nbsp; <input name="BtnCancel" type="button" class="button" id="BtnCancel" value="Cancel" onClick="cancel();"></td>
                      </tr>
                      <tr> 
                        <td align="right">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
				  </form>
                </td>
              </tr>
            </table></td>
          <td width="17%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td> <? include_once("sidebar.php") ?> </td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
</body>
</html>
