<?
	ob_start();
	session_start(); 
	if (!session_is_registered("euser"))
	{
		header("Location:login.php");
		exit;
	}

	error_reporting(E_ALL & ~E_NOTICE);
	include("include/conn.inc");
	  
	if($_GET["act"] == "change")
	{
		$r=mysql_fetch_row($db->query("select * from tbl_adminusers where username='$_SESSION[euser]'"));
		if($_POST['pwd'] != $r[2])
		{
			$msg="<font color='#ff0000'>Invalid Password</font>";
		}
		else
		{
			$qry="update tbl_adminusers set pwd='".$_POST["npwd"]."' where username='".$_SESSION["euser"]."'";
			$db->query($qry);
			header("Location: index.php");
		}
	}
?> 
<html>
<head>
<title>Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css1.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

	function validate()
	{
		if (document.form1.pwd.value == "")
		{
			alert("Please Enter the Password");
			document.form1.pwd.focus();
			return false;
		}
		else if (document.form1.npwd.value == "")
		{
			alert("Please Enter the New Password");
			document.form1.npwd.focus();
			return false;
		}
		else if (document.form1.cpwd.value == "")
		{
			alert("Please Enter Confirm Password");
			document.form1.cpwd.focus();
			return false;
		}
		 
		if(document.form1.npwd.value != document.form1.cpwd.value)
		{
			alert("Re-enter the Password correctly");
			document.form1.cpwd.value="";
			document.form1.cpwd.focus();
			return false;
		} 
		return true;
	}
	
	function cancel()
	{
		form1.action="index.php";
		form1.submit();
	}
</script>
</head>

<body>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="body">
  <tr> 
    <td width="42%" valign="top"><img src="../images/logo_247.gif"></td>
    <td width="39%" align="right" valign="bottom"><font color="#666666" size="5" face="Times New Roman, Times, serif"><strong>Admin Panel</strong></font></td>
    <td width="19%" valign="top">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="80%" valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="body">
              <td> <table border="0" cellspacing="0" cellpadding="1" class="body" width="100%">
                    <tr> 
                      <td height="25" align="center" valign="middle" background="images/tile_back_small1.gif"></td>
                    </tr>
                  </table></td>
              <tr> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td colspan="2">
					<form name="form1" action="change_pwd.php?act=change" method="post" onSubmit="javascript:return validate();">
						
					  <table width="100%" cellpadding="2" cellspacing="0" border="0" class="body">
						<tr> 
						  <td align="right" >&nbsp;</td>
						  <td width="57%">&nbsp;</td>
						</tr>
						<tr> 
						  <td colspan="2" align="center" ><b><? echo $msg ?></b></td>
						</tr>
						<tr> 
						  <td align="right" >&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr> 
						  <td align="right" width="43%" >User Name :</td>
						  <td><input type="text" name="uname" class="inputbox" value="<? echo $_SESSION["euser"] ?>" disabled ></td>
						</tr>
						<tr> 
						  <td align="right">Password :</td>
						  <td><input type="password" name="pwd" class="inputbox"> </td>
						</tr>
						<tr> 
						  <td align="right">New Password :</td>
						  <td><input type="password" name="npwd" class="inputbox"> </td>
						</tr>
						<tr> 
						  <td align="right">Confirm Password :</td>
						  <td><input type="password" name="cpwd" class="inputbox"> </td>
						</tr>
						<tr> 
						  <td>&nbsp;</td>
						</tr>
						<tr> 
						  <td>&nbsp;</td>
						  <td align="left"><input type="submit" name="Add" value="Submit" class="button"> 
							&nbsp;&nbsp; <input type="button" name="Add" value="Cancel" class="button" onClick="javascript:cancel();"> 
						  </td>
						</tr>
					  </table>
					  </form>
  				</td>
              </tr>
            </table></td>
          <td width="17%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td> <? include_once("sidebar.php") ?> </td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<script>
	document.form1.pwd.focus();
</script>
</body>
</html>
