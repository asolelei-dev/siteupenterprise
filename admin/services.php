<?
	ob_start();
	session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include("include/conn.inc");

	if (!session_is_registered("euser"))
	{
		header("Location:login.php");
		exit;
	} 
	
	if(isset($_POST['act']))
	{
		if($_POST['act'] == "Update")
		{
			if(isset($_POST['chk2Min']))
				$strVal = "3";
			else
			{
				$sql_UMSDet = "select m.monitor_interval_level MIL from tbl_users u, tbl_membership m where userid = $_POST[uid] and m.m_id = u.memtype";
				$ds_UMSDet  = $db->query($sql_UMSDet);
				$rs_UMSDet  = mysqli_fetch_array($ds_UMSDet);
				
				$strVal = $rs_UMSDet[0];
			}
			if(isset($_POST['rbsms']))
				$rbsms = 'Y';
			else
				$rbsms = 'N';
				
			$sql_UpdateService = "update tbl_services set monitor_interval_level = '$strVal', status_sms = '$rbsms' where service_id = $_POST[sid]";
			$db->query($sql_UpdateService);
			
			header("Location: members_details.php?txtuserid=$_POST[uid]");
		
		}	//if($_POST['act'] == "Update")
		
	}	//if(isset($_POST['act']))
	
	if(isset($_GET['sid']))
	{
		$sql_UMSDet = "select m.monitor_interval_level MIL from tbl_services s,tbl_users u, tbl_membership m where u.userid = s.userid and s.service_id = $_GET[sid] and m.m_id = u.memtype";
				$ds_UMSDet  = $db->query($sql_UMSDet);
				$rs_UMSDet  = mysqli_fetch_array($ds_UMSDet);
				
				$strVal = $rs_UMSDet[0];
				
		$sql_Serv = "select s.service_id serviceid, s.service_name, s.status_sms, host_name, date_add, sp.service_name port_name, 
							sp.port_no port_no, s.custom_port custom_port, l.loc_name location, cp.period period, s.active active, 
							s.monitor_interval_level
							from tbl_services s, tbl_checkperiods cp, tbl_service_ports sp, tbl_locations l  where 
							s.service_id = $_GET[sid] and s.period = cp.pid and s.location = l.loc_id and s.service_port = sp.service_id";
		$ds_Serv  = $db->query($sql_Serv);
		//echo "<pre>";
		$rs_Serv  = mysqli_fetch_array($ds_Serv, MYSQL_ASSOC);
		//echo "</pre>";
		
	}	//if(isset($_GET['sid']))
	
?>
<html>
<head>
<title>Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css1.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

	function onbtnCancel_Click()
	{
			document.form1.action="members_details.php?txtuserid=<? echo $_REQUEST[uid]; ?>";
			document.form1.submit();
	}
	
	function submitform()
	{
		document.form1.action.value = "update";
		form1.submit();
	}
	
	function onchk2Min_Click(bChecked)
	{
		var frm = document.form1;
		
		frm.act.value = "Update";
		frm.submit();
	}
  </script>
</head>

<body>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="body">
  <tr> 
    <td width="42%" valign="top"><img src="../images/logo_247.gif"></td>
    <td width="39%" align="right" valign="bottom"><font color="#666666" size="5" face="Times New Roman, Times, serif"><strong>Admin Panel</strong></font></td>
    <td width="19%" valign="top">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="80%" valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="body">
              <td> <table border="0" cellspacing="0" cellpadding="1" class="body" width="100%">
                    <tr>
                      <td align="left" valign="middle" background="images/tile_back_small1.gif"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b> 
                        &nbsp;Manage Service Details</b></font></td>
                      <td align="right" valign="middle" background="images/tile_back_small1.gif"><a href="index.php"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b>Main 
                        Menu</b></font></a>&nbsp;&nbsp;&nbsp</td>
                      <td height="25" align="center" valign="middle" background="images/tile_back_small1.gif"></td>
                    </tr>
                  </table></td>
              <tr> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td height="1" colspan="2">
					<form name="form1" method="post">
						
                    <table width="100%" border="0" cellpadding="2" cellspacing="0" class="body">
                      <tr> 
                        <td width="50%">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <?
					  	if($rs_Serv['service_name'] != "")
						{
					  ?>
                      <tr> 
                        <td>Service Name</td>
                        <td><? echo $rs_Serv['service_name']; ?></tr>
                      <?
					  	}
					  ?>
                      <tr> 
                        <td>Host Name</td>
                        <td> <? echo $rs_Serv['host_name']; ?> <input name="act" type="hidden" id="act" value="Update"> 
                          <input name="sid" type="hidden" id="sid" value="<? echo $_GET['sid']; ?>"> 
                          <input name="uid" type="hidden" id="uid" value="<? echo $_GET['uid']; ?>"> 
                        </td>
                      </tr>
                      <tr> 
                        <td>Date Added</td>
                        <td><? echo date("jS M Y", strtotime($rs_Serv['date_add'])); ?></td>
                      </tr>
                      <tr> 
                        <td>Port </td>
                        <td> 
                          <?
								if($rs_Serv['custom_port'] == 0)
									echo $rs_Serv['port_name'];
								else
									echo "Custom Port", $rs_Serv['custom_port'];
							?>
                        </td>
                      </tr>
                      <tr> 
                        <td>Monitoring Location</td>
                        <td><? echo $rs_Serv['location']; ?></td>
                      </tr>
                      <tr> 
                        <td>Current Monitoring Interval</td>
                        <td>Every <? echo $rs_Serv['period']; ?> Minutes</td>
                      </tr>
                      <tr> 
                        <TD>Status</TD>
                        <TD> 
                          <?
								if($rs_Serv['active'] == "Y")
									echo "Active";
								else
									echo "Inactive";
							?>
                        </TD>
                      </tr>
                      <tr> 
                        <TD>&nbsp;</TD>
                        <TD></TD>
                      </tr>
                      <tr> 
                        <TD align="right">SMS Status&nbsp;&nbsp;</TD>
                        <TD>
						<input name="rbsms" type="checkbox" id="rbsms" value="Y"
						<?
							if($rs_Serv['status_sms'] == 'Y')
								echo "Checked";
						?>
						>&nbsp;</TD>
                      </tr>
                      <tr> 
                        <TD>Allow 2Min. Monitoring Intervel For This Service</TD>
                        <TD><input name="chk2Min" type="checkbox" id="chk2Min" value="Y"
						<?
							if($rs_Serv['monitor_interval_level'] == 3)
								echo "Checked";
						?>
						></TD>
                      </tr>
                      <tr> 
                        <TD>&nbsp;</TD>
                        <TD></TD>
                      </tr>
                      <tr> 
                        <TD align="right"><input name="btnUpdate" type="submit" class="button" id="btnUpdate" value="Update">
                          &nbsp;&nbsp;</TD>
                        <TD>&nbsp;&nbsp;
                          <input name="btnCancel" type="button" class="button" id="btnCancel" value="Cancel" onClick="onbtnCancel_Click();"></TD>
                      </tr>
                    </table>
					</form>
                </td>
              </tr>
            </table></td>
          <td width="17%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td> <? include_once("sidebar.php") ?> </td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<script>
	<?
		if($_POST['act'] == "Update")
		{
	?>
			document.form1.action="members_details.php?txtuserid=<? echo $_POST[uid]; ?>";
			document.form1.submit();
	<?
		}
	?>
</script>
</body>
</html>
