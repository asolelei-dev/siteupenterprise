<?  session_start();
 if (!session_is_registered("euser")){
	header("Location:login.php");
	exit;
} 
error_reporting(0);     //0);    //E_ALL & ~E_NOTICE);
include("include/conn.inc");
function unhtmlentities ($string) {
     $trans_tbl = get_html_translation_table(HTML_ENTITIES);
     $trans_tbl = array_flip ($trans_tbl);
     return strtr ($string, $trans_tbl);
}
if(isset($_POST['action']))
{
 if($_POST['action'] == 'edit' ){
 $qry_edit="select * from tbl_mailing where id=".$_POST['txtmailtype'];
 $res_edit=$db->query($qry_edit);
 $r=mysqli_fetch_array($res_edit);
 }
  if($_POST['action'] == 'update' ){
$q="update tbl_mailing set mail_sub = '$_POST[txtsub]', mail_from = '$_POST[txtfrom]', mail_txt = '".htmlentities($_POST['txtarea'],ENT_QUOTES)."', mail_closing = '$_POST[txtclosing]' where id=".$_POST['txtmailtype'];
 $db->query($q);
 	header("Location:edit_mailing.php");
	exit;
 }
}
?>
<html>
<head>
<title>Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css1.css" rel="stylesheet" type="text/css">
<SCRIPT src="inscript/ylib_dom.js"></SCRIPT>
<SCRIPT language=JavaScript src="inscript/pim.js"></SCRIPT>
<SCRIPT language=JavaScript src="inscript/pim_css.js"></SCRIPT>
<LINK href="stylesheets/pim_compose.css" type=text/css rel=stylesheet>
<SCRIPT language=JavaScript src="inscript/editor.js"></SCRIPT>
<SCRIPT language=JavaScript src="inscript/imgover.js"></SCRIPT>
<script language="JavaScript">
 function editfun()
 {
   document.form1.action.value="edit";
   form1.submit();
 }
 function cancel()
 {
   document.form1.action.value=null;
   form1.submit();
 }
 function updatefun()
 {
 	document.form1.txtarea.value = editor.GetHTML();
	form1.submit();
 }
 
function showwindow(value){
popupwin=window.open('showservice.php?act='+value,'','width=450,height=500,scrollbars=yes,left=50,top=20,directories=no,location=no,resizable=no,status=no,toolbar=no');
}

 
</script>
</head>

<body>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="body">
  <tr> 
    <td width="42%" height="1" valign="top"><img src="../images/logo_247.gif"></td>
    <td width="39%" align="right" valign="bottom"><font color="#666666" size="5" face="Times New Roman, Times, serif"><strong>Admin Panel</strong></font></td>
    <td width="19%" valign="top">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="80%" valign="top">
		   <table width="100%" border="0" cellpadding="0" cellspacing="0" class="body">
              <td> <table border="0" cellspacing="0" cellpadding="1" class="body" width="100%">
                  <tr> 
                      <td height="25" align="left" valign="middle" background="images/tile_back_small1.gif"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b> 
                        &nbsp;Manage Mails &raquo; Edit
                        </b></font></td>
                    <td align="right" valign="middle" background="images/tile_back_small1.gif"><a href="index.php"><font face="Verdana, Haettenschweiler" color="#FFFFFF" size="1"><b>Main 
                      Menu</b></font></a>&nbsp;&nbsp;&nbsp</td>
                  </tr>
                </table></td>
              <tr> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td colspan="2"><form name="form1" action="edit_mailing.php" method="post" enctype="multipart/form-data" >
					<input type="hidden" name="action" value="update">
				      
                    <table width="80%" align="center" cellpadding="2" cellspacing="0" border="0" class="body">
                      <tr> 
                        <td width="23%"  align="right">&nbsp;</td>
                        <td width="77%" align="left">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td  align="right" valign="top">Mail Type : </td>
                        <td height="1" align="left"> 
                          <? $mail_qry = $db->query("select id,mail_name from tbl_mailing"); ?>
                          <select name="txtmailtype" type="text" class="select" onChange="javascript: editfun()">
                            <option value="0">- - - - Select Type - - - -</option>
                            <? while($res = mysqli_fetch_array($mail_qry))
						{
						 ?>
                            <option value="<? echo $res['id']; ?>" <? if($r['id'] == $res['id']) echo 'selected'; ?> ><? echo $res['mail_name']; ?></option>
                            <? } ?>
                          </select> </td>
                      </tr>
                      <tr> 
                        <td  align="right" valign="top">Subject : </td>
                        <td height="1" align="left"> <input name="txtsub" type="text" class="box" value="<? echo $r['mail_sub']; ?>" size="50" maxlength="70"> 
                        </td>
                      </tr>
                      <tr> 
                        <td align="right" valign="top">From : </td>
                        <td height="1" align="left"> <input name="txtfrom" type="text" class="box"  size="50" maxlength="90" value="<? echo $r['mail_from']; ?>"> 
                        </td>
                      </tr>
					  <tr><td align="left" class="body" colspan="2"><font color="red">Use %%Field%% without spacing </font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					   Click to <a href="javascript: showwindow(<? echo $_POST['txtmailtype']; ?>)" ><strong><u>Help</u></strong></a> for fields.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  </td>
					  </tr>
                      <TR> 
                        <!-- <TD width="81%" class=body>&nbsp; <textarea name="txtans" cols="70" rows="10" class="box" id="txtans"></textarea> 
          </TD> -->
                        <td colspan="2" align="left">Description:&nbsp; <INPUT TYPE="hidden" name=txtarea value="<? echo unhtmlentities(htmlspecialchars(stripslashes($r['mail_txt']),ENT_QUOTES)) ?>"> 
                          <SCRIPT>
	var idGenerator = new IDGenerator(0);
	var editor = new Editor(idGenerator);
	editor.Instantiate();			
	editor.SetHTML(document.form1.txtarea.value);
 </SCRIPT> </td>
                      </TR>
                      <tr> 
                        <td>&nbsp;</td>
                      </tr>
                      <tr> 
                        <td  align="right" valign="top">Closing : </td>
                        <td height="1" align="left"> <input name="txtclosing" type="text" class="box" value="<? echo $r['mail_closing']; ?>" size="50" maxlength="70"> 
                        </td>
                      </tr>
                      <tr> 
                        <td ></td>
                        <td align="left">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td ></td>
                        <td align="left"> <input type="button" name="edit" value="Update" class="button" onClick="javascript: updatefun()"> 
                          &nbsp;&nbsp; <input type="button" name="Edit" value="Cancel" class="button" onClick="javascript:cancel()"></td>
                      </tr>
                    </table>
                  </form></td>
              </tr>
            </table></td>
          <td width="17%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="body">
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td> <? include_once("sidebar.php") ?> </td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="3">&nbsp;</td>
  </tr>
</table>

</body>
</html>
