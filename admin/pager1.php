<?php 
error_reporting(0);    //E_ALL & ~E_NOTICE);
/************************************************************************************** 
* Class: Pager 
* Author: Radha Kiran
* Methods: 
*         findStart 
*         findPages 
*         pageList 
*         nextPrev 
**************************************************************************************/ 
class Pager 
  { 
  /*********************************************************************************** 
   * int findStart (int limit) 
   * Returns the start offset based on $_GET['page'] and $limit 
   ***********************************************************************************/ 
   function findStart($limit) 
    { 
     if (($_GET['page'] == 1) || ($_GET['page'] == "") || ($_GET['page'] == 0)) 
      { 
       $start = 0; 
       $_GET['page'] = 1; 
      } 
     else 
      { 
       $start = ($_GET['page']-1) * $limit; 
      } 

     return $start; 
    } 
  /*********************************************************************************** 
   * int findPages (int count, int limit) 
   * Returns the number of pages needed based on a count and a limit 
   ***********************************************************************************/ 
   function findPages($count, $limit) 
    { 
     $pages = (($count % $limit) == 0) ? $count / $limit : floor($count / $limit) + 1; 

     return $pages; 
    } 
  /*********************************************************************************** 
   * string pageList (int curpage, int pages) 
   * Returns a list of pages in the format of "� < [pages] > �" 
   ***********************************************************************************/ 
   function pageList($curpage, $pages, $cnt) 
    { 
     $page_list  = ""; 

     /* Print the first and previous page links if necessary */ 
     //if (($curpage != 1) && ($curpage)) 
      //{ 
      // $page_list .= "  <a href=\"".$_SERVER['PHP_SELF']."?page=1\" title=\"First Page\">�</a> "; 
	 // }
	 //else {
		// $page_list .= "�";
      //} 

     //if (($curpage-1) > 0) 
     // { 
     //  $page_list .= "<a href=\"".$_SERVER['PHP_SELF']."?page=".($curpage-1)."\" title=\"Previous Page\"><</a> "; 
    //  } 

     /* Print the numeric page list; make the current page unlinked and bold */ 
     for ($i=1; $i<=$pages; $i++) 
      { 
       if ($i == $curpage) 
        { 
         $page_list .= "<font face=verdana SIZE=1 color=#ff0000><b>".$i."</b></font>"; 
        } 
       else 
        { 
         $page_list .= "<a href=\"".$_SERVER['PHP_SELF']."?cid=" . $_REQUEST['cid'] . "&i=".$cnt."&page=".$i."\" title=\"Page ".$i."\"><font face=verdana size=1 color=#7c7c81>".$i."</font></a>"; 
        } 
       $page_list .= " "; 
      } 

     /* Print the Next and Last page links if necessary */ 
     //if (($curpage+1) <= $pages) 
      //{ 
     //  $page_list .= "<a href=\"".$_SERVER['PHP_SELF']."?page=".($curpage+1)."\" title=\"Next Page\">></a> "; 
    //  } 

     //if (($curpage != $pages) && ($pages != 0)) 
     // { 
     //  $page_list .= "<a href=\"".$_SERVER['PHP_SELF']."?page=".$pages."\" title=\"Last Page\">�</a> "; 
     // } 
     //$page_list .= "</td>\n"; 

     return $page_list; 
    } 
  /*********************************************************************************** 
   * string nextPrev (int curpage, int pages) 
   * Returns "Previous | Next" string for individual pagination (it's a word!) 
   ***********************************************************************************/ 
   function nextPrev($curpage, $pages, $cnt) 
    { 
     $next_prev  = "<table class=bodytext border=0 cellpadding=4 cellspacing=0><tr><td align=right width='50%'>"; 

     if (($curpage-1) <= 0) 
      { 
       $next_prev .= "<font face=verdana size=1>&lt;&lt;Prev</font>"; 
      } 
     else 
      { 
       $next_prev .= "<a href='".$_SERVER['PHP_SELF']."?cid=" . $_REQUEST['cid'] . "&i=".$cnt."&page=".($curpage-1)."'>". "<font face=verdana size=1>&lt;&lt;Prev</font>" . "</a>"; 
      } 

     $next_prev .= "</td><td align=left width='50%'>"; 

     if (($curpage+1) > $pages) 
      { 
       $next_prev .= "<font face=verdana size=1>Next&gt;&gt;</font>"; 
      } 
     else 
      { 
       $next_prev .= "<a href=\"".$_SERVER['PHP_SELF']."?cid=" . $_REQUEST['cid'] . "&i=".$cnt."&page=".($curpage+1)."\">". "<font face=verdana size=1>Next&gt;&gt;</font>" . "</a>"; 
      } 
	  $next_prev .= "</td></tr></table>";

     return $next_prev; 
    } 
  } 
?> 