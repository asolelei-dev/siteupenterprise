<?
	error_reporting(E_ALL  ^ E_NOTICE);
	include("admin/include/conn.inc");
	session_start();

	//Initializing to EMPTY String
	$opt = array('Y' => 'Yes', 'N' => 'No');
	
	$strMSName = "";
	$strMSMFee = "";
	$strMSYFee = "";
	$strNoOfMonitorsAllowed = "";
	$strMonitorsLevel = "";
	$strMultiMonitor = "";
	$strEMailAlerts = "";
	$strMonthlyReports = "";
	$strOnlineStats = "";
	$strControlPanel = "";
	$strHTTPCheck = "";
	$strSMTPCheck = "";
	$strPOP3Check = "";
	$strFTPCheck  ="";
	$strPublicStats = "";
	$strErrorDetails = "";
	$strPingMonitoring = "";
	$strCustomTimeZone = "";
	$strMultiAlerts = "";
	$strCustomPorts = "";
	$strSSLServer = "";
	$strAddnlServiceMonitors = "";
	$strStatus = "";
	$strAction = "";
	
	
	$sql_MSList = "select * from tbl_membership where status =  'Y'";
	$ds_MSList  = $db->query($sql_MSList);
	
	$i=1;
	
	while($rs_MSList = mysqli_fetch_array($ds_MSList))
	{
		$i++;
			
		$strMSName .= "<td align='center'>$rs_MSList[mem_name]</td>";
		$strMSMFee .= "<td  align='center' bgcolor = '#F1F5E6'>\$$rs_MSList[amt_monthly]</td>";
		$strMSYFee .= "<td  align='center' bgcolor = '#F1F5E6'>\$$rs_MSList[amt_yearly]</td>";
		$strNoOfMonitorsAllowed .= "<td  align='center' bgcolor = '#F1F5E6'>$rs_MSList[total_sites]</td>";

		$sql_MIL = "select * from tbl_checkperiods where authority <= $rs_MSList[monitor_interval_level]";
		$ds_MIL  = $db->query($sql_MIL);
		
		$strMin = "";
		
		while($rs_MIL = mysqli_fetch_array($ds_MIL))
		{
			$strMin .= $rs_MIL['period'] . ", ";
		}

		$strMonitorsLevel .= "<td bgcolor = '#F1F5E6'>" . substr($strMin, 0, strlen($strMin)-2) . " Mins. </td>";

		$strMultiMonitor .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['multi_monitor']] . "</td>";
		$strEMailAlerts .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['email_alerts']] . "</td>";
		$strMonthlyReports .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['monthly_reports']] . "</td>";
		$strOnlineStats .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['online_stats']] . "</td>";
		$strControlPanel .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['control_panel']] . "</td>";
		$strHTTPCheck .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['http_check']] . "</td>";
		$strSMTPCheck .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['smtp_check']] . "</td>";
		$strPOP3Check .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['pop3_check']] . "</td>";
		$strFTPCheck  .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['ftp_check']] . "</td>";
		$strPublicStats .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['public_stats']] . "</td>";
		$strErrorDetails .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['error_details']] . "</td>";
		$strPingMonitoring .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['ping_monitoring']] . "</td>";
		$strCustomTimeZone .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['custom_time_zone']] . "</td>";
		$strMultiAlerts .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['multi_alerts']] . "</td>";
		$strCustomPorts .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['custom_ports']] . "</td>";
		$strSSLServer .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['ssl_server']] . "</td>";
		$strSMSAlerts .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['sms_alert']] . "</td>";
		$strAddnlServiceMonitors .= "<td  align='center' bgcolor = '#F1F5E6'>" . $opt[$rs_MSList['addnl_service_monitor']] . "</td>";
		
		if($rs_MSList['status'] == "Y")
			$strStatus .= "<td  align='center' bgcolor = '#F1F5E6'>Active</td>";
		else
			$strStatus .= "<td  align='center' bgcolor = '#F1F5E6'><strong><font color='ff0000'>InActive</font></strong></td>";
					
		$strAction .= "<td bgcolor='#efefef' align='center' style='border:1px solid #ff0ff0'><a href='editmembership.php?mid=$rs_MSList[m_id]'>Edit</a> / <a href='javascript:deleteMemberShip($rs_MSList[m_id])'>Delete</a></td>";
		
	
	}	//while($rs_MSList = mysqli_fetch_array($ds_MSList))
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site Monitor.com</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="monitor_style.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript">
function showlist(nlid){
popupwin=window.open(nlid,'','width=450,height=150,scrollbars=yes,left=100,top=50,directories=no,location=no,resizable=no,status=no,toolbar=no');
}
</script>
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="730" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="504" valign="top"><TABLE class=bodytext border=0 cellPadding=0 cellSpacing=0 width="480">
                          <TBODY>
                            <TR> 
                              <TD align="left" valign="top" class=body> <P><B><font color="#FF6600"><strong>&raquo; 
                                  </strong></font><font color="#006699">Compare 
                                  Features </font></B> </TD>
                            </TR>
                            <TR> 
                              <TD height="25" align="left" valign="top" class=body> 
                                <div align="justify"><br>
                                  The table below is a comparison of the features 
                                  in our free account and premium paid services. 
                                  If you have any questions, please <a href="contact.php" class="me"><u>contact 
                                  us</u></a>. 
                                  <p>If you would like one of our premium plans, 
                                    please <a href="register.php" class="me"><u>register</u></a> 
                                    for a free account and then submit a <a href="advancedservices.php" class="me"><u>payment</u></a>. 
                                    Your account will be upgraded within 24 hours. 
                                    <br>
                                  </p>
                                </div></TD>
                            </TR>
                            <TR> 
                              <TD align="center" valign="top"><table class=mt width="370" style="border:1px solid #336666" cellspacing="1" cellpadding="0">
                                  <tr> 
                                    <td align="left" valign="top"><table width="370" border="0" cellpadding="0" cellspacing="1" bgcolor="f9f9f9" class=formtext>
                                        <tr > 
                                          <td height="20" align="center" bgcolor="#009999" class="c4"><strong>Features</strong> 
                                          </td>
                                          <? echo $strMSName; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;Monthly 
                                            Payment </td>
                                          <? echo $strMSMFee; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;Yearly 
                                            Payment </td>
                                          <? echo $strMSYFee; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<a class=link href="javascript:showlist('compare_list.php#interval')">Monitoring 
                                            Interval</a> </td>
                                          <? echo $strMonitorsLevel; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#servicemon')">Service 
                                            Monitors</a> </td>
                                          <? echo $strNoOfMonitorsAllowed; ?> 
                                        </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#locations')">3 
                                            Monitoring Locations</a> </td>
                                          <? echo $strMultiMonitor; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#emailalerts')">Email 
                                            Alerts</a> </td>
                                          <? echo $strEMailAlerts;; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#reports')">Monthly 
                                            Reports</a> </td>
                                          <? echo $strMonthlyReports; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#onlinestats')">Online 
                                            Statistics</a> </td>
                                          <? echo $strOnlineStats; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#cp')">Control 
                                            Panel</a> </td>
                                          <? echo $strControlPanel; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#http')">Web 
                                            Server (http)</a> </td>
                                          <? echo $strHTTPCheck; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#pop3')">Email 
                                            Server (pop3)</a> </td>
                                          <? echo $strPOP3Check; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#smtp')">Email 
                                            Server (smtp)</a> </td>
                                          <? echo $strSMTPCheck; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#ftp')">FTP 
                                            Server (ftp)</a> </td>
                                          <? echo $strFTPCheck; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#pubstats')">Public 
                                            Statistics</a> </td>
                                          <? echo $strPublicStats; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#error')">Error 
                                            Details</a> </td>
                                          <? echo $strErrorDetails; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#ping')">Ping 
                                            Monitoring</a> </td>
                                          <? echo $strPingMonitoring; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#time')">Custom 
                                            Time Zone</a> </td>
                                          <? echo $strCustomTimeZone; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#multiplealerts')">Multiple 
                                            Alert Contacts</a> </td>
                                          <? echo $strMultiAlerts; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#custports')">Custom 
                                            Ports</a> </td>
                                          <? echo $strCustomPorts; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#ssl')">SSL 
                                            Server</a> </td>
                                          <? echo $strSSLServer; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#sms')">SMS 
                                            Alerts</a> </td>
                                          <? echo $strSMSAlerts; ?> 
                                          <? //echo $strAddnlServiceMonitors; ?>
                                        </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#2min')">2 
                                            Minute Monitoring</a> </td>
                                          <?
										  	for($j=1; $j < $i; $j++)
											{
										  ?>
                                          <td align="center" bgcolor = '#F1F5E6' >Optional: 
                                            $3/mo. </td>
                                          <? } ?>
                                          <? echo $str2MinMonitoring; ?> </tr>
                                        <tr> 
                                          <td height="20"  class="bg3">&nbsp;<A class=link href="javascript:showlist('compare_list.php#additional')">Additional 
                                            Service Monitors</a> </td>
                                          <?
										  	for($j=1; $j < $i; $j++)
											{
										  ?>
                                          <td align="center" bgcolor = 'F1F5E6' >Optional: 
                                            $1/mo. </td>
                                          <? } ?>
                                          <? echo $strASMonitor; ?> </tr>
                                      </table></td>
                                  </tr>
                                </table></TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top">&nbsp;</TD>
                            </TR>
                          </TBODY>
                        </TABLE></td>
						<td align="right" valign="top" height="100%">
                      <? include("rightbar.php"); ?></td>
                      
                    </tr>
                    <tr > 
                      <td background="images/dotline.gif" height="1" colspan="3" valign="top"></td>
                    </tr>
                    <tr align="center" > 
                      <td  height="12" colspan="3" valign="top" class="tre12"></td>
                    </tr>
                    <tr align="center" > 
                      <td colspan="3"><? include("footer.php"); ?></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
