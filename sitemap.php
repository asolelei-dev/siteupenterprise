<?
error_reporting(0);
include("admin/include/conn.inc");
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site Uptime Enterprise</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="monitor_style.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="730" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="504" valign="top"><table class=mt width="379" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td><div align="justify"> 
                                <table width="375" border="0" cellspacing="0" cellpadding="0" class="me">
                                  <tr> 
                                    <td> <font color="#FF6600"><strong>&raquo; 
                                      </strong></font><strong><font color="#006699">Site 
                                      Map</font></strong> </td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                  </tr>
                                  <tr> 
                                    <td height="20" background="images/grade3.gif"> 
                                      <strong><font color="#FF6600"><strong>&raquo; 
                                      </strong></font><font color="#FFFFFF">Home</font></strong></td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                  </tr>
                                </table>
                                <font color="#FF6600">&#8226;</font> <a href="index.php" class="me1"> 
                                Home</a><font color="#FF6600">&nbsp;|&nbsp;</font>Home 
                                page of Site Uptime Enterprise
                                <p><font color="#FF6600">&#8226;</font> <a href="company.php" class="me1"> 
                                  About Us</a><font color="#FF6600">&nbsp;|&nbsp;</font>About 
                                  Site Uptime Enterprise
                                <p><font color="#FF6600">&#8226;</font> <a href="faq.php" class="me1"> 
                                  FAQs</a><font color="#FF6600">&nbsp;|&nbsp;</font>Frequently 
                                  Asked Querstions about Site Uptime Enterprise 
                                <p><font color="#FF6600">&#8226;</font> <a href="services.php" class="me1"> 
                                  Monitoring</a><font color="#FF6600">&nbsp;|&nbsp;</font>Monitoring 
                                  services to all Users 
                                <p><font color="#FF6600">&#8226;</font> <a href="privacy.php" class="me1"> 
                                  Privacy</a><font color="#FF6600">&nbsp;|&nbsp;</font>Site Uptime Enterprise 
                                  Privacy Policy <br>
                                  <br>
                                <table width="375" border="0" cellspacing="0" cellpadding="0" class="me">
                                  <tr> 
                                    <td height="20" background="images/grade3.gif"><strong><font color="#FF6600"><strong>&raquo; 
                                      </strong></font><font color="#FFFFFF">Services</font></strong></td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                  </tr>
                                </table>
                                <p><font color="#FF6600">&#8226;</font> <a href="advancedservices.php" class="me1"> 
                                  Premium Plans</a><font color="#FF6600">&nbsp;|&nbsp;</font>Premium 
                                  Plans for Users of Site Uptime Enterprise 
                                <p><font color="#FF6600">&#8226;</font> <a href="compareservices.php" class="me1"> 
                                  Compare Services</a><font color="#FF6600">&nbsp;|&nbsp;</font>Compare 
                                  Services for Users of Site Uptime Enterprise<br>
                                  <br>
                                <table width="375" border="0" cellspacing="0" cellpadding="0" class="me">
                                  <tr> 
                                    <td height="20" background="images/grade3.gif"><strong><font color="#FF6600"><strong>&raquo; 
                                      </strong></font><font color="#FFFFFF">Sign 
                                      Up</font></strong></td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                  </tr>
                                </table>
                                <p><font color="#FF6600">&#8226;</font> <a href="register.php" class="me1"> 
                                  Signup</a><font color="#FF6600">&nbsp;|&nbsp;</font>Online 
                                  form to join Site Uptime Enterprise <br>
                                  <br>
                                <table width="375" border="0" cellspacing="0" cellpadding="0" class="me">
                                  <tr> 
                                    <td height="20" background="images/grade3.gif"><strong><font color="#FF6600"><strong>&raquo; 
                                      </strong></font><font color="#FFFFFF">Members</font></strong></td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                  </tr>
                                </table>
                                <p><font color="#FF6600">&#8226;</font> <a href="users/index.php" class="me1"> 
                                  Login</a><font color="#FF6600">&nbsp;|&nbsp;</font>Login 
                                  to the Member Center of Site Uptime Enterprise <br>
                                  <br>
                                <table width="375" border="0" cellspacing="0" cellpadding="0" class="me">
                                  <tr> 
                                    <td height="20" background="images/grade3.gif"><strong><font color="#FF6600"><strong>&raquo; 
                                      </strong></font><font color="#FFFFFF">Support</font></strong></td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                  </tr>
                                </table>
                                <p><font color="#FF6600">&#8226;</font> <a href="support.php" class="me1"> 
                                  Support Center</a><font color="#FF6600">&nbsp;|&nbsp;</font>Technical 
                                  Support for Members<br>
                                  <br>
                                  <br>
                                <table width="375" border="0" cellspacing="0" cellpadding="0" class="me">
                                  <tr> 
                                    <td height="20" background="images/grade3.gif"><strong><font color="#FF6600"><strong>&raquo; 
                                      </strong></font><font color="#FFFFFF">Contact</font></strong></td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                  </tr>
                                </table>
                                <p><font color="#FF6600">&#8226;</font> <a href="contact.php" class="me1"> 
                                  Contact Home</a><font color="#FF6600">&nbsp;|&nbsp;</font>Contact 
                                  an Site Uptime Enterprise representative </p>
                                <br>
                              </div></td>
                          </tr>
                        </table></td>
						<td align="right" valign="top" height="100%">
                      <? include("rightbar.php"); ?></td>
                      
                    </tr>
                    <tr > 
                      <td background="images/dotline.gif" height="1" colspan="3" valign="top"></td>
                    </tr>
                    <tr align="center" > 
                      <td  height="12" colspan="3" valign="top" class="tre12"></td>
                    </tr>
                    <tr align="center" > 
                      <td colspan="3"><? include("footer.php"); ?></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
