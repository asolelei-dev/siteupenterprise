<?php

$dt="<table border=1 bgcolor='#cccccc'><tr><td>hi</td></table>";

$pdf = pdf_new();

pdf_open_file($pdf);
pdf_begin_page($pdf, 595, 842);
pdf_set_font($pdf, "Times-Roman", 15, "host");
pdf_set_value($pdf, "textrendering", 0);

pdf_show_xy($pdf, "invoice #$inv_id", 50, 750);
pdf_show_xy($pdf, "Due Date:". $rs_pdf->FieldsKey['next_due_date']->Value(), 50, 730);
pdf_show_xy($pdf, "Domain: ".$rs_domain->FieldsKey['domain_name']->Value(), 400, 720);
pdf_show_xy($pdf, $rs_name->FieldsKey['firstname']->Value()." ".$rs_name->FieldsKey['lastname']->Value(), 400, 700);
pdf_show_xy($pdf, "Invoice Items", 50, 650);
pdf_show_xy($pdf, "Price", 450, 650);
$y=635;
$total=0;
while(!$rs_inv_items->_eof)
{
	
	pdf_show_xy($pdf, $rs_inv_items->FieldsKey['description']->Value(), 50, $y);
	pdf_show_xy($pdf, $rs_inv_items->FieldsKey['price']->Value(), 450, $y);
	$y=$y-15;
	$total=$total+$rs_inv_items->FieldsKey['price']->Value();
	$rs_inv_items->MoveNext();
}
//pdf_lineto($pdf,50,$y);
pdf_show_xy($pdf, "total :".$total, 415, $y-15);
pdf_end_page($pdf);
pdf_close($pdf);

$data = pdf_get_buffer($pdf);

header("Content-type: application/pdf");
header("Content-disposition: inline; filename=test.pdf");
header("Content-length: " . strlen($data));

echo $data;

?> 