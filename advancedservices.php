<?
	session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include("admin/include/conn.inc");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site Uptime Enterprise</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="monitor_style.css" rel="stylesheet" type="text/css">

<script language="JavaScript" type="text/JavaScript">


	function setFormFieldValues(frmOwn, strAmount, strDuration, strDurationCode)
	{
		var frm = document.forms[frmOwn];
		
		frm.a3.value=strAmount;
		frm.t3.value=strDurationCode;
		frm.p3.value=strDuration;
		
		<?
			if(! isset($_SESSION['mnuser']))
			{
		?>
			frm.action = "users/index.php?fw=PPS";
		<?
			}
		?>
	
	}
</script>
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="730" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="504" valign="top"><TABLE class=bodytext border=0 cellPadding=0 cellSpacing=0 width="480">
                          <TBODY>
                            <TR> 
                              <TD align="left" valign="top" class=body> <table width="450" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                    <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                    <td width="430" class="link"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                      </strong></font><font color="#006699">Premium 
                                      Monitoring Services</font></B></td>
                                  </tr>
                                  <tr > 
                                    <td background="images/dotline.gif" colspan="3"></td>
                                  </tr>
                                </table>
                                <P><B></B> </TD>
                            </TR>
                            <TR> 
                              <TD height="25" align="left" valign="top" class=body> 
                                <div align="justify"><br>
                                  Site Monitor now provides additional monitoring 
                                  services for those users who need advanced features. 
                                  <br>
                                  <br>
                                  <a href="compareservices.php" class="kk">Compare 
                                  the features available with our free and Premium 
                                  accounts.</a> <br>
                                  <br>
                                  To upgrade your account, please submit a payment 
                                  below. Your account will be upgraded to Premium 
                                  status within 24 hours. <br>
                                  <br>
                                  <?
								  	$sql_MSDet = "select * from tbl_membership where status = 'Y'";
									$ds_MSDet  = $db->query($sql_MSDet);
									
									//echo mysqli_num_rows($ds_MSDet);
									
									$str = "";
									while ($rs_MSDet  = mysqli_fetch_array($ds_MSDet))
									{
										echo "<a name=$rs_MSDet[m_id]></a>";
										if($rs_MSDet['amt_monthly'] > 0 || $rs_MSDet['amt_yearly'] > 0)
										{
											echo  "<br><hr><br>Our <strong>$rs_MSDet[mem_name]</strong> costs ";
											
											if ($rs_MSDet['amt_monthly'] > 0)
												echo "$$rs_MSDet[amt_monthly]/month or ";
											
											if ($rs_MSDet['amt_yearly'] > 0)
												echo "$$rs_MSDet[amt_yearly]/year ";
												
											echo "and includes the following features in addition to the features available to free accounts: <br><br>";
											echo  "<font color='#FF6600'>&#8226; </font>$rs_MSDet[total_sites] service monitors <br>";
											echo  "<font color='#FF6600'>&#8226; </font>5 and 15 minute monitoring <br>";
	
											if($rs_MSDet['multi_alerts'] == "Y")
											echo  "<font color='#FF6600'>&#8226; </font>Multiple Alert Contacts <br>";
											
											if($rs_MSDet['ping_monitoring'] == "Y")
											echo  "<font color='#FF6600'>&#8226; </font>Monitoring Sites Via Ping<br>";
											
											if($rs_MSDet['ssl_server'] == "Y")
											echo  "<font color='#FF6600'>&#8226; </font>Monitoring SSL Servers<br>";
											
											if($rs_MSDet['custom_ports'] == "Y")
											echo  "<font color='#FF6600'>&#8226; </font>Monitoring Custom Ports<br>";
											
											if($rs_MSDet['addnl_service_monitors'] == "Y")
											echo  "<font color='#FF6600'>&#8226; </font>Additional Service Monitors For Extra Payment<br>";
											
											if($rs_MSDet['sms_alert'] == "Y")
											echo  "<font color='#FF6600'>&#8226; </font>Send SMS Alerts (additional .20 fee per alert applies) <br>";
	
											if($rs_MSDet['error_details'] == "Y")
											echo  "<font color='#FF6600'>&#8226; </font>Provides Error Details <br>";
									?>
                                  <br>
                                  <form name="frm<? echo $rs_MSDet['m_id'] ?>" action="paypalsubmitter.php" method="post">
                                    <input type="hidden" name="item_name" value="<? echo $rs_MSDet['mem_name']; ?>">
                                    <table class="bg1 bodytext " width="100%" cellpadding="1" cellspacing="5">
                                      <tr> 
                                        <td colspan="3"><strong>Click on Appropriate 
                                          Paypal image to Subscribe</strong></td>
                                      </tr>
                                      <tr> 
                                        <td align="center"> <input type="image" src="images/paypal_but.gif" alt="Make payments with PayPal - it's fast, free and secure!"
													 border="0" name="submit" onClick="setFormFieldValues('frm<? echo $rs_MSDet['m_id'] ?>', '<? echo $rs_MSDet['amt_monthly']; ?>', '1', 'M');"> 
                                        </td>
                                        <td align="center"> <input type="image" src="images/paypal_but.gif" alt="Make payments with PayPal - it's fast, free and secure!"
													border="0" name="submit" onClick="setFormFieldValues('frm<? echo $rs_MSDet['m_id'] ?>', '<? echo $rs_MSDet['amt_yearly']; ?>', '1', 'Y');"> 
                                        </td>
                                      </tr>
                                      <tr> 
                                        <td align="center"><font color="#333333"><strong>For 
                                          One Month</strong></font></td>
                                        <td align="center"><font color="#333333"><strong>For 
                                          One Year</strong></font></td>
                                        <td width="33%"><font color="#333333">&nbsp;</font></td>
                                      </tr>
                                    </table>
                                    <p class="c1">
                                      <input type="hidden" name="a3" value="<? echo $rs_MSDet['amt_monthly']; ?>">
                                      <input type="hidden" name="p3" value="1">
                                      <input type="hidden" name="t3" value="M">
                                    </p>
                                  </form>
                                  <?
										}	//if($rs_MSDet['amt_monthly'] > 0 || $rs_MSDet['amt_yearly'] > 0)
										
									}	//while ($rs_MSDet  = mysqli_fetch_array($ds_MSDet))
									
									echo  "<hr>";
									echo $str;
								  ?>
                                  <p><br>
                                    <span class="c1">The following are also available 
                                    on a monthly basis: </span><br>
                                    <br>
                                    <font color="#FF6600">&#8226; </font>2 minute 
                                    monitoring: $3 per month per service monitor<br>
                                    <font color="#FF6600">&#8226; </font>Additional 
                                    service monitors: $1 per month <br>
                                    <br>
                                    If you require custom monitoring solutions 
                                    not displayed above, please contact us. <br>
                                    <br>
                                    <br>
                                    <!--<a class="me" href="payment.php"><u>Submit a Payment</u> </a>-->
                                  </p>
                                </div></TD>
                            </TR>
                            <TR> 
                              <TD align="center" valign="top" class=body>&nbsp;</TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" class=body>&nbsp;</TD>
                            </TR>
                          </TBODY>
                        </TABLE></td>
						<td align="right" valign="top" height="100%">
                      <? include("rightbar.php"); ?></td>
                      
                    </tr>
                    <tr > 
                      <td background="images/dotline.gif" height="1" colspan="3" valign="top"></td>
                    </tr>
                    <tr align="center" > 
                      <td  height="12" colspan="3" valign="top" class="tre12"></td>
                    </tr>
                    <tr align="center" > 
                      <td colspan="3"><? include("footer.php"); ?></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
