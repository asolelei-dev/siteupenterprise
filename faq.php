<?
error_reporting(0);
session_start();
include("admin/include/conn.inc");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site UPtime Enterprise</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="monitor_style.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="730" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="504" valign="top"><TABLE class=mt border=0 cellPadding=0 cellSpacing=0 width="480">
                          <TBODY>
                            <TR> 
                              <TD width="450" valign="top"> <table width="450" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                    <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                    <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                      </strong></font><font color="#006699">Frequently 
                                      Asked Questions</font></B></td>
                                  </tr>
                                  <tr > 
                                    <td background="images/dotline.gif" colspan="3"></td>
                                  </tr>
                                </table>
                                <P align="justify"> <br>
                                <table width="480" class="bodytext">
                                  <? $qry = $db->query("select * from tbl_faqs where status = 'Y'");
								  $i=1;
									while($res = mysqli_fetch_array($qry))
									{
								  ?>
                                  <tr> 
                                    <td align="right" valign="top" class="bodytext"><? echo $i; ?>.</td>
                                    <td align="left" class="me"> <a href="faq.php#<? echo $i; ?>" class="link"><? echo $res['problem']; ?></a> 
                                    </td>
                                  </tr>
                                  <tr> 
                                    <td height="5" colspan="2"></td>
                                  </tr>
                                  <? $i++;
								    } ?>
                                  <tr> 
                                    <td colspan="2" height="40">&nbsp;</td>
                                  </tr>
                                  <? $qry1 = $db->query("select * from tbl_faqs where status = 'Y'");
								  	$i=1;
									while($res1 = mysqli_fetch_array($qry1))
									{
								  ?>
                                  <tr> 
                                    <td align="right" valign="top" class="bodytext"> 
                                      <a name="<? echo $i; ?>"></a> <strong><? echo $i; ?>.&nbsp;</strong> 
                                    </td>
                                    <td valign="top" class="bodytext" align="left">	
                                      <strong><? echo $res1['problem']; ?></strong> 
                                      <br> <div align="justify"><? echo html_entity_decode($res1['solution']); ?></div></td>
                                  </tr>
                                  <tr> 
                                    <td colspan="2" align="right"><a class="link" href="#top">Back 
                                      To Top..</a></td>
                                  </tr>
                                  <tr> 
                                    <td height="1" background="images/dotline.gif" colspan="2"></td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                  </tr>
                                  <? $i++; } ?>
                                </table></TD>
                            </TR>
                          </TBODY>
                        </TABLE></td>
						<td align="right" valign="top" height="100%">
                      <? include("rightbar.php"); ?></td>
                      
                    </tr>
                    <tr > 
                      <td background="images/dotline.gif" height="1" colspan="3" valign="top"></td>
                    </tr>
                    <tr align="center" > 
                      <td  height="12" colspan="3" valign="top" class="tre12"></td>
                    </tr>
                    <tr align="center" > 
                      <td colspan="3"><? include("footer.php"); ?></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
