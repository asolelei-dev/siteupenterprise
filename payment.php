<? 
error_reporting(E_ALL ^ E_NOTICE);
include("admin/include/conn.inc");
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site Uptime Enterprise</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="monitor_style.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="730" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="504" valign="top"><TABLE class=formtext border=0 cellPadding=0 cellSpacing=0 width="480">
                          <TBODY>
                            <TR> 
                              <TD align="left" valign="top" class=body> <table width="450" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                    <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                    <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                      </strong></font><span class="c3">Premium 
                                      Service Payment</span></B></td>
                                  </tr>
                                  <tr background="images/dotline.gif" > 
                                    <td colspan="3"></td>
                                  </tr>
                                </table>
                                <P><B></B> </TD>
                            </TR>
                            <TR> 
                              <TD height="25" align="left" valign="top" class=body> 
                                <div align="left" class="formtext" style="margin-left:10px; margin-right:10px;"> 
                                  <p><br>
                                    Please use the links below to submit a PayPal 
                                    payment for the SiteMonitor Premium service 
                                    you would like to receive. Your account will 
                                    be upgraded within 24 hours. <br>
                                    <br>
                                    If you cannot use PayPal, you may submit a 
                                    <a class="link" href=payment_cc.php><u>credit 
                                    card</u></a> payment. If you have any questions, 
                                    please <a class="link" href='mailto:support@alstrasoft.com'><u>contact 
                                    us</u></a>. <br>
                                    <br>
                                    Monthly payment, Premium Account: $5.00 </p>
                                  <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                    <p class="bg4">
                                      <input type="hidden" name="cmd" value="_xclick-subscriptions">
                                      <input type="hidden" name="business" value="billing@alstrasoft.com">
                                      <input type="hidden" name="item_name" value="Premium Service">
                                      <input type="hidden" name="return" value="http://alstrasoft.com/uptime/payment_thanks.php">
                                      <input type="hidden" name="no_note" value="1">
                                      <input type="hidden" name="currency_code" value="USD">
                                      <input type="hidden" name="cancel_return" value="http://alstrasoft.com/uptime/payment.php">
                                      <input type="image" src="images/paypal_but.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
                                      <input type="hidden" name="a3" value="5.00">
                                      <input type="hidden" name="p3" value="1">
                                      <input type="hidden" name="t3" value="M">
                                      <input type="hidden" name="src" value="1">
                                      <input type="hidden" name="sra" value="1">
                                    </p>
                                  </form>
                                  Yearly payment, Premium Account: $50.00 
                                  <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                    <p class="bg3">
                                      <input type="hidden" name="cmd2" value="_xclick-subscriptions">
                                      <input type="hidden" name="business" value="support@alstrasoft.com">
                                      <input type="hidden" name="item_name" value="Premium Service">
                                      <input type="hidden" name="return" value="http://alstrasoft.com/uptime/payment_thanks.php">
                                      <input type="hidden" name="item_number" value="12">
                                      <input type="hidden" name="no_note" value="1">
                                      <input type="hidden" name="currency_code" value="USD">
                                      <input type="hidden" name="cancel_return" value="http://alstrasoft.com/uptime/payment.php">
                                      <input type="image" src="images/paypal_but.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
                                      <input type="hidden" name="a3" value="50.00">
                                      <input type="hidden" name="p3" value="12">
                                      <input type="hidden" name="t3" value="M">
                                      <input type="hidden" name="src" value="1">
                                      <input type="hidden" name="sra" value="1">
                                    </p>
                                  </form>
                                  Monthly payment, Premium Plus Account: $10.00 
                                  <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                    <p class="bg2">
                                      <input type="hidden" name="cmd2" value="_xclick-subscriptions">
                                      <input type="hidden" name="business" value="support@alstrasoft.com">
                                      <input type="hidden" name="item_name" value="SiteMonitor  Premium Service">
                                      <input type="hidden" name="return" value="http://alstrasoft.com/uptime/payment_thanks.php">
                                      <input type="hidden" name="no_note" value="1">
                                      <input type="hidden" name="currency_code" value="USD">
                                      <input type="hidden" name="cancel_return" value="http://alstrasoft.com/uptime/payment.php">
                                      <input type="image" src="images/paypal_but.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
                                      <input type="hidden" name="a3" value="10.00">
                                      <input type="hidden" name="p3" value="1">
                                      <input type="hidden" name="t3" value="M">
                                      <input type="hidden" name="src" value="1">
                                      <input type="hidden" name="sra" value="1">
                                    </p>
                                  </form>
                                  Yearly payment, Premium Account: $100.00 
                                  <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                    <p class="bg1">
                                      <input type="hidden" name="cmd2" value="_xclick-subscriptions">
                                      <input type="hidden" name="business" value="support@alstrasoft.com">
                                      <input type="hidden" name="item_name" value="SiteMonitor  Premium Service">
                                      <input type="hidden" name="return" value="http://alstrasoft.com/uptime/payment_thanks.php">
                                      <input type="hidden" name="item_number" value="12">
                                      <input type="hidden" name="no_note" value="1">
                                      <input type="hidden" name="currency_code" value="USD">
                                      <input type="hidden" name="cancel_return" value="http://alstrasoft.com/uptime/payment.php">
                                      <input type="image" src="images/paypal_but.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
                                      <input type="hidden" name="a3" value="100.00">
                                      <input type="hidden" name="p3" value="12">
                                      <input type="hidden" name="t3" value="M">
                                      <input type="hidden" name="src" value="1">
                                      <input type="hidden" name="sra" value="1">
                                    </p>
                                  </form>
                                </div></TD>
                            </TR>
                            <TR> 
                              <TD align="center" valign="top" class=body>&nbsp;</TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" class=body>&nbsp;</TD>
                            </TR>
                          </TBODY>
                        </TABLE></td>
						<td align="right" valign="top" height="100%">
                      <? include("rightbar.php"); ?></td>
                      
                    </tr>
                    <tr > 
                      <td background="images/dotline.gif" height="1" colspan="3" valign="top"></td>
                    </tr>
                    <tr align="center" > 
                      <td  height="12" colspan="3" valign="top" class="tre12"></td>
                    </tr>
                    <tr align="center" > 
                      <td colspan="3"><? include("footer.php"); ?></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
