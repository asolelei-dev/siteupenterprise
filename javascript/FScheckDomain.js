/*
PRODUCT: 	JavaScript Utilities for Email/Domain Validation
VERSION:	1.01, April 2004
CONTACT:	Future Shock Ltd, www.Future-Shock.net, post@future-shock.net

This file may be used freely as long as none of the comments are removed.

NOTES:
The optional CheckTLD argument on both functions will verify that the address ends in a two-letter country or well-known TLD.
This can be disabled by sending a value of false.
Both routines use the TITLE attribute of the form element in validity warning messages.
*/

var knownDomsPat=/^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum|co|uk)$/;

function validateDomain(FormField,NoWWW,CheckTLD) {
	// NoWWW and CheckTLD are optional, both accept values of true, false, and null.
	// NoWWW is used to check that a domain name does not begin with 'www.', eg. for WHOIS lokkups.
	DomainName=FormField.value.toLowerCase();
	if (CheckTLD==null) {CheckTLD=true}
	var specialChars="/\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var atom=validChars + '+';
	var atomPat=new RegExp("^" + atom + "$");
	var domArr=DomainName.split(".");
	var len=domArr.length;
	if (len==1)
	{
		FormField.focus();
		return false;
	}
	for (i=0;i<len;i++) {if (domArr[i].search(atomPat)==-1) {FormField.focus(); return false}}
	if ((CheckTLD) && (domArr[domArr.length-1].length!=2) && (domArr[domArr.length-1].search(knownDomsPat)==-1)) {FormField.focus(); return false}
	if ((NoWWW) && (DomainName.substring(0,4).toLowerCase()=="www.")) {FormField.focus(); return false}
	return true;
}
