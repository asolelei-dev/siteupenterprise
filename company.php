<?
error_reporting(0);
include("admin/include/conn.inc");
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site Uptime Enterprise</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="monitor_style.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="730" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="504" valign="top"><table class=bodytext width="480" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td><div align="justify"> 
                                <table width="450" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                    <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                    <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                      </strong></font><font color="#006699">Company</font></B></td>
                                  </tr>
                                  <tr > 
                                    <td background="images/dotline.gif" colspan="3"></td>
                                  </tr>
                                </table>
                               
                                <br>
                                <table width="480" border="0" cellpadding="5" cellspacing="2" class="bodytext border">
                                  <tr>
                                    <td bgcolor="#F2F9FF" class="lineheight"><div align="justify">Site Uptime Enterprise
                                        provides website owners with a free service 
                                        for monitoring their website availability. 
                                        Our services are designed to provide any 
                                        webmaster or website owner with a fast, 
                                        effective and automated method for ensuring 
                                        that their website's are active and working 
                                        properly. Our services are free to registered 
                                        users. </div></td>
                                  </tr>
                                </table>
                                <p class="lineheight" style="margin-right:10px"><br>
                                </p>
                                <table width="450" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                    <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                    <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                      </strong></font><font color="#006699">Mission 
                                      Statement </font></B></td>
                                  </tr>
                                  <tr > 
                                    <td background="images/dotline.gif" colspan="3"></td>
                                  </tr>
                                </table>

                                <br>
                                <table width="480" border="0" cellpadding="5" cellspacing="2" class="bodytext border">
                                  <tr> 
                                    <td bgcolor="#FFFBEC" class="lineheight"><div align="justify"><span class="lineheight">Our 
                                        mission is to provide the best possible 
                                        website monitoring services for small 
                                        and medium sized businesses on the Internet. 
                                        </span><br>
                                        <br>
                                        <a class=xyz href="services.php"><font color="#FF6600">&#8226;</font> 
                                        <span class="c2"><strong>Service Overview</strong></span></a> 
                                        <br>
                                        <a class=xyz href="privacy.php"><font color="#FF6600">&#8226;</font> 
                                        <strong><span class="c2">Privacy Policy</span></strong></a> 
                                        <br>
                                        <a class=xyz href="terms.php"><font color="#FF6600">&#8226;</font> 
                                        <strong><span class="c2">Terms of Service</span></strong></a> 
                                        <br>
                                        <a class=xyz href="testimonials.php"><font color="#FF6600">&#8226;</font> 
                                        <strong><span class="c2">Client Testimonials</span></strong></a> 
                                        <br>
                                        <a class=xyz href="partnership.php"><font color="#FF6600">&#8226;</font> 
                                        <strong><span class="c2">Partnership Opportunities</span></strong></a> 
                                        <br>
                                        <a class=xyz href="contact.php"><font color="#FF6600">&#8226;</font> 
                                        <strong><span class="c2">Contact Us</span></strong></a> 
                                      </div></td>
                                  </tr>
                                </table>
                                <p style="margin-right:10px">&nbsp; </p>
                              </div></td>
                          </tr>
                        </table></td>
						<td align="right" valign="top" height="100%">
                      <? include("rightbar.php"); ?></td>
                      
                    </tr>
                    <tr > 
                      <td background="images/dotline.gif" height="1" colspan="3" valign="top"></td>
                    </tr>
                    <tr align="center" > 
                      <td  height="12" colspan="3" valign="top" class="tre12"></td>
                    </tr>
                    <tr align="center" > 
                      <td colspan="3"><? include("footer.php"); ?></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
