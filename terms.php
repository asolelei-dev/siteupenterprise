<?
include("admin/include/conn.inc");
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site Uptime Enterprise</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="monitor_style.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="730" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="504" valign="top"><TABLE class=bodytext border=0 cellPadding=0 cellSpacing=0 width="480">
                          <TBODY>
                            <TR> 
                              <TD align="left" valign="top" class=body> <table width="450" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                    <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                    <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                      </strong></font><span class="c3">Terms of 
                                      Services</span></B></td>
                                  </tr>
                                  <tr  > 
                                    <td background="images/dotline.gif" colspan="3"></td>
                                  </tr>
                                </table>
                                <P><B></B> 
                                <p align="justify" style="margin-left:10px;margin-right:10px;"> 
                                  By registering with Site Uptime Enterprise, you agree to 
                                  these Terms of Service. </p>
                                <table width="450" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                    <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                    <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                      </strong></font><span class="c3">Free Service</span></B></td>
                                  </tr>
                                  <tr > 
                                    <td background="images/dotline.gif"  colspan="3"></td>
                                  </tr>
                                </table>
                                <P><B></B> 
                                <p align="justify" style="margin-left:10px;margin-right:10px;"> 
                                  As a user of our free monitoring service, you 
                                  agree not to register for more than one account 
                                  and you agree to link to Site Uptime Enterprise from 
                                  the site you are are monitoring. In addition, 
                                  you agree to receive occasional service announcements 
                                  via email. </p>
                                <table width="450" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                    <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                    <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                      </strong></font><span class="c3">All Services</span></B></td>
                                  </tr>
                                  <tr > 
                                    <td background="images/dotline.gif"  colspan="3"></td>
                                  </tr>
                                </table>
                                <p><br>
                                <p align="justify" style="margin-left:10px;margin-right:10px;"> 
                                  By activating a service monitor, you agree that 
                                  you own or have written authorization to use 
                                  our service from the legal owner of the URL(s) 
                                  or IP address(es) to be monitored by the Site Uptime Enterprise. 
                                </p>
                                <p style="margin-left:10px;margin-right:10px;" align="justify"> 
                                  Our service shall only be used in accordance 
                                  with any and all applicable laws and regulations. 
                                </p>
                                <table width="450" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                    <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                    <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                      </strong></font><span class="c3">Disclaimer</span></B></td>
                                  </tr>
                                  <tr > 
                                    <td background="images/dotline.gif"  colspan="3"></td>
                                  </tr>
                                </table>
                                <p style="margin-left:10px;margin-right:10px;" align="justify"> 
                                  THIS SERVICE IS PROVIDED &quot;AS IS&quot; AND 
                                  ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, 
                                  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
                                  MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
                                  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL WE 
                                  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
                                  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
                                  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
                                  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
                                  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
                                  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
                                  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
                                  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
                                  IN ANY WAY OUT OF THE USE OF THIS SERVICE, EVEN 
                                  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
                                </p>
                                <table width="450" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                    <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                    <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                      </strong></font><span class="c3">Termination 
                                      of Service</span></B></td>
                                  </tr>
                                  <tr > 
                                    <td background="images/dotline.gif"  colspan="3"></td>
                                  </tr>
                                </table>
                                <p align="justify" style="margin-left:10px;margin-right:10px;">We 
                                  reserve the right to terminate service for any 
                                  customer at any time, at our sole discretion. 
                                </p>
                                <p align="justify" style="margin-left:10px;margin-right:10px;">If 
                                  you have any questions regarding these Terms 
                                  of Service, please contact <a href="mailto: support@alstrasoft.com" class="link">support@alstrasoft.com</a>. 
                                  <br>
                                </p>
                                <P align="justify" style="margin-left:10px;margin-right:10px;"><br>
                              </TD>
                            </TR>
                          </TBODY>
                        </TABLE></td>
						<td align="right" valign="top" height="100%">
                      <? include("rightbar.php"); ?></td>
                      
                    </tr>
                    <tr > 
                      <td background="images/dotline.gif" height="1" colspan="3" valign="top"></td>
                    </tr>
                    <tr align="center" > 
                      <td  height="12" colspan="3" valign="top" class="tre12"></td>
                    </tr>
                    <tr align="center" > 
                      <td colspan="3"><? include("footer.php"); ?></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
