<?
include("admin/include/conn.inc");
session_start();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site Uptime Enterprise</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="monitor_style.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="730" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="504" valign="top"><TABLE class=mt border=0 cellPadding=0 cellSpacing=0 width="379">
                          <TBODY>
                            <TR> 
                              <TD align="left" valign="top" class=body> <table width="450" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                    <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                    <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                      </strong></font><span class="c3">Web Development 
                                      Resources </span></B></td>
                                  </tr>
                                  <tr background="images/dotline.gif" > 
                                    <td colspan="3"></td>
                                  </tr>
                                </table>
                                <P><br>
                                  <br>
                                  <? 
								$qry = $db->query("select * from tbl_resources where status = 'Y' ");
								while($res = mysqli_fetch_array($qry))
								{  ?>
                                  <a class=kk href="<? echo $res['url']; ?>"><font color="#FF6600">&#8226;&nbsp;&nbsp;</font><? echo $res['title']; ?></a><br>
                                <div class=mt align="justify" style="margin-left=20px;margin-right=15px;"> 
                                  <? echo html_entity_decode($res['description'])."<br><br>"; ?></div>
                                <div class='m' align="right"><a href="#top" class="kk">Back 
                                  To Top..</a></div>
                                <br> 
                                <? } ?>
                              </TD>
                            </TR>
                          </TBODY>
                        </TABLE></td>
						<td align="right" valign="top" height="100%">
                      <? include("rightbar.php"); ?></td>
                      
                    </tr>
                    <tr > 
                      <td background="images/dotline.gif" height="1" colspan="3" valign="top"></td>
                    </tr>
                    <tr align="center" > 
                      <td  height="12" colspan="3" valign="top" class="tre12"></td>
                    </tr>
                    <tr align="center" > 
                      <td colspan="3"><? include("footer.php"); ?></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
