<?
	error_reporting(0);
	include("admin/include/conn.inc");
	session_start();
	
	$time_zone = 0.0;
	$qry_tz = $db->query("select * from tbl_timezones");
	while($res_tz = mysqli_fetch_array($qry_tz))
	{
		$time_zone = $res_tz['tzval'];
		$nMin = $res_tz['tzval'] * 60;
		$prdt = strtotime(date('Y-m-d H:i:s',mktime(gmdate('H'),gmdate('i')+$nMin,gmdate('s'),gmdate('m'),gmdate('d'),gmdate('Y'))));
		if(date('H',$prdt) == 0 && date('i',$prdt) >= 0  && date('i',$prdt) < 15)
		{	
			$count++;	break;
		}
	}
	
	if( $count > 0)
	{
		$da2 = date('Y-m-d H:i:s',mktime(gmdate('H'),gmdate('i'),gmdate('s'),gmdate('m'),gmdate('d'),gmdate('Y')));
		$da1 = date('Y-m-d H:i:s',mktime(gmdate('H'),gmdate('i'),gmdate('s'),gmdate('m'),gmdate('d')-1,gmdate('Y')));
		
		$sql_service = "select s.service_id,s.location from tbl_services s, tbl_users u where u.timezone = $time_zone and s.active = 'Y'";
		$qry_service = $db->query($sql_service);
		
		if(mysqli_num_rows($qry_service)>0)
		{
			while($res_service=mysqli_fetch_array($qry_service))
			{
		 // echo "select * from tbl_dayservices where service_id=$res_service[service_id] and time_of_check between '$da1' and '$da2' order by time_of_check ";
			$qry_day = $db->query("select * from tbl_dayservices where service_id=$res_service[service_id] and time_of_check between '$da1' and '$da2' order by time_of_check ");
			if(mysqli_num_rows($qry_day)>0)
			{	
				$start_time='';
				$last_time='';
				$no_of_checks = 0;
				$down_checks = 0;
				$resp_time = 0;
				$date_of_servie='';
				while($res_day=mysqli_fetch_array($qry_day))
				{
					$date = explode(' ',$res_day['time_of_check']);
					$last_time = $date[1]; // time
					if($start_time == '')
					{
					$date = explode(' ',$res_day['time_of_check']);
					$last_time = $start_time = $date[1]; // time
					$date_of_service = $date[0]; // date
					}
					
					if($res_day['result'] == 'N')
					$down_checks++;
					
					$no_of_checks++;
					$resp_time+=$res_day['resp_time'];
						
				}//while($res_day=mysqli_fetch_array($qry_day))
		
				$resp_time = number_format($resp_time/$no_of_checks, 4, '.', '');
				
				$db->query("insert into tbl_dailyservices (service_id,date_of_service,location_id,no_of_checks,down_checks,start_time,end_time,resp_time) values ($res_service[service_id],'$date_of_service',$res_service[location],$no_of_checks,$down_checks,'$start_time','$last_time',$resp_time)");
				$db->query("update tbl_services set last_check = '".gmdate('Y-m-d H:i:s')."' where service_id = $res_service[service_id]");
							
			}//if(mysqli_num_rows($qry_day)>0)
			} // while($res_service=mysqli_fetch_array($qry_service))
			} //	if(mysqli_num_rows($qry_service)>0)
		
		// for monthly summary
		
		$dt = date('Y-m-d H:i:s',mktime(gmdate('H'),gmdate('i')+$nMin,gmdate('s'),gmdate('m'),gmdate('d'),gmdate('Y')));
		if(date('H',$prdt) == 0 && date('i',$prdt) >= 0  && date('i',$prdt) < 15 && date('d',$prdt) == 1)
		{
			$da1 = date('Y-m-d',mktime(gmdate('H'),gmdate('i'),gmdate('s'),gmdate('m'),1,gmdate('Y')));
			$da2 = date('Y-m-d',mktime(gmdate('H'),gmdate('i'),gmdate('s'),gmdate('m')+1,1,gmdate('Y')));
				
				$qry_service = $db->query($sql_service);
				if(mysqli_num_rows($qry_service)>0)
				{
					while($res_service=mysqli_fetch_array($qry_service))
					{
							//echo "select * from tbl_dailyservices where service_id=$res_service[service_id] and date_of_service between '$da1' and '$da2'";
							$qry_day = $db->query("select * from tbl_dailyservices where 	service_id=$res_service[service_id] and date_of_service between '$da1' and '$da2'");
							if(mysqli_num_rows($qry_day)>0)
							{	
								$date_of_service = date('Y-m-d',strtotime($dt)); // date
								$no_of_checks = 0;
								$down_checks = 0;
								$resp_time = 0;
								//$date_of_servie='';
								$count = 0;
								while($res_day=mysqli_fetch_array($qry_day))
								{
									$down_checks += $res_day['down_checks'];
									$no_of_checks += $res_day['no_of_checks'];
									$resp_time += $res_day['resp_time'];
									$count++;
									
								}//while($res_day=mysqli_fetch_array($qry_day))
					
								$resp_time = number_format($resp_time/$count, 4, '.', '');
								$db->query("insert into tbl_monthlyservices (service_id,date_of_service,no_of_checks,down_checks,resp_time) values ($res_service[service_id],'$date_of_service',$no_of_checks,$down_checks,$resp_time)");
							
								
							
						} // if
					} // while($res_service=mysqli_fetch_array($qry_service))
				} //	if(mysqli_num_rows($qry_service)>0)	
		} // if	
		
		// monthly report 
		
		$dt = date('Y-m-d H:i:s',mktime(gmdate('H'),gmdate('i')+$nMin,gmdate('s'),gmdate('m'),gmdate('d'),gmdate('Y')));
		if(date('H',$prdt) == 0 && date('i',$prdt) >= 0  && date('i',$prdt) < 15 && date('d',$prdt) == 1)
		{
			$da1 = date('Y-m-d',mktime(gmdate('H'),gmdate('i'),gmdate('s'),gmdate('m'),1,gmdate('Y')));
			$da2 = date('Y-m-d',mktime(gmdate('H'),gmdate('i'),gmdate('s'),gmdate('m')+1,1,gmdate('Y')));
				
				
					// for service ports
					$qry_port = $db->query("select * from tbl_service_ports");
					
					$i=0;
					while($res_port=mysqli_fetch_array($qry_port))
					{
						$service_ports[$i][0] = $res_port['service_id'];
						$service_ports[$i][1] = $res_port['service_name'];
						
						$i++;
					}
					
					$count = count($service_ports);
					//print_r($service_ports);
					
					// for location
					$qry_loc = $db->query("select * from tbl_locations");
					
					$i=0;
					while($res_loc=mysqli_fetch_array($qry_loc))
					{
						$service_location[$i][0] = $res_loc['loc_id'];
						$service_location[$i][1] = $res_loc['loc_name'];
						$service_location[$i][2] = $res_loc['loc_country'];
						
						$i++;
					}
					$count_loc = count($service_location);
					//print_r($service_location);
					
					// for check periods
					$qry_period = $db->query("select * from tbl_checkperiods");
					
					$i=0;
					while($res_period=mysqli_fetch_array($qry_period))
					{
						$service_period[$i][0] = $res_period['pid'];
						$service_period[$i][1] = $res_period['period'];
						
						$i++;
					}
					
					$count_period = count($service_period);

				$qry_service = $db->query("select u.userid,name,emailid,memtype,service_id,host_name,service_port,custom_port,period,location from tbl_services s, tbl_users u,tbl_membership m where s.userid = u.userid  and u.report = 'Y' and m.m_id = u.memtype and m.monthly_reports = 'Y' and u.timezone = $time_zone");
			
				if(mysqli_num_rows($qry_service)>0)
				{
					while($res_service=mysqli_fetch_array($qry_service))
					{
						
						$qry_day = $db->query("select * from tbl_dailyservices where 	service_id=$res_service[service_id] and date_of_service between '$da1' and '$da2'");
							if(mysqli_num_rows($qry_day)>0)
							{	
								$date_of_service = date('Y-m-d',strtotime($dt)); // date
								$no_of_checks = 0;
								$down_checks = 0;
								$resp_time = 0;
								//$date_of_servie='';
								$count = 0;
								while($res_day=mysqli_fetch_array($qry_day))
								{
									$down_checks += $res_day['down_checks'];
									$no_of_checks += $res_day['no_of_checks'];
									$resp_time += $res_day['resp_time'];
									$count++;
									
								}//while($res_day=mysqli_fetch_array($qry_day))
					
								$resp_time = number_format($resp_time/$count, 4, '.', '');
								// port
								if($res_service['custom_port'] != 0)
									$port = $res_service['custom_port'];
								else
								{
									for($i=0;$i<$count;$i++)
									if($service_ports[$i][0] == $res_service['service_port'])
									{
										$port = $service_ports[$i][1];
										break;
									}
								}
								
								// location
								for($i=0;$i<$count_loc;$i++)
									if($service_location[$i][0] == $res_service['location'])
									{
										$location = $service_location[$i][1];
										$country = $service_location[$i][2];
										break;
									}
									
								// check periods
								for($i=0;$i<$count_period;$i++)
									if($service_period[$i][0] == $res_service['period'])
									{
										$period = $service_period[$i][1];
										break;
									}
								
								
								$message = "";
								$message.="<html><head><style>.kk{font-family: trebuchet ms; color: #336699; font-size: 12px; 	font-style: normal;";
								$message.= "font-weight: normal; font-variant: normal; text-decoration: none; }";
								$message.= ".bt {font-family: trebuchet ms; font-size: 12px; color: black; } </style></head><body>";
								$message.="<table class=kk border=0 cellspacing='0' cellpadding='0' width='70%' align='center'><tr><td colspan=3 align=left><a href='http://www.alstrasoft.com/uptime' target=_blank><img border=0 src='http://www.alstrasoft.com/uptime/images/logo_247.gif'></td></tr>";
								$message.="<tr><td colspan=3>&nbsp;</td></tr>";
								$message.="<tr><td colspan=3 align=left>Dear <strong>$res_service[name]</strong>,</td></tr>";
								$message.="<tr><td colspan=3 align=center><strong>This is your site Monthly Report for ".gmdate('F',strtotime($da1)).",<br> Details are follows</strong></td></tr>";
								$message.="<tr><td colspan=3>&nbsp;</td></tr>";
								$message.="<tr><td align=right>Host Name</td><td align=center>&nbsp;:&nbsp;</td><td align=left>$res_service[host_name]</td></tr>";
								$message.="<tr><td align=right>Service Port</td><td align=center>&nbsp;:&nbsp;</td><td align=left>$port</td></tr>";
								$message.="<tr><td align=right>Monitoring Location</td><td align=center>&nbsp;:&nbsp;</td><td align=left>$location</td></tr>";
								$message.="<tr><td align=right>Country</td><td align=center>&nbsp;:&nbsp;</td><td align=left>$country</td></tr>";
								$message.="<tr><td align=right>Check Interval</td><td align=center>&nbsp;:&nbsp;</td><td align=left>$period Minutes</td></tr>";
								$message.="<tr><td align=right>Site Uptime</td><td align=center>&nbsp;:&nbsp;</td><td align=left>".number_format((($no_of_checks-$down_checks)*100/$no_of_checks),2,'.','')." %</td></tr>";
								$message.="<tr><td align=right>Site Downtime</td><td align=center>&nbsp;:&nbsp;</td><td align=left>".number_format(($down_checks*100/$no_of_checks),2,'.','')." %</td></tr>";
								$message.="<tr><td align=right>Outages</td><td align=center>&nbsp;:&nbsp;</td><td align=left>$down_checks</td></tr>";
								$message.="<tr><td align=right>Avg. Response Time</td><td align=center>&nbsp;:&nbsp;</td><td align=left>$resp_time Seconds</td></tr>";
								$message.="<tr><td>&nbsp;</td></tr>";
								$message.="<tr><td colspan=3 align=left>Thank You.</td></tr>";
								$message.="<tr><td align=center colspan=3>&nbsp;</td></tr></table>";
				
								 //echo $message;
								
								
								
									$strHeaders  = "MIME-Version: 1.0\r\n";
									$strHeaders .= "Content-type: text/html; charset=iso-8859-1\r\n";
									$strHeaders .= "From: monitor@alstrasoft.com";
									//$strTo = $res_service['emailid'];
										$rs_TempUser = mysqli_fetch_array($db->query("select * from tbl_users where userid = $res_service[userid]"));
										$mails = array();
										$array_mails = array();
										
										$array_mails[0] = trim($rs_TempUser['aemail']);
										$array_mails[1] = trim($rs_TempUser['aemail1']);
										$array_mails[2] = trim($rs_TempUser['aemail2']);
										$array_mails[3] = trim($rs_TempUser['aemail3']);
										$array_mails[4] = trim($rs_TempUser['aemail4']);
										
										$i = 0;
										foreach ($array_mails as $index=>$value)
											if($value != "")
											$mails[$i++]  = $value;
				
											$mails = array_unique($mails);
				
										$strTo = implode(',',$mails);
									
									$strSub = "Monthly Report for ".gmdate('F',strtotime($da1))." from Site UPtime Enterprise";
									@mail($strTo, $strSub , $message, $strHeaders);
									//echo $message;
						} // if
					} // while($res_service=mysqli_fetch_array($qry_service))
				} //	if(mysqli_num_rows($qry_service)>0)	
		} // if
			
	} // count	
?>