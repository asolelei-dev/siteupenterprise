<?
ob_start();
session_start();
error_reporting(0);
include("admin/include/conn.inc");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site Uptime Enterprise</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="monitor_style.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--



function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
<style type="text/css">
<!--
.style1 {
	color: #FF6600;
	font-weight: bold;
}
.style3 {color: #666666}
-->
</style>
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="736" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="546" valign="top"><table width="490" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td valign="top"><table width="500" height="100%" border="0" cellpadding="0" cellspacing="0" class="tre11">
                                <tr> 
                                  <td valign="top"><img src="images/isyoursite.gif" width="192" height="37"></td>
                                  <td>&nbsp;</td>
                                  <td width="189" valign="middle" class="tre11" style="padding-left:10px"><span class="style1" ><img src="images/monitor_icon.gif" width="19" height="19" align="absmiddle">Site 
                                    Monitoring Protocols</span></td>
                                </tr>
                                <tr > 
                                  <td background="images/hor_line.gif" height="1" colspan="3"></td>
                                </tr>
                                <tr> 
                                  <td height="8" valign="top"></td>
                                  <td height="8" background="images/vert_line.gif"></td>
                                  <td height="8" align="center" valign="top" class="tre11" ></td>
                                </tr>
                                <tr> 
                                  <td valign="top"><div align="justify"> 
                                      <table width="95%"  border="0" cellpadding="0" cellspacing="0" class="tre11">
                                        <tr> 
                                          <td valign="top"> <div align="justify"> 
                                              <p style="line-height:20px">Site Uptime Enterprise is a free website monitoring 
                                                service that watches your website 
                                                24 x 7 x 365 and notifies you 
                                                if your becomes unavailable. Website 
                                                downtime can mean lost revenue 
                                                and profits. In addition, users 
                                                to your website may not return 
                                                if they experience a connection 
                                                failure. Register for an account 
                                                today and let Site Uptime Enterprise 
                                                monitor your website for free.<br>
                                              </p>
                                            </div></td>
                                        </tr>
                                        <tr> 
                                          <td align="right"><img src="images/tip3.gif" width="6" height="6"> 
                                            <a href="register.php" class="tre12"><font color="20607C">read 
                                            more&nbsp;&nbsp;</font></a></td>
                                        </tr>
                                      </table>
                                    </div></td>
                                  <td background="images/vert_line.gif">&nbsp;</td>
                                  <td align="center" valign="top" class="tre11" ><table width="90%"  border="0" cellspacing="0" cellpadding="3">
                                      <tr> 
                                        <td class="tre11"><img src="images/tip3.gif" width="3" height="6" align="absmiddle"> 
                                          HTTP : Web Server </td>
                                      </tr>
                                      <tr> 
                                        <td class="tre11"><img src="images/tip3.gif" width="3" height="6" align="absmiddle"> 
                                          POP3 : Email Server </td>
                                      </tr>
                                      <tr> 
                                        <td class="tre11"><img src="images/tip3.gif" width="3" height="6" align="absmiddle"> 
                                          SMTP : Outgoing Email Server </td>
                                      </tr>
                                      <tr> 
                                        <td class="tre11"><img src="images/tip3.gif" width="3" height="6" align="absmiddle"> 
                                          FTP : File Transfer Protocol </td>
                                      </tr>
                                      <tr> 
                                        <td class="tre11"> <img src="images/tip3.gif" width="3" height="6" align="absmiddle"> 
                                          SSL : Secure Socket Layer </td>
                                      </tr>
                                      <tr> 
                                        <td class="tre11"><img src="images/tip3.gif" width="3" height="6" align="absmiddle"> 
                                          Custom Ports</td>
                                      </tr>
                                      <tr> 
                                        <td class="tre11"><img src="images/tip3.gif" width="3" height="6" align="absmiddle"> 
                                          Ping </td>
                                      </tr>
                                    </table></td>
                                </tr>
                                <tr> 
                                  <td valign="top"><table width="95%"  border="0" cellpadding="0" cellspacing="0" class="tre11">
                                      <tr> 
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr> 
                                        <td><img src="images/howitworks.gif" width="123" height="30"></td>
                                      </tr>
                                      <tr> 
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr> 
                                        <td> <div align="justify"> 
                                            <p style="line-height:20px">Multiple 
                                              monitoring servers around the world 
                                              run protocol based tests on your 
                                              website at specific intervals (5, 
                                              15, 30 or 60 minutes) 24 hours per 
                                              day, 7 days per week, 365 days per 
                                              year to insure that your customers 
                                              and users can reach your website. 
                                              If more than one location detects 
                                              a connection failure, an email alert 
                                              is sent to you. When your website 
                                              is active again, you are notified 
                                              as well. <br>
                                              <br>
                                              Our software tracks these failures 
                                              and logs detailed reports of the 
                                              uptime/downtime of your website 
                                              each month. These reports are available 
                                              to you to browse via an account 
                                              control panel. <br>
                                            </p>
                                          </div></td>
                                      </tr>
                                      <tr> 
                                        <td align="right"><img src="images/tip3.gif" width="6" height="6"> 
                                          <a href="services.php" class="tre12"><font color="20607C">read 
                                          more&nbsp;&nbsp;</font></a></td>
                                      </tr>
                                    </table></td>
                                  <td width="3" background="images/vert_line.gif">&nbsp;</td>
                                  <td align="center" valign="top" class="tre11"><table width="96%"  border="0" cellpadding="0" cellspacing="0" class="tre11">
                                      <tr> 
                                        <td><img src="images/advancedfeatures.gif" width="175" height="52"></td>
                                      </tr>
                                      <tr> 
                                        <td><p style="line-height:26px; color: #666666;">In 
                                            additon to our free service, Site Uptime Enterprise offers advanced website monitoring 
                                            solutions for a low monthly fee. Compare 
                                            all of our monitoring services</p></td>
                                      </tr>
                                      <tr> 
                                        <td align="right"><img src="images/tip3.gif" width="6" height="6"> 
                                          <a href="compareservices.php" class="tre12"><font color="20607C">read 
                                          more</font></a></td>
                                      </tr>
                                      <tr> 
                                        <td align="right">&nbsp;</td>
                                      </tr>
                                      <tr> 
                                        <td align="right">&nbsp;</td>
                                      </tr>
                                    </table>
                                    <br></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                      <td width="230" align="center" valign="top"><? include("rightbar.php"); ?></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td height="1" align="center" background="images/hor_line.gif"></td>
              </tr>
              <tr> 
                <td align="center">&nbsp;</td>
              </tr>
              <tr> 
                <td align="center" valign="top"><? include("footer.php"); ?></td>
              </tr>
             </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
