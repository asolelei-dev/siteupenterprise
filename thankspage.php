<?
include("admin/include/conn.inc");
session_start();
error_reporting(0);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site Uptime Enterprise</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="monitor_style.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="730" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="504" valign="top"><table class=mt width="480" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td colspan="2">&nbsp; </td>
                          </tr>
                          <tr> 
                            <td valign="top"><div align="justify"> 
                                <p style="margin-right:10px;margin-left:15px;"> 
                                  <br>
                                  <span class="lineheight">Thank you for registering 
                                  with Site Uptime Enterprise. You will receive an email shortly 
                                  requesting that you confirm your registration. 
                                  If you do not receive the email within 10 minutes, 
                                  Please <a class=link href="contact.php">contact 
                                  us</a>. </span><br>
                                </p>
                              </div></td>
                            <td valign="top"><img src="images/thku.gif" width="118" height="97"></td>
                          </tr>
                          <tr> 
                            <td colspan="2" valign="top">&nbsp;</td>
                          </tr>
                        </table></td>
						<td align="right" valign="top" height="100%">
                      <? include("rightbar.php"); ?></td>
                      
                    </tr>
                    <tr > 
                      <td background="images/dotline.gif" height="1" colspan="3" valign="top"></td>
                    </tr>
                    <tr align="center" > 
                      <td  height="12" colspan="3" valign="top" class="tre12"></td>
                    </tr>
                    <tr align="center" > 
                      <td colspan="3"><? include("footer.php"); ?></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
