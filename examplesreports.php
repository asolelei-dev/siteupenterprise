<?
session_start();
include("admin/include/conn.inc");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site UPtime Enterprise</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="monitor_style.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="730" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="504" valign="top"><TABLE class=formtext border=0 cellPadding=0 cellSpacing=0 width="480">
                          <TBODY>
                            <TR> 
                              <TD align="left" valign="top" class=body> <table width="450" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                    <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                    <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                      </strong></font><font color="#006699">Example 
                                      Uptime Records</font></B></td>
                                  </tr>
                                  <tr > 
                                    <td background="images/dotline.gif" colspan="3"></td>
                                  </tr>
                                </table>
                                <P><B></B> </TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" class=body> <div align="justify"><br>
                                  <strong>Below</strong> is an example of the 
                                  &quot;My Services&quot; section in the user 
                                  control panel. You are able to add/delete/modify 
                                  various monitoring services here. <br>
                                  <br>
                                </div></TD>
                            </TR>
                            <TR> 
                              <TD align="center" valign="top" class=body><table class=mt width="450" style="border:1px solid #003366" cellspacing="1" cellpadding="0">
                                  <tr> 
                                    <td width="339" height="20" class="bbg5"><strong><font color="#006666">&nbsp;<span class="c4">My 
                                      Services</span></font></strong></td>
                                  </tr>
                                  <tr> 
                                    <td align="left" valign="top"><table class=bodytext width="450" border="0" cellspacing="1" cellpadding="0">
                                        <tr bgcolor="#E1F0F4" class="bg4"> 
                                          <td width="100" height="20"><strong><font color="#006666">&nbsp;</font>Host 
                                            Name</strong></td>
                                          <td width="60"><strong>Service</strong></td>
                                          <td width="80"><strong>Last Check</strong></td>
                                          <td align="center"><strong>Action</strong></td>
                                        </tr>
                                        <tr> 
                                          <td height="20"><strong><font color="#006666">&nbsp;</font></strong>yoursite.net</td>
                                          <td>http</td>
                                          <td>4/8/2003</td>
                                          <td height="80" rowspan="4" valign="top"><table class=mt width="100%" border="0" cellspacing="1" cellpadding="0">
                                              <tr align="center"> 
                                                <td width="25%" height="20" class="link"><a class=link href="examplesreports.php">Edit</a></td>
                                                <td width="30%"><a class=link href="examplesreports.php" onclick="return confirm('Are you sure to delete this user service?');"> 
                                                  Delete</a></td>
                                                <td><a class=link href="examplesreports.php#details">Statistics</a></td>
                                              </tr>
                                              <tr align="center"> 
                                                <td width="25%" height="20" class="link"><a class=link href="examplesreports.php">Edit</a></td>
                                                <td width="30%"><a class=link href="examplesreports.php" onclick="return confirm('Are you sure to delete this user service?');"> 
                                                  Delete</a></td>
                                                <td><a class=link href="examplesreports.php#details">Statistics</a></td>
                                              </tr>
                                              <tr align="center"> 
                                                <td width="25%" height="20" class="link"><a class=link href="examplesreports.php">Edit</a></td>
                                                <td width="30%"><a class=link href="examplesreports.php" onclick="return confirm('Are you sure to delete this user service?');"> 
                                                  Delete</a></td>
                                                <td><a class=link href="examplesreports.php#details">Statistics</a></td>
                                              </tr>
                                              <tr align="center"> 
                                                <td width="25%" height="20" class="link"><a class=link href="examplesreports.php">Edit</a></td>
                                                <td width="30%"><a class=link href="examplesreports.php" onclick="return confirm('Are you sure to delete this user service?');"> 
                                                  Delete</a></td>
                                                <td><a class=link href="examplesreports.php#details">Statistics</a></td>
                                              </tr>
                                            </table></td>
                                        </tr>
                                        <tr> 
                                          <td height="20"><strong><font color="#006666">&nbsp;</font></strong>domainname.com</td>
                                          <td>http</td>
                                          <td>4/8/2003</td>
                                        </tr>
                                        <tr> 
                                          <td height="20"><strong><font color="#006666">&nbsp;</font></strong>www.mysite.net</td>
                                          <td>http</td>
                                          <td>4/8/2003</td>
                                        </tr>
                                        <tr> 
                                          <td height="20"><strong><font color="#006666">&nbsp;</font></strong>emailserver.net</td>
                                          <td>pop</td>
                                          <td>4/8/2003</td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                  <tr> 
                                    <td height="20" bgcolor="#E1F0F4" class="bg3"><a class=link href="users/index.php"><strong><font color="#006666">&nbsp;</font></strong>Add</a></td>
                                  </tr>
                                </table></TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" class=body>&nbsp;</TD>
                            </TR>
                            <TR> 
                              <TD height="55" align="left" valign="top" class=body><div align="justify"><strong>Below</strong> 
                                  is am example of the &quot;My Statistics&quot; 
                                  section in the user control panel. You are able 
                                  to check details/summary information for each 
                                  of your various monitoring services here. </div></TD>
                            </TR>
                            <TR> 
                              <TD align="center" valign="top" class=body><table class=mt width="450" style="border:1px solid #336666" cellspacing="1" cellpadding="0">
                                  <tr> 
                                    <td width="339" height="20" class="bbg5"><strong><font color="#006666">&nbsp;<span class="c4">My 
                                      Statistics</span></font></strong></td>
                                  </tr>
                                  <tr> 
                                    <td align="left" valign="top"><table class=bodytext width="450" border="0" cellspacing="1" cellpadding="0">
                                        <tr bgcolor="#E1F0F4" class="bg4"> 
                                          <td width="100" height="20"><strong><font color="#006666">&nbsp;</font>Host 
                                            Name</strong></td>
                                          <td width="60"><strong>Service</strong></td>
                                          <td width="80"><strong>Last Check</strong></td>
                                          <td align="center">&nbsp;</td>
                                        </tr>
                                        <tr> 
                                          <td height="20"><strong><font color="#006666">&nbsp;</font></strong>yoursite.net</td>
                                          <td>http</td>
                                          <td>4/8/2003</td>
                                          <td height="80" rowspan="4" valign="top"><table class=mt width="100%" border="0" cellspacing="1" cellpadding="0">
                                              <tr align="center"> 
                                                <td width="50%" height="20"><a class=link href="examplesreports.php#summary">Summary</a></td>
                                                <td><a class=link href="examplesreports.php#details">Details</a></td>
                                              </tr>
                                              <tr align="center"> 
                                                <td width="50%" height="20"><a class=link href="examplesreports.php#summary">Summary</a></td>
                                                <td><a class=link href="examplesreports.php#details">Details</a></td>
                                              </tr>
                                              <tr align="center"> 
                                                <td width="50%" height="20"><a class=link href="examplesreports.php#summary">Summary</a></td>
                                                <td><a class=link href="examplesreports.php#details">Details</a></td>
                                              </tr>
                                              <tr align="center"> 
                                                <td width="50%" height="20"><a class=link href="examplesreports.php#summary">Summary</a></td>
                                                <td><a class=link href="examplesreports.php#details">Details</a></td>
                                              </tr>
                                            </table></td>
                                        </tr>
                                        <tr> 
                                          <td height="20"><strong><font color="#006666">&nbsp;</font></strong>domainname.com</td>
                                          <td>http</td>
                                          <td>4/8/2003</td>
                                        </tr>
                                        <tr> 
                                          <td height="20"><strong><font color="#006666">&nbsp;</font></strong>www.mysite.net</td>
                                          <td>http</td>
                                          <td>4/8/2003</td>
                                        </tr>
                                        <tr> 
                                          <td height="20"><strong><font color="#006666">&nbsp;</font></strong>emailserver.net</td>
                                          <td>pop</td>
                                          <td>4/8/2003</td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                </table></TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" class=body>&nbsp;</TD>
                            </TR>
                            <TR> 
                              <TD height="25" align="left" valign="middle" class=body> 
                                <a name=details></a> <strong>Below</strong> is 
                                an example of a detailed report. </TD>
                            </TR>
                            <TR> 
                              <TD height="25" align="left" valign="middle" class=bg3>Monitoring 
                                statistics summary of www.mysite.net</TD>
                            </TR>
                            <? $month = array('January','February','March','April','May','June','July','August','September','October','November','December'); ?>
                            <TR> 
                              <TD align="left" valign="top" class=body><TABLE width="100%" border=0 cellPadding=0 cellSpacing=0 class=mt>
                                  <TBODY>
                                    <TR> 
                                      <TD><STRONG>Date:</STRONG></TD>
                                      <TD>&nbsp;</TD>
                                      <TD><SELECT name=Month class="mt">
                                          <?  for($i=0;$i<12;$i++)
											{
										 ?>
                                          <OPTION value="<? echo $i; ?>" <? if($i == 4) echo 'selected'; ?> > 
                                          <? echo $month[$i]?> </OPTION>
                                          <? } ?>
                                        </SELECT></TD>
                                      <TD>&nbsp;</TD>
                                      <TD><SELECT name=Day class="mt">
                                          <?  for($i=1;$i<=31;$i++)
											{
										 ?>
                                          <OPTION value="<? echo $i; ?>" <? if($i == 16) echo 'selected'; ?>> 
                                          <? echo $i; ?> </OPTION>
                                          <? } ?>
                                        </SELECT> </TD>
                                      <TD>&nbsp;</TD>
                                      <TD><SELECT name=Year class="mt">
                                          <?  for($i=date('Y')-1;$i>=1981;$i--)
											{
										 ?>
                                          <OPTION value="<? echo $i; ?>" <? if($i == date('Y')) echo 'selected'; ?>> 
                                          <? echo $i; ?> </OPTION>
                                          <? } ?>
                                        </SELECT> </TD>
                                      <TD>&nbsp;</TD>
                                      <TD height="40"> <INPUT name="submit" type=submit class=input_button value=Show></TD>
                                    </TR>
                                  </TBODY>
                                </TABLE></TD>
                            </TR>
                            <TR> 
                              <TD align="center" valign="top" class=body><table class=formtext width="450" style="border:1px solid #003366" cellspacing="1" cellpadding="0">
                                  <tr> 
                                    <td align="left" valign="top"><table class=mt width="450" border="0" cellspacing="1" cellpadding="0">
                                        <tr class="bg4"> 
                                          <td width="60" height="20"><strong><font color="#006666">&nbsp;</font>City</strong></td>
                                          <td width="80"><strong>Country</strong></td>
                                          <td width="20" align="center"><strong>#</strong></td>
                                          <td><strong>Uptime</strong></td>
                                          <td width="80"><strong>Downtime</strong></td>
                                          <td width="50">&nbsp;</td>
                                        </tr>
                                        <tr> 
                                          <td height="20"><strong><font color="#006666">&nbsp;</font></strong>Toranto</td>
                                          <td>United States</td>
                                          <td align="center">66</td>
                                          <td>100.000%</td>
                                          <td>0.000%</td>
                                          <td align="center"><a class=kk href="examplesreports.php#details2">Details</a></td>
                                        </tr>
                                        <tr> 
                                          <td height="20"><strong><font color="#006666">&nbsp;</font></strong>Dallas</td>
                                          <td>United States</td>
                                          <td align="center">67</td>
                                          <td>100.000%</td>
                                          <td>0.000%</td>
                                          <td align="center"><a class=kk href="examplesreports.php#details2">Details</a></td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                </table></TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" class=body>&nbsp;</TD>
                            </TR>
                            <TR> 
                              <TD height="25" align="left" valign="middle" class=body> 
                                <a name="details2"></a> <strong>Below</strong> 
                                is an example of a hourly detailed report. </TD>
                            </TR>
                            <TR> 
                              <TD height="25" align="left" valign="middle" class=bg3><strong>Hourly 
                                Monitoring statistics for www.mysite.net </strong> 
                              </TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" class=body><TABLE width="80%" border=0 cellPadding=0 cellSpacing=0 class=mt>
                                  <TBODY>
                                    <TR> 
                                      <TD><STRONG>Date:</STRONG></TD>
                                      <TD>&nbsp;</TD>
                                      <TD><SELECT name=select class="mt">
                                          <?  for($i=0;$i<12;$i++)
											{
										 ?>
                                          <OPTION value="<? echo $i; ?>" <? if($i == 4) echo 'selected'; ?> > 
                                          <? echo $month[$i]?> </OPTION>
                                          <? } ?>
                                        </SELECT></TD>
                                      <TD>&nbsp;</TD>
                                      <TD><SELECT name=Day class="mt">
                                          <?  for($i=1;$i<=31;$i++)
											{
										 ?>
                                          <OPTION value="<? echo $i; ?>" <? if($i == 20) echo 'selected'; ?>> 
                                          <? echo $i; ?> </OPTION>
                                          <? } ?>
                                        </SELECT> </TD>
                                      <TD>&nbsp;</TD>
                                      <TD>&nbsp;</TD>
                                      <TD height="40"> <INPUT name="submit2" type=submit class=buttonstyle value=Show></TD>
                                    </TR>
                                  </TBODY>
                                </TABLE></TD>
                            </TR>
                            <TR> 
                              <TD align="center" valign="top" class=body><table class=mt width="450" style="border:1px solid #003366" cellspacing="1" cellpadding="0">
                                  <tr> 
                                    <td align="left" valign="top"><table class=bodytext width="450" border="0" cellspacing="1" cellpadding="0">
                                        <tr bgcolor="#E1F0F4" class="bg4"> 
                                          <td width="175" height="20"><strong><font color="#006666">&nbsp;</font>Check 
                                            time (PST) </strong><br></td>
                                          <td><strong>Result</strong></td>
                                        </tr>
                                        <tr> 
                                          <td height="20"><strong><font color="#006666">&nbsp;</font></strong>12:25 
                                            AM - 10:27 AM</td>
                                          <td>Ok (22 checks)</td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                </table></TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" class=body>&nbsp;</TD>
                            </TR>
                            <TR> 
                              <TD height="20" align="left" valign="middle" class=body> 
                                <a name="summary"></a> <strong>Below</strong> 
                                is an example of a summary report. </TD>
                            </TR>
                            <TR> 
                              <TD height="80" align="left" valign="top" class=body> 
                                <strong>www.mysite.net</strong> <br>
                                Since: 3/5/2003<br>
                                Outages: 17<br>
                                Total Uptime: 98.788%</TD>
                            </TR>
                            <TR> 
                              <TD align="center" valign="top" class=body><table class=bodytext width="450" style="border:1px solid #336666" cellspacing="1" cellpadding="0">
                                  <tr> 
                                    <td align="left" valign="top"><table class=bodytext width="450" border="0" cellspacing="1" cellpadding="0">
                                        <tr class="bg4"> 
                                          <td width="125" height="20"><strong><font color="#006666">&nbsp;</font>Year</strong><br></td>
                                          <td width="125"><strong>Outages</strong></td>
                                          <td><strong>Uptime</strong></td>
                                        </tr>
                                        <tr> 
                                          <td height="20"><strong><font color="#006666">&nbsp;</font></strong>2003</td>
                                          <td>19</td>
                                          <td>98.766%</td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                </table></TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" class=body>&nbsp;</TD>
                            </TR>
                            <TR> 
                              <TD align="center" valign="top" class=body><table class=bodytext width="450" style="border:1px solid #336666" cellspacing="1" cellpadding="0">
                                  <tr> 
                                    <td align="left" valign="top"><table class=bodytext width="450" border="0" cellspacing="1" cellpadding="0">
                                        <tr bgcolor="#E1F0F4" class="bg4"> 
                                          <td><strong><font color="#006666">&nbsp;</font>Year</strong></td>
                                          <td><strong>Month</strong></td>
                                          <td><strong>Outages</strong></td>
                                          <td><strong>Uptime</strong></td>
                                          <td><strong>Downtime</strong></td>
                                        </tr>
                                        <tr> 
                                          <td height="20"><strong><font color="#006666">&nbsp;</font></strong>2003</td>
                                          <td>April</td>
                                          <td>15</td>
                                          <td>99.576%</td>
                                          <td>1 hr ,10min</td>
                                        </tr>
                                        <tr> 
                                          <td height="20"><strong><font color="#006666">&nbsp;</font></strong>2003</td>
                                          <td>March</td>
                                          <td>4</td>
                                          <td>96.576%</td>
                                          <td>1 hr ,19min</td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                </table></TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" class=body>&nbsp;</TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" class=body>Average 
                                response time: 0.323 seconds</TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" height="210" class=body>&nbsp;</TD>
                            </TR>
                          </TBODY>
                        </TABLE></td>
						<td align="right" valign="top" height="100%">
                      <? include("rightbar.php"); ?></td>
                      
                    </tr>
                    <tr > 
                      <td background="images/dotline.gif" height="1" colspan="3" valign="top"></td>
                    </tr>
                    <tr align="center" > 
                      <td  height="12" colspan="3" valign="top" class="tre12"></td>
                    </tr>
                    <tr align="center" > 
                      <td colspan="3"><? include("footer.php"); ?></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
