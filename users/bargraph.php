<?php
ob_start();
include ("../graphs/jpgraph.php");
include ("../graphs/jpgraph_bar.php");

//setlocale (LC_ALL, 'et_EE.ISO-8859-1');
$mon = array('1'=>'January','February','March','April','May','June','July','August','September','October','November','December');
$summary = explode('-',$_GET['qry']);
//print_r($summary);
$data1y = array();
$data2y = array();
$month = array();
for($i=0;$i<count($summary);$i+=2)
{
		$month = array_merge($month,$mon[(int)$summary[$i]]);    // months
		$data1y = array_merge($data1y,$summary[$i+1]); // uptime		//array(10,90,55,80);//,45,55,80,45,55,80,45,55);
		$data2y = array_merge($data2y,(100-$summary[$i+1])); //downtime		//array(90,10,45,20);//,55,45,20,55,45,20,55,45);
}

/*	print_r($data1y);
	print_r($data2y);
	print_r($month);

	echo count($data1y);
	echo count($data2y);
	echo count($month);
*/
// Create the graph. These two calls are always required
$graph = new Graph(200+(count($data1y)*50),150+(count($data2y)*30),"auto");    
$graph->SetScale("textlin");

$graph->SetBackgroundImage("../images/backg.jpg",BGIMG_FILLFRAME);
$graph->SetBackgroundImageMix(60);


$graph->ygrid->SetFill(true,'#FFFFFF@0.7','#BBCCFF@0.7');

//$graph->SetBackgroundImage("../images/backg.jpg",BGIMG_FILLFRAME);
//$graph->SetBackgroundImageMix(15);

//$graph->yaxis->scale->SetGrace(10);

$graph->SetShadow();
$graph->SetMarginColor('whitesmoke');
$graph->img->SetMargin(30,30,40,60);

// Create the bar plots
$b1plot = new BarPlot($data1y);
$b1plot->SetFillColor("lightblue");
$b1plot->value->Show();

//$b1plot->value->SetFont();
$b1plot->value->SetAngle(90); 
$b1plot->SetValuePos('bottom'); 

//$b1plot->value->SetAlign("center","center"); 
$b1plot->value->SetColor("black");
$b1plot->value->SetFormat('%0.2f %%');
$b1plot->SetLegend("Uptime");

$b2plot = new BarPlot($data2y);
$b2plot->SetFillColor("lightpink");
$b2plot->value->Show();
$b2plot->value->SetAngle(90); 
$b2plot->SetValuePos('top'); 
$b2plot->value->SetColor("black");
$b2plot->value->SetFormat('%0.2f %%');

$b2plot->SetLegend("Downtime");

// Create the grouped bar plot
$gbplot = new AccBarPlot(array($b1plot,$b2plot));

// ...and add it to the graPH
$graph->Add($gbplot);

$gbplot->SetFillGradient("navy","lightsteelblue",GRAD_MIDVER);
$gbplot->SetAlign("center");
// tab title


/*$graph->tabtitle->Set(' Monthly Monitoring Chart ' );
$graph->tabtitle->SetFont(FF_ARIAL,FS_BOLD,13);
$graph->tabtitle->SetColor('darkred','#E1E1FF');
*/



$graph->SetTitleBackground('black');
//$graph->title->SetAlign("center");
$graph->title->Set("Month-wise Monitoring Chart");
$graph->title->SetColor('gold');

$graph->xaxis->SetTickLabels($month); //$gDateLocale->GetShortMonth());


$graph->legend->SetFillColor("#FFFFFF");
$graph->legend->SetLayout(LEGEND_HOR);
$graph->legend->Pos(0.45,0.97,"center","bottom");

$gbplot->SetShadow("gray5",6,4,true);


$gbplot->SetWidth(0.3);




// Display the graph
$graph->Stroke();

?>
