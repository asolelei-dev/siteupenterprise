<?
include("../admin/include/conn.inc");
ob_start();
error_reporting(0);
session_start();
if(!isset($_SESSION['mnuser']))
{
header("Location: index.php");
exit();
}

/* getting member type in session

$mem_qry = mysqli_fetch_array($db->query("select memtype from tbl_users where userid = $_SESSION[mnuser]"));
$_SESSION['memid'] = $mem_qry[0];
 */
$d = date('j');
$m = date('n');
$y = date('Y');
if(isset($_POST['action']))
{
	if($_POST['action'] == 'statistics' && isset($_POST['Month']))
	{
	 	$d = $_POST['Day'];
		$m = $_POST['Month'];
	}
	
		if($_POST['action'] == 'details' && isset($_POST['Month']))
	{
	 	$d = $_POST['Day'];
		$m = $_POST['Month'];
	}

}

		$res_mem = mysqli_fetch_array($db->query("select custom_time_zone,public_stats from tbl_membership where m_id = $_SESSION[memid]"));
		$time_zone = 0;
		$outage_status = $res_mem['public_stats'];
		
		if($res_mem['custom_time_zone'] == 'Y')
		{ 
		  	$res_time = mysqli_fetch_array($db->query("select timezone from tbl_users where userid = $_SESSION[mnuser]"));
			$time_zone = $res_time['timezone'];
			$res_tzstr = mysqli_fetch_array($db->query("select tzstring from tbl_timezones where tzval = $time_zone"));
			$time_zone_str = $res_tzstr['tzstring'];	
		}
		else
			$time_zone_str = "GMT 0.00";	

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site Uptime Enterprise</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../monitor_style.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
function sumfun(sid)
{
document.sumfrm.serviceid.value=sid;
document.sumfrm.submit();
}

function statfun(sid)
{
document.statfrm.serviceid.value=sid;
document.statfrm.submit();
}

function detailsfun(sid,lid)
{
document.detailsfrm.serviceid.value=sid;
document.detailsfrm.locationid.value=lid;
document.detailsfrm.submit();
}

function  outagefun(y)
{
document.outagesform.year.value = y;
document.outagesform.submit();
}


function  outagefun1(m,y)
{
document.outagesform1.month.value = m;
document.outagesform1.year.value = y;
document.outagesform1.submit();
}



</script>
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="730" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="504" align="center" valign="top"> 
                        <? if(!isset($_POST['action']))
					  { ?>
                        <div id="servicelist" style="DISPLAY: <? if(!isset($_POST['action'])) echo ''; else echo 'none'; ?> "> 
                          <TABLE class=mt border=0 cellPadding=0 cellSpacing=0 width="450">
                            <TBODY>
                              <TR> 
                                <TD align="left" valign="top" class=body> <table width="450" border="0" cellspacing="0" cellpadding="0">
                                    <tr> 
                                      <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                      <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                      <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                        </strong></font><span class="c3">My Statistics</span></B></td>
                                    </tr>
                                    <tr > 
                                      <td background="../images/dotline.gif" colspan="3"></td>
                                    </tr>
                                  </table>
                                  <P>&nbsp;</TD>
                              </TR>
                              <TR> 
                                <TD height="15" align="left" valign="top" class=body></TD>
                              </TR>
                              <TR> 
                                <TD align="center" valign="top" class=body><table class=formtext width="450" style="border:1px solid #003366" cellspacing="1" cellpadding="0">
                                    <tr> 
                                      <td width="339" height="20" class="bbg5"><strong><font color="#006666">&nbsp;<span class="c4">My 
                                        Statistics</span></font></strong></td>
                                    </tr>
                                    <tr> 
                                      <td align="left" valign="top"><table class=formtext width="450" border="0" cellspacing="1" cellpadding="0">
                                          <tr class="bg4"> 
                                            <td width="100" height="20"><strong><font color="#006666">&nbsp;</font>Host 
                                              Name</strong></td>
                                            <td width="45"><strong>Service</strong></td>
                                            <td width="80"><strong>Last Check</strong></td>
                                            <td colspan="3" align="center"><strong>Action</strong></td>
                                          </tr>
                                          <? $ser_qry = $db->query("select service_id,host_name,service_port,custom_port,last_check from tbl_services where userid = $_SESSION[mnuser] order by date_add desc");
										 if(mysqli_num_rows($ser_qry)>0)
										 {
											 $i=0;
											while($ser=mysqli_fetch_array($ser_qry))
											{
											 $i++;
											?>
                                          <tr> 
                                            <td height="32"><strong><font color="#006666">&nbsp;</font></strong><? echo $ser['host_name']; ?></td>
                                            <? if($ser['custom_port'] == 0)
												  	{
												  		$sq = mysqli_fetch_array($db->query("select service_name from tbl_service_ports where service_id=$ser[service_port]"));
														$port  = $sq[0];
												  	}
												  	else
												  		$port = $ser['custom_port'];
												  ?>
                                            <td width="60"><? echo $port; ?></td>
                                            <?
												   	$d1=explode(' ',$ser['last_check']);
												   	$dt=explode('-',$d1[0]);
												  ?>
                                            <td> 
                                              <?  echo $dt[1]."/".$dt[2]."/".$dt[0]; ?>
                                            </td>
                                            <td align="center" height="20"><a class=link href="javascript: sumfun(<? echo $ser['service_id']; ?>)" >Summary</a></td>
                                            <td align="center" height="20"><a class=link href="javascript: statfun(<? echo $ser['service_id']; ?>)" >Details</a></td>
                                          </tr>
                                          <? } // while
									} else {  ?>
                                          <tr> 
                                            <td colspan="6" class="kk" align="center"> 
                                              There are no Statistics </td>
                                          </tr>
                                          <? } ?>
                                          <tr> 
                                            <td> <form action="mystatistics.php" method="post" name='statfrm'>
                                                <input type="hidden" value=0 name="serviceid">
                                                <input type="hidden" value="statistics" name="action">
                                              </form>
                                              <form action="mystatistics.php" method="post" name='sumfrm'>
                                                <input type="hidden" value=0 name="serviceid">
                                                <input type="hidden" value="summary" name="action">
                                              </form></td>
                                          </tr>
                                        </table></td>
                                    </tr>
                                  </table></TD>
                              </TR>
                              <TR> 
                                <TD align="left" valign="top" class=body>&nbsp;</TD>
                              </TR>
                            </TBODY>
                          </TABLE>
                        </div>
                        <? } ?>
                        <? if(isset($_POST['action'])&&$_POST['action']=='statistics')
					  {
					  ?>
                        <div id="statistics_list" style="DISPLAY: <? if(isset($_POST['action'])&&$_POST['action']=='statistics') echo ''; else echo 'none'; ?>"> 
                          <TABLE class=formtext border=0 cellPadding=0 cellSpacing=0 width="450">
                            <TBODY>
                              <? $res_host = mysqli_fetch_array($db->query("select host_name from tbl_services where service_id=$_POST[serviceid]")); ?>
                              <TR>
                                <TD height="20" align="left" valign="top" class=body>&nbsp;</TD>
                              </TR>
                              <TR> 
                                <TD height="20" align="left" valign="top" class=body>&nbsp;</TD>
                              </TR>
                              <TR> 
                                <TD height="20" align="left" valign="top" class=body> 
                                  <P class="bbg5"><font size="2"> <span class="c4">Daily 
                                    Monitoring Summary for</span><strong><span  class="c4"> 
                                    </span></strong> <span class="c4"><strong><? echo $res_host[0]; ?></strong></span> 
                                    </font> </TD>
                              </TR>
                              <TR> 
                                <TD height="15" align="left" valign="top" class=body></TD>
                              </TR>
                              <TR> 
                                <TD align="center" valign="top" class=body> <TABLE width="450" border=0 cellPadding=0 cellSpacing=0 class=mt dwcopytype="CopyTableCell">
                                    <TBODY>
                                      <? $month = array('1'=>'January','February','March','April','May','June','July','August','September','October','November','December'); ?>
                                      <TR> 
                                        <TD align="left" valign="top" class=body> 
                                          <form name="listfrm" action="mystatistics.php" method="post">
                                            <TABLE width="100%" border=0 cellPadding=0 cellSpacing=0 class=bg2>
                                              <TBODY>
                                                <TR> 
                                                  <TD width="13%"><STRONG>Date:</STRONG></TD>
                                                  <TD width="1%">&nbsp;</TD>
                                                  <? if ($m == 1) $y1="-".$y; else $y1 = '';?>
                                                  <TD width="19%"><SELECT name=Month class="mt">
                                                      <OPTION value="<? echo date('n'); ?>" <? if(date('m') == $m) echo 'selected'; ?> > 
                                                      <? echo $month[(int)date('m')].$y1; ?> 
                                                      </OPTION>
                                                      <? if(date('m') == '01')
										 	{
										?>
                                                      <OPTION value=12 <? if($m==12) echo 'selected'; ?> > 
                                                      <? echo ($month[12])." - ".($y-1); ?> 
                                                      </OPTION>
                                                      <? } else { ?>
                                                      <OPTION value="<? echo date('m')-1; ?>" <? if(date('m')-1 == $m) echo 'selected'; ?> > 
                                                      <? echo $month[(int)date('m')-1]; ?> 
                                                      </OPTION>
                                                      <? } ?>
                                                    </SELECT></TD>
                                                  <TD width="0%">&nbsp;</TD>
                                                  <TD width="19%"><SELECT name=Day class="mt">
                                                      <?
										  for($i=1;$i<=date('t','Y-m-d');$i++)
											{
										 ?>
                                                      <OPTION value="<? echo $i; ?>" <? if($i == $d) echo 'selected'; ?>> 
                                                      <? echo $i; ?> </OPTION>
                                                      <? } ?>
                                                    </SELECT> </TD>
                                                  <TD width="0%">&nbsp;</TD>
                                                  <TD width="48%" height="40"> 
                                                    <input type="hidden" value="<? echo $_POST['serviceid']; ?>" name="serviceid"> 
                                                    <input type="hidden" value="statistics" name="action"> 
                                                    <INPUT name="submit" type=submit class=buttonstyle value=Show></TD>
                                                </TR>
                                              </TBODY>
                                            </TABLE>
                                          </form></TD>
                                      </TR>
                                      <TR> 
                                        <TD align="center" valign="top" class=body> 
                                          <table class=mt width="450" style="border:1px solid #336666" cellspacing="1" cellpadding="0">
                                            <tr> 
                                              <td align="left" valign="top"> <table class=mt width="450" border="0" cellspacing="1" cellpadding="0">
                                                  <tr class="bg4"> 
                                                    <td width="60" height="20" align="center"><strong><font color="#006666">&nbsp;</font>City</strong></td>
                                                    <td width="80" align="center"><strong>Country</strong></td>
                                                    <td width="25" align="center"><strong>#</strong></td>
                                                    <td width="70" align="center"><strong>Uptime</strong></td>
                                                    <td width="70" align="center"><strong>Downtime</strong></td>
                                                    <td width="50" align="center"><strong>Action</strong></td>
                                                  </tr>
                                                  <?	
										  	$count = 0;
											
										  	if(date('m') == 1 && $m == 12)
											   $y1=$y-1;
											   else
											   $y1=$y;
											  if(date('m')==$m && date('Y')==$y1)
											  {
											  	$nMin = $time_zone * 60;
												$da1 = date('Y-m-d H:i:s',mktime(0,0-$nMin,0,$m,$d,$y1));
											   $da2 = date('Y-m-d H:i:s',mktime(23,59-$nMin,59,$m,$d,$y1));
												$qry_service = $db->query("select * from tbl_dayservices where service_id= $_POST[serviceid] and time_of_check between '$da1' and '$da2' order by time_of_check ");
												$count = mysqli_num_rows($qry_service);
													
													$no_of_checks = 0;
													$down_checks = 0;
												while($res_service = mysqli_fetch_array($qry_service))
												{
													$locationid = $res_service['location'];
													$no_of_checks++;
													if($res_service['result'] == 'N')
													$down_checks++; 
												}
												
											}
											  else
											{
												
											  $qry_service = $db->query("select location_id,no_of_checks,down_checks from tbl_dailyservices where service_id= $_POST[serviceid] and date_of_service = '$y1-$m-$d' ");
											//echo "select location_id,no_of_checks,down_checks from tbl_dailyservices where service_id= $_POST[serviceid] and date_of_service = '$y-$m-$d' ";
													if( ($count = mysqli_num_rows($qry_service))>0)
													{
													$res_service = mysqli_fetch_array($qry_service);
													$locationid = $res_service['location_id'];
													$no_of_checks = $res_service['no_of_checks'];
													$down_checks = $res_service['down_checks'];
													}
													
											}
											if($count > 0)
											{
											$res_loc = mysqli_fetch_array($db->query("select loc_name,loc_country from tbl_locations where loc_id = $locationid")); ?>
                                                  <tr> 
                                                    <td height="20" align="center" width="90"><? echo $res_loc['loc_name']; ?></td>
                                                    <td align="center" ><? echo $res_loc['loc_country']; ?></td>
                                                    <td align="center" ><? echo $no_of_checks; ?></td>
                                                    <td align="center" > 
                                                      <? $uptime = (($no_of_checks-$down_checks)*100)/$no_of_checks;
										  echo number_format($uptime,2,'.',''); 
										  ?>
                                                      %</td>
                                                    <td align="center"><? echo number_format(100-$uptime,2,'.',''); ?>%</td>
                                                    <td align="center" height="20"><a class=kk href="javascript: detailsfun(<? echo $_POST['serviceid']; ?>,<? echo $locationid; ?>)" >Details</a></td>
                                                  </tr>
                                                  <? } else { ?>
                                                  <tr> 
                                                    <td class="c3" colspan="6" height="30" align="center"> 
                                                      <strong>There are no statistics.</strong></td>
                                                  </tr>
                                                  <? } ?>
                                                </table>
                                                <form action="mystatistics.php" method="post" name='detailsfrm'>
                                                  <input type="hidden" value=0 name="serviceid">
                                                  <input type="hidden" value="<? echo $m; ?>" name="Month">
                                                  <input type="hidden" value="<? echo $d; ?>" name="Day">
                                                  <input type="hidden" value="<? echo $locationid; ?>" name="locationid">
                                                  <input type="hidden" value="details" name="action">
                                                </form></td>
                                            </tr>
                                          </table></TD>
                                      </TR>
                                    </TBODY>
                                  </TABLE></TD>
                              </TR>
                              <TR> 
                                <TD align="left" valign="top" class=body>&nbsp;</TD>
                              </TR>
                              <tr> 
                                <td align="center"> 
                                  <? if($count > 0)
											{
										?>
                                  <table class=formtext width="450"  cellspacing="1" cellpadding="0">
                                    <tr> 
                                      <td > <img src="pie3dgraphdaily.php?uptime=<? echo $uptime; ?>&date=<? echo $month[(int)$m]."/".$d; ?>" align="middle" > 
                                      </td>
                                    </tr>
                                  </table>
                                  <?
										  }
										?>
                                </td>
                              </tr>
                            </TBODY>
                          </TABLE>
                        </div>
                        <?  } 
					  if(isset($_POST['action'])&&$_POST['action']=='details')
					  { ?>
                        <div id="details_list" style="DISPLAY: <? if(isset($_POST['action'])&&$_POST['action']=='details') echo ''; else echo 'none'; ?>"> 
                          <?
							$res_host = mysqli_fetch_array($db->query("select host_name from tbl_services where service_id=$_POST[serviceid]"));
							?>
                          <TABLE class=mt border=0 cellPadding=0 cellSpacing=0 width="450">
                            <TBODY>
                              <? $res_loc = mysqli_fetch_array($db->query("select loc_name from tbl_locations where loc_id = $_POST[locationid]")); ?>
                              <TR> 
                                <TD align="left" valign="top" class=body> <P class="bbg5"><span class="c4"><font size="2">Daily 
                                    Monitoring Details for</font></span><font size="2"> 
                                    &nbsp;<span class="c4"><strong><? echo $res_host[0]; ?> </strong>
                                    </span><br>
                                    <span class="c4">from</span> <font color="#FFCC00"><? echo $res_loc[0]; ?></font></font></B> 
                                </TD>
                              </TR>
                              <TR> 
                                <TD height="10" align="left" valign="top" class=body></TD>
                              </TR>
                              <TR> 
                                <TD align="center" valign="top" class=body> <TABLE width="379" border=0 cellPadding=0 cellSpacing=0 class=mt dwcopytype="CopyTableCell">
                                    <TBODY>
                                      <? $month = array('1'=>'January','February','March','April','May','June','July','August','September','October','November','December'); ?>
                                      <TR> 
                                        <TD align="left" valign="top" class=body> 
                                          <form name="detailsfrm" action="mystatistics.php" method="post">
                                            <TABLE width="100%" border=0 cellPadding=0 cellSpacing=0 class=bodytext>
                                              <TBODY>
                                                <TR> 
                                                  <TD><STRONG>Date:</STRONG></TD>
                                                  <TD width="20">&nbsp;</TD>
                                                  <TD><SELECT name=Month class="mt">
                                                      <OPTION value="<? echo date('m'); ?>" <? if(date('m') == $m) echo 'selected'; ?> > 
                                                      <? echo $month[(int)date('m')]; ?> 
                                                      </OPTION>
                                                      <? if(date('m') == '01')
										 	{
										?>
                                                      <OPTION value=12 <? if($m==12) echo 'selected'; ?> > 
                                                      <? echo $month[12]; ?> </OPTION>
                                                      <? } else { ?>
                                                      <OPTION value="<? echo date('m')-1; ?>" <? if(date('m')-1 == $m) echo 'selected'; ?> > 
                                                      <? echo $month[(int)date('m')-1]; ?> 
                                                      </OPTION>
                                                      <? } ?>
                                                    </SELECT></TD>
                                                  <TD>&nbsp;</TD>
                                                  <TD><SELECT name=Day class="mt">
                                                      <?
										  for($i=1;$i<=date('t','Y-m-d');$i++)
											{
										 ?>
                                                      <OPTION value="<? echo $i; ?>" <? if($i == $d) echo 'selected'; ?>> 
                                                      <? echo $i; ?> </OPTION>
                                                      <? } ?>
                                                    </SELECT> </TD>
                                                  <TD>&nbsp;</TD>
                                                  <TD align="left" > <input type="hidden" value="<? echo $_POST['serviceid']; ?>" name="serviceid"> 
                                                    <input type="hidden" value="<? echo $_POST['locationid']; ?>" name="locationid"> 
                                                    <input type="hidden" value="details" name="action"> 
                                                    <INPUT name="submit" type=submit class=input_button value=Show> 
                                                  </TD>
                                                  <TD>&nbsp;</TD>
                                                  <TD height="40">&nbsp; </TD>
                                                </TR>
                                              </TBODY>
                                            </TABLE>
                                          </form></TD>
                                      </TR>
                                      <TR> 
                                        <TD align="center" valign="top" class=body> 
                                          <table class=formtext width="450" style="border:1px solid #336666" cellspacing="1" cellpadding="0">
                                            <tr bgcolor="#E1F0F4" class="bg4"> 
                                              <td width="175" height="19"><strong><font color="#006666">&nbsp;</font>Check 
                                                time ( 
                                                <? echo $time_zone_str; ?>
                                                ) </strong><br></td>
                                              <td><strong>Result</strong></td>
                                            </tr>
                                            <?
								
								
								 if(date('m') == 1 && $m == 12)
								   $y1=$y-1;
							   		else
							  	   $y1=$y;
							   		$nMin = $time_zone * 60;
								   $da1 = date('Y-m-d H:i:s',mktime(0,0-$nMin,0,$m,$d,$y1));
								   $da2 = date('Y-m-d H:i:s',mktime(23,59-$nMin,59,$m,$d,$y1));
								$qry_service = $db->query("select * from tbl_dayservices where service_id= $_POST[serviceid] and time_of_check between '$da1' and '$da2' order by time_of_check ");
								$res_period = mysqli_fetch_array($db->query("select p.period from tbl_checkperiods p, tbl_services s where s.service_id= $_POST[serviceid] and p.pid = s.period"));
								$res_period = $res_period[0];
								//echo "select * from tbl_dayservices where service_id= $_POST[serviceid] and time_of_check between '$da1' and '$da2' order by time_of_check";
								if(mysqli_num_rows($qry_service)>0)
								{
								$start = '';
								$last = '';
								$result = '';
								while($res_service = mysqli_fetch_array($qry_service))
								{
									if($start == '' && $result == '')
									{
										$last = $start = $res_service['time_of_check'];
										$result = $res_service['result'];
									}
									
									if($res_service['result'] == $result)
									{
										$last = $res_service['time_of_check'];
									}
									else
									{
									
							?>
                                            <tr> 
                                              <td height="20" class="mt"><strong><font color="#006666">&nbsp;</font></strong> 
                                                <? $hour=date('h',strtotime($start));
								   $min= date('i',strtotime($start))+($time_zone * 60); 
								   echo date('h:i A',mktime($hour,$min,0,date('m,d,Y',strtotime($start))));
								?>
                                                &nbsp; - &nbsp; 
                                                <? 
												$last = $last;
									$hour=date('h',strtotime($last));
								   $min= date('i',strtotime($last))+($time_zone * 60); 
								   echo date('h:i A',mktime($hour,$min+$res_period,0,date('m,d,Y',strtotime($last))));
								?>
                                              </td>
                                              <td>&nbsp; 
                                                <? if($result == 'Y' ) echo 'OK'; else echo 'Failed'; ?>
                                              </td>
                                            </tr>
                                            <?
								$result = $res_service['result'];
								$last=$start = $res_service['time_of_check'];	
								} 
							}	
							
							?>
                                            <tr> 
                                              <td height="20" class="mt"><strong><font color="#006666">&nbsp;</font></strong> 
                                                <? $hour=date('h',strtotime($start));
								   $min= date('i',strtotime($start))+($time_zone * 60); 
								   echo date('h:i A',mktime($hour,$min,0,date('m,d,Y',strtotime($start))));
								?>
                                                &nbsp; - &nbsp; 
                                                <? 
								   $hour=date('h',strtotime($last));
								   $min= date('i',strtotime($last))+($time_zone * 60); 
								   echo date('h:i A',mktime($hour,$min+$res_period,0,date('m,d,Y',strtotime($last))));
								?>
                                              </td>
                                              <td>&nbsp; 
                                                <? if($result == 'Y' ) echo 'OK'; else echo 'Failed'; ?>
                                              </td>
                                            </tr>
                                            <?
							} else {  ?>
                                            <tr> 
                                              <td class="c3" colspan="2" height="30" align="center"> 
                                                <strong>There are no statistics.</strong></td>
                                            </tr>
                                            <? } ?>
                                          </table></TD>
                                      </TR>
                                    </TBODY>
                                  </TABLE></TD>
                              </TR>
                              <TR> 
                                <TD align="left" valign="top" class=body>&nbsp;</TD>
                              </TR>
                            </TBODY>
                          </TABLE>
                        </div>
                        <?  } 
					  if(isset($_POST['action'])&&$_POST['action']=='summary')
					  { ?>
                        <div id="summary_list" style="DISPLAY: <? if(isset($_POST['action'])&&$_POST['action']=='summary') echo ''; else echo 'none'; ?>"> 
                          <?
						  $res_service = mysqli_fetch_array($db->query("select host_name,date_add from tbl_services where service_id = $_POST[serviceid]"));
						  $da1 = date('Y-m-d',mktime(0,0,0,$m,1,$y));
							$d1 = date('t',strtotime($da1));
							$da2 = date('Y-m-d',mktime(0,0,0,$m,$d1,$y));
						  
						  	//echo "select * from tbl_dailyservices where service_id=$_POST[serviceid] and date_of_service between '$da1' and '$da2'";
						    $qry_day = $db->query("select * from tbl_dailyservices where service_id=$_POST[serviceid] and date_of_service between '$da1' and '$da2'");
							$qry_summary = $db->query("select * from tbl_monthlyservices where service_id = $_POST[serviceid] and YEAR(date_of_service) = ".date('Y')." order by date_of_service desc");
						  
				if(mysqli_num_rows($qry_summary)>0 ||  mysqli_num_rows($qry_day)>0)
				{
				?>
                          <TABLE class=formtext border=0 cellPadding=0 cellSpacing=0 width="379">
                            <TBODY>
                              <?
						  $tot_checks = 0;
						  $downtime = 0;
						  $i=0;
						  $resp_time = 0;
						  
						  //$date_of_servie='';
						  // present month details
						  
 						    
							if(mysqli_num_rows($qry_day)>0)
							{	
						  		$date_of_service = $da1; // date
								$no_of_checks = 0;
								$down_checks = 0;
								$resptime = 0;
								$count = 0;
								while($res_day=mysqli_fetch_array($qry_day))
								{
									$down_checks += $res_day['down_checks'];
									$no_of_checks += $res_day['no_of_checks'];
									$resptime += $res_day['resp_time'];
									$count++;
									
								}//while($res_day=mysqli_fetch_array($qry_day))
					
								$resptime = number_format($resptime/$count, 4, '.', '');		
								
									$summary[$i][0] = $date_of_service;
									$summary[$i][1] = $no_of_checks;
									$tot_checks += $no_of_checks;
									$summary[$i][2] = $down_checks;
									$downtime += $down_checks;
									$resp_time += $resptime;
									$i++;
						  
						  }// if (mysqli_num_rows());
						  
						  
						  
						 // echo mysqli_num_rows($qry_summary);
						  while($res_summary = mysqli_fetch_array($qry_summary))
						  {
						  	$summary[$i][0] = $res_summary['date_of_service'];
							$summary[$i][1] = $res_summary['no_of_checks'];
							$tot_checks += $res_summary['no_of_checks'];
							$summary[$i][2] = $res_summary['down_checks'];
							$downtime += $res_summary['down_checks'];
							$resp_time += $res_summary['resp_time'];
							$i++;
						  }
						  
						  ?>
                              <TR> 
                                <TD height="80" align="left" valign="top" class=body> 
                                  <strong><? echo $res_service['host_name']; ?></strong> 
                                  <br>
                                  Since: <? echo date('m/d/Y',strtotime($res_service['date_add'])); ?><br>
                                  Outages: <? echo $downtime; ?><br>
                                  Total Uptime: 
                                  <?  $uptime = number_format((($tot_checks - $downtime)*100)/$tot_checks,2,'.',','); 
											  echo $uptime; ?>
                                  %</TD>
                              </TR>
                              <TR> 
                                <TD align="center" valign="top" class=body> <table align="left" class=formtext width="450" style="border:1px solid #336666; margin-left: 6px;" cellspacing="1" cellpadding="0">
                                    <tr> 
                                      <td align="left" valign="top"> <table align="left" class=mt width="450" border="0" cellspacing="1" cellpadding="0">
                                          <tr class="bg4"> 
                                            <td width="125" height="20"><strong><font color="#006666">&nbsp;</font>Year</strong><br></td>
                                            <td width="125"><strong>Outages</strong></td>
                                            <td><strong>Uptime</strong></td>
                                          </tr>
                                          <tr> 
                                            <td height="20"><strong><font color="#006666">&nbsp;</font></strong><? echo $y; ?></td>
                                            <td> 
                                              <? if($outage_status == 'Y') { ?>
                                              <a href="javascript: outagefun(<? echo $y; ?>)" class="mt"><? echo $downtime; ?></a> 
                                              <? } else { ?>
                                              <a class="mt"><? echo $downtime; ?></a> 
                                              <? } ?>
                                            </td>
                                            <td><? echo $uptime; ?>%</td>
                                          </tr>
                                        </table>
                                        <form name="outagesform" action="mystatistics.php" method="post">
                                          <input type="hidden" name="year" value="0">
                                          <input type="hidden" name="serviceid" value="<? echo $_POST['serviceid']; ?>">
                                          <input type="hidden" name="action" value="outages">
                                        </form></td>
                                    </tr>
                                  </table></TD>
                              </TR>
                              <TR> 
                                <TD align="left" valign="top" class=body>&nbsp; </TD>
                              </TR>
                              <tr> 
                                <td align="center"> <table class=formtext width="450"  cellspacing="1" cellpadding="0">
                                    <tr> 
                                      <td > <img src="pie3dgraphyear.php?uptime=<? echo $uptime; ?>&year=<? echo $y; ?>" align="middle" > 
                                      </td>
                                    </tr>
                                  </table></td>
                              </tr>
                              <TR> 
                                <TD align="left" valign="top" class=body>&nbsp;</TD>
                              </TR>
                              <TR> 
                                <TD align="center" valign="top" class=body><table class=formtext width="450" style="border:1px solid #336666" cellspacing="1" cellpadding="0">
                                    <tr> 
                                      <td align="left" valign="top"> <table class=formtext width="450" border="0" cellspacing="1" cellpadding="0">
                                          <tr bgcolor="#E1F0F4" class="bg4"> 
                                            <td><strong><font color="#006666">&nbsp;</font>Year</strong></td>
                                            <td><strong>Month</strong></td>
                                            <td><strong>Outages</strong></td>
                                            <td><strong>Uptime</strong></td>
                                            <td><strong>Downtime</strong></td>
                                          </tr>
                                          <? 
										$graph_summary = array();
										for($i=0;$i<count($summary);$i++)
											{
											$graph_summary = array_merge($graph_summary,date('m',strtotime($summary[$i][0])),number_format((($summary[$i][1]-$summary[$i][2])*100)/$summary[$i][1],2,'.',','));
										?>
                                          <tr> 
                                            <td height="20"><strong><font color="#006666">&nbsp;</font></strong><? echo $y; ?></td>
                                            <td><? echo date('F',strtotime($summary[$i][0])); 
										  ?></td>
                                            <td> 
                                              <? if($outage_status == 'Y') { ?>
                                              <a href="javascript: outagefun1(<? echo date('m',strtotime($summary[$i][0])) ?>,<? echo $y; ?>)" class="mt"><? echo $summary[$i][2]; ?></a> 
                                              <? } else { ?>
                                              <a class="mt"><? echo $summary[$i][2]; ?></a> 
                                              <? } ?>
                                            </td>
                                            <td><? echo number_format((($summary[$i][1]-$summary[$i][2])*100)/$summary[$i][1],2,'.',','); ?>%</td>
                                            <td><? echo number_format(($summary[$i][2]*100)/$summary[$i][1],2,'.',','); ?>% 
                                            </td>
                                          </tr>
                                          <? } ?>
                                        </table>
                                        <form name="outagesform1" action="mystatistics.php" method="post">
                                          <input type="hidden" name="month" value="0">
                                          <input type="hidden" name="year" value="0">
                                          <input type="hidden" name="serviceid" value="<? echo $_POST['serviceid']; ?>">
                                          <input type="hidden" name="action" value="outages">
                                        </form></td>
                                    </tr>
                                  </table></TD>
                              </TR>
                              <TR> 
                                <TD align="left" valign="top" class=body>&nbsp;</TD>
                              </TR>
                              <TR> 
                                <TD align="left" valign="top" class=body>Average 
                                  response time: <? echo number_format($resp_time/$i,4,'.',','); ?> 
                                  seconds</TD>
                              </tr>
                              <TR> 
                                <TD align="left" valign="top" class=body>&nbsp;</TD>
                              </TR>
                              <tr> 
                                <td align="center"> <table class=mt width="370"  cellspacing="1" cellpadding="0">
                                    <tr> 
                                      <td align="center" valign="top" > 
                                        <? //echo print_r($graph_summary); ?>
                                        <a href="bargraph.php?qry=<? echo implode('-',$graph_summary); ?>" target="_blank"> 
                                        <img border="0" src="bargraph.php?qry=<? echo implode('-',$graph_summary); ?>" alt="Click  to view in full page" <? if(count($summary)>3) { ?> width="360" height="250" <? } ?> align="absmiddle"></a>	
                                      </td>
                                    </tr>
                                    <TR> 
                                      <TD align="center" height="15" valign="middle" class="mt"><font color="#FD99A3"><strong>Click 
                                        on the above graph to view in full page</strong></font></TD>
                                    </TR>
									<tr><td height="15"></td></tr>
									<tr>
                                      <td align="center" class="bodytext"> If you want the users 
                                        of your site to see the site's statistics,<br> 
                                        place this link on your site:<br> <a href="statistics.php?sid=<? echo $_POST['serviceid']; ?>" target="_blank" class="kkk">Statistics Link</a> 
                                      </td>
                                    </tr>
                                  </table></td>
                            </TBODY>
                          </TABLE>
                          <? } // if(mysqli_num_rows($qry_summary)>0 &&  mysqli_num_rows($qry_day)) 
				  else
				  {
				  ?>
                          <p align="left" class="me" style="margin-left:10px;"><br>
                            <br>
                            There are no statistics</p>
                          <? } ?>
                        </div>
                        <?  } 
					  if(isset($_POST['action'])&&$_POST['action']=='outages')
					  { ?>
                        <div id="outages_list" style="DISPLAY: <? if(isset($_POST['action'])&&$_POST['action']=='outages') echo ''; else echo 'none'; ?>"> 
                          <TABLE class=mt border=0 cellPadding=0 cellSpacing=0 width="450">
                            <TBODY>
                              <TR> 
                                <TD height="10" align="left" valign="top" class=body></TD>
                              </TR>
                              <TR> 
                                <TD align="center" valign="top" class=body> <table class=formtext width="450" style="border:1px solid #336666" cellspacing="1" cellpadding="0">
                                    <tr> 
                                      <td align="left" valign="top"><table class=formtext width="450" border="0" cellspacing="1" cellpadding="0">
                                          <tr bgcolor="#E1F0F4" class="bg4"> 
                                            <td width="175" height="20"><strong><font color="#006666">&nbsp;</font>Check 
                                              time (
                                              <? echo $time_zone_str; ?>
                                              ) </strong><br></td>
                                            <td><strong>Result</strong></td>
                                          </tr>
                                          <?
										if(isset($_POST['month']))
										{
										$qry_day = $db->query("select * from tbl_dayservices where service_id = $_POST[serviceid] and YEAR(time_of_check) = $_POST[year] and MONTH(time_of_check) = $_POST[month] order by time_of_check desc");
										//echo "select * from tbl_dayservices where service_id = $_POST[serviceid] and YEAR(time_of_check) = $_POST[year] and MONTH(time_of_check) = $_POST[month] ";
										}
										else
										{
										$qry_day = $db->query("select * from tbl_dayservices where service_id = $_POST[serviceid] and YEAR(time_of_check) = $_POST[year] order by time_of_check desc ");
										//echo "select * from tbl_dayservices where service_id = $_POST[serviceid] and YEAR(time_of_check) = $_POST[year] ";
										}
									if (mysqli_num_rows($qry_day) > 0)
									{
										
										$checks = 0;
										$start = '';
										
										while($res = mysqli_fetch_array($qry_day))
										{	
											if($start == '')
											$start = $res['time_of_check'];	
											if($res['result'] == 'N' && (date('Y-m-d',strtotime($res['time_of_check'])) == date('Y-m-d',strtotime($start))))
											{
												
												if($checks == 0)
												$start =  $res['time_of_check'];
												$checks++;
												
												
											}
											else if($checks != 0)
											{
											?>
                                          <tr> 
                                            <td height="20"><strong><font color="#006666">&nbsp;</font></strong> 
                                              <? 
												
												//echo date('m/d/Y h:i:s A',strtotime($start));
												$hour = date('h',strtotime($start));
												$min = date('i',strtotime($start))+($time_zone * 60);
												$sec = date('s',strtotime($start));
												$mon = date('m',strtotime($start));
												$days = date('d',strtotime($start));
												$year = date('Y',strtotime($start));
												echo date('m/d/Y h:i:s A',mktime($hour,$min,$sec,$mon,$days,$year));
											?>
                                            </td>
                                            <td><? echo "Failed"; ?>&nbsp;(<? echo $checks; ?> 
                                              checks)</td>
                                          </tr>
                                          <?  
										  	$checks =0;
											} 
											
											if(date('Y-m-d',strtotime($res['time_of_check'])) != date('Y-m-d',strtotime($start)))
											{
											$start = $res['time_of_check'];
												if($res['result'] == 'N')
												$checks = 1;
												
											}
											
											} // while
											if($checks !=0 )
											{
											?>
                                          <tr> 
                                            <td height="20"><strong><font color="#006666">&nbsp;</font></strong> 
                                              <? 
												//echo date('m/d/Y h:i:s A',strtotime($start));
												$hour = date('h',strtotime($start));
												$min = date('i',strtotime($start))+($time_zone * 60);
												$sec = date('s',strtotime($start));
												$mon = date('m',strtotime($start));
												$days = date('d',strtotime($start));
												$year = date('Y',strtotime($start));
												echo date('m/d/Y h:i:s A',mktime($hour,$min,$sec,$mon,$days,$year));
											?>
                                            </td>
                                            <td><? echo "Failed"; ?>&nbsp;(<? echo $checks; ?> 
                                              checks)</td>
                                          </tr>
                                          <?
									 	}
									
									}
									else
									{  
									?>
                                          <tr> 
                                            <td class="me" colspan="2" height="30" align="center"> 
                                              <font size="1">There are no statistics.</font></td>
                                          </tr>
                                          <? } ?>
                                        </table></td>
                                    </tr>
                                  </table></td>
                              </tr>
                          </table>
                        </div>
                        <? } ?>
                      </td>
                      <td align="right" valign="top" height="100%"> 
                        <? include("rightbar.php"); ?>
                      </td>
                    </tr>
                    <tr > 
                      <td background="../images/dotline.gif" height="1" colspan="3" valign="top"></td>
                    </tr>
                    <tr align="center" > 
                      <td  height="12" colspan="3" valign="top" class="tre12"></td>
                    </tr>
                    <tr align="center" > 
                      <td colspan="3"> 
                        <? include("footer.php"); ?>
                      </td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
