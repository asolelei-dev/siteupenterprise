<?
ob_start();
include ("../graphs/jpgraph.php"); 
include ("../graphs/jpgraph_pie.php"); 
include ("../graphs/jpgraph_pie3d.php"); 

$uptime = $_GET['uptime'];
$date = explode('/',$_GET['date']);
$month = $date[0];
$day = $date[1];
$daysuf = date('S',mktime(0,0,0,$month,$day,date('Y')));


if($uptime == 100)
{
$data = array($uptime); 
$legend = array("Uptime");
$colors = array("chartreuse3");
}
elseif($uptime == 0)
{
$data = array(100-$uptime); 
$legend = array("Downtime");
$colors = array("chocolate2");
}
else
{
$data = array($uptime,100-$uptime); 
$legend = array("Uptime","Downtime");
$colors = array("chartreuse3","chocolate2");
}


$graph = new PieGraph(350,225,"auto"); 
$graph->SetShadow(); 
//$graph->img->SetImgFormat("jpeg"); 
$graph->SetBackgroundImage("../images/backg.jpg",BGIMG_FILLFRAME);
$graph->SetBackgroundImageMix(60);
//$graph->SetColor(); 
  
//$graph->AdjBackgroundImage(); 

$graph->title->Set("$day$daysuf $month Monintoring Chart"); 
//$graph->title->SetFont(FF_VERDANA,FS_BOLD,15);
$graph->title->SetColor("lightslategray");



$p1 = new PiePlot3D($data); 
$p1->ExplodeSlice(1,25);
$p1->SetLabelPos(0.6);
$p1->SetLegends($legend);
$graph->legend->Pos(0.05,0.8,"right","center");
$p1->value->SetColor("royalblue"); 
$p1->value->SetFont(FF_FONT1,FS_BOLD); 
$p1->value->SetFormat("%0.2f %%");

$p1->SetSliceCOlors($colors);
$p1->SetStartAngle(250);
$p1->SetAngle(45);
$p1->SetSize(0.4);
$p1->SetCenter(0.5,0.52);

$graph->Add($p1); 
$graph->Stroke(); 

?>