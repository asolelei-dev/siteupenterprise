<?
include("../admin/include/conn.inc");
ob_start();
error_reporting(0);
$d = date('j');
$m = date('n');
$y = date('Y');

		$res_service = mysqli_fetch_array($db->query("select * from tbl_services where service_id = $_REQUEST[sid]"));
		$res_mem = mysqli_fetch_array($db->query("select memtype from tbl_users where userid = $res_service[userid]"));
		$res_mem = mysqli_fetch_array($db->query("select custom_time_zone,public_stats from tbl_membership where m_id = $res_mem[memtype]"));
		$time_zone = 0;
		if($res_mem['custom_time_zone'] == 'Y')
		{ 
		  	$res_time = mysqli_fetch_array($db->query("select timezone from tbl_users where userid = $res_service[userid]"));
			$time_zone = $res_time['timezone'];
			$res_tzstr = mysqli_fetch_array($db->query("select tzstring from tbl_timezones where tzval = $time_zone"));
			$time_zone_str = $res_tzstr['tzstring'];	
		}
		else
			$time_zone_str = "GMT 0.00";
?>
<html>
   <head>
      <title>Site Uptime Enterprise - Free Website Monitoring Service</title>
	  <link rel="stylesheet" href="../monitor_style.css" type="text/css">	  
  </head>
  
  <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" rightmargin="0" marginwidth="0" marginheight="0">
<table cellpadding="0" cellspacing="0" border="0" width="450" height="100%" align=center class="formtext"> 
<tr>
<td align="center" valign="top">
<table width="100%">
        <tr>
          <td valign="top" class="formtext">&nbsp;</td>
        </tr>
        <tr> 
          <td valign="top" class="formtext">&nbsp;</td>
        </tr>
        <tr> 
          <td align="center" valign="middle" class="formtext"> 
            <?
						  
						    $da1 = date('Y-m-d',mktime(0,0,0,$m,1,$y));
							$d1 = date('t',strtotime($da1));
							$da2 = date('Y-m-d',mktime(0,0,0,$m,$d1,$y));
						  
						  	
						  	//echo "select * from tbl_dailyservices where service_id=$_REQUEST[sid] and date_of_service between '$da1' and '$da2'";
						    $qry_day = $db->query("select * from tbl_dailyservices where service_id=$_REQUEST[sid] and date_of_service between '$da1' and '$da2'");
							$qry_summary = $db->query("select * from tbl_monthlyservices where service_id = $_REQUEST[sid] and YEAR(date_of_service) = ".date('Y')." order by date_of_service desc");
						  
				if(mysqli_num_rows($qry_summary)>0 ||  mysqli_num_rows($qry_day)>0)
				{
				?>
            <TABLE class=formtext border=0 cellPadding=0 cellSpacing=0 width="100%">
              <TBODY>
                <?
						  $tot_checks = 0;
						  $downtime = 0;
						  $i=0;
						  $resp_time = 0;
						  
						  //$date_of_servie='';
						  // present month details
						  
 						    
							if(mysqli_num_rows($qry_day)>0)
							{	
						  		$date_of_service = $da1; // date
								$no_of_checks = 0;
								$down_checks = 0;
								$resptime = 0;
								$count = 0;
								while($res_day=mysqli_fetch_array($qry_day))
								{
									$down_checks += $res_day['down_checks'];
									$no_of_checks += $res_day['no_of_checks'];
									$resptime += $res_day['resp_time'];
									$count++;
									
								}//while($res_day=mysqli_fetch_array($qry_day))
					
								$resptime = number_format($resptime/$count, 4, '.', '');		
								
									$summary[$i][0] = $date_of_service;
									$summary[$i][1] = $no_of_checks;
									$tot_checks += $no_of_checks;
									$summary[$i][2] = $down_checks;
									$downtime += $down_checks;
									$resp_time += $resptime;
									$i++;
						  
						  }// if (mysqli_num_rows());
						  
						  
						  
						 // echo mysqli_num_rows($qry_summary);
						  while($res_summary = mysqli_fetch_array($qry_summary))
						  {
						  	$summary[$i][0] = $res_summary['date_of_service'];
							$summary[$i][1] = $res_summary['no_of_checks'];
							$tot_checks += $res_summary['no_of_checks'];
							$summary[$i][2] = $res_summary['down_checks'];
							$downtime += $res_summary['down_checks'];
							$resp_time += $res_summary['resp_time'];
							$i++;
						  }
						  
						  ?>
                <TR> 
                  <TD height="30" align="center" valign="top" class=body> <h3 class="bg1"><? echo $res_service['host_name']; ?></h3></td>
                </tr>
                <tr> 
                  <td align="center" height="20"> <strong>Since :</strong> <? echo date('F d, Y',strtotime($res_service['date_add'])); ?> 
                  </td>
                </tr>
                <tr> 
                  <td align="center" height="20"> <strong>Outages :</strong> <? echo $downtime; ?> 
                  </td>
                </tr>
                <tr> 
                  <td align="center" height="20"> <strong>Total Uptime :</strong> 
                    <?  $uptime = number_format((($tot_checks - $downtime)*100)/$tot_checks,2,'.',','); 
											  echo $uptime; ?>
                    %</TD>
                </TR>
                <?
				$res_last = mysqli_fetch_array($db->query("select time_of_check from tbl_dayservices where service_id = $_REQUEST[sid] order by time_of_check desc limit 0,1"));
			?>
                <tr> 
                  <td align="center" height="20"> <strong>Last Check :</strong> 
                    <? 	$nMin = $time_zone * 60;
						$dt = strtotime($res_last['time_of_check']);
						echo date('F  d, Y \a\t h:i A',mktime(date("H",$dt), date("i",$dt)+$nMin, date("s",$dt), date("m",$dt), date("d",$dt), date("Y",$dt)));
						?>&nbsp;(<? echo $time_zone_str; ?> )
                  </td>
                </tr>
                <tr> 
                  <td align="center" height="20"></td>
                </tr>
                <TR> 
                  <TD align="center" valign="top" class=body> <table width="300" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="cccccc" class=mt>
                      <tr class="bg4"> 
                        <td width="125" height="20"><strong>&nbsp;&nbsp;<font color="#006666">&nbsp;</font>Year</strong><br></td>
                        <td width="125"><strong>&nbsp;&nbsp;Outages</strong></td>
                        <td><strong>&nbsp;&nbsp;Uptime</strong></td>
                      </tr>
                      <tr bgcolor="ffffff"> 
                        <td height="20"><strong>&nbsp;&nbsp;<font color="#006666">&nbsp;</font></strong><? echo $y; ?></td>
                        <td> <a class="mt"><strong>&nbsp;&nbsp;</strong><? echo $downtime; ?></a> 
                        </td>
                        <td><strong>&nbsp;&nbsp;</strong><? echo $uptime; ?>%</td>
                      </tr>
                    </table></TD>
                </TR>
                <TR> 
                  <TD align="left" valign="top" class=body>&nbsp; </TD>
                </TR>
                <TR> 
                  <TD align="left" valign="top" class=body>&nbsp;</TD>
                </TR>
                <TR> 
                  <TD align="center" valign="top" class=body><table width="350" border="0" cellpadding="0" cellspacing="1" bgcolor="cccccc" class=formtext>
                      <tr bgcolor="#E1F0F4" class="bg4"> 
                        <td><strong>&nbsp;&nbsp;<font color="#006666"></font><font color="#006666">&nbsp;</font>Year</strong></td>
                        <td><strong>&nbsp;&nbsp;<font color="#006666"></font>Month</strong></td>
                        <td><strong>&nbsp;&nbsp;<font color="#006666"></font>Outages</strong></td>
                        <td><strong>&nbsp;&nbsp;<font color="#006666"></font>Uptime</strong></td>
                        <td><strong>&nbsp;&nbsp;<font color="#006666"></font>Downtime</strong></td>
                      </tr>
                      <? 
										$graph_summary = array();
										for($i=0;$i<count($summary);$i++)
											{
											$graph_summary = array_merge($graph_summary,date('m',strtotime($summary[$i][0])),number_format((($summary[$i][1]-$summary[$i][2])*100)/$summary[$i][1],2,'.',','));
										?>
                      <tr bgcolor="ffffff"> 
                        <td height="20"><strong>&nbsp;&nbsp;<font color="#006666"></font><font color="#006666">&nbsp;</font><font color="#006666">&nbsp;</font></strong><? echo $y; ?></td>
                        <td><strong>&nbsp;&nbsp;<font color="#006666"></font><font color="#006666">&nbsp;</font></strong><? echo date('F',strtotime($summary[$i][0])); 
										  ?></td>
                        <td> <a class="mt"><strong>&nbsp;&nbsp;<font color="#006666"></font><font color="#006666">&nbsp;</font></strong><? echo $summary[$i][2]; ?></a> 
                        </td>
                        <td><strong>&nbsp;&nbsp;<font color="#006666"></font><font color="#006666">&nbsp;</font></strong><? echo number_format((($summary[$i][1]-$summary[$i][2])*100)/$summary[$i][1],2,'.',','); ?>%</td>
                        <td><strong>&nbsp;&nbsp;<font color="#006666"></font><font color="#006666">&nbsp;</font></strong><? echo number_format(($summary[$i][2]*100)/$summary[$i][1],2,'.',','); ?>% 
                        </td>
                      </tr>
                      <? } ?>
                    </table></TD>
                </TR>
                <TR> 
                  <TD align="center" valign="middle" height="50" class=body>Average 
                    response time: <strong><? echo number_format($resp_time/$i,4,'.',','); ?></strong> 
                    seconds</TD>
                </tr>
              </TBODY>
            </TABLE>
            <? } // if(mysqli_num_rows($qry_summary)>0 &&  mysqli_num_rows($qry_day)) 
				  else
				  {
				  ?>
            <span align="left" class="me" style="margin-left:10px;"><br>
            There are no statistics</span> 
            <? } ?>
          </td>
        </tr>
        <tr> 
          <td height="30" align="center" class=tre11-blue> <strong>Get Notified When Your 
            Site Goes Down!</strong> </td>
        </tr>
        <tr> 
          <td height="40" align="center" class=hlink> <a href="http://www.alstrasoft.com/uptime">Site Uptime Enterprise 
            Website Monitoring</a> </td>
        </tr>
        <tr> 
          <td align="center" class=formtext> Site Uptime Enterprise provides <b>free</b> web 
            site monitoring services. <br>
            We monitor your site 24/7/365 from multiple geographic locations.<br>
            When your site is unavailable, we immediately notify you via email. 
          </td>
        </tr>
        <tr> 
          <td height="40" align="center" class=hlink> <a href="http://www.alstrasoft.com/uptime/register.php">Register 
            Today!</a> </td>
        </tr>
		<tr> 
          <td height="15" align="left" class=hlink> <strong>Monitored By :</strong> </td>
        </tr>
		<tr> 
          <td align="left" class=hlink><a href="http://www.alstrasoft.com/uptime" class="hlink"><img src="../images/logo_247.gif" border="0"></a></td>
        </tr>
      </table>
</td></tr></table>

</html>
