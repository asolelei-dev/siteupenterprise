<?
include("../functions.php");
include("../admin/include/conn.inc");
error_reporting(0);
ob_start();
session_start();

if(!isset($_SESSION['mnuser']))
{
header("Location: index.php");
exit();
}
// Define variables to prevent undefined variable errors.
$location = ''; 
$updateloc = false;

// print_r($_POST);
if(isset($_REQUEST['qcid']))
{
	$res_loop = true;

				
		$res_QCResp = mysqli_fetch_array($db->query("select * from tbl_quickcheck where qc_id = $_GET[qcid]"));
				
		$qry_loc = "select * from tbl_locations where status = 'Y' order by loc_id";
			$lids = array();
			$results = array();
			$res_times = array();
			$i = 0;
			$rs_loc = $db->query($qry_loc);
			while($ds_loc = mysqli_fetch_array($rs_loc))
				$lids[$i++] = $ds_loc['loc_id'];
	
			for($i=0;$i<count($lids);$i++)	
			{
				$str = 'loc'.$lids[$i];
				$locations[$i] = 'location'.$lids[$i];
				$results[$i] = 'result'.$lids[$i];
				$res_times[$i] = 'resp_time'.$lids[$i];
			}
				
			$location = 'Y';
			
			for($i=0;$i<count($lids);$i++)
			{
				if($res_QCResp[$locations[$i]] == 'Y' && $res_QCResp[$results[$i]] == "")
				{
					$res_loop = false;
					break;
				}
				else
				{
					$result = $res_QCResp[$results[$i]];
					$resp = $res_QCResp[$res_times[$i]];
					$location = $res_QCResp[$locations[$i]];
				}
			}
		
				
		if($res_loop == false)
		{
			//sleep(20);
			header("Location: myservices.php?qcid=$_GET[qcid]&&service_id=$_GET[service_id]");
			exit();
		}
		elseif($location == 'Y')
		{
			$rs_Temp = mysqli_fetch_array($db->query("select * from tbl_services where service_id = $_GET[service_id]"));
			
			$res_port = mysqli_fetch_array($db->query("select service_name,port_no from tbl_service_ports where service_id =$rs_Temp[service_port]"));
			if($rs_Temp['custom_port'] == 0)
			$port = $res_port['service_name'];
			else
			$port = $rs_Temp['custom_port'];
			
			$res_mem = mysqli_fetch_array($db->query("select custom_time_zone from tbl_membership where m_id = $_SESSION[memid]"));
			$time_zone = 0;
			if($res_mem['custom_time_zone'] == 'Y')
			{ 
				$res_time = mysqli_fetch_array($db->query("select timezone from tbl_users where userid = $_SESSION[mnuser]"));
				$time_zone = $res_time['timezone'];
				$res_tzstr = mysqli_fetch_array($db->query("select tzstring from tbl_timezones where tzval = $time_zone"));
				$time_zone_str = $res_tzstr['tzstring'];	
			}
			else
				$time_zone_str = "GMT 0.00";
			
			$d = $res_QCResp['date'];
			$nMin = $time_zone * 60;
			$hour = date('h',strtotime($d));
			$min = date('i',strtotime($d));
			$sec = date('s',strtotime($d));
			$mon = date('m',strtotime($d));
			$days = date('d',strtotime($d));
			$year = date('Y',strtotime($d));
			$dt = date('m/d/Y h:i:s A',mktime($hour,$min+$nMin,$sec,$mon,$days,$year));
			$dt = explode(' ',$dt);  
			$strtime = $dt[0]."&nbsp;".$dt[1]." $time_zone_str ";
			
			
			if($result == "Failed")
			{
			$alert_type = "Site is Not Available";
			$result = "Failed";
			}
			else
			{
			$alert_type = "Site is Available";
			$result = "Ok";
			}
					
			
						
			$sql_TempUser = "select * from tbl_users where userid = '$rs_Temp[userid]'";
			$ds_TempUser  = $db->query($sql_TempUser);
			$rs_TempUser  = mysqli_fetch_array($ds_TempUser);
					
			// mail sending
			/**************************************************************************
							sending Mail for added new service
			***************************************************************************/
			
				 $res_mail = mysqli_fetch_array($db->query("select * from tbl_mailing where id = 2"));
				 $desc[0] = $res_mail['mail_txt'];
				 $desc[1] = $res_mail['mail_sub'];
				 $desc = implode('^|^',$desc);
				 $desc = str_replace('%%name%%',$rs_TempUser['name'],$desc);
				 $desc = str_replace('%%emailid%%',$rs_TempUser['emailid'],$desc);
				 $desc = str_replace('%%password%%',$rs_TempUser['password'],$desc);
				 $desc = str_replace('%%joindate%%',date('m-d-Y',strtotime($rs_TempUser['joindate'])),$desc);
				 $desc = str_replace('%%company%%',$rs_TempUser['company'],$desc);
				 $desc = str_replace('%%address%%',$rs_TempUser['address'],$desc);
				 $desc = str_replace('%%city%%',$rs_TempUser['city'],$desc);
				 $desc = str_replace('%%zip%%',$rs_TempUser['zip'],$desc);
				 $desc = str_replace('%%aemail%%',$rs_TempUser['aemail'],$desc);
				 
				 if(strstr($desc,'%%memtype%%'))
				 {
				   $q = mysqli_fetch_array($db->query("select mem_name from tbl_memebership where m_id = $rs_TempUser[memetyp]"));
	 			   $desc = str_replace('%%memtype%%',$q[0],$desc); 
				 }
				 if(strstr($desc,'%%state%%'))
				 {
				   $q = mysqli_fetch_array($db->query("select statename from tbl_states where stateid = $rs_TempUser[state]"));
	 			   $desc = str_replace('%%state%%',$q[0],$desc); 
				 }
				 if(strstr($desc,'%%country%%'))
				 {
				   $q = mysqli_fetch_array($db->query("select name from tbl_country where countryid = $rs_TempUser[country]"));
	 			   $desc = str_replace('%%country%%',$q[0],$desc); 
				 }
				 
				 if(strstr($desc,'%%location%%'))
				 {
				   $q = mysqli_fetch_array($db->query("select loc_name from tbl_locations where loc_id = $rs_Temp[location]"));
	 			   $desc = str_replace('%%location%%',$q[0],$desc); 
				 }
				 				 
				 $desc = str_replace('%%time%%',$strtime,$desc);
				 
				 $desc = str_replace('%%host_name%%',$rs_Temp['host_name'],$desc);
				 $desc = str_replace('%%date_add%%',date('m-d-Y',strtotime($rs_Temp['date_add'])),$desc);
				 $desc = str_replace('%%service_port%%',$port,$desc);
				 $desc = str_replace('%%period%%',$rs_Temp['period'],$desc);
				 $desc = str_replace('%%alert_type%%',$alert_type,$desc);
				 $desc = str_replace('%%result%%',$result,$desc);
				 $desc = explode('^|^',$desc);
			$message="<style> .kk { font-family: trebuchet ms; color: #336699; 	font-size: 11px; font-style: normal; font-weight: normal; font-variant: normal; text-decoration: none;}</style>";
			$message.="<table width='90%' align='center'><tr><td colspan=3 class=kk>";
			$message.="<a href='http://www.alstrasoft.com/uptime' target=_blank><img border=0 src='http://www.alstrasoft.com/uptime/images/logo_247.gif' align='center'></a></td></tr><tr>";
			$message.="<tr><td class=kk colspan=3 height=20 valign='bottom'>";
			$message.= html_entity_decode($desc[0]);
			$message.="</td></tr>";
			$message.="<tr><td colspan=3 class=kk height=40>For further assistance, please contact <a href='mailto:support@alstrasoft.com'>support@alstrasoft.com</a>.</td></tr>";
			$message.="<tr><td colspan=3 class=kk height=40 align=left><br>$res_mail[mail_closing]</td></tr></table>";
						
				$headers  = "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
				$headers .= "From: $res_mail[mail_from]\r\n";
			 	$to = $rs_TempUser['emailid'];
			 	$subject=$desc[1];
				mail($to, $subject, $message, $headers);
			
			//echo $message;			
			
			/*******************************************************************************/
	
	header("Location: myservices.php");
	exit();
	}
$db->query("delete from tbl_quickcheck where qc_id = $_GET[qcid]");
}


if(isset($_POST['action']))
{
if($_POST['action']=='add')
{
$qry_mem = mysqli_fetch_array($db->query("select total_sites from tbl_membership where m_id = $_SESSION[memid]"));
$qry_service = mysqli_fetch_array($db->query("select count(service_id) from tbl_services where userid = $_SESSION[mnuser]"));
if($qry_mem[0]>$qry_service[0])
{
	if(isset($_POST['alert_emails']))
		$alert_emails = implode(',',$_POST['alert_emails']);
	else
		$alert_emails = NULL;
	
	if(isset($_POST['alert_mobiles']))
		$alert_mobiles = implode(',',$_POST['alert_mobiles']);
	else
		$alert_mobiles = NULL;
	
	$smsalert_down = $_POST['downSmsAlert'];
	
	 if($_POST['txtstatusemail'] == 1)
	 $semail = 'Y';
	 else
	 $semail = 'N';
	 if(isset($_POST['txtsms']) && $_POST['txtsms']==1)
	 $ssms = 'Y';
	 else
	 $ssms = 'N';
	 
	 if($_POST['txtactive']==1)
	 $sactive = 'Y';
	 else
	 $sactive = 'N';
	 if(isset($_POST['txtcustom']))
	  $custom = $_POST['txtcustom'];
	 else
	 $custom = 0;
		
	 $qry="insert into tbl_services(service_name,host_name,userid,date_add,service_port,custom_port,period,location,status_email,email,status_sms,last_check,active,monitor_interval_level,mobile,multi_mails,multi_mobiles,smsalert_down)
	  values('".$_POST['txtsname']."','".$_POST['txthostname']."','".$_SESSION['mnuser']."','".date('Y-m-d H:i:s')."','"
		.$_POST['txtserviceid']."','".$custom."','".$_POST['txtperiod']."','".$_POST['txtlocation']."','".$semail."','"
		.$_POST['txtemail']."','".$ssms."','" .date('Y-m-d H:i:s')."','".$sactive."','".$_POST['monitor_level']."','$_POST[txtmobile]','$alert_emails','$alert_mobiles','$smsalert_down')";
		$db->query($qry);
		
		 $ser_id=mysql_insert_id();
	
			if($custom == 0 )
			{
			$q = mysqli_fetch_array($db->query("select port_no from tbl_service_ports where service_id = $_POST[txtserviceid]"));
			$port = $q['port_no'];
			}
			else
			$port = $custom;
				
				$quickurl = splitaddr($_POST['txthostname']);
				$service_id = $_POST['txtserviceid'];
				$loc[0] = $_POST['txtlocation'];
				$qry_loc = "select * from tbl_locations where status = 'Y'  order by loc_id";
				$lids = array();
				$temp = array();
				$locids = array();
				$results = array();
				$res_times = array();
				$res_ips = array();
				$i = 0;
				$rs_loc = $db->query($qry_loc);
				while($ds_loc = mysqli_fetch_array($rs_loc))
				{
					$lids[$i] = $ds_loc['loc_id'];
					$res_ips[$i] = $ds_loc['ip_addr'];
					$i++;
				}
		
				for($i=0;$i<count($lids);$i++)	
				{
					$locids[$i] = "'N'";
					$locations[$i] = 'location'.$lids[$i];
					$results[$i] = 'result'.$lids[$i];
					$res_times[$i] = 'resp_time'.$lids[$i];
				}
					
				for($j=0;$j<count($loc);$j++) {
					for($i=0;$i<count($lids);$i++)
					{
						$resptime = checkurl($res_ips[$i],80);
						$str = 'loc'.$lids[$i];
						$loc[$j]." == ".$lids[$i];
						if($loc[$j] == $lids[$i])
						{
							$locids[$i] = "'Y'";
							if($resptime[0] != 'Ok')
							$locids[$i] = "'E'";
						}
						
					} // for
				} // for
				
				$locations1 = implode(',',$locations); // database field names for quick check  like location1
				$locids1 = implode(',',$locids); // values location status for quick check  like 'Y' or 'N'
				 
				
				$sql_InsertQC = "insert into tbl_quickcheck(qc_url,date,qc_port,$locations1) values('$quickurl','".gmdate('Y-m-d H:i:s')."', $service_id, $locids1)";
					$db->query($sql_InsertQC);
					
					$qc_id = mysql_insert_id();
					
					$res_port = mysqli_fetch_array($db->query("select port_no from tbl_service_ports where service_id = $service_id"));
					$loc_ip = mysqli_fetch_array($db->query("select * from tbl_locations where loc_id=1"));
					if($locids[0] == "'Y'" && $res_ips[0] == $loc_ip['ip_addr'])
					{
						$resptime = checkurl($_POST['txthostname'],$res_port['port_no']);
						//print_r($resptime);
						$db->query("update tbl_quickcheck set $results[0]='$resptime[0]', $res_times[0]='$resptime[1]' where qc_id = $qc_id");
					}
						
				
				//sleep(20);
				header("Location: myservices.php?qcid=$qc_id&&service_id=$ser_id");
				exit();
			
} // if
} // if action ==addd

if($_POST['action'] == 'delete')
{
// echo "select * from tbl_services where service_id = $_POST[serviceid]";
 $rs_Temp = mysqli_fetch_array($db->query("select * from tbl_services where service_id = $_POST[serviceid]"));
				
			$sql_TempUser = "select * from tbl_users where userid = '$rs_Temp[userid]'";
			$ds_TempUser  = $db->query($sql_TempUser);
			$rs_TempUser  = mysqli_fetch_array($ds_TempUser);
			$rs_TempUser['multi_mails'];
			if($rs_TempUser['multi_mails'] != '')
			{
				$mails1 = $mails = array();
				$mails1 = explode(',',$rs_TempUser['multi_mails']);
				
				foreach($mails1 as $index=>$value)
					if($value != $_POST['serviceid'])
						$mails[] = $value;
				$mails = implode(',',$mails);
				$db->query("update tbl_users set multi_mails = $mails where userid = $rs_Temp[userid]");
			}
			if($rs_TempUser['multi_mobiles'] != '')
			{
				$mobiles1 = $mobiles = array();
				$mobiles1 = explode(',',$rs_TempUser['multi_mobiles']);
				foreach($mobiles1 as $index=>$value)
					if($value != $_POST['serviceid'])
						$mobiles[] = $value;
				 $mobiles = implode(',',$mobiles);
				$db->query("update tbl_users set multi_mobiles = $mobiles where userid = $rs_Temp[userid]");
			}
			
		
		if($rs_Temp['custom_port'] == 0)
		{
		$q = mysqli_fetch_array($db->query("select service_name from tbl_service_ports where service_id = $rs_Temp[service_port]"));
		$port = $q[0];
		}
		else
		$port = $rs_Temp['custom_port'];
				 
				 $res_mail = mysqli_fetch_array($db->query("select * from tbl_mailing where id = 3"));
				 $desc[0] = $res_mail['mail_txt'];
				 $desc[1] = $res_mail['mail_sub'];
				 $desc = implode('^|^',$desc);
				 $desc = str_replace('%%name%%',$rs_TempUser['name'],$desc);
				 $desc = str_replace('%%emailid%%',$rs_TempUser['emailid'],$desc);
				 $desc = str_replace('%%password%%',$rs_TempUser['password'],$desc);
				 $desc = str_replace('%%joindate%%',date('m-d-Y',strtotime($rs_TempUser['joindate'])),$desc);
				 $desc = str_replace('%%company%%',$rs_TempUser['company'],$desc);
				 $desc = str_replace('%%address%%',$rs_TempUser['address'],$desc);
				 $desc = str_replace('%%city%%',$rs_TempUser['city'],$desc);
				 $desc = str_replace('%%zip%%',$rs_TempUser['zip'],$desc);
				 $desc = str_replace('%%aemail%%',$rs_TempUser['aemail'],$desc);
				 
				 if(strstr($desc,'%%memtype%%'))
				 {
				   $q = mysqli_fetch_array($db->query("select mem_name from tbl_memebership where m_id = $rs_TempUser[memetyp]"));
	 			   $desc = str_replace('%%memtype%%',$q[0],$desc); 
				 }
				 if(strstr($desc,'%%state%%'))
				 {
				   $q = mysqli_fetch_array($db->query("select statename from tbl_states where stateid = $rs_TempUser[state]"));
	 			   $desc = str_replace('%%state%%',$q[0],$desc); 
				 }
				 if(strstr($desc,'%%country%%'))
				 {
				   $q = mysqli_fetch_array($db->query("select name from tbl_country where countryid = $rs_TempUser[country]"));
	 			   $desc = str_replace('%%country%%',$q[0],$desc); 
				 }
				 
				 if(strstr($desc,'%%location%%'))
				 {
				   $q = mysqli_fetch_array($db->query("select loc_name from tbl_locations where loc_id = $rs_Temp[location]"));
	 			   $desc = str_replace('%%location%%',$q[0],$desc); 
				 }
				 				 
				 			 
				 $desc = str_replace('%%host_name%%',$rs_Temp['host_name'],$desc);
				 $desc = str_replace('%%date_add%%',date('m-d-Y',strtotime($rs_Temp['date_add'])),$desc);
				 $desc = str_replace('%%service_port%%',$port,$desc);
				 $desc = str_replace('%%period%%',$rs_Temp['period'],$desc);
				 $desc = explode('^|^',$desc);
			$message="<style> .kk { font-family: trebuchet ms; color: #336699; 	font-size: 11px; font-style: normal; font-weight: normal; font-variant: normal; text-decoration: none;}</style>";
			$message.="<table width='90%' align='center'><tr><td colspan=3 class=kk>";
			$message.="<a href='http://www.alstrasoft.com/uptime' target=_blank><img border=0 src='http://www.alstrasoft.com/uptime/images/logo_247.gif' align='center'></a></td></tr><tr>";
			$message.="<tr><td class=kk colspan=3 height=20 valign='bottom'>";
			$message.= html_entity_decode($desc[0]);
			$message.="</td></tr>";
			$message.="<tr><td colspan=3 class=kk height=40>For further assistance, please contact <a href='mailto:support@alstrasoft.com'>support@alstrasoft.com</a>.</td></tr>";
			$message.="<tr><td colspan=3 class=kk height=40 align=left><br>$res_mail[mail_closing]</td></tr></table>";
						
				$headers  = "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
				$headers .= "From: $res_mail[mail_from]\r\n";
				$to = $rs_TempUser['emailid'];
				$subject=$desc[1];
				mail($to, $subject, $message, $headers);
			
			//echo $message;			
			
			/*******************************************************************************/

		$qr = "delete from tbl_services where service_id = $_POST[serviceid]";
		$db->query($qr);
		
		header("Location: myservices.php");
		exit();
}

if($_POST['action']=='update')
{
$loc = mysqli_fetch_array($db->query("select ip_addr from tbl_locations where loc_id = $_POST[txtlocation]"));
$resptime = checkurl($loc['ip_addr'],80);
if($resptime[0] == 'Ok')
{
			if(isset($_POST['alert_emails']))
				$alert_emails = implode(',',$_POST['alert_emails']);
			else
				$alert_emails = NULL;
			
			if(isset($_POST['alert_mobiles']))
				$alert_mobiles = implode(',',$_POST['alert_mobiles']);
			else
				$alert_mobiles = NULL;
			
			$smsalert_down = $_POST['downSmsAlert'];
			
		 if($_POST['txtstatusemail'] == 1)
		 $semail = 'Y';
		 else
		 $semail = 'N';
		 if(isset($_POST['txtsms']) && $_POST['txtsms']==1)
		 $ssms = 'Y';
		 else
		 $ssms = 'N';
		 
		 if($_POST['txtactive']==1)
		 $sactive = 'Y';
		 else
		 $sactive = 'N';
		 if(isset($_POST['txtcustom']))
		  $custom = $_POST['txtcustom'];
		 else
		 $custom = 0;
		
		$qry="update tbl_services set service_name = '$_POST[txtsname]', host_name = '$_POST[txthostname]', 
			service_port = '$_POST[txtserviceid]', custom_port = '$custom', period = '$_POST[txtperiod]', 
			location = '$_POST[txtlocation]', status_email = '$semail', email='$_POST[txtemail]',
			status_sms = '$ssms', active = '$sactive', mobile = '$_POST[txtmobile]',multi_mails = '$alert_emails', 
			multi_mobiles = '$alert_mobiles', smsalert_down = $smsalert_down where service_id = $_POST[service_id]";
			$db->query($qry);
		
}
else
{
 $updateloc = true;
} // if resptime
} // update
} // act
//check for member levels
$sql_MSDet = "select * from tbl_membership where m_id = $_SESSION[memid]";
$ds_MSDet  = $db->query($sql_MSDet);
$rs_MSDet  = mysqli_fetch_array($ds_MSDet);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site Uptime Enterprise</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../monitor_style.css" rel="stylesheet" type="text/css">
<script src="../javascript/FScheckDomain.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">

function trim(inputString)
{
   // Removes leading and trailing spaces from the passed string. Also removes
   // consecutive spaces and replaces it with one space. If something besides
   // a string is passed in (null, custom object, etc.) then return the input.
	
   var retValue = inputString;

   var ch = retValue.substring(0, 1);
   while (ch == " ") { // Check for spaces at the beginning of the string
      retValue = retValue.substring(1, retValue.length);
      ch = retValue.substring(0, 1);
   }

   ch = retValue.substring(retValue.length-1, retValue.length);
   while (ch == " ") { // Check for spaces at the end of the string
      retValue = retValue.substring(0, retValue.length-1);
      ch = retValue.substring(retValue.length-1, retValue.length);
   }

   return retValue; // Return the trimmed string back to the user
} // Ends the "trim" function


function checkport(frm)
{
<? $qr = mysqli_fetch_array($db->query("select * from tbl_service_ports where port_no < 0")); 
?>

	if(<? echo $qr[0] ?> == frm.txtserviceid.value)
	{
	
	frm.txtcustom.disabled = false;
	frm.txtcustom.value='';
	frm.txtcustom.focus();
	}
	else
	{
	frm.txtcustom.value=0;
	frm.txtcustom.disabled = true;
	}
}


function deletefun(sid)
{
if(confirm("Do You want to delete"))
{
document.delfrm.serviceid.value=sid;
document.delfrm.submit();
}
}

function editfun(sid)
{
document.edfrm.serviceid.value=sid;
document.edfrm.submit();
}

function statfun(sid)
{
document.statfrm.serviceid.value=sid;
document.statfrm.submit();
}


function addvalidate()
{
var frm = document.addfrm;
  
  	if(frm.txthostname.value == '')
	{
	alert("Please Enter Host Name ");
	frm.txthostname.focus();
	return false;
	}
	if(!validateDomain(frm.txthostname))
	{
	alert("Please enter the host name in the form of 'alstrasoft.com'");
	return false;
	}
	
	if((frm.txtcustom.disabled == false)&&isNaN(frm.txtcustom.value))
	{
	alert("Please Enter Valid Port Number");
	frm.txtcustom.focus();
	return false;
	}
	
 if(trim(frm.txtemail.value) == '')	
 {
 	alert("Please Enter a Alert Email ID, It should not be Empty");
	frm.txtemail.focus();
	return false;
 }
	
  if(trim(frm.txtemail.value) != '')
  {
  
	var Temp     = frm.txtemail;
	var AtSym    = Temp.value.indexOf('@');
	var Period   = Temp.value.lastIndexOf('.');
	var Space    = Temp.value.indexOf(' ');
	var Length   = Temp.value.length - 1;

	  if (AtSym < 1 || Period <= AtSym+1 || Period == Length)                  
	  {  
		 alert('Please enter a valid e-mail address!')
		 Temp.focus();
		 return false;
	  }
  }  
  
  	 if(trim(frm.txtmobile.value).length >= 0 )
	  {
		  if(isNaN(frm.txtmobile.value))
		  {
				alert("Please Enter a Valid Mobile Number");
				frm.txtmobile.focus();
				return false;
		  }
	  }

return true;
}

function editvalidate()
{
var frm = document.editfrm;
  
	if(frm.txthostname.value == '')
	{
	alert("Please Enter Host Name ");
	frm.txthostname.focus();
	return false;
	}
	if(!validateDomain(frm.txthostname))
	{
	alert("Please enter the host name in the form of 'alstrasoft.com'");
	return false;
	}
	if((frm.txtcustom.disabled == false)&&isNaN(frm.txtcustom.value))
	{
	alert("Please Enter Valid Port Number");
	frm.txtcustom.focus();
	return false;
	}
	
	 if(trim(frm.txtemail.value) == '')	
	 {
		alert("Please Enter a Alert Email ID, It should not be Empty");
		frm.txtemail.focus();
		return false;
	 }
	
	 if(trim(frm.txtemail.value) == '')
	  {
		var Temp     = frm.txtemail;
		var AtSym    = Temp.value.indexOf('@');
		var Period   = Temp.value.lastIndexOf('.');
		var Space    = Temp.value.indexOf(' ');
		var Length   = Temp.value.length - 1;
	
		  if (AtSym < 1 || Period <= AtSym+1 || Period == Length)                  
		  {  
			 alert('Please enter a valid e-mail address!')
			 Temp.focus();
			 return false;
		  }
	  }  
	   
	  if(trim(frm.txtmobile.value).length >= 0 )
	  {
		  if(isNaN(frm.txtmobile.value))
		  {
				alert("Please Enter a Valid Mobile Number");
				frm.txtmobile.focus();
				return false;
		  }
	  }
return true;
}

</script>
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="730" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="504" align="center" valign="top">
					   <? if((!isset($_POST['action'])||$_POST['action'] == 'add'||($_POST['action'] == 'update' && $updateloc != true)||$_POST['action'] == 'delete') && $location != 'E')
					   { ?>
					  <div id="servicelist">
					  	<TABLE class=mt border=0 cellPadding=0 cellSpacing=0 width="480">
                          <TBODY>
                            <TR> 
                                <TD align="left" valign="top" class=body> <table width="450" border="0" cellspacing="0" cellpadding="0">
                                    <tr> 
                                      <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                      <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                      <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                        </strong></font><font color="#006699">My 
                                        Services</font></B></td>
                                    </tr>
                                    <tr > 
                                      <td background="../images/dotline.gif" colspan="3"></td>
                                    </tr>
                                  </table>
                                  </TD>
                            </TR>
                            <TR> 
                              <TD height="15" align="left" valign="top" class=body></TD>
                            </TR>
							
                            <TR> 
                              <TD align="center" valign="top" class=body><table class=mt width="450" style="border:1px solid #336666" cellspacing="1" cellpadding="0">
                                  <tr> 
                                      <td width="339" height="20" class="bbg5"><strong><font color="#006666">&nbsp;<span class="c4">My 
                                        Services</span></font></strong></td>
                                  </tr>
                                  <tr> 
                                    <td align="left" valign="top"><table width="450" border="0" cellpadding="0" cellspacing="1" bgcolor="efefef" class=formtext>
                                        <tr bgcolor="#E1F0F4" class="bg4"> 
                                          <td width="100" height="20"><strong><font color="#006666">&nbsp;</font>Host 
                                            Name</strong></td>
                                          <td width="45"><strong>Service</strong></td>
                                          <td width="80"><strong>Last Check</strong></td>
                                          <td colspan="3" align="center"><strong>Action</strong></td>
                                        </tr>
                                        <? $ser_qry = $db->query("select service_id,host_name,service_port,custom_port,last_check from tbl_services where userid = $_SESSION[mnuser] order by date_add desc");
										if(mysqli_num_rows($ser_qry)>0)
									{
										 $i=0;
										while($ser=mysqli_fetch_array($ser_qry))
										{
										 $i++;
										?>
										<tr bgcolor="ffffff"> 
                                          <td height="30" class="bodytext"><strong><font color="#006666">&nbsp;</font></strong><? echo $ser['host_name']; ?></td>
										  <? if($ser['custom_port'] == 0)
										  {
										  $sq = mysqli_fetch_array($db->query("select service_name from tbl_service_ports where service_id=$ser[service_port]"));
										  $port  = $sq[0];
										  }
										  else
										  $port = $ser['custom_port'];
										  ?>
                                          <td width="60"  class="bodytext"><? echo $port; ?></td>
										  <?
										   $d=explode(' ',$ser['last_check']);
										   $dt=explode('-',$d[0]);
										  ?>
                                          <td  class="bodytext"><?  echo $dt[1]."/".$dt[2]."/".$dt[0]; ?></td>
                                          <td width="40" height="20" align="center"><a class=kk href="javascript: editfun(<? echo $ser['service_id']; ?>)">Edit</a></td>
                                          <td width="60" align="center"><a class=kk href="javascript: deletefun(<? echo $ser['service_id']; ?>)">Delete</a></td>
                                          <td><a class=kk href="javascript: statfun(<? echo $ser['service_id']; ?>)">Statistics</a></td>
                                           </tr>
											  <? } // while
										  } else { 
											  ?>
											  <tr bgcolor="ffffff">
											  
                                            <td colspan="6" align="center" class="kk"> 
                                              There are no services </td> </tr>
									 <? } ?>
											  
											  <tr><td>
												  	<form action="myservices.php" method="post" name='edfrm'>
													<input type="hidden" value=0 name="serviceid">
													<input type="hidden" value="edit" name="action">
													</form>
	
											  		<form action="myservices.php" method="post" name='delfrm'>
													<input type="hidden" value=0 name="serviceid">
													<input type="hidden" value="delete" name="action">
													</form>
													
													<form action="mystatistics.php" method="post" name='statfrm'>
													<input type="hidden" value=0 name="serviceid">
													<input type="hidden" value="statistics" name="action">
													</form>
													
											  </td></tr>
                                         </table></td>
                                  </tr>
								  <? $res_user = mysqli_fetch_array($db->query("select add_sites from tbl_users where userid=$_SESSION[mnuser]")); 
								  	$qry_mem = mysqli_fetch_array($db->query("select total_sites from tbl_membership where m_id = $_SESSION[memid]"));
										if($i<$qry_mem['total_sites']+$res_user['add_sites'])
										{
									 ?>
                                  <tr> 
                                    <td height="15">
									<form action="myservices.php" method="post" name='ss'>
                                          <p>
                                            <input type="hidden" value="new" name="action">
                                            <a class=link href="javascript: document.ss.submit();"><strong><font color="#006666">&nbsp;</font></strong>Add</a> 
                                          </p>
                                        </form>
									</td>
                                  </tr>
								  <? } ?> 
                                </table></TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" class=body>&nbsp;</TD>
                            </TR>
                          </TBODY>
                        </TABLE>
					  </div>
					  <? }
					   if((isset($_POST['action'])&&$_POST['action']=='new') || (!isset($_POST['action']) && $location == 'E'))
					  { ?>
					  <div id="addlist" >
					  
					  	<TABLE class=bodytext border=0 cellPadding=0 cellSpacing=0 width="480">
                          <TBODY>
                            <TR> 
                                <TD align="left" valign="top" class=body> <table width="450" border="0" cellspacing="0" cellpadding="0">
                                    <tr> 
                                      <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                      <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                      <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                        </strong></font><font color="#006699">My 
                                        Services</font></B></td>
                                    </tr>
                                    <tr > 
                                      <td background="../images/dotline.gif" colspan="3"></td>
                                    </tr>
                                  </table>
                                  </TD>
                            </TR>
                            <TR> 
                                <TD height="15" align="left" valign="top" class=body></TD>
                            </TR>
							<? if($location == 'E') { ?>
							<TR> 
                                <TD height="60" align="center" valign="top" class=tre12> 
                                  <strong><font color="#FF0000">Your selected server 
                                  is not available at this time.<br>
                                  <br>Please try for 
                                  another Server.</font></strong> </TD>
                            </TR>
							<? $db->query("delete from tbl_services where service_id = $_REQUEST[service_id]"); } ?>
                            <TR> 
                              <TD align="center" valign="top" class=body>
							  <form name="addfrm" action="myservices.php" method="post" onSubmit="javascript:return addvalidate()">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr> 
                                        <td width="15" ><img src="../images/topleft_corner.gif" width="15" height="14"></td>
                                        <td width="95%" height="14" background="../images/topbg_corner.gif"></td>
                                        <td width="23" ><img src="../images/topright_corner.gif" width="23" height="14"></td>
                                      </tr>
                                      <tr> 
                                        <td background="../images/leftbg_corner.gif" >&nbsp;</td>
                                        <td height="14"><table class=bodytext width="100%"  cellspacing="1" cellpadding="4">
                                            <TBODY>
                                              <TR class="mt"> 
                                                <TD width="40%" height="20" align="right" noWrap class="bg2"><LABEL for=Name><STRONG>&nbsp; 
                                                  Service name:&nbsp;</STRONG></LABEL></TD>
                                                <TD width="60%" height="20" bgcolor="F6FBFC">&nbsp;&nbsp;&nbsp; 
                                                  <INPUT name='txtsname' type="text" class="txtfield" size=30></TD>
                                              </TR>
                                              <TR class=mt> 
                                                <TD height="20" align="right" noWrap class="bg2"><LABEL for=label><STRONG> 
                                                  &nbsp;HostName:&nbsp;</STRONG></LABEL></TD>
                                                <TD height="20" bgcolor="F6FBFC">&nbsp;&nbsp;&nbsp; 
                                                  <INPUT name='txthostname' class="txtfield" id=label size=30></TD>
                                              </TR>
                                              <TR class=mt> 
                                                <TD height="20" align="right" noWrap class="bg2"><LABEL for=ServiceId><STRONG> 
                                                  &nbsp;Service:&nbsp;</STRONG></LABEL></TD>
                                                <TD height="20" bgcolor="F6FBFC">&nbsp;&nbsp;&nbsp; 
                                                  <SELECT name='txtserviceid' class="selstyle" id="txtserviceid" onChange="javascript: checkport(this.form)">
                                                    <? $qry = $db->query("select * from tbl_service_ports where status='Y'"); 
									/*	 if( $act < 0)
										 $ch1 = $_POST['ServiceId'];
										 else
										 $ch1 = 0;
										*/ 
										while($res = mysqli_fetch_array($qry))
										{	
											//	if( $ch1 == 0 )
												//	$ch1 = $res['service_id'];
													
												$bShow = false;
													
												if($res['port_no'] == 80 && $rs_MSDet['http_check'] == "Y")
												{
														$bShow = true;
												}
												
												if($res['port_no'] == 21 && $rs_MSDet['ftp_check'] == "Y")
												{
														$bShow = true;
												}

												if($res['port_no'] == 25 && $rs_MSDet['smtp_check'] == "Y")
												{
														$bShow = true;
												}

												if($res['port_no'] == 110 && $rs_MSDet['pop3_check'] == "Y")
												{
														$bShow = true;
												}

												if($res['port_no'] == 2534 && $rs_MSDet['ping_monitoring'] == "Y")
												{
														$bShow = true;
												}

												if($res['port_no'] == -430 && $rs_MSDet['custom_ports'] == "Y")
												{
														$bShow = true;
												}

												if($res['port_no'] == 443 && $rs_MSDet['ssl_server'] == "Y")
												{
														$bShow = true;
												}
																								
												if($bShow)
												{
													
										 ?>
                                                    <option value="<? echo $res['service_id']?>"  ><? echo $res['service_name'] ?></option>
                                                    <?
												}	//if($BShow)
											}	//while($res = mysqli_fetch_array($qry))
										?>
                                                  </SELECT>
                                                  Custom port: 
                                                  <INPUT size=5 value=0 name='txtcustom' disabled></TD>
                                              </TR>
                                              <TR class=mt> 
                                                <TD height="20" align="right" noWrap class="bg2"><LABEL for=CheckPeriod><STRONG> 
                                                  &nbsp;Check period:&nbsp;</STRONG></LABEL></TD>
                                                <TD height="20" bgcolor="F6FBFC">&nbsp;&nbsp;&nbsp; 
												  <SELECT name='txtperiod' class="selstyle" id="txtperiod">
                                                    <?
													$res_users = mysqli_fetch_array($db->query("select mem_auth from tbl_users where userid = $_SESSION[mnuser]"));
													$authority = 1;
													if($res_users['mem_auth'] >= $rs_MSDet['monitor_interval_level'])
														$authority = $res_users['mem_auth'];
													else
														$authority = $rs_MSDet['monitor_interval_level'];
													
								$q = $db->query("select * from tbl_checkperiods where status = 'Y' and authority <= $authority ");
								while($r=mysqli_fetch_array($q))
								{
								?>
                                                    <OPTION value="<? echo $r['pid']; ?>" ><? echo $r['period']; ?>&nbsp;minutes</OPTION>
                                                    <? } ?>
                                                  </SELECT> </TD>
                                              </TR>
                                              <TR class=mt> 
                                                <TD height="20" align="right" noWrap class="bg2"><STRONG> 
                                                  &nbsp;Location:&nbsp;</STRONG></TD>
                                                <TD height="20" bgcolor="F6FBFC"> 
                                                  <TABLE class=mt cellSpacing=0 cellPadding=2 width="100%" border=0>
                                                    <TBODY>
                                                      <TR> 
                                                        <TD>&nbsp;</TD>
                                                        <TD>&nbsp;<STRONG>City</STRONG>&nbsp;</TD>
                                                        <TD>&nbsp;<STRONG>Country</STRONG>&nbsp;</TD>
                                                      </TR>
                                                      <? $qry_loc = $db->query("select * from tbl_locations where status = 'Y'"); 
										$i=0;
										 while($r_loc = mysqli_fetch_array($qry_loc))
										{	
											
										?>
                                                      <tr> 
                                                        <td><input type="radio" value="<? echo $r_loc['loc_id']; ?>" name="txtlocation" <? if($i++ == 0) echo 'checked'; ?>  ></td>
                                                        <td width="130" height="19">&nbsp;&nbsp;<? echo $r_loc['loc_name']; ?></td>
                                                        <td>&nbsp;&nbsp;<? echo $r_loc['loc_country']; ?> 
                                                        </td>
                                                      </tr>
                                                      <? } ?>
                                                    </TBODY>
                                                  </TABLE></TD>
                                              </TR>
                                              <TR class=mt> 
                                                <TD height="20" align="right" noWrap class="bg2"><LABEL for=SendAlert><STRONG> 
                                                  &nbsp;Send alert:&nbsp;</STRONG></LABEL></TD>
                                                
                                                      <? $res_mail = mysqli_fetch_array($db->query("select emailid,aemail from tbl_users where userid=$_SESSION[mnuser]"));?>
                                                	<TD height="20" bgcolor="F6FBFC"> 
													<INPUT type=checkbox value=1 size=30 name='txtstatusemail' checked >
                                                      </TD>
                                              </TR>
                                              <TR class=mt> 
                                                <TD height="20" align="right" noWrap class="bg2"><LABEL for=SupportEmail><STRONG> 
                                                  &nbsp;Email alert :&nbsp;</STRONG></LABEL></TD>
                                                <TD height="20" bgcolor="F6FBFC">&nbsp;
                                                  <INPUT name='txtemail' type='text' class="txtfield" value='' size=30 ></TD>
                                              </TR>
											  <?
											  	$res_users = mysqli_fetch_array($db->query("select * from tbl_users where userid = $_SESSION[mnuser]"));
											    $array_mails = array();
												$array_mobiles = array();
												
												$array_mails[0] = trim($res_users['aemail']);
												$array_mails[1] = trim($res_users['aemail1']);
												$array_mails[2] = trim($res_users['aemail2']);
												$array_mails[3] = trim($res_users['aemail3']);
												$array_mails[4] = trim($res_users['aemail4']);
												
												$array_mobiles[0] = trim($res_users['mobile']);
												$array_mobiles[1] = trim($res_users['mobile1']);
												$array_mobiles[2] = trim($res_users['mobile2']);
												$array_mobiles[3] = trim($res_users['mobile3']);
												$array_mobiles[4] = trim($res_users['mobile4']);
												
												$array_mobiles = array_unique($array_mobiles);
												$count = count($array_mails);
											?>
											<TR class=mt> 
                                                <TD height="20" align="right" valign="middle" noWrap class="bg2"><LABEL for=SupportEmail><STRONG> 
                                                  Other alert Mails :&nbsp;</STRONG></LABEL></TD>
												  <TD height="20" bgcolor="F6FBFC">
											 <?
											  foreach ($array_mails as $index=>$value)
											  {
											  	if($value != NULL)
												{
											  ?>
											  &nbsp;<INPUT name='alert_emails[]' type='checkbox' value='<? echo $index; ?>' size=30 ><? echo $value; ?><br>
											 <? } // if
											 } // for ?>
                                              </TD>
											  </TR>
											  
                                              <TR class=mt> 
                                                <TD height="20" align="right" noWrap class="bg2"><LABEL for=SendSms><STRONG> 
                                                  &nbsp;Send SMS alert:&nbsp;</STRONG></LABEL></TD>
											  <?  $type = mysqli_fetch_array($db->query("select s.mobile,u.smscredits from tbl_users u, tbl_services s where u.userid = s.userid and u.userid=$_SESSION[mnuser]")); ?>
                                                <TD height="20" bgcolor="F6FBFC">&nbsp;
                                                    <input value="1" type=checkbox size=30 name='txtsms' <? if($rs_MSDet['sms_alert'] == 'N' || $type['smscredits'] <= 0)  echo 'disabled'; ?> >
													</TD>
											<TR class=mt> 
                                               <TD height="20" align="right" noWrap class="bg2"><LABEL for=SendSms><STRONG> 
                                                  &nbsp;Mobile no:&nbsp; </STRONG></LABEL></TD>
                                                      <TD height="20" bgcolor="F6FBFC">&nbsp;
													  <input name="txtmobile" type="text" class="txtfield" value="" size="15">
                                                       </TD>
                                                  
                                              </TR>
											  <TR class=mt> 
                                                <TD height="20" align="right" valign="middle" noWrap class="bg2"><LABEL for=SupportEmail><STRONG> 
                                                  Other alert Mobile Nos :&nbsp;</STRONG></LABEL></TD>
												  <TD height="20" bgcolor="F6FBFC">
											 <?
											  foreach ($array_mobiles as $index=>$value)
											  {
											  	if($value != NULL)
												{
											  ?>
											   &nbsp;<INPUT name='alert_mobiles[]' type='checkbox' value='<? echo $index; ?>' size=30 <? if($rs_MSDet['sms_alert'] == 'N' || $type['smscredits'] <= 0)  echo 'disabled'; ?>><? echo $value; ?><br>
											 <? } // if
											 } // for ?>
											   </TD>
                                              </TR>
                                              <TR class=mt> 
                                                <TD height="20" align="right" noWrap class="bg2"><LABEL for=Enabled><STRONG> 
                                                  Service &nbsp;Active:&nbsp;</STRONG></LABEL></TD>
                                                <TD height="20" bgcolor="F6FBFC"><INPUT type=checkbox value="1" CHECKED name='txtactive'></TD>
                                              </TR>
											  <TR class=mt> 
                                                <TD height="20" align="right" noWrap class="bg2"><LABEL for=Enabled><STRONG> 
                                                  &nbsp;SMS Alert When<br>
                                                  Site will be down :&nbsp;</STRONG></LABEL></TD>
                                                <TD height="20" valign="middle" bgcolor="F6FBFC">
												<INPUT type=radio value="0" CHECKED name='downSmsAlert'>&nbsp;None&nbsp;
												<INPUT type=radio value="15" name='downSmsAlert'>&nbsp;Every 15 Min&nbsp;
												<INPUT type=radio value="30" name='downSmsAlert'>&nbsp;Every 30 Min&nbsp;
												</TD>
                                              </TR>
                                              <TR class=mt> 
                                                <TD height="30" colSpan=2 align="center"> 
                                                  <INPUT type=hidden value=add name=action> 
                                                  <input type="hidden" name="monitor_level" value="<? echo $rs_MSDet['monitor_interval_level']; ?>"> 
                                                  <input name="submit" type="submit" class="buttonstyle" value="ADD" > 
                                                </TD>
                                              </TR>
                                            </TBODY>
                                          </TABLE></td>
                                        <td background="../images/rightbg_corner.gif" >&nbsp;</td>
                                      </tr>
                                      <tr> 
                                        <td background="../images/bottomleft_corner.gif" >&nbsp;</td>
                                        <td height="14" background="../images/bottombg_corner.gif"></td>
                                        <td ><img src="../images/bottomright_corner.gif" width="23" height="22"></td>
                                      </tr>
                                    </table>
                                  </form>
								</TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" class=body>&nbsp;</TD>
                            </TR>

                          </TBODY>
                        </TABLE>	
					  </div>
					  <? } 
				  	if((isset($_POST['action']) && $_POST['action']=='edit') || ($updateloc == true && $_POST['action']=='update'))
					   {
					   ?>
					   		<div id="editlist" >
					   
					  	<TABLE class=mt border=0 cellPadding=0 cellSpacing=0 width="480">
                          <TBODY>
                            <TR> 
                                <TD align="left" valign="top" class=body> <table width="450" border="0" cellspacing="0" cellpadding="0">
                                    <tr> 
                                      <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                      <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                      <td width="430" class="c4"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                        </strong></font><font color="#006699">My 
                                        Services</font></B></td>
                                    </tr>
                                    <tr > 
                                      <td background="../images/dotline.gif" colspan="3"></td>
                                    </tr>
                                  </table>
                                  </TD>
                            </TR>
                            <TR> 
                              <TD height="15" align="left" valign="top" class=body></TD>
                            </TR>
							<? 
							$serviceid = $_POST['serviceid'];
							if($updateloc == true) { 
							$serviceid = $_POST['txtserviceid'];
							?>
							<TR> 
                                <TD height="60" align="center" valign="top" class=tre12> 
                                  <strong><font color="#FF0000">Your selected server 
                                  is not available at this time.<br>
                                  <br>Please update with another Server.</font></strong> </TD>
                            </TR>
							<? } ?>
                            <TR> 
                              <TD align="center" valign="top" class=body>
							  <form name="editfrm" action="myservices.php" method="post" onSubmit="javascript:return editvalidate()">
                                    <br>
                                    <table width="80%" border="0" cellspacing="0" cellpadding="0">
                                      <tr> 
                                        <td width="15" ><img src="../images/topleft_corner.gif" width="15" height="14"></td>
                                        <td width="95%" height="14" background="../images/topbg_corner.gif"></td>
                                        <td width="23" ><img src="../images/topright_corner.gif" width="23" height="14"></td>
                                      </tr>
                                      <tr> 
                                        <td background="../images/leftbg_corner.gif" >&nbsp;</td>
                                        <td height="14"><table class=formtext width="450" cellspacing="1" cellpadding="0">
                                            <? $res_edit = mysqli_fetch_array($db->query("select * from tbl_services where service_id=$serviceid"));
												?>
                                            <tbody>
                                              <tr class="mt"> 
                                                <td width="30%" height="30" align="right" nowrap class="bg2"><label for=Name><strong>&nbsp; 
                                                  Service name:&nbsp;</strong></label></td>
                                                <td width="70%" height="30" bgcolor="F6FBFC"><input type="text" size=30 name='txtsname' value="<? echo $res_edit['service_name']; ?>" >
                                                </td>
                                              </tr>
                                              <tr class=mt> 
                                                <td height="30" align="right" nowrap class="bg2"><label for=label3 class="formtext"><strong> 
                                                  &nbsp; &nbsp;HostName:&nbsp;</strong></label></td>
                                                <td width="70%" height="30" bgcolor="F6FBFC"><input id=label3 size=30 name='txthostname' value="<? echo $res_edit['host_name']; ?>"></td>
                                              </tr>
                                              <tr class=mt> 
                                                <td height="30" align="right" nowrap class="bg2"><label for=ServiceId><strong> 
                                                  &nbsp; &nbsp;Service:&nbsp;</strong></label></td>
                                                <td width="70%" height="30" bgcolor="F6FBFC"> 
                                                  <select name='txtserviceid' onChange="javascript: checkport(this.form)">
                                                    <? $qry = $db->query("select * from tbl_service_ports where status='Y'"); 
									/*	 if( $act < 0)
										 $ch1 = $_POST['ServiceId'];
										 else
										 $ch1 = 0;
										*/ 
										while($res = mysqli_fetch_array($qry))
										{	
											//	if( $ch1 == 0 )
												//	$ch1 = $res['service_id'];
													
												$bShow = false;
													
												if($res['port_no'] == 80 && $rs_MSDet['http_check'] == "Y")
												{
														$bShow = true;
												}
												
												if($res['port_no'] == 21 && $rs_MSDet['ftp_check'] == "Y")
												{
														$bShow = true;
												}

												if($res['port_no'] == 25 && $rs_MSDet['smtp_check'] == "Y")
												{
														$bShow = true;
												}

												if($res['port_no'] == 110 && $rs_MSDet['pop3_check'] == "Y")
												{
														$bShow = true;
												}

												if($res['port_no'] == 2534 && $rs_MSDet['ping_monitoring'] == "Y")
												{
														$bShow = true;
												}

												if($res['port_no'] == -430 && $rs_MSDet['custom_ports'] == "Y")
												{
														$bShow = true;
												}

												if($res['port_no'] == 443 && $rs_MSDet['ssl_server'] == "Y")
												{
														$bShow = true;
												}
																								
												if($bShow)
												{
													
										 ?>
                                                    <option value="<? echo $res['service_id']?>" <? if($res_edit['service_port'] == $res['service_id']) echo 'selected'; ?> ><? echo $res['service_name'] ?></option>
                                                    <?
												}	//if($BShow)
											}	//while($res = mysqli_fetch_array($qry))
										?>
                                                  </select>
                                                  Custom port: 
                                                  <input size=5 name='txtcustom' <? if($res_edit['custom_port'] == 0 ) echo 'value=0 disabled'; else echo "value = $res_edit[custom_port]"; ?>></td>
                                              </tr>
                                              <tr class=mt> 
                                                <td height="30" align="right" nowrap class="bg2"><label for=CheckPeriod><strong> 
                                                  &nbsp; &nbsp;Check period:&nbsp;</strong></label></td>
                                                <td width="70%" height="30" bgcolor="F6FBFC"> 
                                                  <select name='txtperiod'>
                                                    <? 
													$q = $db->query("select * from tbl_checkperiods where status = 'Y' and authority <= $res_edit[monitor_interval_level]");
								while($r=mysqli_fetch_array($q))
								{
								?>
                                                    <option value="<? echo $r['pid']; ?>" <? if($r['pid'] == $res_edit['period']) echo 'selected'; ?> ><? echo $r['period']; ?>&nbsp;minutes</option>
                                                    <? } ?>
                                                  </select> </td>
                                              </tr>
                                              <tr class=mt> 
                                                <td height="30" align="right" nowrap class="bg2"><strong> 
                                                  &nbsp; &nbsp;Location:&nbsp;</strong></td>
                                                <td width="70%" height="30" bgcolor="F6FBFC"> 
                                                  <table class=mt cellspacing=0 cellpadding=2 width="100%" border=0>
                                                    <tbody>
                                                      <tr> 
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;<strong>City</strong>&nbsp;</td>
                                                        <td>&nbsp;<strong>Country</strong>&nbsp;</td>
                                                      </tr>
                                                      <? $qry_loc = $db->query("select * from tbl_locations where status = 'Y'"); 
										while($r_loc = mysqli_fetch_array($qry_loc))
										{	
										?>
                                                      <tr> 
                                                        <td><input type="radio" value="<? echo $r_loc['loc_id']; ?>"  name="txtlocation" <? if($r_loc['loc_id'] == $res_edit['location']) echo 'checked'; ?>  ></td>
                                                        <td width="130" height="19">&nbsp;&nbsp;<? echo $r_loc['loc_name']; ?></td>
                                                        <td>&nbsp;&nbsp;<? echo $r_loc['loc_country']; ?> 
                                                        </td>
                                                      </tr>
                                                      <? } ?>
                                                    </tbody>
                                                  </table></td>
                                              </tr>
                                              <TR class=mt> 
                                                <TD height="20" align="right" noWrap class="bg2"><LABEL for=SendAlert><STRONG> 
                                                  &nbsp;Send alert:&nbsp;</STRONG></LABEL></TD>
                                                
                                                   	<TD height="20" bgcolor="F6FBFC"> 
													<INPUT type=checkbox value=1 size=30 name='txtstatusemail' <? if($res_edit['status_email'] == 'Y') echo 'checked'; ?> >
                                                      </TD>
                                              </TR>
											  <TR class=mt> 
                                                <TD height="20" align="right" noWrap class="bg2"><LABEL for=SupportEmail><STRONG> 
                                                  &nbsp;Email alert :&nbsp;</STRONG></LABEL></TD>
                                                <TD height="20" bgcolor="F6FBFC">&nbsp;
                                                  <INPUT name='txtemail' type='text' class="txtfield" value='<? echo $res_edit['email']; ?>' size=30 ></TD>
                                              </TR>
                                              <?
											  	$res_users = mysqli_fetch_array($db->query("select * from tbl_users where userid = $_SESSION[mnuser]"));	
												$check_mails = explode(',',$res_edit['multi_mails']);
												$check_mobiles = explode(',',$res_edit['multi_mobiles']);
												$array_mails = array();
												$array_mobiles = array();
												
												$array_mails[0] = trim($res_users['aemail']);
												$array_mails[1] = trim($res_users['aemail1']);
												$array_mails[2] = trim($res_users['aemail2']);
												$array_mails[3] = trim($res_users['aemail3']);
												$array_mails[4] = trim($res_users['aemail4']);
												
												$array_mobiles[0] = trim($res_users['mobile']);
												$array_mobiles[1] = trim($res_users['mobile1']);
												$array_mobiles[2] = trim($res_users['mobile2']);
												$array_mobiles[3] = trim($res_users['mobile3']);
												$array_mobiles[4] = trim($res_users['mobile4']);
												
												
												$array_mobiles = array_unique($array_mobiles);
												$count = count($array_mails);
											?>
											<TR class=mt> 
                                                <TD height="20" align="right" valign="middle" noWrap class="bg2"><LABEL for=SupportEmail><STRONG> 
                                                  Other alert Mails :&nbsp;</STRONG></LABEL></TD>
												  <TD height="20" bgcolor="F6FBFC">
											 <?
											 $i = 0;
											  foreach ($array_mails as $index=>$value)
											  {
												  if($value != NULL)
												  {
												  ?>
													&nbsp;<INPUT name='alert_emails[]' type='checkbox' value='<? echo $index; ?>' size=30 <? if($check_mails[$i] == $index  && $check_mails[$i] != '') { echo 'checked'; $i++; } ?>><? echo $value; ?><br>
												 <? } // if  
											  } // foreach ?>
											 </TD>
                                              </TR>
											  <TR class=td> 
                                                <TD height="20" align="right" noWrap class="bg2"><LABEL for=SendSms><STRONG> 
                                                  &nbsp;Send SMS alert:&nbsp;</STRONG></LABEL></TD>
											  <?  $type = mysqli_fetch_array($db->query("select s.mobile,u.smscredits from tbl_users u, tbl_services s where u.userid = s.userid and u.userid=$_SESSION[mnuser]")); ?>
                                               <TD height="20" bgcolor="F6FBFC">&nbsp;
                                                 <input value="1" type=checkbox size=30 name='txtsms' <? if($rs_MSDet['sms_alert'] == 'N' || $type['smscredits'] <= 0)  echo 'disabled'; ?> <? if($res_edit['status_sms'] == 'Y') echo 'checked'; ?>>
											</TD>
											<TR class=td> 
                                               <TD height="20" align="right" noWrap class="bg2"><LABEL for=SendSms><STRONG> 
                                                  &nbsp;Mobile no:&nbsp; </STRONG></LABEL></TD>
                                                      <TD height="20" bgcolor="F6FBFC">&nbsp;<input name="txtmobile" type="text" class="txtfield" value="<? echo $res_edit['mobile']; ?>" size="15" >
                                                       </TD>
                                                  
                                              </TR>
											  <TR class=mt> 
                                                <TD height="20" align="right" valign="middle" noWrap class="bg2"><LABEL for=SupportEmail><STRONG> 
                                                  Other alert Mobile Nos :&nbsp;</STRONG></LABEL></TD>
												  <TD height="20" bgcolor="F6FBFC">
											 <?
											  $i = 0;
											  foreach ($array_mobiles as $index=>$value)
											  {
											  	if($value != NULL)
												{
												  ?>
												  &nbsp;<INPUT name='alert_mobiles[]' type='checkbox' value='<? echo $index; ?>' size=30 <? if($rs_MSDet['sms_alert'] == 'N' || $type['smscredits'] <= 0)  echo 'disabled'; ?> <? if($check_mobiles[$i] == $index  && $check_mobiles[$i] != '') { echo 'checked'; $i++; } ?>><? echo $value; ?><br>
												 <? } // if 
											} // foreach ?>
											 </TD>
                                              </TR>
                                              <tr class=mt> 
                                                <td height="30" align="right" nowrap class="bg2"><label for=ative><strong> 
                                                  &nbsp;Service &nbsp;Active:&nbsp;</strong></label></td>
                                                <td width="70%" height="30" bgcolor="F6FBFC"><input type=checkbox value="1" name='txtactive' <? if($res_edit['active'] == 'Y') echo 'checked'; ?>></td>
                                              </tr>
											  <TR class=mt> 
                                                <TD height="20" align="right" noWrap class="bg2"><LABEL for=Enabled><STRONG> 
                                                  &nbsp;SMS Alert When<br>
                                                  Site will be down :&nbsp;</STRONG></LABEL></TD>
                                                <TD height="20" valign="middle" bgcolor="F6FBFC">
												<INPUT type=radio value="0" name='downSmsAlert' <? if($res_edit['smsalert_down'] == 0) echo 'checked'; ?>>&nbsp;None&nbsp;
												<INPUT type=radio value="15" name='downSmsAlert' <? if($res_edit['smsalert_down'] == 15) echo 'checked'; ?>>&nbsp;Every 15 Min&nbsp;
												<INPUT type=radio value="30" name='downSmsAlert' <? if($res_edit['smsalert_down'] == 30) echo 'checked'; ?>>&nbsp;Every 30 Min&nbsp;
												</TD>
                                              </TR>
                                              <tr align="center" class=mt> 
                                                <td height="30" colspan=2><strong>&nbsp;</strong><strong>&nbsp;</strong> 
                                                  <input type=hidden value=update name=action> 
                                                  <input type=hidden value="<? echo $_POST['serviceid']; ?>" name='service_id'> 
                                                  <input name="submit2" type=submit class="buttonstyle" value=Update> 
                                                  &nbsp;&nbsp; <input name="button" type=button class="buttonstyle" onClick="javascript: window.location='myservices.php';" value=Cancel></td>
                                              </tr>
                                            </tbody>
                                          </table></td>
                                        <td background="../images/rightbg_corner.gif" >&nbsp;</td>
                                      </tr>
                                      <tr> 
                                        <td background="../images/bottomleft_corner.gif" >&nbsp;</td>
                                        <td height="14" background="../images/bottombg_corner.gif"></td>
                                        <td ><img src="../images/bottomright_corner.gif" width="23" height="22"></td>
                                      </tr>
                                    </table>
                                  </form>
								</TD>
                            </TR>
                            <TR> 
                              <TD align="left" valign="top" class=body>&nbsp;</TD>
                            </TR>
                          </TBODY>
                        </TABLE>	
					  </div>
					   <? } ?>
					  </td>
						<td align="right" valign="top" height="100%">
                      <? include("rightbar.php"); ?></td>
                      
                    </tr>
                    <tr > 
                      <td background="../images/dotline.gif" height="1" colspan="3" valign="top"></td>
                    </tr>
                    <tr align="center" > 
                      <td  height="12" colspan="3" valign="top" class="tre12"></td>
                    </tr>
                    <tr align="center" > 
                      <td colspan="3"><? include("footer.php"); ?></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
