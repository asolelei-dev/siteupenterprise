
<html>
   <head>
     
      <link href="images/mystyle.css" rel="stylesheet" type="text/css">

  <title>Site Uptime Enterprise - Free Website Monitoring Service</title>
  
<link href="monitor_style.css" rel="stylesheet" type="text/css">
</head>

<body>

<table width=390 border=0 cellpadding=3 class="bodytext">
<tr>
<td>


<a name=interval>
<p class="formtext" style="font-size:15px;"><strong>Monitoring Interval:</strong><br>
The free account allows you to monitor your URL's every 30 or 60 minutes. The Premium and Premium Plus accounts allow you to monitor your URL's every 5 or 15 minutes. 
      <p><a href="#" class="link" style="font-size:15px;" onClick="window.parent.close()"><strong>Close 
        Window</strong></a><strong></font> <br>
        </strong><br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=servicemon>
<p class="bodytext" style="font-size:15px;"><strong>Service Monitors:</strong><br>
The free account allows you to monitor up to 1 service monitor. The Premium account allows 5 service monitors and Premium Plus account allows 10 service monitors. Additional service monitors can be activated for $1 per month each.
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<a name=locations>
<p class="mt" style="font-size:15px;"><strong>Multiple Monitoring Locations:</strong><br>
All accounts allow monitoring from multiple locations.
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<a name=multiplealerts>
<p class="mt" style="font-size:15px;"><strong>Multiple Alert Contacts:</strong><br>
The free account allows you you to set up one alert contact.  The Premium accounts allow multiple contacts.
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=emailalerts>
<p class="mt" style="font-size:15px;"><strong>Email Alerts:</strong><br>
All accounts allow alerts to be sent via email.
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<a name=reports>
<p class="mt" style="font-size:15px;"><strong>Monthly Reports:</strong><br>
All accounts receive monthly email uptime reports.
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=onlinestats>
<p class="mt" style="font-size:15px;"><strong>Online Statistics:</strong><br>
All accounts provide daily and monthly reports and statistics online.
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=cp>
<p class="mt" style="font-size:15px;"><strong>Control Panel:</strong><br>
All accounts have access to an online control panel which allows you to set up and delete monitoring URL's, view reports, change contacts, etc.
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=http>
<p class="mt" style="font-size:15px;"><strong>Web Server (http):</strong><br>
All accounts allows users to monitor Web Servers (http).
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=pop3>
<p class="mt" style="font-size:15px;"><strong>Email Server (pop3):</strong><br>
All accounts allow users to monitor mail Servers (pop3).
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=ftp>
<p class="mt" style="font-size:15px;"><strong>FTP Server (ftp):</strong><br>
All accounts allow users to monitor FTP Servers (ftp).
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<a name=smtp>
<p class="mt" style="font-size:15px;"><strong>Outgoing Email Server (smtp):</strong><br>
All accounts allows users to monitor Outgoing Email Servers (smtp).
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=custports>
<p class="mt" style="font-size:15px;"><strong>Custom Ports:</strong><br>
The Premium Plus account allows users to set up and monitor custom ports.
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=ssl>
<p class="mt" style="font-size:15px;"><strong>SSL Server:</strong><br>
The Premium Plus account allows users to set up and monitor SSL servers.
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<a name=sms>
<p class="mt" style="font-size:15px;"><strong>SMS Alerts:</strong><br>
        The Premium Plus account allows users to receive alerts via SMS messages 
        to a cell phone. A charge of $.20 per SMS alert applies for each alert 
        sent. In order to activate SMS alerts for your account, you will need 
        to submit a $5.00 deposit. If you need to verify that we support your 
        mobile network, please let us know and we will send you a test SMS alert. 
      <p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=2min>
<p class="mt" style="font-size:15px;"><strong>2 Minute Monitoring Interval:</strong><br>
        For users who need monitoring at 2 minute intervals, this option is available 
        for $3 per month per service monitor. 
      <p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=additional>
<p class="mt" style="font-size:15px;"><strong>Additional Service Monitors:</strong><br>
For users who need more than 5 service monitors, this option is available for $1 per month per service monitor.
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=ping>
<p class="mt" style="font-size:15px;"><strong>Ping Monitoring:</strong><br>
For users who want monitor their server using Ping, this is available with our premium and Premium Plus accounts.
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=time>
<p class="mt" style="font-size:15px;"><strong>Custom Time Zone:</strong><br>
Our Premium and Premium Plus accounts allow users to set custom time zones for each service monitor. By default, monitoring time is set to PST.
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=pubstats>
<p class="bodytext" style="font-size:15px;"><strong>Public Statistics:</strong><br>
All of our accounts come with the ability to publicly display your monitoring statistics. However, our Premium and Premium Plus accounts provide more detailed information.
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<a name=error>
<p class="bodytext" style="font-size:15px;"><strong>Public Statistics:</strong><br>
Our Premium and Premium Plus accounts will display the type of error that was encountered when a service monitor fails, i.e. timeout, 404, etc.
<p><a href="#" class="me" style="font-size:15px;" onClick="window.parent.close()">Close Window</a></font>


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


</td>
</tr>
</table>





</html>
