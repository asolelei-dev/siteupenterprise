<?
include("admin/include/conn.inc");
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>WELCOME - Site Uptime Enterprise</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="monitor_style.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="5" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="770" border="0" cellpadding="1" cellspacing="1" bgcolor="5A5A5A">
        <tr>
          <td bgcolor="ffffff"><table width="770" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="6"><? include("header.php"); ?></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="730" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="504" valign="top"><TABLE class=formtext border=0 cellPadding=0 cellSpacing=0 width="480">
                          <TBODY>
                            <TR> 
                              <TD align="left" valign="top" class=body> 
                                <P>
<table width="450" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" bgcolor="#FFEFE8">&nbsp;</td>
                                    <td width="3" bgcolor="#FF6600">&nbsp;</td>
                                    <td width="430" class="c3"><B><font color="#FF6600"><strong>&nbsp;&nbsp;&nbsp;&raquo; 
                                      </strong></font><span class="c3">Service 
                                      Overview</span></B> </td>
                                  </tr>
                                  <tr > 
                                    <td background="images/dotline.gif" colspan="3"></td>

                                  </tr>
                                </table>
                                <P>Your website <B>is</B> your business as long 
                                  as customers can reach it. Even if your website 
                                  is up 99% of the time, this translates into 
                                  over 7 hours per month where your customers 
                                  cannot reach you. 
<P align="justify">Site Uptime Enterprise helps you eliminate 
                                  downtime by providing <B>free</B> remote monitoring 
                                  services for your website. When your website 
                                  goes down, we notify you instantly via email 
                                  so that you may fix problems before they cost 
                                  your e-business money, customers or reputation. 
                                  In addition, our service provides detailed statistics 
                                  about your website's availability and response 
                                  times so that you can track how often your site 
                                  is down. 
                                <P align="justify">Our Multi-Check technology 
                                  allows us to monitor your website from diverse 
                                  geographic regions to ensure that your website 
                                  is accessible from multiple Internet points. 
                                  Each monitoring location, operating independently 
                                  and time-synchronized, tests your website and 
                                  reports the results to a central database. Before 
                                  alerting you of a problem, our software verifies 
                                  the error with multiple stations. Therefore, 
                                  you are notified about problems that you have 
                                  control over and not about isolated Internet 
                                  outages. 
                                <P><span class="c1"><strong>Free account features: 
                                  </strong></span> 
                                <UL>
                                  <font color="#FF6600">&#8226; </font>Free account 
                                  activation, no costs involved.<br>
                                  <font color="#FF6600">&#8226; </font>Ability 
                                  to monitor web servers, email servers and FTP 
                                  servers. <br>
                                  <font color="#FF6600">&#8226; </font>Monitoring 
                                  intervals of 30 or 60 minutes. <br>
                                  <font color="#FF6600">&#8226; </font>No software 
                                  to install or maintain. <br>
                                  <font color="#FF6600">&#8226; </font>Monitoring 
                                  from multiple locations eliminates false alarms. 
                                  <br>
                                  <font color="#FF6600">&#8226; </font>Access 
                                  alert <A class=link 
              href="examplesreports.php"><strong>reports and statistics</strong></A> 
                                  for your website online. <br>
                                  <font color="#FF6600">&#8226; </font>Uptime 
                                  reports include average response time by Year, 
                                  Month and &nbsp;Day. <br>
                                  <font color="#FF6600">&#8226; </font>Publicly 
                                  viewable statistics summary.<br>
                                </UL>
                                <P align="justify">If you need additional monitoring 
                                  features, Site Uptime Enterprise now offers two <A class=link href="advancedservices.php"><strong>Premium 
                                  monitoring services</strong></A> starting at 
                                  $5.00 per month. For a comparison of the features 
                                  available with our free and Premium accounts, 
                                  please click <A class=link
            href="compareservices.php"><strong>here</strong></A>. 
                                <P class="c1"><strong>Premium features include:</strong> 
                                <UL>
                                  <LI>Ability to monitor custom server ports 
                                  <LI>Ability to monitor SSL servers 
                                  <LI>Ability to recieve SMS Alerts 
                                  <LI>Ability to set custom time zones 
                                  <LI>Ability to monitor using Ping 
                                  <LI>Multiple alert contacts 
                                  <LI>2, 5, and 15 minute monitoring intervals 
                                  </LI>
                                </UL></TD>
                            </TR>
                          </TBODY>
                        </TABLE></td>
						<td align="right" valign="top" height="100%">
                      <? include("rightbar.php"); ?></td>
                      
                    </tr>
                    <tr > 
                      <td background="images/dotline.gif" height="1" colspan="3" valign="top"></td>
                    </tr>
                    <tr align="center" > 
                      <td  height="12" colspan="3" valign="top" class="tre12"></td>
                    </tr>
                    <tr align="center" > 
                      <td colspan="3"><? include("footer.php"); ?></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
